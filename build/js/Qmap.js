//window["text" + pageNumber]

var marker_fasumWait        = require('../imgs/marker/Area_Selection_Fasum_Wait@1x.png')
var marker_keamananWait     = require('../imgs/marker/Area_Selection_Keamanan_Wait@1x.png')
var marker_kebersihanWait   = require('../imgs/marker/Area_Selection_Kebersihan_Wait@1x.png')
var marker_parkirWait       = require('../imgs/marker/Area_Selection_Parkir_Wait@1x.png')
var marker_listrikWait      = require('../imgs/marker/Area_Selection_Listrik_Wait@1x.png')
var marker_airWait          = require('../imgs/marker/Area_Selection_Air_Wait@1x.png')
var marker_temuanWait       = require('../imgs/marker/Area_Selection_Temuan_dan_Kehilangan_Wait@1x.png')
var marker_lainnyaWait      = require('../imgs/marker/Area_Selection_Lainnya_Wait@1x.png')
var marker_fasumProgress        = require('../imgs/marker/Area_Selection_Fasum_Process@1x.png')
var marker_keamananProgress     = require('../imgs/marker/Area_Selection_Keamanan_Process@1x.png')
var marker_kebersihanProgress   = require('../imgs/marker/Area_Selection_Kebersihan_Process@1x.png')
var marker_parkirProgress       = require('../imgs/marker/Area_Selection_Parkir_Process@1x.png')
var marker_listrikProgress      = require('../imgs/marker/Area_Selection_Listrik_Process@1x.png')
var marker_airProgress          = require('../imgs/marker/Area_Selection_Air_Process@1x.png')
var marker_temuanProgress       = require('../imgs/marker/Area_Selection_Temuan_dan_Kehilangan_Process@1x.png')
var marker_lainnyaProgress      = require('../imgs/marker/Area_Selection_Lainnya_Process@1x.png')
var marker_fasumDone        = require('../imgs/marker/Area_Selection_Fasum_Finish@1x.png')
var marker_keamananDone     = require('../imgs/marker/Area_Selection_Keamanan_Finish@1x.png')
var marker_kebersihanDone   = require('../imgs/marker/Area_Selection_Kebersihan_Finish@1x.png')
var marker_parkirDone       = require('../imgs/marker/Area_Selection_Parkir_Finish@1x.png')
var marker_listrikDone      = require('../imgs/marker/Area_Selection_Listrik_Finish@1x.png')
var marker_airDone          = require('../imgs/marker/Area_Selection_Air_Finish@1x.png')
var marker_temuanDone       = require('../imgs/marker/Area_Selection_Temuan_dan_Kehilangan_Finish@1x.png')
var marker_lainnyaDone      = require('../imgs/marker/Area_Selection_Lainnya_Finish@1x.png')

function qmap() {
    //Extended for GeoJSON
    L.TopoJSON = L.GeoJSON.extend({
      addData: function(jsonData) {
        if (jsonData.type === "Topology") {
          for (key in jsonData.objects) {
            geojson = topojson.feature(jsonData, jsonData.objects[key]);
            L.GeoJSON.prototype.addData.call(this, geojson);
          }
        }
        else {
          L.GeoJSON.prototype.addData.call(this, jsonData);
        }
      }
    });

    //Function to add layer GeoJSON
    this.topoLayer = new L.TopoJSON();

    //Atributes for waze
    this.wazeMarkers = [];

    //Variable for Complaints
    this.complaintMarkers = [];
    this.complaintCluster = L.markerClusterGroup();

    // Variable for Service
    this.serviceMarkers = [];
    this.serviceCluster = L.markerClusterGroup()
}

//var map;
qmap.prototype.createMap = function(mapid){
    var root = this;

    // this.map = L.map(mapid,{  zoomControl:false , attributionControl:false})
    // .setView([-6.174668,106.827126], 10);
    // L.control.zoom( {position : "topleft"}).addTo(this.map);

    // var Esri_WorldImagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    //  attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
    // });

    // L.tileLayer('http://localhost:32768/styles/bright-v9/rendered/{z}/{x}/{y}.png', {
    //    maxZoom: 18
    // }).addTo(this.map);

    // var baseLayer = L.gridLayer.googleMutant({
    //     type: 'roadmap' // valid values are 'roadmap', 'satellite', 'terrain' , 'traffic' and 'hybrid'
    // }).addTo(this.map);
    // http://localhost:32777/styles/bright-v9/rendered/{z}/{x}/{y}.png

    //Enable Google Traffic
    //baseLayer.addGoogleLayer('TrafficLayer');

    //Function to add marker
    // L.marker([-6.174668,106.827126]).addTo(this.map);
    // L.marker([-6.174668,106.827126], {icon: wazeHazardIcon}).addTo(this.map);
    //loadFile("topojson/eez_land.json", root.drawTopo, root);

    var osmLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>',
    thunLink = '<a href="http://thunderforest.com/">Thunderforest</a>';

    var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        osmAttrib = '&copy; ' + osmLink + ' Contributors',
        landUrl = 'http://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png',
        thunAttrib = '&copy; '+osmLink+' Contributors & '+thunLink;

    var osmMap = L.tileLayer(osmUrl, {}),
        landMap = L.tileLayer(landUrl, {});
        googleMap = L.gridLayer.googleMutant({type: 'roadmap' // valid values are 'roadmap', 'satellite', 'terrain' , 'traffic' and 'hybrid'
                    });
        ESRIMap = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {});
        brightMap = L.tileLayer('http://localhost:32768/styles/bright-v9/rendered/{z}/{x}/{y}.png', {});

    this.map = L.map(mapid,{
        zoomControl:false ,
        attributionControl:false,
        layers: [googleMap]
    })
    .setView([-6.174668,106.827126], 10);

    L.control.zoom( {position : "bottomleft"}).addTo(this.map);

    var baseLayers = {
        "OSM Mapnik": osmMap,
        "Landscape": landMap,
        "Google Maps": googleMap,
        "ESRI Satellite": ESRIMap,
    };

    //Overlay Control for waze data
    root.wazeData = new L.LayerGroup();
    loadFile("http://waze.qlue.id/jakarta.json", root.drawWaze, root);

    var overlays = {
        "Waze Data": root.wazeData
    };

    //Add control layer and overlay to menu
    this.layersControl = L.control.layers(baseLayers,overlays).addTo(this.map);
    this.layersControl.setPosition('topleft');
    //Event listener on map
    this.map.on('baselayerchange', function (e) {
        console.log(e.name);
    });

    this.map.on('resize', function (e) {
        //alert('hai');
    })
}

qmap.prototype.refreshMap = function(){
    this.map.invalidateSize(false);
}

qmap.prototype.drawComplaint = function(data,reportCategoryId) {

    // specify popup options
    var customOptions = {   'maxWidth' : '1500',
                            'className' : 'custom'  }
    if (data.length>0) {

        //draw Marker
        for (i=0; i<data.length; i++) {
            // Create variable
            var lat = data[i].latitude;
            var lng = data[i].longitude;
            var orientation = "";

            // Create content
            var customPopup = orientation;
            var icon;
            // WAITING
            // ON PROGRESS
            // DONE

            //Define the icon by category and status
            if (data[i].status == "WAITING") {
                switch(data[i].ReportCategoryId) {
                    case 'RCATBSD123'   :   icon = marker_fasumWait;
                                            break;
                    case 'RCATTAR123'   :   icon = marker_keamananWait;
                                            break;
                    case 'RCATTAR125'     : icon = marker_kebersihanWait;
                                            break;
                    case 'RCATBSD124'     : icon = marker_parkirWait;
                                            break;
                    case 'RCATBSD001'     : icon = marker_listrikWait;
                                            break;
                    case 'RCATBSD002'     : icon = marker_airWait;
                                            break;
                    case 'RCATBSD126'     : icon = marker_temuanWait;
                                            break;
                    case 'RCATTAR127'     : icon = marker_lainnya;Wait
                                            break;
                    default     :   icon = marker_lainnyaWait;
                                            break;
                }
            } else if (data[i].status == "ON PROGRESS") {
                 switch(data[i].ReportCategoryId) {
                    case 'RCATBSD123'   :   icon = marker_fasumProgress;
                                            break;
                    case 'RCATTAR123'   :   icon = marker_keamananProgress;
                                            break;
                    case 'RCATTAR125'     : icon = marker_kebersihanProgress;
                                            break;
                    case 'RCATBSD124'     : icon = marker_parkirProgress;
                                            break;
                    case 'RCATBSD001'     : icon = marker_listrikProgress;
                                            break;
                    case 'RCATBSD002'     : icon = marker_airProgress;
                                            break;
                    case 'RCATBSD126'     : icon = marker_temuanProgress;
                                            break;
                    case 'RCATTAR127'     : icon = marker_lainnyaProgress
                                            break;
                    default     :   icon = marker_lainnyaProgress;
                                            break;
                }
            } else if (data[i].status == "DONE") {
                switch(data[i].ReportCategoryId) {
                    case 'RCATBSD123'   :   icon = marker_fasumDone;
                                            break;
                    case 'RCATTAR123'   :   icon = marker_keamananDone;
                                            break;
                    case 'RCATTAR125'     : icon = marker_kebersihanDone;
                                            break;
                    case 'RCATBSD124'     : icon = marker_parkirDone;
                                            break;
                    case 'RCATBSD001'     : icon = marker_listrikDone;
                                            break;
                    case 'RCATBSD002'     : icon = marker_airDone;
                                            break;
                    case 'RCATBSD126'     : icon = marker_temuanDone;
                                            break;
                    case 'RCATTAR127'     : icon = marker_lainnyaDone;
                                            break;
                    default     :   icon = marker_lainnyaDone;
                                            break;
                }
            }

            // Create Marker
            var marker = L.marker([lat,lng],{
                icon : L.icon({
                    //iconUrl: 'img/road_closure_pin.png',
                    iconUrl: icon,
                    // shadowUrl: 'leaf-shadow.png',
                    //iconSize:     [30, 30], // size of the icon
                    // shadowSize:   [50, 64], // size of the shadow
                    //iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
                    // shadowAnchor: [4, 62],  // the same for the shadow
                    //popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
                }),
                id : data[i].id,
                beforePictures : data[i].beforePictures,
                content : data[i].content,
                Username : data[i].User.name,
                reportCategoryId : data[i].ReportCategoryId
            }).bindPopup(customPopup,customOptions);

            //markers.addLayer(marker);
            this.complaintCluster.addLayer(marker);
            this.map.addLayer(this.complaintCluster);

            marker.on('popupopen', function(e) {
                var that = this;

                // Check image
                var img = new Image();
                img.onload = function(){
                    $('.leaflet-popup-content-wrapper').css('background-image','url("'+that.options.beforePictures+'")');
                    // console.log($('.leaflet-popup-content').height());
                    // $('.leaflet-popup-content').height(413);
                    // console.log($('.leaflet-popup-content').height());
                    var orientation = "";
                    //alert(this.width+','+this.height);
                    if (this.width > this.height){
                        orientation = 'landscape';
                        //$('.leaflet-popup-content').css("cssText", "height: 250px !important;");
                        //setTimeout(function() {$('.leaflet-popup-content').height(250);}, 50);
                    } else {
                        orientation = 'portrait';
                        //$('.leaflet-popup-content').css("cssText", "height: 650px !important;");
                        //setTimeout(function() {$('.leaflet-popup-content').height(400);}, 50);
                    }
                    var calcHeight = ((204/this.width)*this.height)+115;
                    setTimeout(function() {$('.leaflet-popup-content').height(calcHeight);}, 50);

                    content = "<a class='username-leaflet-popup-content'>"+that.options.Username+"</a><br>";
                    content += that.options.id;
                    content += "<hr class='style-one'>";
                    content += that.options.content;
                    that._popup.setContent("<div class='inside-leaflet-popup-content' >"+content+"</div>");

                    //setTimeout(function() {$('.leaflet-popup-content').height(413);}, 100);
                    //that._popup.options.maxHeight = '500';
                };
                img.src = this.options.beforePictures;
            });
        }

    }
}

qmap.prototype.drawService = function(data, serviceCategoryId) {
    // console.log(data);
    // specify popup options
    var customOptions = {   'maxWidth' : '1500',
                            'className' : 'custom'  }
    if (data.length>0) {

        //draw Marker
        for (i=0; i<data.length; i++) {
            // Create variable
            var lat = data[i].latitude;
            var lng = data[i].longitude;
            var orientation = "";

            // Create content
            var customPopup = orientation;
            var icon;
            // WAITING
            // ON PROGRESS
            // DONE

            //Define the icon by category and status
            if (data[i].status == "WAITING") {
                switch(data[i].ReportCategoryId) {
                    case 'RCATBSD123'   :   icon = marker_fasumWait;
                                            break;
                    case 'RCATTAR123'   :   icon = marker_keamananWait;
                                            break;
                    case 'RCATTAR125'     : icon = marker_kebersihanWait;
                                            break;
                    case 'RCATBSD124'     : icon = marker_parkirWait;
                                            break;
                    case 'RCATBSD001'     : icon = marker_listrikWait;
                                            break;
                    case 'RCATBSD002'     : icon = marker_airWait;
                                            break;
                    case 'RCATBSD126'     : icon = marker_temuanWait;
                                            break;
                    case 'RCATTAR127'     : icon = marker_lainnya;Wait
                                            break;
                    default     :   icon = marker_lainnyaWait;
                                            break;
                }
            } else if (data[i].status == "ON PROGRESS") {
                 switch(data[i].ReportCategoryId) {
                    case 'RCATBSD123'   :   icon = marker_fasumProgress;
                                            break;
                    case 'RCATTAR123'   :   icon = marker_keamananProgress;
                                            break;
                    case 'RCATTAR125'     : icon = marker_kebersihanProgress;
                                            break;
                    case 'RCATBSD124'     : icon = marker_parkirProgress;
                                            break;
                    case 'RCATBSD001'     : icon = marker_listrikProgress;
                                            break;
                    case 'RCATBSD002'     : icon = marker_airProgress;
                                            break;
                    case 'RCATBSD126'     : icon = marker_temuanProgress;
                                            break;
                    case 'RCATTAR127'     : icon = marker_lainnyaProgress
                                            break;
                    default     :   icon = marker_lainnyaProgress;
                                            break;
                }
            } else if (data[i].status == "DONE") {
                switch(data[i].ReportCategoryId) {
                    case 'RCATBSD123'   :   icon = marker_fasumDone;
                                            break;
                    case 'RCATTAR123'   :   icon = marker_keamananDone;
                                            break;
                    case 'RCATTAR125'     : icon = marker_kebersihanDone;
                                            break;
                    case 'RCATBSD124'     : icon = marker_parkirDone;
                                            break;
                    case 'RCATBSD001'     : icon = marker_listrikDone;
                                            break;
                    case 'RCATBSD002'     : icon = marker_airDone;
                                            break;
                    case 'RCATBSD126'     : icon = marker_temuanDone;
                                            break;
                    case 'RCATTAR127'     : icon = marker_lainnyaDone;
                                            break;
                    default     :   icon = marker_lainnyaDone;
                                            break;
                }
            }

            // Create Marker
            var marker = L.marker([lat,lng],{
                icon : L.icon({
                    //iconUrl: 'img/road_closure_pin.png',
                    iconUrl: icon,
                    // shadowUrl: 'leaf-shadow.png',
                    //iconSize:     [30, 30], // size of the icon
                    // shadowSize:   [50, 64], // size of the shadow
                    //iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
                    // shadowAnchor: [4, 62],  // the same for the shadow
                    //popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
                }),
                id : data[i].id,
                beforePictures : data[i].beforePictures,
                content : data[i].content,
                Username : data[i].User.name,
                ServiceCategoryId : data[i].serviceCategoryId
            }).bindPopup(customPopup,customOptions);

            //markers.addLayer(marker);
            this.serviceCluster.addLayer(marker);
            this.map.addLayer(this.serviceCluster);

            marker.on('popupopen', function(e) {
                var that = this;

                // Check image
                var img = new Image();
                img.onload = function(){
                    $('.leaflet-popup-content-wrapper').css('background-image','url("'+that.options.beforePictures+'")');
                    // console.log($('.leaflet-popup-content').height());
                    // $('.leaflet-popup-content').height(413);
                    // console.log($('.leaflet-popup-content').height());
                    var orientation = "";
                    //alert(this.width+','+this.height);
                    if (this.width > this.height){
                        orientation = 'landscape';
                        //$('.leaflet-popup-content').css("cssText", "height: 250px !important;");
                        //setTimeout(function() {$('.leaflet-popup-content').height(250);}, 50);
                    } else {
                        orientation = 'portrait';
                        //$('.leaflet-popup-content').css("cssText", "height: 650px !important;");
                        //setTimeout(function() {$('.leaflet-popup-content').height(400);}, 50);
                    }
                    var calcHeight = ((204/this.width)*this.height)+115;
                    setTimeout(function() {$('.leaflet-popup-content').height(calcHeight);}, 50);

                    content = "<a class='username-leaflet-popup-content'>"+that.options.Username+"</a><br>";
                    content += that.options.id;
                    content += "<hr class='style-one'>";
                    content += that.options.content;
                    that._popup.setContent("<div class='inside-leaflet-popup-content' >"+content+"</div>");

                    //setTimeout(function() {$('.leaflet-popup-content').height(413);}, 100);
                    //that._popup.options.maxHeight = '500';
                };
                img.src = this.options.beforePictures;
            });
        }

    }
}

qmap.prototype.removeComplaint = function(reportCategoryId) {
    var that = this;

    that.complaintCluster.eachLayer(function(marker) {
        if (marker.options.reportCategoryId == reportCategoryId) {
            that.complaintCluster.removeLayer(marker);
        }
    });
}

qmap.prototype.removeService = function(serviceCategoryId) {
    var that = this;

    that.serviceCluster.eachLayer(function(marker) {
        if (marker.options.serviceCategoryId == serviceCategoryId) {
            that.serviceCluster.removeLayer(marker);
        }
    });
}

qmap.prototype.drawTopo = function(root){
    var data = JSON.parse(this.responseText);
    root.topoLayer.addData(data);
    root.topoLayer.addTo(root.map);
}

qmap.prototype.drawWaze = function(root){
    var data = JSON.parse(this.responseText);
    loadFile(data.url, root.drawWazeObject, root);
}

qmap.prototype.drawWazeObject = function(root){
    var data = JSON.parse(this.responseText);

    if (data.alerts.length>0) {
        for (i = 0; i < data.alerts.length; i++) {
            var content = "<b>Peringatan</b><br>";
            content += "Kota : "+data.alerts[i].city+"<br>";
            content += "Jalan : "+data.alerts[i].street+"<br>";
            content += "Tipe : "+data.alerts[i].type+"<br>";
            content += "Sub Tipe : "+data.alerts[i].subtype+"<br>";
            var type = data.alerts[i].type;
            var subtype = data.alerts[i].subtype;
            var x = data.alerts[i].location.x;
            var y = data.alerts[i].location.y;
            switch  (type) {
                case 'JAM'              :
                // L.marker([y,x], {icon: wazeAlertIcon}).addTo(root.map).bindPopup(content);
                //                             break;
                    switch (subtype) {
                        case 'JAM_HEAVY_TRAFFIC'   :    L.marker([y,x], {icon: JAM_HEAVY_TRAFFIC}).addTo(root.wazeData).bindPopup(content);
                                                        break;
                        case 'JAM_STAND_STILL_TRAFFIC'   :    L.marker([y,x], {icon: JAM_STAND_STILL_TRAFFIC}).addTo(root.wazeData).bindPopup(content);
                                                        break;
                        case 'JAM_MODERATE_TRAFFIC'   :    L.marker([y,x], {icon: JAM_MODERATE_TRAFFIC}).addTo(root.wazeData).bindPopup(content);
                                                        break;

                        default                    :    L.marker([y,x], {icon: wazeAlertIcon}).addTo(root.wazeData).bindPopup(content);
                                                        break;
                    }
                    break;
                case 'WEATHERHAZARD'    :
                    switch (data.alerts[i].subtype) {
                        case 'HAZARD_ON_ROAD_CAR_STOPPED'   :   L.marker([data.alerts[i].location.y,data.alerts[i].location.x], {icon: HAZARD_ON_ROAD_CAR_STOPPED}).addTo(root.wazeData).bindPopup(content);
                                                                break;
                        case 'HAZARD_ON_ROAD_CONSTRUCTION'   :   L.marker([data.alerts[i].location.y,data.alerts[i].location.x], {icon: HAZARD_ON_ROAD_CONSTRUCTION}).addTo(root.wazeData).bindPopup(content);
                                                                break;
                        case 'HAZARD_ON_SHOULDER_CAR_STOPPED'   :   L.marker([data.alerts[i].location.y,data.alerts[i].location.x], {icon: HAZARD_ON_SHOULDER_CAR_STOPPED}).addTo(root.wazeData).bindPopup(content);
                                                                break;
                        case 'HAZARD_ON_SHOULDER_ANIMALS'   :   L.marker([data.alerts[i].location.y,data.alerts[i].location.x], {icon: wazeAnimalIcon}).addTo(root.wazeData).bindPopup(content);
                                                                break;


                        default                             :   L.marker([data.alerts[i].location.y,data.alerts[i].location.x], {icon: wazeHazardIcon}).addTo(root.wazeData).bindPopup(content);
                                                                break;
                    }
                                                                                        break;
                case 'ACCIDENT'         :   L.marker([data.alerts[i].location.y,data.alerts[i].location.x], {icon: ACCIDENT}).addTo(root.wazeData).bindPopup(content);
                                            break;
                case 'ROAD_CLOSED'      :   L.marker([data.alerts[i].location.y,data.alerts[i].location.x], {icon: wazeRoadCloseIcon}).addTo(root.wazeData).bindPopup(content);
                                            break;
                default                 :   L.marker([data.alerts[i].location.y,data.alerts[i].location.x]).addTo(root.wazeData).bindPopup(content);
                                            console.log(data.alerts[i].type);
            }
        }
    }

    if (data.jams.length>0) {
        for (i = 0; i < data.jams.length; i++) {
            var path = [];
            var color;
            switch(data.jams[i].level) {
                case 5  :   color = "#bf0707";
                            break;
                case 4  :   color = "#ff4141";
                            break;
                case 3  :   color = "#f7725d";
                            break;
                case 2  :   color = "#ffa042";
                            break;
                case 1  :   color = "#ffd241";
                            break;
                default :   color = "#000000";
                            break;
            }
            var content = "<b>Kemacetan</b><br>";
            content += "Kota : "+data.jams[i].city+"<br>";
            content += "Jalan : "+data.jams[i].street+"<br>";
            content += "Level : "+data.jams[i].level;
            for (j = 0; j < data.jams[i].line.length; j++) {
                var line = data.jams[i].line[j];
                path.push([line.y,line.x]);
            }
            var polyline = L.polyline(path, {
                color: color,
                opacity: 1,
                weight: 4
            }).addTo(root.wazeData).bindPopup(content);

        }
    }
}


var ACCIDENT = L.icon({
    //iconUrl: 'img/road_closure_pin.png',
    iconUrl: 'http://waze.qlue.id/icon/ACCIDENT.png',
    // shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});

var JAM_STAND_STILL_TRAFFIC = L.icon({
    //iconUrl: 'img/road_closure_pin.png',
    iconUrl: 'http://waze.qlue.id/icon/JAM_STAND_STILL_TRAFFIC.png',
    // shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});

var JAM_MODERATE_TRAFFIC = L.icon({
    //iconUrl: 'img/road_closure_pin.png',
    iconUrl: 'http://waze.qlue.id/icon/JAM_MODERATE_TRAFFIC.png',
    // shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});


var JAM_HEAVY_TRAFFIC = L.icon({
    //iconUrl: 'img/road_closure_pin.png',
    iconUrl: 'http://waze.qlue.id/icon/JAM_HEAVY_TRAFFIC.png',
    // shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});

var wazeAnimalIcon = L.icon({
    //iconUrl: 'img/road_closure_pin.png',
    iconUrl: 'http://waze.qlue.id/icon/icon_report_hazard_animals.png',
    // shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});

var HAZARD_ON_ROAD_CONSTRUCTION = L.icon({
    //iconUrl: 'img/road_closure_pin.png',
    iconUrl: 'http://waze.qlue.id/icon/HAZARD_ON_ROAD_CONSTRUCTION.png',
    // shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});

var HAZARD_ON_ROAD_CAR_STOPPED = L.icon({
    //iconUrl: 'img/road_closure_pin.png',
    iconUrl: 'http://waze.qlue.id/icon/HAZARD_ON_ROAD_CAR_STOPPED.png',
    // shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});

var HAZARD_ON_SHOULDER_CAR_STOPPED = L.icon({
    //iconUrl: 'img/road_closure_pin.png',
    iconUrl: 'http://waze.qlue.id/icon/HAZARD_ON_SHOULDER_CAR_STOPPED.png',
    // shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});

var wazeRoadCloseIcon = L.icon({
    //iconUrl: 'img/road_closure_pin.png',
    iconUrl: 'http://waze.qlue.id/icon/smallpin_closure.png',
    // shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});

var wazeAlertIcon = L.icon({
    //iconUrl: 'https://wiki.waze.com/wiki/images/e/e2/Alert_pin_loads%402x.png',
    iconUrl: 'http://waze.qlue.id/icon/smallpin_traffic.png',
    // shadowUrl: 'leaf-shadow.png',
     iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
     iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});

var wazeAccidentIcon = L.icon({
    //iconUrl: 'https://wiki.waze.com/wiki/images/0/04/Alert_pin_accident%402x.png',
    iconUrl: 'http://waze.qlue.id/icon/smallpin_traffic.png',
    // shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});



var wazeHazardIcon = L.icon({
    iconUrl: 'http://waze.qlue.id/icon/smallpin_hazard.png',
    // shadowUrl: 'leaf-shadow.png',
    iconSize:     [30, 30], // size of the icon
    // shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [14, 27], // point of the icon which will correspond to marker's location
    // shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -24] // point from which the popup should open relative to the iconAnchor
});






function xhrSuccess () { this.callback.apply(this,this.arguments); }

function xhrError () { console.error(this.statusText); }

function loadFile (sURL, fCallback /*, argumentToPass1, argumentToPass2, etc. */) {
  var oReq = new XMLHttpRequest();
  oReq.callback = fCallback;
  oReq.arguments = Array.prototype.slice.call(arguments, 2);
  oReq.onload = xhrSuccess;
  oReq.onerror = xhrError;
  oReq.open("get", sURL, true);
  oReq.send(null);
}

module.exports = qmap;
