# TODO

1. Client dashboard right bar component is not activated after login :+1:
2. Back button at login cause error after sign in :+1:
3. Server side rendering
4. Cannot open react state when upload photo in add news component :+1:
5. DRY in report component (Optional) :+1:
6. DRY in Register component (Optional) :+1:
7. Show placeholder in upload file (Optional) :+1:
8. Multiple Batch actions after login dashboard for report component :+1: (solve with multiple axios fetching with .then)
9. Back End Validation for register manager :+1:
10. Change btoa & atob decode for cross browser compatibility
11. https://api.qluster.id/api/companies/COMPSML123/advanced-query-house/search?properties=TOWNBSD124,all can get empty clusters! :+1:
12. dispatchOccupantCode & dispatchManagerCode API still returns empty array :+1:
13. Batch Actions for cluster list & town list in dispatchAllTownshipClusterList :+1:
14. Get all house billing still get an empty result from API :+1:
15. Resize picture are cropped from upload dashboard news :+1:
16. Occupant report API still gets different results
17. Map Layer causes dashboard to perform slower
18. Fetch house in map only shows house with master only. It must be showing all house :+1:
19. After Add Edit anything should be showing with the same field
20. Send Billing, register manager still returns success with invalid code that was not on the house code list  (eg. HOMEBSD123 returns success)

# package.json script for production

```
"scripts": {
  "start": "serve -s build",
  "prestart": "npm install -g serve",
  "local": "react-scripts start",
  "build": "react-scripts build",
  "test": "react-scripts test --env=jsdom",
  "eject": "react-scripts eject"
}

```

# package.json script for development

```
"scripts": {
  "start": "PORT=3001 react-scripts start",
  "build": "react-scripts build",
  "test": "react-scripts test --env=jsdom",
  "eject": "react-scripts eject"
}

```

# deploy for glc steps

```
$ ssh glc-qluster@110.5.110.222
$ cd qluster-dashboard
$ git pull origin development
$ sudo reboot now
$ ssh glc-qluster@110.5.110.222
```
