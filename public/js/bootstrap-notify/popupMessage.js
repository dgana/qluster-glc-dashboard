module.exports = {
  message: function (icon, message, type, delay, from, align, offset) {
    $.notify({
      icon: icon,
      message: message,
    }, {
      type: type,
      delay: delay,
      offset: offset,
      placement: {
        from: from,
        align: align
      }
    })
  }
}
