import getHeaders from './getHeaders'
import { url } from '../../config'

const axiosConfigRefresh = () =>{
let { authorization,refreshtoken } = getHeaders()
	return {
    method:'post',
    url:url + 'managers/refresh',
    headers: {
      'Authorization': authorization,
      'RefreshToken': refreshtoken
    },
    responseType:'json',
    data: {}
  }
}

export default axiosConfigRefresh;
