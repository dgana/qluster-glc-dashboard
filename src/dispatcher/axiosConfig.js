import getHeaders from './getHeaders'

const axiosconfig = dataConfig => {

let { method, url, data, headers } = dataConfig
let authorization = getHeaders().authorization

	if (data && headers) {
		headers['Authorization'] = authorization
		return {
      method,
      url,
      data,
      headers,
      responseType:'json'
    }
	} else if (headers) {
		headers['Authorization'] = authorization
		return {
      method,
      url,
      data,
      headers,
      responseType:'json',
    }
	} else return { method,url, headers:{ 'Authorization': authorization }, responseType:'json' }
}

export default axiosconfig
