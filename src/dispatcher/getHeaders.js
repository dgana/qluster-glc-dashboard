const getToken = () => {
  if (localStorage.getItem('token'))
  return JSON.parse(localStorage.getItem('token'))
  return { authorization:null, refreshtoken:null }
}

export default getToken
