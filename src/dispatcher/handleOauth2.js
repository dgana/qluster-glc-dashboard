import axios from 'axios'
import axiosConfig from './axiosConfig.js'
import axiosConfigRefresh from './axiosConfigRefresh.js'

const handleOauth2 = (config) => {

  const fetching = async () => {
    console.log(config)
  if(config.err.response.status === 401 && config.err.response.data.result.message === 'token kadaluarsa'){

      // mengubah token yg sudah expired dengan token baru
      const resRefresh = await axios(axiosConfigRefresh())

      localStorage.setItem('token', JSON.stringify(resRefresh.headers))
      try{
        const res = await axios(axiosConfig(config.dataConfig))
        if (config.cb) config.cb(null,res.data.result)
        if (config.action) config.dispatch(config.action(res.data.result))
      }
      catch(e){
        config.cb ? config.cb(e,null) : console.log(e.response)
      }
    } else if (config.err.response.status === 401 && config.err.response.data.result.message === 'token invalid') {
     localStorage.clear()
   } else {
     console.log('server error!',config.err.response);
     config.cb(config.err.response.data.result,null)
   }
 }

 fetching()

}

export default handleOauth2
