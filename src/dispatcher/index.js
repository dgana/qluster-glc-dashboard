import axios from 'axios'
import { AES,enc } from 'crypto-js'

import popup from '../../public/js/bootstrap-notify/popupMessage'
import { url } from '../../config'
import axiosConfig from './axiosConfig.js'
import axiosConfigRefresh from './axiosConfigRefresh.js'
// import getHeaders from './getHeaders'
import handleOauth2 from './handleOauth2'

import {
  actionClientLogin,
  actionLogoutClient,
  actionSidebarToggle,
  actionOccupantCode,
  actionManagerCode,
  actionAddRole,
  actionResetRoleOption,
  actionEditProfile,
  actionResetOccupantCode,
  actionResetManagerCode,
  actionGetFeatures,
  actionResetFeatures
} from '../action'

import {
  actionSidebarNavigation,
  actionSidebarActive
} from '../action/sidebar'

import {
  actionClientRoute
} from '../action/route'

import {
  actionServiceReport,
  actionComplaintReport,
  actionOccupantReport,
  actionManagerReport,
  actionRoleReport,
  actionSosReport,
  actionBillingReport,
  actionCctvReport,
  actionHouseReport,
  actionClusterReport,
  actionGpsReport,
  actionResetHouseReport
} from '../action/report'

import {
  actionTabPanelRumah,
  actionTabPanelCluster,
  // actionTabPanelTownship
} from '../action/tabPanel'

import {
  actionFetchNews,
  actionAddNews,
  // actionAddNewsComment,
  actionDeleteNews,
  actionResetNews
} from '../action/news'

import {
  actionAllHouseList,
  actionAddHouseList,
  actionResetHouse
} from '../action/registerHouse'

import {
  actionHouseByCluster,
  actionRemoveHouseByCluster,
  actionLoadHouseByCluster,
  actionResetHouseByCluster,
  actionHouseByClusterSimple
} from '../action/filterHouse'

import {
  actionAllClusterList,
  actionAddClusterList,
  actionResetClusterList,
  actionDeleteCluster,
  actionEditCluster
} from '../action/registerCluster'

import {
  actionShowSummary
} from '../action/summary'

import {
  actionEditRole,
  actionRoleList
} from '../action/manajemenPeranan'

import {
  actionAdminManagementList,
  actionEditUserDashboard
} from '../action/manajemenAdmin'

import {
  actionSearchHouseByIdAndName,
  actionResetHouseByIdAndName,
  actionEditHouseByIdAndName,
  actionDeleteHouseByIdAndName,
  actionCurrentHouseSearch
} from '../action/manajemenRumah'

import {
  actionCctvQmap,
  actionResetCctvQmap,
  actionRegisterCctv,
  actionEditCctv,
  actionDeleteCctv
} from '../action/cctvList'

import {
  actionHideMapMenu,
  actionShowMapMenu
} from '../action/general'

// *************************** Dispather General ************************** //

export const dispatchHideMapMenu = () => dispatch => dispatch(actionHideMapMenu())
export const dispatchShowMapMenu = () => dispatch => dispatch(actionShowMapMenu())
export const dispatchCurrentHouseSearch = search => dispatch => dispatch(actionCurrentHouseSearch(search))
export const dispatchRoute = (role, route = []) => dispatch => dispatch(actionClientRoute(route))
export const dispatchSidebarToggle = () => dispatch => dispatch(actionSidebarToggle())

export const dispatchSidebar = (sidebar, payload = {}) => {
  switch(sidebar) {
    case 'path':
      return dispatch => dispatch(actionSidebarNavigation(payload))
    case 'active':
      return dispatch => dispatch(actionSidebarActive(payload))
    default:
      return dispatch => dispatch(actionSidebarNavigation(payload))
  }
}

// *************************** Dispather Reset ************************** //

export const dispatchResetNews = () => dispatch => dispatch(actionResetNews())
export const dispatchResetOccupantCode = () => dispatch => dispatch(actionResetOccupantCode())
export const dispatchResetManagerCode = () => dispatch => dispatch(actionResetManagerCode())
export const dispatchResetHouse = () => dispatch => dispatch(actionResetHouse())
export const dispatchResetClusterList = () => dispatch => dispatch(actionResetClusterList())
export const dispatchResetRoleOption = () => dispatch => dispatch(actionResetRoleOption())
export const dispatchResetCctvQmap = () => dispatch => dispatch(actionResetCctvQmap())
export const dispatchResetHouseByCluster = () => dispatch => dispatch(actionResetHouseByCluster())
export const dispatchResetHouseReport = () => dispatch => dispatch(actionResetHouseReport())
export const dispatchResetHouseByIdAndName = () => dispatch => dispatch(actionResetHouseByIdAndName())
export const dispatchResetFeatures = () => dispatch => dispatch(actionResetFeatures())

// *************************** Dispather Login & Logout Dashboard ************************** //

export const dispatchUserLogin = (email, password, cb) => {
  const passEncrypted = AES.encrypt(password, 'qluster123').toString()
  // config for axios
  const dataConfig = {
    method : 'post',
    url : url + 'managers/logindashboard',
    data : {
       email: email,
       password: passEncrypted
     },
    headers : { 'content-type': 'application/json' }
  }
  return dispatch => {
    const Login = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        localStorage.setItem('token', JSON.stringify(res.headers))
        const user = res.headers.usertoken.split(' ')[1]
        const data = JSON.parse(AES.decrypt(user.toString(),process.env.REACT_APP_SECRET_KEY||'qluster123').toString(enc.Utf8))
        popup.message('pe-7s-check',`<span class="alert-popup-text-message">Login berhasil!</span>`,'success', 2000 ,'bottom','right')
        dispatch(actionClientLogin(data))
      }
      catch(err) {
        popup.message('pe-7s-attention',`<span class="alert-popup-text-message">Username atau password salah!</span>`,'danger', 2000 ,'bottom','right')
        cb()
      }
    }
    Login()
  }
}

export const dispatchClientLogout = (cb) => dispatch => dispatch(actionLogoutClient(cb))

export const dispatchRecoveryPassword = (email,cb) => {
  return dispatch => {
    const sending = async () =>{
      try{
        const res = await axios({method:'post',responseType:'json',url:url+'managers/forget',data:{email}})
        cb(null,res.data.result.message)
      }
      catch(e){
        cb(e.response.data.result.message,null)
      }
    }
    sending()
  }
}

// *************************** Dispather /daftar-pengelola & /daftar-penghuni ************************** //

export const dispatchOccupantCode = (cb) => {

  const dataConfig = {
    method:'get',
    url: url + 'managers/showhousecode',
  }

  return dispatch => {
    const fetchingOccupantCode = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        cb(null,res.data.result)
        dispatch(actionOccupantCode(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionOccupantCode,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
    }
    fetchingOccupantCode()
  }
}

export const dispatchManagerCode = (cb) => {

  const dataConfig = {
    method:'get',
    url: url + 'managers/showdivisioncode',
  }

  return dispatch => {
    const fetchingManagerCode = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        cb(null,res.data.result)
        dispatch(actionManagerCode(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionManagerCode,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
    }
    fetchingManagerCode()
  }
}

export const dispatchGetFeatures = () => {
  const dataConfig = {
    method:'get',
    url: url + 'managers/featurechecklist',
  }

  return dispatch => {
    const fetchingGetFeatures = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        return dispatch(actionGetFeatures(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionGetFeatures,
        }
        handleOauth2(dataConfigOauth)
       }
    }
    fetchingGetFeatures()
  }
}


export const dispatchAddRole = (newRole, cb) => {
  const dataConfig = {
    method:'post',
    url: url + 'managers/role',
    data: newRole,
    headers:{ContentType:'application/json'}
  }

  return dispatch => {
    const addRole = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
        // return dispatch(actionAddRole(res.data.result.features))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionAddRole,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
    }
    addRole()
  }
}

// *************************** Dispather /manajemen-admin ************************** //

export const dispatchAdminManagementList = (cb) => {
  const dataConfig = {
    method:'get',
    url: url + 'managers/managermanagement',
  }

  return dispatch => {
    const fetchAdminManagementList = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        dispatch(actionAdminManagementList(res.data.result))
        cb(null, res.data.result)
      }
      catch(err){
        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){
            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try{
              const res = await axios(axiosConfig(dataConfig))
              dispatch(actionAdminManagementList(res.data.result))
              cb(null, res.data.result)
            }
            catch(e){
              cb(e,null)
            }
          } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
    }
    fetchAdminManagementList()
  }
}

export const dispatchEditUserDashboard = (form, cb) => {

  const dataConfig = {
    method:'put',
    url: url + `managers/${form.userId}`,
    data: {
      name: form.name,
      roleId: form.roleId,
      isActive: String(form.status==='aktif'),
    },
    headers:{ContentType:'application/json'}
  }
  return dispatch => {
    const editUserDashboard = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
        return dispatch(actionEditUserDashboard(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionEditUserDashboard,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
    }
    editUserDashboard()
  }
}

export const dispatchEditUserDashboardPassword = (form, cb) => {

  const dataConfig = {
    method:'put',
    url:url + `managers/${form.userId}/changepassword`,
    data: {
      newPassword: form.password,
      confirmNewPassword: form.confirmPassword,
    },
    headers:{ContentType:'application/json'}
  }
  return dispatch => {
    const editUserDashboardPassword = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
    }
    editUserDashboardPassword()
  }
}

// *************************** Dispather Pendaftaran / Dashboard / Lokasi ************************** //

export const dispatchHouseByClusterSimple = clusterId => {
  return dispatch => {
    return axios({
      method: 'get',
      responseType: 'json',
      url: url + 'companies/house-by-cluster-simple/' + clusterId
    })
    .then(res => {
      return dispatch(actionHouseByClusterSimple(res.data.result))
    })
    .catch(err => {
      console.log(err);
    })
  }
}

export const dispatchLoadHouseByCluster = () => dispatch => dispatch(actionLoadHouseByCluster())
  export const dispatchRemoveHouseByCluster = clusterId => dispatch => dispatch(actionRemoveHouseByCluster(clusterId))

// *************************** Dispather Manajemen / CCTV ************************** //

export const dispatchDeleteCctv = (cctvId, cb) => {

  const dataConfig = {
    method:'delete',
    url:url + `cctvs/${cctvId}`,
  }

  return dispatch => {
    const deleteCctv = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
        return dispatch(actionDeleteCctv(cctvId))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionDeleteCctv,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
     }
     deleteCctv()
   }
}

export const dispatchEditCctv = (form, cb) => {
  const dataConfig = {
    method:'put',
    url:url + `cctvs/${form.cctvId}`,
    data: {
      latitude: form.latitude,
      longitude: form.longitude,
      url: form.url,
      name: form.name,
      isActive: String(form.isActive)
    },
    headers:{
      ContentType:'application/json'}
  }

  return dispatch => {
    const editCctv = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
        return dispatch(actionEditCctv(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionEditCctv,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
     }
     editCctv()
   }
}

// *************************** Dispather Pendaftaran / Dashboard / CCTV ************************** //

export const dispatchRegisterCctv = (cctvList, cb) => {

  const cctvArr = cctvList.map(cctv => {delete cctv.id; return cctv})

  const dataConfig = {
    method: 'post',
    url: url + 'cctvs/',
    data: { cctvArr },
    headers:{ContentType:'application/json'}
  }

  return dispatch => {
    const fetchingRegisterCctv = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
        return dispatch(actionRegisterCctv(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionRegisterCctv,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
     }
     fetchingRegisterCctv()
   }
}

// *************************** Dispather Manajemen / Peran ************************** //

export const dispatchRoleList = (cb) => {

  const dataConfig = {
   method: 'get',
   url: url + 'managers/features',
  }

  return dispatch => {

    const fetchingRoles = async () =>{
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null,res.data.result.role)
        dispatch(actionRoleList(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionRoleList,
          cb
        }
        handleOauth2(dataConfigOauth)
      }
    }
    fetchingRoles()
  }
}

export const dispatchEditRole = (form, cb) => {

  const dataConfig = {
   method: 'put',
   url: url + 'managers/features',
   data: {
     newName: form.newName,
     roleFeature: form.roleFeature,
   },
   headers: {
     ContentType:'application/json'
   }
  }

  return dispatch => {

    const editRole = async () =>{
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
        dispatch(actionEditRole(form))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionEditRole,
          cb
        }
        handleOauth2(dataConfigOauth)
      }
    }
    editRole()
  }
}

// *************************** Dispather Manajemen Rumah ************************** //

export const dispatchSearchHouseByIdAndName = (value, cb) => {

  const dataConfig = {
    method: 'get',
    url: url + `houses/search/${value}`,
  }

  return dispatch => {
    const searchHouseByIdAndName = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
        return dispatch(actionSearchHouseByIdAndName(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionSearchHouseByIdAndName,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
     }
     searchHouseByIdAndName()
   }

}

export const dispatchEditHouse = (form, cb) => {
  const dataConfig = {
    method: 'put',
    url: url + `houses/${form.houseId}`,
    data: {
      address: form.address,
      number:form.number,
      latitude: form.latitude,
      longitude: form.longitude,
      ClusterId: form.clusterId,
      handoverDate:String(form.handoverDate)
    },
    headers: {
      ContentType:'application/json'
    }
  }

  return dispatch => {
    const EditHouse = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
        return dispatch(actionEditHouseByIdAndName(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionEditHouseByIdAndName,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
     }
     EditHouse()
   }
}

export const dispatchDeleteHouse = (houseId, cb) => {
  return dispatch => {
    return axios({
      method: 'delete',
      responseType: 'json',
      url: url + 'houses/' + houseId,
      headers: {'Authorization': JSON.parse(localStorage.getItem('token')).authorization},
    })
    .then(res => {
      cb(null, res.data.result)
      return dispatch(actionDeleteHouseByIdAndName(res.data.result.data.id))
    })
    .catch(err => {
      cb(err, null)
    })
  }
}

// *************************** Dispather Edit Profile ************************** //

export const dispatchEditProfile = (form, cb) => {
  let data = new FormData()
  if(typeof form.picture !== 'string') {
    data.append('file', form.picture[0])
    return dispatch => {
      return axios({
        method: 'post',
        url: url + 'uploads/single',
        data: data
      })
      .then(picture => {
        return axios({
          method: 'put',
          responseType: 'json',
          url: url + 'managers/',
          headers: {
            Authorization: JSON.parse(localStorage.getItem('token')).authorization,
            ContentType:'application/json'
          },
          data: {
            name: form.name,
            picture: picture.data.result
          }
        })
        .then(res => {
          const token = JSON.parse(localStorage.getItem('token'))
          const newToken = {...token,usertoken:res.headers.usertoken}
          localStorage.setItem('token',JSON.stringify(newToken))
          cb(null, res.data.result)
          return dispatch(actionEditProfile({name:form.name,picture:form.picture[0].preview}))
        })
        .catch(err => {
          cb(err, null)
        })
      })
      .catch(err => {
        console.log(err.response);
      })
    }
  } else {
    return dispatch => {
      return axios({
        method: 'put',
        responseType: 'json',
        url: url + 'managers',
        headers: {
          Authorization: JSON.parse(localStorage.getItem('token')).authorization,
          ContentType:'application/x-www-form-urlencoded'
        },
        data: {
          name: form.name,
          picture: form.picture
        }
      })
      .then(res => {
        cb(null, res.data.result)
        if(res.data.result.length !== 0) {
          const token = JSON.parse(localStorage.getItem('token'))
          const newToken = {...token,usertoken:res.headers.usertoken}
          localStorage.setItem('token',JSON.stringify(newToken))
          return dispatch(actionEditProfile({name:form.name,picture:form.picture}))
        }
      })
      .catch(err => {
        cb(err, null)
      })
    }
  }
}
// *************************** Dispather Change Password ************************** //

export const dispatchChangePassword = (form, cb) => {
  return dispatch => {
    return axios({
      method: 'put',
      url: 'https://qluster-il-api.appspot.com/api/managers/changepassword',
      headers: {
        Authorization: JSON.parse(localStorage.getItem('token')).authorization,
        ContentType:'application/x-www-form-urlencoded',
      },
      data: {
        ...form
      }
    })
    .then(res => {
      cb(null, res.data.result)
    })
    .catch(err => {
      cb(err.response.data, null)
    })
  }
}

// *************************** Dispather Summary ************************** //

export const dispatchShowSummary = (clusterId, month, cb) => {
  const dataConfig ={
    method: 'get',
    url: url + `summaries/${clusterId}/search?month=${month}`,

  }

  return dispatch => {
    const showSummery = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
        return dispatch(actionShowSummary(res.data.result))
      }
      catch (err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionShowSummary,
          cb
        }
        handleOauth2(dataConfigOauth)
      }
    }
    showSummery()
  }
}

// *************************** Dispather News ************************** //

export const dispatchFetchNews = () => {
  const dataConfig = {
    method: 'get',
    url: url + 'news/manager/list'
  }

  return dispatch => {
    const fetchingNews = async() => {
      try{
        const response = await axios(axiosConfig(dataConfig))
        return dispatch(actionFetchNews(response.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionFetchNews,
        }
        handleOauth2(dataConfigOauth)
       }
     }
     fetchingNews()
   }
}

export const dispatchDeleteNews = (newsId) => {

  // const dataConfig = {
  //   method: 'delete',
  //   url: url + 'news/manager/' + newsId
  // }
  const dataConfigRefresh = url + 'managers/refresh'

  return dispatch => {
    const deleteNews = async () =>{
      // const response = await axios(axiosConfig(dataConfig))
      try {
        popup.message('pe-7s-like2', `<span class="alert-popup-text-message">Berita berhasil dihapus</span>`, 'success', 2000, 'bottom', 'right')
        dispatch(actionDeleteNews(newsId))
      }
      catch (err) {
        if (err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa') {
            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh(dataConfigRefresh))
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))

            // const resDelete = await axios(axiosConfig(dataConfig))
            popup.message('pe-7s-like2', `<span class="alert-popup-text-message">Berita berhasil dihapus</span>`, 'success', 2000, 'bottom', 'right')
            dispatch(actionDeleteNews(newsId))
          }
        if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
          localStorage.clear()
        }
        popup.message('pe-7s-attention', `<span class="alert-popup-text-message">Berita tidak ditemukan</span>`, 'danger', 2000, 'bottom', 'right')
      }
    }
    deleteNews()
  }
}


export const dispatchAddNews = (news, cb) => {
  let data = new FormData()
  const dataConfig = {
    method: 'post',
      url: url + `news/manager/category/${news.category}`,
      headers: {'Authorization': JSON.parse(localStorage.getItem('token')).authorization},
    data: {
      title: news.title,
      content: news.content,
    }
  }
  if (news.pictures.length === 0) {
    return dispatch => {
      const AddNews = async () =>{
        try{
            const res = await axios(axiosConfig(dataConfig))
            cb(null, res.data.result)
            dispatch(actionAddNews(res.data.result))
        }
        catch(err){
          const dataConfigOauth = {
            err,
            dataConfig,
            dispatch,
            action:actionAddNews,
            cb,
          }
          handleOauth2(dataConfigOauth)
        }
      }
      AddNews()
    }
  } else {
    news.pictures.forEach((item, index) => {
      return data.append('file', news.pictures[index])
    })

    return dispatch => {
      return axios({
        method: 'post',
        url: url + 'uploads/multi',
        data: data,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data; boundary='+data._boundary
        },
      })
      .then(picture => {
        return axios({
          method: 'post',
          responseType: 'json',
          url: url + `news/manager/category/${news.category}`,
          headers: {'Authorization': JSON.parse(localStorage.getItem('token')).authorization},
          data: {
            title: news.title,
            content: news.content,
            pictures: picture.data.result,
          }
        })
        .then(res => {
          setTimeout(() => {
            cb(null, res.data.result)
            dispatch(actionAddNews(res.data.result))
          }, 3000)
        })
        .catch(err => {
          cb(err, null)
        })
      })
    }
  }
}

// *************************** Dispather Billing ************************** //

export const dispatchSendBilling = (billArr, cb) => {
  return dispatch => {
    return axios({
      method: 'post',
      responseType: 'json',
      url: url + 'invoices',
      headers: {'Authorization': JSON.parse(localStorage.getItem('token')).authorization},
      data: {
        billArr
      }
    })
    .then(res => {
      let response = res.data.result
      response.failed = response.failed.filter(item => item.id)
      cb(null, response)
    })
    .catch(err => {
      cb(err, null)
    })
  }
}

export const dispatchAddOccupant = (occupant, cb) => {

  const dataConfig = {
    method:'post',
    url: url + 'residents/register',
    data:{
      arrUser: occupant
    },
    headers:{ ContentType:'application/json' }
  }

  return dispatch => {
    const addOccupant = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        cb(null,res.data.result)
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
    }
    addOccupant()
  }


}

export const dispatchAddManager = (manager, cb) => {

    const dataConfig = {
      method:'post',
      url: url + 'managers/register',
      data:{
        arrUser: manager
      },
      headers:{ ContentType:'application/json' }
    }

    return dispatch => {
      const addManager = async() => {
        try {
          const res = await axios(axiosConfig(dataConfig))
          cb(null,res.data.result)
        }
        catch(err){
          const dataConfigOauth = {
            err,
            dataConfig,
            dispatch,
            cb
          }
          handleOauth2(dataConfigOauth)
         }
      }
      addManager()
    }

}

// *************************** Dispather Town & Cluster List ************************** //

export const dispatchTabPanel = (lokasi) => {
  switch(lokasi) {
    case 'rumah':
      return dispatch => dispatch(actionTabPanelRumah(lokasi))
    case 'cluster':
      return dispatch => dispatch(actionTabPanelCluster(lokasi))
    default:
      return dispatch => dispatch(actionTabPanelRumah(lokasi))
  }
}

// *************************** Dispather Bulk Insert House, Town & Cluster List ************************** //

export const dispatchAddClusterList = newList => dispatch => dispatch(actionAddClusterList(newList))

export const dispatchAddHouseList = newList => dispatch => dispatch(actionAddHouseList(newList))

export const dispatchAllHouseList = compId => {
  return dispatch => {
    return axios({
      method: 'get',
      responseType: 'json',
      url: url + 'companies/' + compId + '/get-house/search?clusterId=all'
    })
    .then(res => {
      return dispatch(actionAllHouseList(res.data.result))
    })
    .catch(err => {
      console.log(err);
    })
  }
}

export const dispatchDeleteCluster = (clusterId, cb) => {

  const dataConfig = {
    method: 'delete',
    url: url + `clusters/${clusterId}`
  }

  return dispatch => {
    const deleteCluster = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
        return dispatch(actionDeleteCluster(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionDeleteCluster,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
     }
     deleteCluster()
   }
}

export const dispatchAllClusterList = cb => {
  console.log('AJBJKAHDJ');
  const dataConfig = {
    method: 'get',
    url: url + 'clusters'
  }

  return dispatch => {
    const fetchingAllClusterList = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        dispatch(actionAllClusterList(res.data.result))
        cb(null, res.data.result)
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action: actionAllClusterList,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
     }
     fetchingAllClusterList()
   }
}

export const dispatchRegisterCluster = (cluster, cb) => {
  const clusterArr = [cluster]

  const dataConfig = {
    method: 'post',
    url: url + 'clusters',
    data: {clusterArr},
    headers:{ContentType:'application/json'}
  }

  return dispatch => {
    const fetchingRegisterHouse = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
     }
     fetchingRegisterHouse()
   }
}

export const dispatchRegisterHouse = (houselist, cb) => {
  console.log('houselist',houselist);
  const houseArr = houselist.map( (house,index) =>{
    const { address,longitude,HouseName,latitude,size,handoverDate,ClusterId } = house
    if(handoverDate!==null)
    return {number:HouseName,address,longitude,latitude,size:+size,handoverDate:String(handoverDate),ClusterId}
    else return {number:HouseName,address,longitude,latitude,size:+size,handoverDate:handoverDate,ClusterId}
  })

  const dataConfig = {
    method: 'post',
    url: url + 'houses',
    data: {
      houseArr
    },
    headers:{ContentType:'application/json'}
  }

  return dispatch => {
    const fetchingRegisterHouse = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        console.log('res',res);
        cb(null, res.data.result)
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
     }
     fetchingRegisterHouse()
   }
}

export const dispatchEditCluster = (form, cb) => {
  const dataConfig = {
    method: 'put',
    url: url + `clusters/${form.id}`,
    data: {
      id:form.id,
      address: form.address,
      name: form.name,
      latitude: form.latitude,
      longitude: form.longitude,
    },
    headers:{ContentType:'application/json'}
  }

  return dispatch => {
    const editCluster = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
        return dispatch(actionEditCluster(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionEditCluster,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
     }
     editCluster()
   }
}

// *************************** Dispather /map ************************** //

export const dispatchAllCategories = cb => {

  const dataConfig = {
    method: 'get',
    url: url + `maps`
  }

  return dispatch => {
    const editCluster = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result)
      }
      catch(err) {
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
     }
     editCluster()
   }

  // return dispatch => {
  //   return axios({
  //     method: 'get',
  //     responseType: 'json',
  //     url: url + 'maps',
  //     headers: {'Authorization': JSON.parse(localStorage.getItem('token')).authorization},
  //   })
  //   .then(res => {
  //     cb(null, res.data.result)
  //   })
  //   .catch(err => {
  //     cb(err, null)
  //   })
  // }
}

export const dispatchComplaintQmap = (categoryId, cb) => {
  // return dispatch => {
  //   return axios({
  //     method: 'get',
  //     responseType: 'json',
  //     url: url + 'maps/complains/' + categoryId,
  //     headers: {'Authorization': JSON.parse(localStorage.getItem('token')).authorization},
  //   })
  //   .then(res => {
  //     cb(null, res.data.result, categoryId)
  //   })
  //   .catch(err => {
  //     cb(err, null, categoryId)
  //   })
  // }

  const dataConfig = {
    method: 'get',
    url: url + `maps/complains/${categoryId}`
  }

  return dispatch => {
    const complaintQmap = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result, categoryId)
      }
      catch(err){
        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){
            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try {
              const res = await axios(axiosConfig(dataConfig))
              cb(null, res.data.result, categoryId)
            }
            catch(e){
              cb(err, null, categoryId)
            }
         } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
     }
     complaintQmap()
   }
}

export const dispatchServiceQmap = (id, categoryId, cb) => {
  return dispatch => {
    return axios({
      method: 'get',
      responseType: 'json',
      url: url + 'companies/' + id + '/all-service/search?categories=' + categoryId
    })
    .then(res => {
      cb(null, res.data.result, categoryId)
    })
    .catch(err => {
      cb(err, null, categoryId)
    })
  }
}

export const dispatchBillingQmap = (categoryId, cb) => {

  const dataConfig = {
    method: 'get',
    url: url + 'maps/invoices'
  }

  return dispatch => {
    const BillingQmap = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result, categoryId)
      }
      catch(err){
        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){
            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try {
              const res = await axios(axiosConfig(dataConfig))
              cb(null, res.data.result, categoryId)
            }
            catch(e){
              cb(err, null, categoryId)
            }
         } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
     }
     BillingQmap()
   }
}

export const dispatchSosQmap = (id, categoryId, cb) => {
  return dispatch => {
    return axios({
      method: 'get',
      responseType: 'json',
      url: url + 'companies/' + id + '/all-sos'
    })
    .then(res => {
      cb(null, res.data.result, categoryId)
    })
    .catch(err => {
      cb(err, null, categoryId)
    })
  }
}

export const dispatchHouseByCluster = clusterId => {

  const dataConfig = {
    method: 'get',
    url: url + `houses/${clusterId}`
  }

  return dispatch => {
    const HouseByCluster = async() => {
      try{
        const res = await axios(axiosConfig(dataConfig))
        return dispatch(actionHouseByCluster(res.data.result))
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionHouseByCluster,
        }
        handleOauth2(dataConfigOauth)
       }
     }
     HouseByCluster()
   }
}

export const dispatchCctvQmap = (categoryId, cb) => {
  const dataConfig = {
    method: 'get',
    url: url + 'cctvs'
  }

  return dispatch => {
    const fetchingCctvQmap = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        cb(null, res.data.result, categoryId)
        dispatch(actionCctvQmap(res.data.result))
        cb(null, res.data.result)
      }
      catch(err){
        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){
            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try {
              const res = await axios(axiosConfig(dataConfig))
              cb(null, res.data.result, categoryId)
              dispatch(actionCctvQmap(res.data.result))
              cb(null, res.data.result)
            }
            catch(e){
              cb(e, null, null)
            }
         } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
     }
     fetchingCctvQmap()
   }
}

// *************************** Dispather /laporan ************************** //

export const dispatchCctvReport = cb =>{

  const dataConfig = {
    method: 'get',
    url: url + 'reports/cctvs'
  }

  return dispatch => {
    const cctvReport = async () => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        const getCctvReport = localStorage.getItem('cctvReport')
        if(!getCctvReport) {
          cb(null, res.data.result)
          dispatch(actionCctvReport(res.data.result))
        } else {
          const decode = atob(getCctvReport)
          const localStorageCctvReport = JSON.parse(decode)
          if(localStorageCctvReport.length === res.data.result.length) {
            cb(null, res.data.result)
          } else {
            const reportLength = res.data.result.length
            const currentReportLength = localStorageCctvReport.length
            if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan CCTV baru</span>`,'success', 2000 ,'bottom','right')
            else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan CCTV telah dihapus</span>`,'danger', 2000 ,'bottom','right')
            cb(null, res.data.result)
            dispatch(actionCctvReport(res.data.result))
          }
        }
      }
      catch(err){

        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){
            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try {
              const res = await axios(axiosConfig(dataConfig))
              const getCctvReport = localStorage.getItem('cctvReport')
              if(!getCctvReport) {
                cb(null, res.data.result)
                dispatch(actionCctvReport(res.data.result))
              } else {
                const decode = atob(getCctvReport)
                const localStorageCctvReport = JSON.parse(decode)
                if(localStorageCctvReport.length === res.data.result.length) {
                  cb(null, res.data.result)
                } else {
                  const reportLength = res.data.result.length
                  const currentReportLength = localStorageCctvReport.length
                  if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan CCTV baru</span>`,'success', 2000 ,'bottom','right')
                  else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan CCTV telah dihapus</span>`,'danger', 2000 ,'bottom','right')
                  cb(null, res.data.result)
                  dispatch(actionCctvReport(res.data.result))
                }
              }
            }
            catch(e){
              cb(e, null, null)
            }
         } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
    }
    cctvReport()
  }
}

export const dispatchServiceReport = (id, cb) => {
  return dispatch => {
    return axios({
      method: 'get',
      responseType: 'json',
      url: url + 'companies/' + id + '/all-service/search?categories=all'
    })
    .then(res => {
      const getServiceReport = localStorage.getItem('serviceReport')
      if(!getServiceReport) {
        cb(null, res.data.result)
        return dispatch(actionServiceReport(res.data.result))
      } else {
        const decode = atob(getServiceReport)
        const localStorageServiceReport = JSON.parse(decode)
        if(localStorageServiceReport.length === res.data.result.length) {
          cb(null, res.data.result)
        } else {
          cb(null, res.data.result)
          const reportLength = res.data.result.length
          const currentReportLength = localStorageServiceReport.length
          if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan jasa baru</span>`,'success', 2000 ,'bottom','right')
          else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan jasa telah dihapus</span>`,'danger', 2000 ,'bottom','right')
          return dispatch(actionServiceReport(res.data.result))
        }
      }
    })
    .catch(err => {
      cb(err, null)
    })
  }
}

export const dispatchComplaintReport = cb => {

  const dataConfig = {
    method: 'get',
    url: url + 'reports/complains',
  }

  return dispatch => {
    const complaintReport = async () =>{

      try{
        const res = await axios(axiosConfig(dataConfig))
        const getComplaintReport = localStorage.getItem('complaintReport')
        if(!getComplaintReport) {
          cb(null, res.data.result)
          return dispatch(actionComplaintReport(res.data.result))
        } else {
          const decode = atob(getComplaintReport)
          const localStorageComplaintReport = JSON.parse(decode)
          if(localStorageComplaintReport.length === res.data.result.length) {
            cb(null, res.data.result)
          } else {
            cb(null, res.data.result)
            const reportLength = res.data.result.length
            const currentReportLength = localStorageComplaintReport.length
            if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan keluhan baru</span>`,'success', 2000 ,'bottom','right')
            else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan keluhan telah dihapus</span>`,'danger', 2000 ,'bottom','right')
            return dispatch(actionComplaintReport(res.data.result))
          }
        }
      }
      catch(err){
        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){

            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try{
              const res = await axios(axiosConfig(dataConfig))
              const getComplaintReport = localStorage.getItem('complaintReport')
              if(!getComplaintReport) {
                cb(null, res.data.result)
                return dispatch(actionComplaintReport(res.data.result))
              } else {
                const decode = atob(getComplaintReport)
                const localStorageComplaintReport = JSON.parse(decode)
                if(localStorageComplaintReport.length === res.data.result.length) {
                  cb(null, res.data.result)
                } else {
                  cb(null, res.data.result)
                  const reportLength = res.data.result.length
                  const currentReportLength = localStorageComplaintReport.length
                  if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan keluhan baru</span>`,'success', 2000 ,'bottom','right')
                  else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan keluhan telah dihapus</span>`,'danger', 2000 ,'bottom','right')
                  return dispatch(actionComplaintReport(res.data.result))
                }
              }
            }
            catch(e){
              cb(e,null)
            }
          } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
    }
    complaintReport()
  }
}

export const dispatchOccupantReport = cb => {

  const dataConfig = {
    method: 'get',
    url: url + 'reports/residents',
  }

  return dispatch => {
    const occupantReport = async() => {

      try {
        const res = await axios(axiosConfig(dataConfig))
        const getOccupantReport = localStorage.getItem('occupantReport')
        if (!getOccupantReport) {
          cb(null, res.data.result)
          return dispatch(actionOccupantReport(res.data.result))
        } else {
          const decode = atob(getOccupantReport)
          const localStorageOccupantReport = JSON.parse(decode)
          if (localStorageOccupantReport.length === res.data.result.length) {
            cb(null, res.data.result)
          } else {
            cb(null, res.data.result)
            const reportLength = res.data.result.length
            const currentReportLength = localStorageOccupantReport.length
            if (reportLength > currentReportLength) popup.message('pe-7s-refresh', `<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan penghuni baru</span>`, 'success', 2000, 'bottom', 'right')
            else popup.message('pe-7s-refresh', `<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan penghuni telah dihapus</span>`, 'danger', 2000, 'bottom', 'right')
            return dispatch(actionOccupantReport(res.data.result))
          }
        }
      } catch (err) {
        if (err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa') {

          // mengubah token yg sudah expired dengan token baru
          const resRefresh = await axios(axiosConfigRefresh())
          localStorage.setItem('token', JSON.stringify(resRefresh.headers))
          try {
            const res = await axios(axiosConfig(dataConfig))
            const getOccupantReport = localStorage.getItem('occupantReport')
            if (!getOccupantReport) {
              const res = await axios(axiosConfig(dataConfig))
              cb(null, res.data.result)
              return dispatch(actionOccupantReport(res.data.result))
            } else {
              const decode = atob(getOccupantReport)
              const localStorageOccupantReport = JSON.parse(decode)
              if (localStorageOccupantReport.length === res.data.result.length) {
                cb(null, res.data.result)
              } else {
                cb(null, res.data.result)
                const reportLength = res.data.result.length
                const currentReportLength = localStorageOccupantReport.length
                if (reportLength > currentReportLength) popup.message('pe-7s-refresh', `<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan penghuni baru</span>`, 'success', 2000, 'bottom', 'right')
                else popup.message('pe-7s-refresh', `<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan penghuni telah dihapus</span>`, 'danger', 2000, 'bottom', 'right')
                return dispatch(actionOccupantReport(res.data.result))
              }
            }
          } catch (e) {
            cb(e, null)
          }
        } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
          localStorage.clear()
        } else console.log(err.response.data);
      }
    }
    occupantReport()
  }
}

export const dispatchManagerReport = cb => {

  const dataConfig = {
    method: 'get',
    url: url + 'reports/managers'
  }

  return dispatch => {
    const managerReport = async () => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        const getManagerReport = localStorage.getItem('managerReport')
        if(!getManagerReport) {
          cb(null, res.data.result)
          return dispatch(actionManagerReport(res.data.result))
        } else {
          const decode = atob(getManagerReport)
          const localStorageManagerReport = JSON.parse(decode)
          if(localStorageManagerReport.length === res.data.result.length) {
            cb(null, res.data.result)
          } else {
            cb(null, res.data.result)
            const reportLength = res.data.result.length
            const currentReportLength = localStorageManagerReport.length
            if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan pengelola baru</span>`,'success', 2000 ,'bottom','right')
            else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan pengelola telah dihapus</span>`,'danger', 2000 ,'bottom','right')
            return dispatch(actionManagerReport(res.data.result))
          }
        }
      }
      catch(err){
        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){

            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try{
              const res = await axios(axiosConfig(dataConfig))
              const getManagerReport = localStorage.getItem('managerReport')
              if(!getManagerReport) {
                cb(null, res.data.result)
                return dispatch(actionManagerReport(res.data.result))
              } else {
                const decode = atob(getManagerReport)
                const localStorageManagerReport = JSON.parse(decode)
                if(localStorageManagerReport.length === res.data.result.length) {
                  cb(null, res.data.result)
                } else {
                  cb(null, res.data.result)
                  const reportLength = res.data.result.length
                  const currentReportLength = localStorageManagerReport.length
                  if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan pengelola baru</span>`,'success', 2000 ,'bottom','right')
                  else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan pengelola telah dihapus</span>`,'danger', 2000 ,'bottom','right')
                  return dispatch(actionManagerReport(res.data.result))
                }
              }
            }
            catch(e){
              cb(e,null)
            }
          } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
    }
    managerReport()
  }
}

export const dispatchRoleReport = (cb, type) => {

  const dataConfig = {
   method: 'get',
   url: url + 'managers/features',
  }

  return dispatch => {

    const fetchingRoles = async () =>{
      try{
        const res = await axios(axiosConfig(dataConfig))
        dispatch(actionRoleReport(res.data.result))
        cb(null,res.data.result.role)
      }
      catch(err){
        const dataConfigOauth = {
          err,
          dataConfig,
          dispatch,
          action:actionRoleReport,
          cb
        }
        handleOauth2(dataConfigOauth)
       }
    }
    fetchingRoles()
  }
}

export const dispatchSosReport = (id, cb) => {
  return dispatch => {
    return axios({
      method: 'get',
      responseType: 'json',
      url: url + 'companies/' + id + '/all-sos'
    })
    .then(res => {
      const getSosReport = localStorage.getItem('sosReport')
      if(!getSosReport) {
        cb(null, res.data.result)
        return dispatch(actionSosReport(res.data.result))
      } else {
        const decode = atob(getSosReport)
        const localStorageSosReport = JSON.parse(decode)
        if(localStorageSosReport.length === res.data.result.length) {
          cb(null, res.data.result)
        } else {
          cb(null, res.data.result)
          const reportLength = res.data.result.length
          const currentReportLength = localStorageSosReport.length
          if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan SOS baru</span>`,'success', 2000 ,'bottom','right')
          else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan SOS telah dihapus</span>`,'danger', 2000 ,'bottom','right')
          return dispatch(actionSosReport(res.data.result))
        }
      }
    })
    .catch(err => {
      cb(err, null)
    })
  }
}

export const dispatchBillingReport = cb => {

  const dataConfig = {
    method:'get',
    url:url + 'invoices/report'
  }


  return dispatch => {
    const billingReport = async () => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        const getBillingReport = localStorage.getItem('billingReport')
        if(!getBillingReport) {
          cb(null, res.data.result)
          return dispatch(actionBillingReport(res.data.result))
        } else {
          const decode = atob(getBillingReport)
          const localStorageBillingReport = JSON.parse(decode)
          if(localStorageBillingReport.length === res.data.result.length) {
            cb(null, res.data.result)
          } else {
            cb(null, res.data.result)
            const reportLength = res.data.result.length
            const currentReportLength = localStorageBillingReport.length
            if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan tagihan baru</span>`,'success', 2000 ,'bottom','right')
            else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan tagihan telah dihapus</span>`,'danger', 2000 ,'bottom','right')
            return dispatch(actionBillingReport(res.data.result))
          }
        }
      }
      catch(err){
        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){

            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try {
              const res = await axios(axiosConfig(dataConfig))
              const getBillingReport = localStorage.getItem('billingReport')
              if(!getBillingReport) {
                cb(null, res.data.result)
                return dispatch(actionBillingReport(res.data.result))
              } else {
                const decode = atob(getBillingReport)
                const localStorageBillingReport = JSON.parse(decode)
                if(localStorageBillingReport.length === res.data.result.length) {
                  cb(null, res.data.result)
                } else {
                  cb(null, res.data.result)
                  const reportLength = res.data.result.length
                  const currentReportLength = localStorageBillingReport.length
                  if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan tagihan baru</span>`,'success', 2000 ,'bottom','right')
                  else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan tagihan telah dihapus</span>`,'danger', 2000 ,'bottom','right')
                  return dispatch(actionBillingReport(res.data.result))
                }
              }
            }
            catch(e){
              cb(e,null)
            }
          } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
    }
    billingReport()
  }
}

export const dispatchHouseReport = cb => {

  const dataConfig = {
    method:'get',
    url:url + 'reports/houses'
  }

  return dispatch => {
    const houseReport = async () => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        const getHouseReport = localStorage.getItem('houseReport')
        if(!getHouseReport) {
          cb(null, res.data.result)
          return dispatch(actionHouseReport(res.data.result))
        } else {
          const decode = atob(getHouseReport)
          const localStorageHouseReport = JSON.parse(decode)
          if(localStorageHouseReport.length === res.data.result.length) {
            cb(null, res.data.result)
          } else {
            cb(null, res.data.result)
            const reportLength = res.data.result.length
            const currentReportLength = localStorageHouseReport.length
            if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan rumah baru</span>`,'success', 2000 ,'bottom','right')
            else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan rumah telah dihapus</span>`,'danger', 2000 ,'bottom','right')
            return dispatch(actionHouseReport(res.data.result))
          }
        }
      }
      catch(err){
        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){

            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try {
              const res = await axios(axiosConfig(dataConfig))
              const getHouseReport = localStorage.getItem('houseReport')
              if(!getHouseReport) {
                cb(null, res.data.result)
                return dispatch(actionHouseReport(res.data.result))
              } else {
                const decode = atob(getHouseReport)
                const localStorageHouseReport = JSON.parse(decode)
                if(localStorageHouseReport.length === res.data.result.length) {
                  cb(null, res.data.result)
                } else {
                  cb(null, res.data.result)
                  const reportLength = res.data.result.length
                  const currentReportLength = localStorageHouseReport.length
                  if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan rumah baru</span>`,'success', 2000 ,'bottom','right')
                  else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan rumah telah dihapus</span>`,'danger', 2000 ,'bottom','right')
                  return dispatch(actionHouseReport(res.data.result))
                }
              }
            }
            catch(e){
              cb(e,null)
            }
          } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
    }
    houseReport()
  }
}

export const dispatchClusterReport = cb => {
  const dataConfig = {
    method: 'get',
    url: url + 'reports/clusters',
  }

  return dispatch => {
    const clusterReport = async () => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        const getClusterReport = localStorage.getItem('clusterReport')
        if(!getClusterReport) {
          cb(null, res.data.result)
          return dispatch(actionClusterReport(res.data.result))
        } else {
          const decode = atob(getClusterReport)
          const localStorageClusterReport = JSON.parse(decode)
          if(localStorageClusterReport.length === res.data.result.length) {
            cb(null, res.data.result)
          } else {
            cb(null, res.data.result)
            const reportLength = res.data.result.length
            const currentReportLength = localStorageClusterReport.length
            if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan cluster baru</span>`,'success', 2000 ,'bottom','right')
            else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan cluster telah dihapus</span>`,'danger', 2000 ,'bottom','right')
            return dispatch(actionClusterReport(res.data.result))
          }
        }
      }
      catch(err){
        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){

            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try {
              const res = await axios(axiosConfig(dataConfig))
              const getClusterReport = localStorage.getItem('clusterReport')
              if(!getClusterReport) {
                cb(null, res.data.result)
                return dispatch(actionClusterReport(res.data.result))
              } else {
                const decode = atob(getClusterReport)
                const localStorageClusterReport = JSON.parse(decode)
                if(localStorageClusterReport.length === res.data.result.length) {
                  cb(null, res.data.result)
                } else {
                  cb(null, res.data.result)
                  const reportLength = res.data.result.length
                  const currentReportLength = localStorageClusterReport.length
                  if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan cluster baru</span>`,'success', 2000 ,'bottom','right')
                  else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan cluster telah dihapus</span>`,'danger', 2000 ,'bottom','right')
                  return dispatch(actionClusterReport(res.data.result))
                }
              }
            }
            catch(e){
              cb(e,null)
            }
          } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
    }
    clusterReport()
  }
}

export const dispatchGpsReport = cb => {
  const dataConfig = {
    method: 'get',
    url: url + 'gps',
  }

  return dispatch => {
    alert('TES')
    const gpsReport = async () => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        const getGpsReport = localStorage.getItem('gpsReport')
        if(!getGpsReport) {
          cb(null, res.data.result)
          return dispatch(actionGpsReport(res.data.result))
        } else {
          const decode = atob(getGpsReport)
          const localStorageGpsReport = JSON.parse(decode)
          if(localStorageGpsReport.length === res.data.result.length) {
            cb(null, res.data.result)
          } else {
            cb(null, res.data.result)
            const reportLength = res.data.result.length
            const currentReportLength = localStorageGpsReport.length
            if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan GPS baru</span>`,'success', 2000 ,'bottom','right')
            else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan  GPS telah dihapus</span>`,'danger', 2000 ,'bottom','right')
            return dispatch(actionGpsReport(res.data.result))
          }
        }
      }
      catch(err){
        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){

            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try {
              const res = await axios(axiosConfig(dataConfig))
              const getGpsReport = localStorage.getItem('gpsReport')
              if(!getGpsReport) {
                cb(null, res.data.result)
                return dispatch(actionGpsReport(res.data.result))
              } else {
                const decode = atob(getGpsReport)
                const localStorageGpsReport = JSON.parse(decode)
                if(localStorageGpsReport.length === res.data.result.length) {
                  cb(null, res.data.result)
                } else {
                  cb(null, res.data.result)
                  const reportLength = res.data.result.length
                  const currentReportLength = localStorageGpsReport.length
                  if (reportLength > currentReportLength) popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">Terima ${reportLength - currentReportLength} laporan GPS baru</span>`,'success', 2000 ,'bottom','right')
                  else popup.message('pe-7s-refresh',`<span class="alert-popup-text-message">${currentReportLength - reportLength} laporan GPS telah dihapus</span>`,'danger', 2000 ,'bottom','right')
                  return dispatch(actionGpsReport(res.data.result))
                }
              }
            }
            catch(e){
              cb(e,null)
            }
          } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
    }
    gpsReport()
  }
}

export const dispatchBillingItems = (cb) => {

  const dataConfig = {
   method: 'get',
   responseType: 'json',
   url: url + 'invoices/item',
  }

  return dispatch => {

    const fetchItems = async() => {
      try {
        const res = await axios(axiosConfig(dataConfig))
        const initDetail = res.data.result.map(item => ({ name:item.name.toLowerCase(), charge:0, remaining:0 }))
        cb(null,res.data.result,initDetail)
      }
      catch(err){
        if(err.response.status === 401 && err.response.data.result.message === 'token kadaluarsa'){

            // mengubah token yg sudah expired dengan token baru
            const resRefresh = await axios(axiosConfigRefresh())
            localStorage.setItem('token', JSON.stringify(resRefresh.headers))
            try {
              const res = await axios(axiosConfig(dataConfig))
              const initDetail = res.data.result.map(item => ({ name:item.name.toLowerCase(), charge:0, remaining:0 }))
              cb(null,res.data.result,initDetail)
            }
            catch(e){
              cb(e,null,null)
            }
          } else if (err.response.status === 401 && err.response.data.result.message === 'token invalid') {
           localStorage.clear()
         } else console.log(err.response.data);
       }
    }
    fetchItems()
  }
}
