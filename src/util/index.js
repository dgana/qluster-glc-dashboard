module.exports = {
  capitalizeFirstLetter: str => {
    return str[0].toUpperCase() + str.slice(1);
  },
  limitString: (str, limit) => {
    return str.length >= limit ? str.substring(0,limit) + '...' : str.substring(0,limit)
  },
  limitString40: str => {
    return str.length >= 40 ? str.substring(0,40) + '...' : str.substring(0,40)
  },
  limitString30: str => {
    return str.length >= 30 ? str.substring(0,30) + '...' : str.substring(0,30)
  },
  standardAddress: str => {
    return str.length >= 20  ? str.substring(0,20 ) + '...' : str.substring(0,20)
  },
  shortAddress: str => {
    return str.length >= 10  ? str.substring(0,10 ) + '...' : str.substring(0,10)
  },
  jsUcfirst: str => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  },
  fullDate: date => {
    let fullDate = new Date(date)
    let month = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    return fullDate.getDate() + '/' + month[(fullDate.getMonth())] + '/' +  fullDate.getFullYear() + ' - ' +  fullDate.getHours() + ':' + fullDate.getMinutes()
  },
  completeDate: date => {
    let fullDate = new Date(date)
    let newMinutes = fullDate.getMinutes()
    if (String(fullDate.getMinutes()).length === 1) newMinutes = "0" + fullDate.getMinutes()
    let month = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    return fullDate.getDate() + ' ' + month[(fullDate.getMonth())] + ' ' +  fullDate.getFullYear() + ' - ' +  fullDate.getHours() + ':' + newMinutes
  },
  adminDate: date => {
    let fullDate = new Date(date)
    // let month = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    return fullDate.getMonth()+1 + '/' + fullDate.getDate() + '/' +  fullDate.getFullYear() + ', ' +  fullDate.getHours() + ':' + fullDate.getMinutes() + ' PM'
  },
  newsDate: date => {
    let newsDate = new Date(date)
    let month = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    return newsDate.getDate() + ' ' + month[(newsDate.getMonth())] + ' ' +  newsDate.getFullYear()
  },
  momentDate: date => {
    let momentDate = new Date(date)
    if (momentDate.getMonth()+1 >= 10){
      if (momentDate.getDate() < 10){
        return momentDate.getFullYear()+""+(momentDate.getMonth()+1)+"0"+momentDate.getDate()
      } else {
        return momentDate.getFullYear()+""+(momentDate.getMonth()+1)+""+momentDate.getDate()
      }
    } else {
      if (momentDate.getDate() < 10){
        return momentDate.getFullYear()+"0"+(momentDate.getMonth()+1)+"0"+momentDate.getDate()
      } else {
        return momentDate.getFullYear()+"0"+(momentDate.getMonth()+1)+""+momentDate.getDate()
      }
    }
  },

  getYear: date => {
    let momentDate = new Date(date)
    return momentDate.getFullYear()
  },

  getMonth: date => {
    let momentDate = new Date(date)
    return momentDate.getMonth()
  },

  monthYear: date => {
    let monthYear = new Date(date)
    let month = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
    return month[(monthYear.getMonth())] + ' ' +  monthYear.getFullYear().toString()
  }

}
