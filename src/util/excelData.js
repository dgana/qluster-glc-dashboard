export const billData = [
  {
    invoiceNumber:'A01',
    billing: 'Air: 200000, IPL: 0,Lain-lain:100000,Denda:0',
    sisa: 'Air: 200000, IPL: 0,Lain-lain:100000,Denda:0',
    downloadLink: 'http://qlue.co.id/site/',
    HouseId: 'j7hczdqx',
    dueDate: '06/21/2017',
  },
  {
    invoiceNumber:'A02',
    billing: 'Air: 200000, IPL: 0,Lain-lain:100000,Denda:0',
    sisa: 'Air: 100000, IPL: 0,Lain-lain:100000,Denda:0',
    downloadLink: 'http://qlue.co.id/site/',
    HouseId: 'j7hczdsn',
    dueDate: '06/21/2017',
  },
  {
    invoiceNumber:'A03',
    billing: 'Air: 200000, IPL: 0,Lain-lain:100000,Denda:0',
    sisa: 'Air: 50000, IPL: 0,Lain-lain:100000,Denda:0',
    downloadLink: 'http://qlue.co.id/site/',
    HouseId: 'j7hczdt9',
    dueDate: '06/08/2017',
  },
  {
    invoiceNumber:'A04',
    billing: 'Air: 200000, IPL: 150000,Lain-lain:100000,Denda:0',
    sisa: 'Air: 200000, IPL: 150000,Lain-lain:100000,Denda:0',
    downloadLink: 'http://qlue.co.id/site/',
    HouseId: 'j7hczduh',
    dueDate: '07/29/2017',
  },
  {
    invoiceNumber:'A05',
    billing: 'Air: 0, IPL: 180000,Lain-lain:100000,Denda:0',
    sisa: 'Air: 0, IPL: 180000,Lain-lain:100000,Denda:0',
    downloadLink: 'http://qlue.co.id/site/',
    HouseId: 'j7hczdv2',
    dueDate: '09/08/2017',
  },
]

export const occupantData = [
  {
    name: 'Gana',
    email: 'gana@tes.com',
    phone: '08129042724',
    code: '161pj5hn8fc2',
    status: 'master'
  },
  {
    name: 'Alvin',
    email: 'alvin@tes.com',
    phone: '08128313833',
    code: 'HOMEBSD123',
    status: 'family'
  },
  {
    name: 'Angga',
    email: 'angga@tes.com',
    phone: '08129329488',
    code: 'HOME123',
    status: 'family'
  },
  {
    name: 'Iqbal',
    email: 'iqbal@tes.com',
    phone: '08129328389',
    code: 'HOMEBSD123',
    status: 'family'
  },
  {
    name: 'Timothy',
    email: 'timothy@tes.com',
    phone: '08881297338',
    code: 'HOMEBSD123',
    status: 'master'
  },
]

export const managerData = [
  {
    name: 'Gana',
    email: 'gana@tes.com',
    phone: '08129042724',
    code: '161pj5hn8fc3',
    status: 'admin'
  },
  {
    name: 'Alvin',
    email: 'alvin@tes.com',
    phone: '08128313833',
    code: '161pj5hn8fc3',
    status: 'manager'
  },
  {
    name: 'Angga',
    email: 'angga@tes.com',
    phone: '08129329488',
    code: '161pj5hn8fc3',
    status: 'keamanan'
  },
  {
    name: 'Iqbal',
    email: 'iqbal@tes.com',
    phone: '08129328389',
    code: '161pj5hn8fc3',
    status: 'keamanan'
  },
  {
    name: 'Timothy',
    email: 'timothy@tes.com',
    phone: '08881297338',
    code: '161pj5hn8fc3',
    status: 'timber'
  },
]

export const addHouseData = [
  {
    ClusterId: '0c51j5c9bnan',
    HouseName: 'A-001',
    address: 'Jl. Pejaten Raya No. 1',
    latitude: '-6.29247365597285',
    longitude: '106.66541017592',
    handoverDate: '06/12/2017'
  },
  {
    ClusterId: '0c51j5c9bnan',
    HouseName: 'A-002',
    address: 'Jl. Pejaten Raya No. 2',
    latitude: '-6.29205008715804',
    longitude: '106.662120614201',
    handoverDate: '06/12/2017'
  },
  {
    ClusterId: '0c51j5c9bnan',
    HouseName: 'B-001',
    address: 'Jl. BSD No. 1',
    latitude: '-6.30316840411929',
    longitude: '106.652561724186',
    handoverDate: '06/12/2017'
  },
  {
    ClusterId: '0c51j5c9bnan',
    HouseName: 'B-002',
    address: 'Jl. BSD No. 2',
    latitude: '-6.30215265912322',
    longitude: '106.649797707796',
    handoverDate: '06/12/2017'
  },
  {
    ClusterId: '0c51j5c9bnan',
    HouseName: 'C-001',
    address: 'Jl. BSD No. 3',
    latitude: '-6.27325841452501',
    longitude: '106.821553297341',
    handoverDate: '06/12/2017'
  },
  {
    ClusterId: '0c51j5c9bnan',
    HouseName: 'D-001',
    address: 'Jl. BSD No. 4',
    latitude: '-6.27725841452501',
    longitude: '106.721553297341',
    handoverDate: '06/12/2017'
  },
  {
    ClusterId: '0c51j5c9bnan',
    HouseName: 'D-002',
    address: 'Jl. BSD No. 5',
    latitude: '-6.27325341452501',
    longitude: '106.891553297341',
    handoverDate: '06/12/2017'
  },
]
