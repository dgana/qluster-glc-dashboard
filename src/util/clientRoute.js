module.exports = [
  {
    name: "/dashboard",
    title: "Dashboard",
    path: "Dashboard",
  },
  {
    name: "/map",
    title: "Peta",
    path: "Peta",
  },
  {
    name: "/laporan-jasa",
    title: "Laporan Jasa",
    path: "Laporan / Jasa",
  },
  {
    name: "/laporan-keluhan",
    title: "Laporan Keluhan",
    path: "Laporan / Keluhan",
  },
  {
    name: "/laporan-sos",
    title: "Laporan SOS",
    path: "Laporan / SOS",
  },
  {
    name: "/laporan-tagihan",
    title: "Laporan Tagihan",
    path: "Laporan / Tagihan",
  },
  {
    name: "/laporan-penghuni",
    title: "Laporan Penghuni",
    path: "Laporan / Penghuni",
  },
  {
    name: "/laporan-pengelola",
    title: "Laporan Pengelola",
    path: "Laporan / Pengelola",
  },
  {
    name: "/laporan-admin",
    title: "Laporan Admin",
    path: "Laporan / Admin",
  },
  {
    name: "/laporan-peranan",
    title: "Laporan Peranan",
    path: "Laporan / Peranan",
  },
  {
    name: "/laporan-cctv",
    title: "Laporan CCTV",
    path: "Laporan / CCTV",
  },
  {
    name: "/laporan-rumah",
    title: "Laporan Rumah",
    path: "Laporan / Rumah",
  },
  {
    name: "/laporan-cluster",
    title: "Laporan Cluster",
    path: "Laporan / Cluster",
  },
  {
    name: "/laporan-township",
    title: "Laporan Township",
    path: "Laporan / Township",
  },
  {
    name: "/kirim-tagihan",
    title: "Kirim Tagihan",
    path: "Kirim Tagihan",
  },
  {
    name: "/news",
    title: "Berita",
    path: "Berita",
  },
  {
    name: "/daftar-aktivasi",
    title: "Daftar Aktivasi",
    path: "Pendaftaran / Qluster App / Aktivasi",
  },
  {
    name: "/daftar-pengelola",
    title: "Daftar Pengelola",
    path: "Pendaftaran / Qluster App / Pengelola",
  },
  {
    name: "/daftar-penghuni",
    title: "Daftar Penghuni",
    path: "Pendaftaran / Qluster App / Penghuni",
  },
  {
    name: "/daftar-admin",
    title: "Daftar Admin",
    path: "Pendaftaran / Dashboard / Admin",
  },
  {
    name: "/daftar-cctv",
    title: "Daftar CCTV",
    path: "Pendaftaran / Dashboard / CCTV",
  },
  {
    name: "/daftar-lokasi",
    title: "Daftar Lokasi",
    path: "Pendaftaran / Dashboard / Lokasi",
  },
  {
    name: "/daftar-peranan",
    title: "Daftar Peranan",
    path: "Pendaftaran / Dashboard / Peranan",
  },
  {
    name: "/manajemen-admin",
    title: "Manajemen Admin",
    path: "Manajemen / Admin",
  },
  {
    name: "/manajemen-cctv",
    title: "Manajemen CCTV",
    path: "Manajemen / CCTV",
  },
  {
    name: "/manajemen-lokasi",
    title: "Manajemen Lokasi",
    path: "Manajemen / Lokasi",
  },
  {
    name: "/manajemen-peranan",
    title: "Manajemen Peranan",
    path: "Manajemen / Peranan",
  }
]
