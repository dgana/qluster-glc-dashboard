import React from 'react'

// Images
import redCircle from '../../../public/imgs/icon/red_circle.svg'
import greenCircle from '../../../public/imgs/icon/green_circle.png'

module.exports = [
  {
    Header: "No.",
    columns: [
      {
        Header: "No.",
        accessor: "number",
        width: 65,
        sortMethod: (a, b) => {
          if (a === b) return 0
          a = Number(a)
          b = Number(b)
          return a > b ? 1 : -1;
        }
      }
    ]
  },
  {
    Header: "Status",
    columns: [
      {
        Header: "Status",
        accessor: "isActive",
        width: 135,
        Cell: ({ value }) => (
          value ? <img style={{width: 30}} src={greenCircle} alt="Aktif" /> :
          <img style={{width: 38, paddingLeft: 5}} src={redCircle} alt="Non-Aktif" />
        ),
        filterMethod: (filter, row) => {
          if (filter.value === "all") return true
          if (filter.value === "true") return row[filter.id] === true
          if (filter.value === "false") return row[filter.id] === false
        },
        Filter: ({ filter, onChange }) =>
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%" }}
            value={filter ? filter.value : "all"}>
            <option value="all">Semua</option>
            <option value="true">Aktif</option>
            <option value="false">Non-Aktif</option>
          </select>
      }
    ]
  },
  {
    Header: "Nama",
    columns: [
      {
        Header: "Nama",
        accessor: "name",
        filterMethod: (filter, row) => {
          console.log(filter, row, filter.value);
          return row[filter.id].toLowerCase().includes(filter.value)
        }
      }
    ]
  },
  {
    Header: "Email",
    columns: [
      {
        Header: "Email",
        accessor: "email",
        filterMethod: (filter, row) => {
          console.log(filter, row, filter.value);
          return row[filter.id].toLowerCase().includes(filter.value)
        }
      }
    ]
  },
  {
    Header: "No. Telpon",
    columns: [
      {
        Header: "No. Telpon",
        accessor: "primaryPhone"
      }
    ]
  },
  {
    Header: "Rumah",
    columns: [
      {
        Header: "Rumah",
        accessor: "houseName",
        width: 100
      }
    ]
  },
  {
    Header: "Tanggal Gabung",
    columns: [
      {
        Header: "Tanggal Gabung",
        accessor: "createdAt",
        filterMethod: (filter, row) => {
          console.log(filter, row, filter.value);
          return row[filter.id].toLowerCase().includes(filter.value)
        }
      }
    ]
  }
]
