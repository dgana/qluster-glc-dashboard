import React from 'react'

module.exports = [
  {
    Header: "No.",
    columns: [
      {
        Header: "No.",
        accessor: "number",
        width: 65,
        sortMethod: (a, b) => {
          if (a === b) return 0
          a = Number(a)
          b = Number(b)
          return a > b ? 1 : -1;
        }
      }
    ]
  },
  {
    Header: "ID",
    columns: [
      {
        Header: "ID",
        accessor: "id",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Nama",
    columns: [
      {
        Header: "Nama",
        accessor: "name",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Alamat",
    columns: [
      {
        Header: "Alamat",
        accessor: "address",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Jumlah Rumah",
    columns: [
      {
        Header: "Jumlah Rumah",
        accessor: "numberOfHouses"
      }
    ]
  },
  {
    Header: "Tanggal Dibuat",
    columns: [
      {
        Header: "Tanggal Dibuat",
        accessor: "createdAt",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  }
]
