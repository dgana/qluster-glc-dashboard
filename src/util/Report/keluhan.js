import React from 'react'

// Images
import redCircle from '../../../public/imgs/icon/red_circle.svg'
import greenCircle from '../../../public/imgs/icon/green_circle.png'
import yellowCircle from '../../../public/imgs/icon/yellow_icon.png'
import invalidCircle from '../../../public/imgs/icon/attention-2180765_640.png'

module.exports = [
  {
    Header: "No.",
    columns: [
      {
        Header: "No.",
        accessor: "number",
        width: 65,
        sortMethod: (a, b) => {
          if (a === b) return 0
          a = Number(a)
          b = Number(b)
          return a > b ? 1 : -1;
        }
      }
    ]
  },
  {
    Header: "Status",
    columns: [
      {
        Header: "Status",
        accessor: "status",
        width: 125,
        Cell: ({ value }) => (
          value === 'done' ? <img style={{width: 30}} src={greenCircle} alt="done" /> :
          value === 'waiting' ? <img style={{width: 38, paddingLeft: 5}} src={redCircle} alt="waiting" /> :
          value === 'invalid' ? <img style={{width: 26}} src={invalidCircle} alt="invalid" /> :
          <img style={{width: 30}} src={yellowCircle} alt="progress" />
        ),
        filterMethod: (filter, row) => {
          if (filter.value === "all") return true
          if (filter.value === "waiting") return row[filter.id] === "waiting"
          if (filter.value === "progress") return row[filter.id] === "progress"
          if (filter.value === "done") return row[filter.id] === "done"
          if (filter.value === "invalid") return row[filter.id] === "invalid"
        },
        Filter: ({ filter, onChange }) =>
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%" }}
            value={filter ? filter.value : "all"}>
            <option value="all">Semua</option>
            <option value="waiting">Menunggu</option>
            <option value="progress">Proses</option>
            <option value="done">Selesai</option>
            <option value="invalid">Invalid</option>
          </select>
      }
    ]
  },
  {
    Header: "Keluhan",
    columns: [
      {
        Header: "Keluhan",
        accessor: "title",
        width: 250,
        filterMethod: (filter, row) => {
          console.log(filter, row, filter.value);
          return row[filter.id].toLowerCase().includes(filter.value)
        }
      }
    ]
  },
  {
    Header: "Rumah",
    columns: [
      {
        Header: "Rumah",
        accessor: "houseName"
      }
    ]
  },
  {
    Header: "Tanggal",
    columns: [
      {
        Header: "Tanggal",
        accessor: "createdAt",
        filterMethod: (filter, row) => {
          console.log(filter, row, filter.value);
          return row[filter.id].toLowerCase().includes(filter.value)
        }
      }
    ]
  },
  {
    Header: "Expand",
    columns: [
      {
        expander: true,
        Header: () => (<strong>Detail</strong>),
        width: 65,
        Expander: ({ isExpanded, ...rest }) => (
          <div>
            { isExpanded ? <span>&#x2299;</span> : <span>&#x2295;</span> }
          </div>
        ),
        style: {
          cursor: "pointer",
          fontSize: 24,
          padding: "0",
          textAlign: "center",
          userSelect: "none"
        }
      }
    ]
  }
]
