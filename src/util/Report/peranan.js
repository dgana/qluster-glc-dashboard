import React from 'react'

module.exports = [
  {
    Header: "No.",
    columns: [
      {
        Header: "No.",
        accessor: "number",
        width: 65,
        sortMethod: (a, b) => {
          if (a === b) return 0
          a = Number(a)
          b = Number(b)
          return a > b ? 1 : -1;
        }
      }
    ]
  },
  {
    Header: "ID",
    columns: [
      {
        Header: "ID",
        accessor: "id",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Nama Peran",
    columns: [
      {
        Header: "Nama Peran",
        accessor: "role",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Tanggal Perubahan Terakhir",
    columns: [
      {
        Header: "Tanggal Perubahan Terakhir",
        accessor: "updatedAt",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Expand",
    columns: [
      {
        expander: true,
        Header: () => (<strong>Hak Akses</strong>),
        width: 100,
        Expander: ({ isExpanded, ...rest }) => (
          <div>
            { isExpanded ? <span>&#x2299;</span> : <span>&#x2295;</span> }
          </div>
        ),
        style: {
          cursor: "pointer",
          fontSize: 24,
          padding: "0",
          textAlign: "center",
          userSelect: "none"
        }
      }
    ]
  }
]
