import React from 'react'

// Images
import redCircle from '../../../public/imgs/icon/red_circle.svg'
import greenCircle from '../../../public/imgs/icon/green_circle.png'

module.exports = [
  {
    Header: "No.",
    columns: [
      {
        Header: "No.",
        accessor: "number",
        width: 65,
        sortMethod: (a, b) => {
          if (a === b) return 0
          a = Number(a)
          b = Number(b)
          return a > b ? 1 : -1;
        }
      }
    ]
  },
  {
    Header: "Status",
    columns: [
      {
        Header: "Status",
        accessor: "status",
        width: 135,
        Cell: ({ value }) => (
          value === 'paid' ? <img style={{width: 30}} src={greenCircle} alt="paid" /> :
          <img style={{width: 38, paddingLeft: 5}} src={redCircle} alt="unpaid" />
        ),
        filterMethod: (filter, row) => {
          if (filter.value === "all") return true
          if (filter.value === "unpaid") return row[filter.id] === "unpaid"
          if (filter.value === "paid") return row[filter.id] === "paid"
        },
        Filter: ({ filter, onChange }) =>
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%" }}
            value={filter ? filter.value : "all"}>
            <option value="all">Semua</option>
            <option value="unpaid">Belum Bayar</option>
            <option value="paid">Sudah Bayar</option>
          </select>
      }
    ]
  },
  {
    Header: "No. Invoice",
    columns: [
      {
        Header: "No. Invoice",
        accessor: "invoiceNumber",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Penghuni",
    columns: [
      {
        Header: "Penghuni",
        accessor: "penghuni",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Cluster",
    columns: [
      {
        Header: "Cluster",
        accessor: "cluster",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Nominal",
    columns: [
      {
        Header: "Nominal",
        accessor: "nominal",
        sortMethod: (a, b) => {
          if (a === b) return 0
          a = Number(a.split(" ")[1].slice(0,-2).split(',').join(''))
          b = Number(b.split(" ")[1].slice(0,-2).split(',').join(''))
          return a > b ? 1 : -1;
        },
        filterMethod: (filter, row) => {
          return String(row[filter.id].split(" ")[1].slice(0,-2).split(',').join('')).includes(filter.value)
        }
      }
    ]
  },
  {
    Header: "Periode",
    columns: [
      {
        Header: "Periode",
        accessor: "periode",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  }
]
