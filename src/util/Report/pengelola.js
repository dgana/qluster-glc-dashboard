import React from 'react'

// Images
import redCircle from '../../../public/imgs/icon/red_circle.svg'
import greenCircle from '../../../public/imgs/icon/green_circle.png'

module.exports = [
  {
    Header: "No.",
    columns: [
      {
        Header: "No.",
        accessor: "number",
        width: 65,
        sortMethod: (a, b) => {
          if (a === b) return 0
          a = Number(a)
          b = Number(b)
          return a > b ? 1 : -1;
        }
      }
    ]
  },
  {
    Header: "Status",
    columns: [
      {
        Header: "Status",
        accessor: "inActive",
        width: 135,
        Cell: ({ value }) => (
          value ? <img style={{width: 30}} src={greenCircle} alt="Aktif" /> :
          <img style={{width: 38, paddingLeft: 5}} src={redCircle} alt="Non-Aktif" />
        ),
        filterMethod: (filter, row) => {
          if (filter.value === "all") return true
          if (filter.value === "aktif") return row[filter.id] === true
          if (filter.value === "nonAktif") return row[filter.id] === false
        },
        Filter: ({ filter, onChange }) =>
          <select
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%" }}
            value={filter ? filter.value : "all"}>
            <option value="all">Semua</option>
            <option value="aktif">Aktif</option>
            <option value="nonAktif">Non-Aktif</option>
          </select>
      }
    ]
  },
  {
    Header: "Nama",
    columns: [
      {
        Header: "Nama",
        accessor: "name",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Email",
    columns: [
      {
        Header: "Email",
        accessor: "email",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "No. Telpon",
    columns: [
      {
        Header: "No. Telpon",
        accessor: "primaryPhone",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Divisi",
    columns: [
      {
        Header: "Divisi",
        accessor: "division",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  },
  {
    Header: "Tanggal Gabung",
    columns: [
      {
        Header: "Tanggal Gabung",
        accessor: "createdAt",
        filterMethod: (filter, row) => row[filter.id].toLowerCase().includes(filter.value)
      }
    ]
  }
]
