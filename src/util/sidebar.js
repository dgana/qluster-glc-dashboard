module.exports = {
  defaultState: {
    active: [0],
    title: ""
  },
  dashboard: {
    active: [1],
    title: 'Dashboard'
  },
  maps: {
    active: [0,1],
    title: "Peta"
  },
  report: {
    active: [0,0,1],
    title: ""
  },
  reportQlusterApp: {
    active: [0,0,1,1],
    title: ""
  },
  // reportService: {
  //   active: [0,0,1,1,1],
  //   title: "Jasa"
  // },
  reportComplaint: {
    active: [0,0,1,1,1],
    title: "Keluhan"
  },
  // reportSos: {
  //   active: [0,0,1,1,0,0,1],
  //   title: "SOS"
  // },
  reportBilling: {
    active: [0,0,1,1,0,1],
    title: "Tagihan"
  },
  reportOccupant: {
    active: [0,0,1,1,0,0,1],
    title: "Penghuni"
  },
  reportManager: {
    active: [0,0,1,1,0,0,0,1],
    title: "Pengelola"
  },
  reportDashboard: {
    active: [0,0,1,0,0,0,0,0,1],
    title: ""
  },
  // reportAdmin: {
  //   active: [0,0,1,0,0,0,0,0,0,0,1,1],
  //   title: "Admin"
  // },
  reportRole: {
    active: [0,0,1,0,0,0,0,0,1,1],
    title: "Peranan"
  },
  reportCctv: {
    active: [0,0,1,0,0,0,0,0,1,0,1],
    title: "CCTV"
  },
  reportCctv: {
    active: [0,0,1,0,0,0,0,0,1,0,1],
    title: "CCTV"
  },
  reportGps: {
    active: [0,0,1,0,0,0,0,0,1,0,0,1],
    title: "CCTV"
  },
  reportHouse: {
    active: [0,0,1,0,0,0,0,0,1,0,0,0,1],
    title: "Rumah"
  },
  reportCluster: {
    active: [0,0,1,0,0,0,0,0,1,0,0,0,0,1],
    title: "Cluster"
  },
  invoice: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
    title: "Kirim Tagihan"
  },
  news: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
    title: "Berita"
  },
  register: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
    title: ""
  },
  registerQlusterApp: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
    title: ""
  },
  registerManager: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1],
    title: "Pengelola"
  },
  registerOccupant: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1],
    title: "Penghuni"
  },
  registerDashboard: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1],
    title: ""
  },
  registerCctv: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,1],
    title: "Pendaftaran"
  },
  registerLocation: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,1],
    title: "Pendaftaran"
  },
  registerRole: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,1],
    title: "Pendaftaran"
  },
  management: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
    title: "Manajemen"
  },
  managementAdmin: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
    title: "Manajemen"
  },
  managementCctv: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1],
    title: "Manajemen"
  },
  managementLocation: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1],
    title: "Manajemen"
  },
  managementRole: {
    active: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1],
    title: "Manajemen"
  },
  profile: {
    active: [0],
    title: "Profil"
  },
  changePassword: {
    active: [0],
    title: "Ubah Password"
  },
  notification: {
    active: [0],
    title: "Notifikasi"
  }
}
