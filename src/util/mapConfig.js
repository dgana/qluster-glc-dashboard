module.exports = {
  sereniaHills: {
    latitude: -6.311800823272236,
    longitude: 106.7748785018921,
    zoom: 15
  },
  greenLakeCity: {
    latitude: -6.187488737210719,
    longitude: 106.70282363891602,
    zoom: 14
  }
}
