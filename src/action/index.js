export const actionResetFeatures = () => {
  return {
    type: 'RESET_FEATURES'
  }
}

export const actionGetFeatures = (result) => {
  return {
    type: 'FEATURES',
    payload:result.features
  }
}

export const actionResetRoleOption = () => {
  return {
    type: 'RESET_ROLE_OPTION'
  }
}

export const actionResetManagerCode = () => {
  return {
    type: 'RESET_MANAGER_CODE'
  }
}

export const actionResetOccupantCode = () => {
  return {
    type: 'RESET_OCCUPANT_CODE'
  }
}

export const actionEditProfile = (user) => {
  return {
    type: 'EDIT_PROFILE',
    payload: user
  }
}

export const actionLogoutClient = (cb) => {
  return {
    type: 'CLIENT_LOGOUT',
    payload: cb
  }
}

export const actionClientLogin = (token) => {
  return {
    type: 'SET_CURRENT_USER',
    payload: token
  }
}

export const actionSidebarToggle = () => {
  return {
    type: 'SIDEBAR_TOGGLE'
  }
}

export const actionOccupantCode = (occupant) => {
  return {
    type: 'OCCUPANT_CODE',
    payload: occupant
  }
}

export const actionManagerCode = (manager) => {
  return {
    type: 'MANAGER_CODE',
    payload: manager
  }
}

export const actionAddRole = role => {
  return {
    type: 'ADD_USER_ROLE',
    payload: role
  }
}
