export const actionAllClusterList = cluster => {
  return {
    type: 'CLUSTER_LIST',
    payload: cluster
  }
}

export const actionAddClusterList = cluster => {
  return {
    type: 'ADD_CLUSTER_LIST',
    payload: cluster
  }
}


export const actionResetClusterList = () => {
  return {
    type: 'RESET_CLUSTER_LIST'
  }
}

export const actionDeleteCluster = cluster => {
  return {
    type: 'DELETE_CLUSTER_LIST',
    payload: cluster.data
  }
}

export const actionEditCluster = cluster => {
  return {
    type: 'EDIT_CLUSTER_LIST',
    payload: cluster.data
  }
}
