export const actionCctvReport = report => {
  return {
    type: 'CCTV_REPORT',
    payload: report
  }
}

export const actionServiceReport = report => {
  return {
    type: 'SERVICE_REPORT',
    payload: report
  }
}

export const actionComplaintReport = report => {
  return {
    type: 'COMPLAINT_REPORT',
    payload: report
  }
}

export const actionOccupantReport = report => {
  return {
    type: 'OCCUPANT_REPORT',
    payload: report
  }
}

export const actionManagerReport = report => {
  return {
    type: 'MANAGER_REPORT',
    payload: report
  }
}

export const actionRoleReport = report => {
  return {
    type: 'ROLE_REPORT',
    payload: report.role
  }
}

export const actionSosReport = report => {
  return {
    type: 'SOS_REPORT',
    payload: report
  }
}

export const actionBillingReport = report => {
  return {
    type: 'BILLING_REPORT',
    payload: report
  }
}

export const actionHouseReport = report => {
  return {
    type: 'HOUSE_REPORT',
    payload: report
  }
}

export const actionClusterReport = report => {
  return {
    type: 'CLUSTER_REPORT',
    payload: report
  }
}

export const actionTownshipReport = report => {
  return {
    type: 'TOWNSHIP_REPORT',
    payload: report
  }
}

export const actionResetHouseReport = () => {
  return {
    type: 'RESET_HOUSE_REPORT',
  }
}

export const actionGpsReport = report => {
  return {
    type: 'GPS_REPORT',
    payload: report
  }
}
