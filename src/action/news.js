export const actionFetchNews = (news) => {
  return {
    type: 'FETCH_NEWS',
    payload: news
  }
}

export const actionAddNews = (news) => {
  return {
    type: 'ADD_NEWS',
    payload: news
  }
}

export const actionDeleteNews = (newsId) => {
  return {
    type: 'DELETE_NEWS',
    payload: newsId
  }
}

export const actionAddNewsComment = (news, index) => {
  return {
    type: 'ADD_NEWS_COMMENT',
    payload: news,
    index: index
  }
}

export const actionResetNews = () => {
  return {
    type: 'RESET_NEWS'
  }
}
