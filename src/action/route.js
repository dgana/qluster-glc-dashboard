export const actionClientRoute = (nav) => {
  return {
    type: 'CLIENT_PATH',
    payload: nav
  }
}

export const actionAdminRoute = (nav) => {
  return {
    type: 'ADMIN_PATH',
    payload: nav
  }
}
