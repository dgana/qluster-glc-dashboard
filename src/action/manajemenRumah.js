export const actionSearchHouseByIdAndName = house => {
  return {
    type: 'HOUSE_LIST_MANAGEMENT',
    payload: house
  }
}

export const actionResetHouseByIdAndName = () => {
  return {
    type: 'RESET_HOUSE_LIST_MANAGEMENT'
  }
}

export const actionEditHouseByIdAndName = house => {
  return {
    type: 'EDIT_HOUSE_LIST_MANAGEMENT',
    payload: house.data
  }
}

export const actionDeleteHouseByIdAndName = house => {
  return {
    type: 'DELETE_HOUSE_LIST_MANAGEMENT',
    payload: house
  }
}

export const actionCurrentHouseSearch = search => {
  return {
    type: 'CURRENT_HOUSE_SEARCH',
    payload: search
  }
}
