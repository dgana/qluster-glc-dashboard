export const actionEditRole = role => {
  return {
    type: 'EDIT_ROLE',
    payload: role
  }
}

export const actionRoleList = result => {
  return {
    type: 'ROLE_LIST',
    payload: result.role
  }
}
