export const actionCctvQmap = (cctv) => {
  return {
    type: 'CCTV_LIST',
    payload: cctv
  }
}

export const actionResetCctvQmap = () => {
  return {
    type: 'RESET_CCTV_LIST'
  }
}

export const actionRegisterCctv = cctv => {
  return {
    type: 'ADD_CCTV_LIST',
    payload: cctv
  }
}

export const actionEditCctv = cctv => {
  return {
    type: 'EDIT_CCTV_LIST',
    payload: cctv.data
  }
}

export const actionDeleteCctv = cctv => {
  return {
    type: 'DELETE_CCTV_LIST',
    payload: cctv
  }
}
