export const actionAllHouseList = house => {
  return {
    type: 'HOUSE_LIST',
    payload: house
  }
}

export const actionAddHouseList = newList => {
  return {
    type: 'ADD_HOUSE_LIST',
    payload: newList
  }
}

export const actionResetHouse = () => {
  return {
    type: 'RESET_HOUSE_LIST'
  }
}

export const actionDeleteHouse = house => {
  return {
    type: 'DELETE_HOUSE_LIST',
    payload: house
  }
}

export const actionEditHouse = house => {
  return {
    type: 'EDIT_HOUSE_LIST',
    payload: house
  }
}
