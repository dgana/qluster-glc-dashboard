export const actionHouseByCluster = house => {
  return {
    type: 'FILTER_HOUSE_LIST',
    payload: house
  }
}

export const actionRemoveHouseByCluster = house => {
  return {
    type: 'REMOVE_FILTER_HOUSE_LIST',
    payload: house
  }
}

export const actionLoadHouseByCluster = () => {
  return {
    type: 'LOAD_FILTER_HOUSE_LIST'
  }
}

export const actionResetHouseByCluster = () => {
  return {
    type: 'RESET_FILTER_HOUSE_LIST'
  }
}

export const actionHouseByClusterSimple = house => {
  return {
    type: 'SIMPLE_FILTER_HOUSE_LIST',
    payload: house
  }
}
