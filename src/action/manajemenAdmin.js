export const actionAdminManagementList = admin => {
  return {
    type: 'ADMIN_MANAGEMENT_LIST',
    payload: admin
  }
}

export const actionEditUserDashboard = admin => {
  return {
    type: 'EDIT_ADMIN_MANAGEMENT',
    payload: admin
  }
}
