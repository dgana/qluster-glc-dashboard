export const actionTabPanelRumah = (lokasi) => {
  return {
    type: 'TAB_PANEL_RUMAH',
    payload: lokasi
  }
}

export const actionTabPanelCluster = (lokasi) => {
  return {
    type: 'TAB_PANEL_CLUSTER',
    payload: lokasi
  }
}

export const actionTabPanelTownship = (lokasi) => {
  return {
    type: 'TAB_PANEL_TOWNSHIP',
    payload: lokasi
  }
}
