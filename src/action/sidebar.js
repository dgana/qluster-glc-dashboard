export const actionSidebarNavigation = (nav) => {
  return {
    type: 'SIDEBAR_NAVIGATION',
    payload: nav
  }
}

export const actionSidebarActive = (active) => {
  return {
    type: 'SIDEBAR_ACTIVE',
    payload: active
  }
}
