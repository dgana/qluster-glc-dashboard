export const actionHideMapMenu = () => {
  return {
    type: 'MAP_MENU_DISPLAY',
  }
}

export const actionShowMapMenu = () => {
  return {
    type: 'MAP_MENU_SHOW',
  }
}
