export const actionShowSummary = summary => {
  return {
    type: 'GET_SUMMARY',
    payload: summary
  }
}
