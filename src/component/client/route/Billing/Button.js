// React, React-Router, PropTypes & Redux Dependencies
import React from 'react';
import PropTypes from 'prop-types'

const Button = props => (
  <div className="row" style={{marginTop:15}}>
    <div className="row">
      <div className="col-xs-12">
        <button className="btn btn-default btn-sm" style={{margin: '0px 12px'}} onClick={props._onClickAddRow}>
          <span className="fa fa-plus" style={{marginRight: 8}}></span>
          Baris Baru
        </button>
        {props._Confirm}
        {props._onSubmitForm}
      </div>
    </div>
  </div>
)

Button.propTypes = {
  _onClickAddRow: PropTypes.func.isRequired,
  _onSubmitForm: PropTypes.node.isRequired,
  _Confirm: PropTypes.node.isRequired
}

export default Button
