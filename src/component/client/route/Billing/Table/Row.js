import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

// Plugin Dependencies
import Select from 'react-virtualized-select'
import { SingleDatePicker } from 'react-dates'
import NumberFormat from 'react-number-format'
import moment from 'moment'
import ReactTooltip from 'react-tooltip'

// Icon Rupiah
import rp from '../../../../../../public/imgs/icon/rp.png'

const selectStyle = {
  textAlign:'left',
  cursor:'pointer'
}

const currencyStyle = {
  height: 32,
  paddingLeft: 40,
  width: '100%',
  fontSize: 13  ,
  border: '1px solid rgb(220,220,220)',
  color: '#545454',
  opacity: 1,
  boxShadow: '0 2px 6px 0 rgba(0,0,0,0.1)',
  outline: 'inherit'
}

class Row extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mouseEnter: false,
      width: 0,
      height: 0
    }
  }

  static propTypes = {
    tagihan: PropTypes.object,
    billingItems: PropTypes.array,
    index: PropTypes.number,
    _copyDetail: PropTypes.func,
    _onChange: PropTypes.func,
    _onDeleteRow: PropTypes.func
  }


  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions = () => {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  componentDidMount() {
    moment.locale("id") // Ubah tanggal menjadi bahasa indonesia
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  _showCopydetailDetail = (id, index) => {
    return (
      <div style={{position:'absolute', top:5, left: 26}}>
        <ReactTooltip id={"copyDetail-" + id} place={"top"} />
        <i
          data-for={"copyDetail-" + id}
          data-tip={"Salin Detail Tagihan No. " + Number(index)}
          data-iscapture="true"
          onClick={() => this.props._copyDetail(id)}
          className="fa fa-clone fa-1-7x report-table-icon">
        </i>
      </div>
    )
  }

  _renderBillingColumn = (tagihan, name, type, billingItems, _onChange) => {
    return billingItems.map((item,i) =>
     tagihan.detail[i] ?
        <div className="row" key={i}>
          { type === 'charge' ?
            <div className="col-xs-12"  style={{lineHeight:2,fontSize:10, marginBottom: 4}}>
              <img src={rp} alt="icon rupiah" style={{width:35, position:'absolute', marginLeft:5}}></img>
              <NumberFormat
                maxLength={11}
                style={currencyStyle}
                value={tagihan.detail[i].charge === 0 ? "" : tagihan.detail[i].charge||0}
                thousandSeparator={true}
                placeholder={item.name === 'Lain-lain' ? 'Lain2' : item.name}
                onChange={(e, value) => _onChange({name: "detail",type:'charge', id: tagihan.id, e:e, index:i },  value)} />
            </div> : null
          }
        </div> : null
    )
  }

  _renderRemainingColumn = (index,tagihan,name,type,billingItems,_onChange) => {
    const { mouseEnter, width } = this.state
    return billingItems.map( (item,i) =>
       tagihan.detail[i] ?
        <div className="row" style={{marginBottom: 4}} key={i}>
        { i===0?
            <div className="col-xs-12" onMouseEnter={() => this.setState({ mouseEnter: true })} onMouseLeave={() => this.setState({ mouseEnter: false })}>
            { mouseEnter ? this._showCopydetailDetail(tagihan.id, index + 1) :
              <img src={rp} alt="icon rupiah" style={{width:35, position:'absolute', marginLeft:5}}></img>
            }
              <NumberFormat
                maxLength={11}
                style={currencyStyle}
                value={tagihan.detail[i].remaining === 0 ? "" : tagihan.detail[i].remaining||0}
                thousandSeparator={true}
                placeholder={width < 1248 ? 'Sisa pembayaran '+item.name : item.name}
                onChange={(e, value) => _onChange({ detailName: 'listrik', name: "detail", type:'remaining', id: tagihan.id, e:e, index:i },  value)} />
            </div>
          :
           <div className="col-xs-12">
              <img src={rp} alt="icon rupiah" style={{width:35, position:'absolute', marginLeft:5}}></img>
              <NumberFormat
                maxLength={11}
                style={currencyStyle}
                value={tagihan.detail[i].remaining === 0 ? "" : tagihan.detail[i].remaining||0}
                thousandSeparator={true}
                placeholder={width < 1248 ? 'Sisa pembayaran '+item.name : item.name}
                onChange={(e, value) => _onChange({ detailName: 'listrik', name: "detail", type:'remaining', id: tagihan.id, e:e, index:i },  value)} />
            </div>
        }
        </div> : null
    )
  }

  render() {
    const { tagihan, _onChange, _onDeleteRow, index, houseCode, billingItems } = this.props

    return (
      <tr className="billingTable rowHeight">
        <td style={{textAlign:'center'}}>{index + 1}</td>
        <td className="nomorNota" >
          <input
            style={{width: '100%'}}
            className="form-control"
            placeholder="No. Nota"
            value={tagihan.invoiceNumber}
            onChange={(e) => _onChange({ id: tagihan.id, name: 'invoiceNumber' }, e.target.value)}
          />
        </td>
        <td className="pembayaran">
          { this._renderBillingColumn(tagihan, 'detail', 'charge', billingItems, _onChange)
          }
        </td>
        <td className="sisaPembayaran">
          { this._renderRemainingColumn(index,tagihan,'detail','remaining',billingItems,_onChange)
          }
        </td>
        <td className="downloadLink">
          <input
            className="form-control"
            placeholder="Link Unduh"
            style={{width: '100%'}}
            value={tagihan.downloadLink}
            onChange={(e) => _onChange({ id: tagihan.id, name: 'downloadLink' }, e.target.value)}
          />
        </td>
        <td className="kodeRumahTagihan">
          <Select
            name="code"
            style={selectStyle}
            value={tagihan.HouseId}
            placeholder={tagihan.placeholder === undefined ? "Kode..." : tagihan.placeholder}
            options={houseCode.result}
            clearable={false}
            onChange={(e) => _onChange({ id: tagihan.id, name: 'HouseId' }, e.value)} />
        </td>
        <td>
          <SingleDatePicker
            date={tagihan.dueDate ? moment(tagihan.dueDate) : tagihan.dueDate }
            isOutsideRange={() => false}
            onDateChange={date => _onChange({ id: tagihan.id, name: 'dueDate'}, date._d)}
            focused={tagihan.focused}
            displayFormat={"DD MMMM YY"}
            placeholder={"Tenggat Bayar"}
            numberOfMonths={1}
            onFocusChange={({ focused }) => _onChange({ id: tagihan.id, name: 'dueDate'}, focused)}
            />
        </td>
        <td style={{textAlign:'center'}} className="detailTable">
          <ReactTooltip id={"deletedetailRow-" + tagihan.id} place={"top"} />
          <i
            data-for={"deletedetailRow-" + tagihan.id}
            data-tip={"Hapus Baris No. " + Number(index + 1)}
            data-iscapture="true"
            style={{paddingLeft: 10}}
            className="fa fa-trash fa-1-7x report-table-icon"
            onClick={() => _onDeleteRow(tagihan.id)}>
          </i>
        </td>
      </tr>
    )
  }
}

const mapStateToProps = state => {
  return {
    houseCode: state.houseCode
  }
}

export default connect(mapStateToProps)(Row)
