// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Plugin Dependencies
import $ from 'jquery'

// Table Component
import Row from './Row'

class Table extends Component {
  static propTypes = {
    formTagihan: PropTypes.array,
    billingItems: PropTypes.array,
    _onChange: PropTypes.func,
    _onDeleteRow: PropTypes.func,
    _copyDetail: PropTypes.func
  }

  componentDidMount() {
    $(document).ready(function() {
      $('#table-daftar-tagihan').DataTable({ "bSort" : false, "iDisplayLength": 10000 })
      $('#table-daftar-tagihan_wrapper .row').first().css('display','none')
      $('#table-daftar-tagihan_wrapper .row').last().css('display','none')
    })
  }

  render() {
    const { formTagihan, _onChange, _onDeleteRow, _copyDetail,billingItems } = this.props
    return (
      <div className="row billingTable">
        <div className="col-xs-12 billingTable">
          <table id="table-daftar-tagihan" className="table tableAuto billingTable">
            <thead className="billingTitle">
              <tr>
                <th style={{width: '5%', textAlign:'center'}}>No.</th>
                <th style={{width: '15%', textAlign:'center'}}>No. Nota</th>
                <th style={{width: '17.5%', textAlign:'center'}}>Tagihan</th>
                <th style={{width: '17.5%', textAlign:'center'}}>Sisa Pembayaran</th>
                <th style={{width: '15%', textAlign:'center'}}>Link Unduh</th>
                <th style={{width: '1%', textAlign:'center'}}>Kode Rumah</th>
                <th style={{width: '10%', textAlign:'center'}}>Tenggat Bayar</th>
                <th style={{width: '5%', textAlign:'center'}}></th>
              </tr>
            </thead>
            <tbody>
            { formTagihan.map((item, index) => (
              <Row
                key={index}
                tagihan={item}
                billingItems={billingItems}
                index={index}
                _copyDetail={_copyDetail}
                _onChange={(id, e) => _onChange(id, e)}
                _onDeleteRow={(id) => _onDeleteRow(id)} />
              ))
            }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default Table
