// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

// Common component
import DashboardLayout from '../../common/DashboardLayout'
import Wrapper from '../../common/Content/Wrapper'
import Body from '../../common/Content/Body'
import Box from '../../common/Box'
import BoxBody from '../../common/Box/Body'
import Loading from '../../common/Loading'
import EmptyBox from '../../common/EmptyBox'

// Daftar Component
import Register from './Register'
import Table from './Table/'
import Button from './Button'

// Plugin Dependencies
import Scroll from 'react-scroll'
import xlsx from 'xlsx'
import Confirm from 'react-confirm-bootstrap'
const scroll = Scroll.animateScroll
import $ from 'jquery'
import popup from '../../../../../public/js/bootstrap-notify/popupMessage'
import moment from 'moment'
import _ from 'lodash'

// Utility
import { limitString30 } from '../../../../util'

// Dispatcher
import { dispatchOccupantCode, dispatchSendBilling, dispatchResetOccupantCode,dispatchBillingItems } from '../../../../dispatcher'

let currentState = {
  formTagihan: [
    {
      id: 1,
      focused: false,
      invoiceNumber:'',
      HouseId:'',
      dueDate: null,
      downloadLink: '',
      detail: [
        { name: "ipl", charge: 0, remaining: 0},
        { name: "air", charge: 0, remaining: 0},
        { name: "lain2", charge: 0, remaining: 0},
        { name: "denda", charge: 0, remaining: 0}
      ]
    }
  ],
  billingItems: {
    fetched: false,
    result: []
  },
  buttonDisable: false
}

class Billing extends Component {
  constructor(props) {
    super(props);
    this.state = currentState
  }

  static propTypes = {
    id: PropTypes.string
  }

  componentWillUnmount() {
    currentState = this.state
  }

  componentDidMount() {
    if (!this.props.houseCode.fetched) {
      this.props.dispatchOccupantCode(this._occupantCodeCallback)
    }

    if (!this.state.billingItems.fetched) {
      this.props.dispatchBillingItems(this._billingItemsCallback)
    }

  }

  _reloadOccupantCode = () => {
    this.props.dispatchResetOccupantCode()
    this.props.dispatchOccupantCode(this._occupantCodeCallback)
  }

  _billingItemsCallback = (err, result, data) => {

    if (result) {
      this.setState({
        formTagihan: [
          {
            id: 1,
            focused: false,
            invoiceNumber:'',
            HouseId:'',
            dueDate: null,
            downloadLink: '',
            detail: data
          }
        ],
        billingItems:{
          fetched: true,
          result
        }
      })
    }
    else console.log(err);
  }

  _occupantCodeCallback = (err, result) => {
    if (result) {
      // console.log('hasil fetch',result);
    } else {
      console.log('data error',err);
      popup.message('pe-7s-attention',`<span class="alert-popup-text-message">API kode rumah sedang mengalami gangguan</span>`,'danger', 5000 ,'bottom','right')
    }
  }

  _onSubmitForm = () => {
    const { formTagihan } = this.state
    const { dispatchSendBilling} = this.props
    const filteredInvoiceNumber = formTagihan.filter(item => item.invoiceNumber === '')
    const filteredDetail = formTagihan.filter(item => item.detail.filter(bill => (bill.charge === 0)).length === 4)
    const filteredDownloadLink = formTagihan.filter(item => item.downloadLink === '')
    const filteredHouseId = formTagihan.filter(item => item.HouseId === '')
    const filteredDueDate = formTagihan.filter(item => item.dueDate === null)

    let html = '<h5>Harap lengkapi form tagihan dibawah ini: </h5><ol>'

    console.log(filteredDetail);
    console.log(filteredDetail.length)

    if(filteredInvoiceNumber.length !== 0){
      formTagihan.map((item, index) => {
        if (filteredInvoiceNumber.filter(bill => bill.id === item.id).length !== 0 ) {
          html += `<li> Nomor Nota - Baris ${index + 1}</li>`
        }
        return html
      })
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })
    } else if(filteredDetail.length !== 0) {
      formTagihan.map((item, index) => {
        if (filteredDetail.filter(bill => bill.id === item.id).length !== 0 ) {
          html += `<li> Tagihan - Baris ${index + 1}</li>`
        }
        return html
      })
      popup.message('', html+'</ol><p style="margin-top:15px; opacity:0.6;">* Minimal 1 tagihan terisi</p>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })
    } else if(filteredDownloadLink.length !== 0) {
      formTagihan.map((item, index) => {
        if (!item.downloadLink) {
          html += `<li> Download Link - Baris ${index + 1}</li>`
        }
        return html
      })
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })
    } else if(filteredHouseId.length !== 0) {
      formTagihan.map((item, index) => {
        if (!item.HouseId) {
          html += `<li> Kode Rumah - Baris ${index + 1}</li>`
        }
        return html
      })
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })
    } else if(filteredDueDate.length !== 0) {
      formTagihan.map((item, index) => {
        if (item.dueDate === null) {
          html += `<li> Tenggat Bayar - Baris ${index + 1}</li>`
        }
        return html
      })
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })
    } else {
      this.setState({ buttonDisable: true })
      return dispatchSendBilling(formTagihan, this._dispatchSendBillingCallback)
    }
  }

  _dispatchSendBillingCallback = (err, result) => {
    if (result) {
      this.setState({ buttonDisable: false })
      const newFailed = result.failed.map((obj, index) => {
        return {
          ...obj,
          id: index + 1,
          focused: false,
          placeholder: 'Invalid Code'
        }
      })

      if (result.failed.length === 0) {
        this.setState({
          formTagihan: [
            {
              id: 1,
              focused: false,
              invoiceNumber:'',
              HouseId:'',
              dueDate: null,
              downloadLink: '',
              detail: [
                { name: "ipl", charge: 0, remaining: 0},
                { name: "air", charge: 0, remaining: 0},
                { name: "lain2", charge: 0, remaining: 0},
                { name: "denda", charge: 0, remaining: 0}
              ]
            }
          ]
        })
      } else {
        this.setState({
          formTagihan: newFailed,
          buttonDisable: false
        })
      }

      if(result.failed.length === 0) {
        popup.message('pe-7s-like2',`<span class="alert-popup-text-message">Tagihan berhasil dikirim</span>`,'success', 5000 ,'bottom','right')
      } else if (result.succeed.length === 0) {
        let html = '<h5>Gagal dalam pengiriman tagihan: </h5><ol>'
        result.failed.map(item => {
          return html += `<li><span style="width:25%;display:inline-block">${item.invoiceNumber}:</span><em>${item.message}</em></li>`
        })
        popup.message('', html+'</ol>','danger', 10000 ,'bottom','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else {
        let html = '<h5>Gagal dalam pengiriman tagihan: </h5><ol>'
        result.failed.map(item => {
          return html += `<li><span style="width:25%;display:inline-block">${item.invoiceNumber}:</span> <em>${item.message}</em></li>`
        })
        popup.message('pe-7s-like2',`<span class="alert-popup-text-message">${result.succeed.length} Tagihan berhasil dikirim</span>`,'success', 5000 ,'bottom','right')
        popup.message('', html+'</ol>','danger', 5000 ,'bottom','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      }
    } else {
      console.log(err);
    }
  }

  _onClickAddRow = (e) => {
    const { formTagihan } = this.state
    this.setState(prevState => ({
      formTagihan: [
        ...formTagihan,
        {
          id: prevState.formTagihan[prevState.formTagihan.length-1].id + 1,
          focused: false,
          invoiceNumber:'',
          HouseId:'',
          dueDate: null,
          downloadLink: '',
          detail: [
            { name: "ipl", charge: 0, remaining: 0},
            { name: "air", charge: 0, remaining: 0},
            { name: "lain2", charge: 0, remaining: 0},
            { name: "denda", charge: 0, remaining: 0}
          ]
        }
      ]
    }))
  }

  _copyDetail = id => {
    console.log('masuk copy');
    const { formTagihan } = this.state
    const i = formTagihan.findIndex(item => item.id === id)

    let newFormTagihan = formTagihan
    let item = newFormTagihan[i]
    let { detail } = item
    let newDetail = detail.map(data =>({name:data.name,charge:data.charge,remaining:data.charge}))
    let newItem = { ...item,detail:newDetail}
    newFormTagihan[i] = newItem

    this.setState({
      formTagihan: newFormTagihan
    })
  }

  _onChange = (obj, value) => {

    const { formTagihan } = this.state
        , { name, id } = obj
        , i = formTagihan.findIndex(item => item.id === id)

    const changeForm = (i,key,value) => {
      let newFormTagihan = formTagihan
        , item = newFormTagihan[i]
        , newItem = null

        if(name==='dueDate')
          if(typeof value==='object'){
            if(!value) return null // handle SingleDatePicker that passes null
            else newItem = {
              ...item,
              [key]:value,
              focused:false
            }
          } else newItem = { //change others field and handle close popup SingleDatePicker
            ...item,
            focused:value
          }
        else newItem = {
          ...item,
          [key]:value,
          focused:false
        }

        newFormTagihan[i] = newItem
        this.setState({
          formTagihan: newFormTagihan
        })
      }

    const changeDetail = (i,obj,value) => {
      let item = formTagihan[i]
        , { detail } = item
        detail[obj.index] = obj.type==='charge' ? {...detail[obj.index],charge:+value||0} : {...detail[obj.index],remaining:+value||0}
        return detail
    }

    if(name === 'detail') {
      const newDetail = changeDetail(i,obj,value)
      changeForm(i,name,newDetail)
    } else changeForm(i,name,value)
  }

  _onDeleteRow = id => {
    console.log('hapus=----',id);
    const { formTagihan } = this.state

    if(formTagihan.length === 1) {
      this.setState({
        formTagihan: [
          {
            id: 1,
            focused: false,
            invoiceNumber:'',
            HouseId:'',
            dueDate: null,
            downloadLink: '',
            detail: [
              { name: "ipl", charge: 0, remaining: 0},
              { name: "air", charge: 0, remaining: 0},
              { name: "lain2", charge: 0, remaining: 0},
              { name: "denda", charge: 0, remaining: 0}
            ]
          }
        ]
      })
    } else {
      this.setState(prevState => ({
        formTagihan: prevState.formTagihan.filter(item => item.id !== id)
      }))
    }
  }

  _onUpload = file => {
    const { formTagihan } = this.state
    const { houseCode } = this.props
    const { filename, result } = file
    // console.log('filename',filename);

    // Scroll to bottom after upload file
    scroll.scrollToBottom()

    // If the filetype xlsx
    const workbook = xlsx.read(btoa(result), { type: 'base64' })
    let xlsxObjects = xlsx.utils.sheet_to_json(workbook.Sheets.Sheet1)

    // If the filetype csv
    // Trim and Split file
    let arr = result.trim().split('\n')
    let array = []
    arr.forEach(item => {
      array.push(item.split('|'))
    })

    // Convert Multidimensional array into array of objects
    let keys = array.shift();
    let csvObjects = array.map(values => {
      return keys.reduce((o, k, i) => {
        o[k] = values[i]
        return o
      }, {})
    })

    let newHouseId = []
    newHouseId = houseCode.result.map(item => {
      return {
        label: limitString30(item.label),
        value: item.value
      }
    })


    // Check the filetype xlsx / csv and insert into arrayObjectFile
    const getFileExt = filename.slice(filename.length-3, filename.length)
    const arrayObjectFile = getFileExt === 'csv' ? csvObjects : xlsxObjects

    const newArrayObjectFile = arrayObjectFile.map((item, index) => {
      const splitBilling = item.billing.split(',')
      const splitRemainingBilling = item.sisa.split(',')

      const assignBilling = splitBilling.map(item => {
        const newType = item.split(':')[0].toLowerCase().trim()
        const newPrice = item.split(':')[1].trim()
        return {
          type: newType, charge: newPrice
        }
      })

      const assignSisa = splitRemainingBilling.map(item => {
        const newType = item.split(':')[0].toLowerCase().trim()
        const newPrice = item.split(':')[1].trim()
        return {
          type: newType, remaining: newPrice
        }
      })

      var assignDetail = []
      for(let i=0; i<assignBilling.length;i++){
        assignDetail.push({name:assignBilling[i].type,charge:assignBilling[i].charge,remaining:assignSisa[i].remaining})
      }

      const filterHouseIdOptions = newHouseId.filter((data) => data.value === item.HouseId).length !== 0

      if(filterHouseIdOptions) {
        return {
          ...item,
          dueDate: moment(item.dueDate),
          id: formTagihan[formTagihan.length - 1].id + (index + 1),
          focused: false,
          detail:assignDetail
        }
      } else if (!filterHouseIdOptions) {
        return {
          ...item,
          dueDate: moment(item.dueDate),
          id: formTagihan[formTagihan.length - 1].id + (index + 1),
          focused: false,
          detail:assignDetail,
          placeholder: 'Invalid Code',
        }
      } else {
        return {
          ...item,
          dueDate: moment(item.dueDate),
          id: formTagihan[formTagihan.length - 1].id + (index + 1),
          focused: false,
          detail:assignDetail,
          placeholder: 'Invalid Code'
        }
      }

    })

    // Filter the previous state array whether the form is empty or not
    const newFilteredArray = formTagihan.filter(x => x.invoiceNumber && x.downloadLink && x.houseId && x.detail && x.dueDate)

    delete newArrayObjectFile.sisa
    delete newArrayObjectFile.billing
    delete newFilteredArray.sisa
    delete newFilteredArray.billing
    // If the newFilteredArray are exist then spread it with the new uploaded file
    if(newFilteredArray) {
      this.setState({
        formTagihan: [...newFilteredArray, ...newArrayObjectFile]
      })
    } else {
      this.setState({
        formTagihan: [...newArrayObjectFile]
      })
    }

  }

  _onConfirmDeleteAllRow = () => {
    this.setState({
      formTagihan: [
        {
          id: 1,
          focused: false,
          invoiceNumber:'',
          HouseId:'',
          dueDate: null,
          downloadLink: '',
          detail: [
            { name: "ipl", charge: 0, remaining: 0},
            { name: "air", charge: 0, remaining: 0},
            { name: "lain2", charge: 0, remaining: 0},
            { name: "denda", charge: 0, remaining: 0}
          ]
        }
      ],
      billingItems: {
        fetched: true,
        result: [
          { name: "Ipl", charge: 0, remaining: 0 },
          { name: "Air", charge: 0, remaining: 0 },
          { name: "Listrik", charge: 0, remaining: 0 },
          { name: "Lain-lain", charge: 0, remaining: 0 },
          { name: "Denda", charge: 0, remaining: 0 }
        ]
      },
      buttonDisable: false
    })
  }

  render() {
    const { formTagihan, buttonDisable,billingItems } = this.state
    const { houseCode, route } = this.props
    console.log('tampil data tagihan---',formTagihan);
    return (
      <DashboardLayout>
        <Wrapper>
        { _.has(route, 'pathname') ?
          !houseCode.fetched || !billingItems.fetched?
          <Loading>
            <EmptyBox />
          </Loading> :
          <Body>
            <Box size="col-md-12">
              <BoxBody>
                <Register
                  title={"Kirim Tagihan Pembayaran"}
                  _reloadOccupantCode={this._reloadOccupantCode}
                  _onUpload={(file) => this._onUpload(file)}/>
                <Table
                  formTagihan={formTagihan}
                  billingItems={billingItems.result}
                  _copyDetail={this._copyDetail}
                  _onDeleteRow={(id) => this._onDeleteRow(id)}
                  _onChange={(id, e) => this._onChange(id, e)} />
                <Button
                  _onClickAddRow={this._onClickAddRow}
                  _onClickDeleteAllRow={this._onClickDeleteAllRow}
                  _onSubmitForm={
                    <Confirm
                      onConfirm={this._onSubmitForm}
                      body="Kirim Tagihan?"
                      confirmText="Kirim"
                      cancelText="Batal"
                      confirmBSStyle="success"
                      title={'Kirim Tagihan Pembayaran'}>
                      { buttonDisable ?
                        <button disabled={true} type="submit" className="btn btn-qluster-success billing-submit-button-margin margin-top-billing-submit pull-right">
                          Harap Tunggu...
                        </button> :
                        <button type="submit" className="btn btn-qluster-success billing-submit-button-margin margin-top-billing-submit pull-right">
                          Kirim Tagihan
                        </button>
                      }
                    </Confirm>
                  }
                  _Confirm={
                    <Confirm
                      onConfirm={this._onConfirmDeleteAllRow}
                      body="Hapus Seluruh Baris?"
                      confirmText="Hapus"
                      cancelText="Keluar"
                      title={'Kirim Tagihan'}>
                        <button className="btn btn-default btn-sm">
                          <span className="fa fa-refresh" style={{marginRight: 8}}></span>
                          Hapus Semua Baris
                        </button>
                    </Confirm>
                  } />
              </BoxBody>
            </Box>
          </Body> :
          <Loading>
            <EmptyBox />
          </Loading>
        }
        </Wrapper>
      </DashboardLayout>
    )
  }
}

const mapStateToProps = state => {
  return {
    houseCode: state.houseCode,
    route: state.router.location
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchBillingItems: (cb) => dispatch(dispatchBillingItems(cb)),
    dispatchResetOccupantCode: () => dispatch(dispatchResetOccupantCode()),
    dispatchOccupantCode: (cb) => dispatch(dispatchOccupantCode(cb)),
    dispatchSendBilling: (form, cb) => dispatch(dispatchSendBilling(form, cb)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Billing)
