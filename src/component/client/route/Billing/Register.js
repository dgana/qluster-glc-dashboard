// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Bootstrap Popup Message
import popup from '../../../../../public/js/bootstrap-notify/popupMessage'

// Plugin Dependencies
import FileReaderInput from 'react-file-reader-input'
import Workbook from 'react-excel-workbook'
import ReactTooltip from 'react-tooltip'

// Import Excel Data
import { billData } from '../../../../util/excelData'

// Component Style
const formStyle = {
  borderRadius: 2,
  border: '1px solid rgb(230,230,230)',
  boxShadow: '0 2px 6px 0 rgba(0,0,0,0.1)',
  marginBottom: 10,
  height:35,
}

const labelStyle = {
  paddingTop:6,
  width:'100%',
  cursor:'pointer',
  marginBottom: 30
}

const templateStyle = {
  cursor: 'pointer',
  paddingLeft: 5,
  fontSize: 11
}

class Register extends Component {
  static propTypes = {
    title: PropTypes.string,
    _onUpload: PropTypes.func,
    _reloadOccupantCode: PropTypes.func
  }

  handleChange = (e, results) => {
    console.log('results',results);
    let filename = results[0][1].name
    let result = results[0][0].target.result
    let object = { filename, result }
    this.props._onUpload(object)
    popup.message('pe-7s-check',`<span class="alert-popup-text-message">${filename} berhasil diupload</span>`,'success', 1000 ,'bottom','right')
  }

  render() {
    const { title, _onUpload, _reloadOccupantCode } = this.props
    return (
      <div>
        <h4 className="text-center" style={{marginBottom: 15}}>{title}</h4>
        <ReactTooltip id="reload-kirim-tagihan" place={"bottom"} />
        <button
          data-for="reload-kirim-tagihan"
          data-tip="Reload Kode Rumah"
          data-iscapture="true"
          onClick={() => _reloadOccupantCode()}
          type="submit"
          className="btn btn-qluster-primary pull-right"
          style={{position:'absolute', right:24, top:16, border:'none'}}>
          <span className="pe-7s-refresh" style={{paddingRight: 6}}></span>
           Reload
        </button>
        <div className="row">
          <div className="col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8" style={formStyle}>
            <div className="form-group">
            <form>
             <label htmlFor="my-file-input" style={labelStyle}>Unggah File ( .xlsx / .csv)</label>
             <FileReaderInput as="binary" id="my-file-input" value="" onChange={this.handleChange}>
               <button style={{display:"none"}}>Select a file!</button>
             </FileReaderInput>
           </form>
            </div>
          </div>
        </div>
        <div className="row" style={{marginBottom: 25}}>
          <div className="col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8">
            <p className="text-center" style={{marginTop:8, fontSize:11}}>Belum punya template?
              <Workbook filename="tagihan.xlsx" element={<u style={templateStyle}>Unduh Sekarang</u>} >
                <Workbook.Sheet data={billData} name="Sheet1">
                  <Workbook.Column label="invoiceNumber" value="invoiceNumber"/>
                  <Workbook.Column label="billing" value="billing"/>
                  <Workbook.Column label="sisa" value="sisa"/>
                  <Workbook.Column label="downloadLink" value="downloadLink"/>
                  <Workbook.Column label="HouseId" value="HouseId"/>
                  <Workbook.Column label="dueDate" value="dueDate"/>
                </Workbook.Sheet>
              </Workbook>
            </p>
          </div>
        </div>
      </div>
    )
  }
}

export default Register
