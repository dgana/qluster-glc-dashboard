// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react';
import { connect } from 'react-redux'

// Common Component
import DashboardLayout from '../../common/DashboardLayout'
import Wrapper from '../../common/Content/Wrapper'
import Body from '../../common/Content/Body'
import Loading from '../../common/Loading'
import EmptyBox from '../../common/EmptyBox'

// News Component
import AddNews from './AddNews'
import NewsList from './NewsList'

// Dispatcher
import {
  dispatchResetNews,
  dispatchFetchNews
} from '../../../../dispatcher'

class News extends Component {
  componentDidMount() {
    this.props.dispatchFetchNews()
  }

  _reloadFetchNews = () => {
    this.props.dispatchResetNews()
    this.props.dispatchFetchNews()
  }

  render() {
    return (
      <DashboardLayout>
        <Wrapper>
          <Body>
            <AddNews _reloadFetchNews={this._reloadFetchNews} size="col-md-12" />
            <NewsList size="col-md-12" />
            { !this.props.newsList.fetched ?
              <Loading marginBottom="35%">
                <EmptyBox />
              </Loading> : null
            }
          </Body>
        </Wrapper>
      </DashboardLayout>
    )
  }
}

const mapStateToProps = state => {
  return {
    compId: state.showClient.companyId,
    newsList: state.newsList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchFetchNews: () => dispatch(dispatchFetchNews()),
    dispatchResetNews: () => dispatch(dispatchResetNews())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(News);
