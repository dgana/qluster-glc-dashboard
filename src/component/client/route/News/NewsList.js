// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Common Component
import Box from '../../common/Box'
import Header from '../../common/Box/Header'
import Body from '../../common/Box/Body'

// Plugin Dependencies
import Confirm from 'react-confirm-bootstrap'
import _ from 'lodash'
import moment from 'moment'
import Gallery from 'react-grid-gallery'

// Utility
import { newsDate } from '../../../../util'

// Dispatcher
import { dispatchDeleteNews } from '../../../../dispatcher'

class NewsList extends Component {
  componentDidMount() {
    moment.locale('id')
  }

  _deleteNews = (newsId) => {
    return this.props.dispatchDeleteNews(newsId)
  }

  render() {
    const { title, size, newsList } = this.props
    const sortedNews = _.sortBy(newsList.list, "createdAt").reverse()
    return (
      <div>
      { sortedNews.map((item, index) => {
          const newPictures = item.pictures.map(pic =>({
            src: pic,
            thumbnail: pic,
            thumbnailWidth: 0,
            thumbnailHeight: 0,
            alt: pic,
            rowHeight:180
          }))
          return (
            <Box key={index} title={title} size={size} padding={0}>
              <Header padding="16px 24px 0px">
                <p className="pull-left">{newsDate(item.createdAt)}</p>
                <h4 style={{paddingTop:5, paddingBottom:10}}>{item.title}</h4>
                <div className="box-tools pull-right">
                  <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus"></i>
                  </button>
                  <div className="btn-group">
                    <button type="button" className="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                      <i className="fa fa-angle-down fa-1-7x"></i></button>
                    <ul className="dropdown-menu" role="menu">
                      <Confirm
                        onConfirm={() => this._deleteNews(item.id)}
                        body={"Hapus berita " + item.title + " ?"}
                        confirmText="Hapus"
                        cancelText="Batal"
                        confirmBSStyle="success"
                        title={'Berita'}>
                        <li><a href="#">Hapus</a></li>
                      </Confirm>
                    </ul>
                    </div>
                </div>
              </Header>
              <Body padding="5px 24px 24px">
                <hr style={{margin: '0px 0px 15px'}} />
                <p id="p_wrap">{item.content}</p>
                <Gallery images={newPictures} />
              </Body>
            </Box>
          )
        })
      }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.client.user,
    newsList: state.newsList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchDeleteNews:(newsId) => dispatch(dispatchDeleteNews(newsId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);
