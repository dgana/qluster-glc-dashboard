// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Common Component
import Box from '../../common/Box'
import Header from '../../common/Box/Header'
import Body from '../../common/Box/Body'
import Footer from '../../common/Box/Footer'

// Plugin Dependencies
import popup from '../../../../../public/js/bootstrap-notify/popupMessage'
import Select from 'react-virtualized-select'
import { Modal } from 'react-bootstrap'
import Dropzone from 'react-dropzone'
import ReactTooltip from 'react-tooltip'

// Dispatcher
import { dispatchFetchNews, dispatchAddNews } from '../../../../dispatcher'

const iconStyle = {
  background: 'inherit',
  border: 'none',
  padding: "14px 18px 14px 14px",
  opacity:'0.75'
}

const closeStyle = {
  width:20,
  height:15,
  position:'absolute',
  top:'0',
  right:0,
  cursor:'pointer'
}

class AddNews extends Component {
  constructor(props) {
    super(props)
    this.state = {
      form: {
        pictures: [],
        content: '',
        title: '',
        category:''
      },
      buttonDisable: false,
      showModalImage: false,
      showModalLocation: false
    }
  }

  onDrop = (imgFiles) => {
    let arr = []
    imgFiles.map(img => arr.push(img))
    const addPicture = this.state.form.pictures.concat(arr)
    const newPicture = {
     ...this.state.form,
     pictures: addPicture
    }
    this.setState({
      form: newPicture
    })
  }

  handleOnChange = (event) => {
    const newContent = {
      ...this.state.form,
      [event.target.name]: event.target.value
    }
    this.setState({
      form: newContent
    })
  }

  handleOnSelect = e => {
    const newForm = { ...this.state.form,category: e.value }
    this.setState({form:newForm})
  }

  postButtonOnClick = () => {
    const { dispatchAddNews } = this.props
    const { form } = this.state

    if (form.content.length === 0 || form.title.length === 0){
      popup.message('pe-7s-news-paper',`<span class="alert-popup-text-message">Judul dan Konten berita wajib diisi</span>`,'danger', 1000 ,'bottom','right')
    } else if (form.pictures.length > 10){
      popup.message('pe-7s-news-paper',`<span class="alert-popup-text-message">Upload Foto Maksimal 10</span>`,'danger', 1000 ,'bottom','right')
    } else {
      this.setState({ buttonDisable: true })
      dispatchAddNews(form, this._callbackAddNews)
    }
  }

  _onDeletePhoto = id => {
    const newPhoto = {
      ...this.state.form,
      pictures: this.state.form.pictures.filter((item, index) => index !== id)
    }
    this.setState({
      form: newPhoto
    })
  }

  _callbackAddNews = (err, result) => {
    if(result) {
      this.setState(prevState => ({
        ...prevState,
        buttonDisable: false,
        form: {
          pictures: [],
          content: '',
          title: '',
          category:''
        }
      }))
      popup.message('pe-7s-like2',`<span class="alert-popup-text-message">Berita berhasil ditambah</span>`,'success', 1000 ,'bottom','right')
    } else {
      popup.message('pe-7s-attention',`<span class="alert-popup-text-message">API tambah berita sedang mengalami gangguan</span>`,'danger', 1000 ,'bottom','right')
    }
  }

  openModalImage = () => {
    this.setState({ showModalImage: true });
  }

  _closeModalImage = () => {
    this.setState({ showModalImage: false });
  }

  _resetPhoto = () => {
    this.setState(prevState => ({
      form: {
        ...prevState.form,
        pictures: []
      }
    }))
    this._closeModalImage()
  }

  _modalUploadImage = form => (
    <Modal show={this.state.showModalImage} onHide={this._closeModalImage} dialogClassName="modalEdit">
      <Modal.Header bsClass="modalHeaderContainer">
        <p style={{margin:'4px 0px'}}>Unggah Foto</p>
      </Modal.Header>
      <Modal.Body bsClass="modalBodyContainer">
        <div className='container-fluid'>
          <div className="form-group">
          { form.pictures.length === 0 ? null :
            form.pictures.map((file, index) => (
            <div key={index} className="dropzone-width dropzone dropzone-square-sm">
              <img className="dropzone-img" src={file.preview} alt="dropzone" />
              <div style={closeStyle} onClick={() => this._onDeletePhoto(index)}>
                  <i className="fa fa-times" aria-hidden="true"></i>
              </div>
            </div>
            ))
          }
            <div className="dropzone-width">
              <Dropzone
                onDrop={this.onDrop}
                className='dropzone'
                activeClassName='active-dropzone'
                multiple={true}>
                <div className="fa fa-plus fa-2x dropzone-box-add"></div>
              </Dropzone>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <button
          className="btn btn-qluster-success pull-right"
          onClick={() => this._closeModalImage()}>
          Tambah Foto
        </button>
        <button
          className="btn btn-default pull-right"
          style={{fontSize: 16, marginRight: 8}}
          onClick={() => this._resetPhoto()}>
          Batal
        </button>
      </Modal.Footer>
    </Modal>
  )

  render() {
    const { form, buttonDisable } = this.state
    const { _reloadFetchNews } = this.props

    return (
      <Box size={this.props.size} padding="8px 0px 0px">
        <Header title="Tambah Berita" />
        <ReactTooltip id="reload-berita" place={"bottom"} />
        <button
          data-for="reload-berita"
          data-tip="Reload List Berita"
          data-iscapture="true"
          onClick={() => { _reloadFetchNews() }}
          type="submit"
          className="btn btn-qluster-primary pull-right"
          style={{position:'absolute', right:24, top:10, border:'none'}}>
          <span className="pe-7s-refresh"></span> Reload
        </button>
        <Body padding="16px 20px">
          <div className="form-group" style={{width:200}}>
            <Select
              value={form.category || '0bs0j5daa1x5'}
              placeholder="Pilih kategori berita"
              clearable={false}
              searchable={false}
              options={[{value:'0bs0j5daa1x5',label:'Pengumuman'},{value:'0bs0j5daa1x6',label:'Kegiatan'},{value:'0bs0j5daa1x7',label:'Promo'}]}
              onChange={(e) => this.handleOnSelect(e)} />
          </div>
          <div className="form-group">
            <input
              name="title"
              type="text"
              className="form-control"
              style={{marginBottom:12}}
              rows="4"
              value={ form.title }
              placeholder="Masukkan judul berita anda disini"
              onChange={ this.handleOnChange } />

            <textarea
              name="content"
              className="form-control"
              rows="4"
              value={ form.content }
              placeholder="Masukkan isi berita anda disini"
              onChange={ this.handleOnChange }>
            </textarea>
          </div>
          <div className="form-group">
          { form.pictures.length === 0 ? null :
            form.pictures.map((file, index) => (
            <div key={index} className="dropzone-width dropzone dropzone-square">
              <img className="dropzone-img" src={file.preview} alt="dropzone" />
              <div style={closeStyle} onClick={() => this._onDeletePhoto(index)}>
                  <i className="fa fa-times" aria-hidden="true"></i>
              </div>
            </div>
            ))
          }
          </div>
          {/* Modal Upload Image */
            this._modalUploadImage(form)
          }
        </Body>
        <Footer padding="0px 0px 0px 12px">
          <ReactTooltip id="upload-image-news" place={"bottom"} />
          <button
            type="button"
            style={iconStyle}
            className="btn btn-news btn-default btn-sm"
            onClick={() => this.openModalImage()}>
            <span
              data-for="upload-image-news"
              data-tip="Unggah Foto"
              data-iscapture="true"
              className="fa fa-image fa-1-7x">
            </span>
          </button>
          <div className="pull-right">
          { buttonDisable ?
            <button
              type="button"
              disabled={true}
              style={{width: 170}}
              className={ form.content.length === 0 || form.title.length === 0 ? 'btn btn-qluster' : 'btn btn-qluster bg-qluster-green' }
              onClick={ this.postButtonOnClick }>
              <p className="text-center text-white" style={{margin:"10px 0px", fontSize:16}}>
                <span className="fa fa-paper-plane" style={{marginRight: 8}}></span>
                Harap Tunggu...
              </p>
            </button> :
            <button
              type="button"
              style={{width: 120}}
              className={ form.content.length === 0 || form.title.length === 0 ? 'btn btn-qluster' : 'btn btn-qluster bg-qluster-green' }
              onClick={ this.postButtonOnClick }>
              <p className="text-center text-white" style={{margin:"10px 0px", fontSize:16}}>
                <span className="fa fa-paper-plane" style={{marginRight: 8}}></span>
                Kirim
              </p>
            </button>
          }
          </div>
        </Footer>
      </Box>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchFetchNews: () => dispatch(dispatchFetchNews()),
    dispatchAddNews: (news, cb) => dispatch(dispatchAddNews(news, cb))
  }
}

export default connect(null, mapDispatchToProps)(AddNews);
