// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

import Logo from '../../../../../public/imgs/logo/Main Logo Qluster.png'

// Dispatcher
import { dispatchRecoveryPassword } from '../../../../dispatcher'

// Plugin Dependencies
import popup from '../../../../../public/js/bootstrap-notify/popupMessage'

class Recovery extends Component {
  constructor() {
    super()
    this.state = {
      email:'',
      button: true,
      isSuccess:false,
      order:''
    }
  }

  handleChange = (e) => {
    this.setState({ email: e.target.value })
  }

  submitRecovery = (e) => {
    e.preventDefault()
    this.setState({
      button: false
    })
    const { email } = this.state
    this.props.dispatchRecoveryPassword(email,this._callbackRecovery)
  }

  _callbackRecovery = (err,res) => {
    if (res) {
      popup.message('pe-7s-like2', `<span class="alert-popup-text-message">Kata sandi berhasil disunting</span>`, 'success', 2000, 'bottom', 'right')
      this.setState({button:true,email:'',isSuccess:true,order:res})
      console.log(res);
    }else {
      popup.message('pe-7s-attention', `<span class="alert-popup-text-message">${err}!</span>`, 'danger', 2000, 'bottom', 'right')
      this.setState({button:true,email:'',isSuccess:false})
    }

  }


  render() {
    const { email, button,order,isSuccess } = this.state

    return (
      <div className="login-background" style={{height: window.innerHeight}}>
        <div className="login-box" style={{marginTop: '-4%'}}>
          <div className="login-logo">
            <img style={{width:'90%', position:'relative', top:25,marginBottom:25}} src={Logo} alt='Qluster Logo'></img>
          </div>
            {
              isSuccess ?
              (<div className="login-box-body" style={{backgroundColor:'inherit'}}>
                <p className="login-box-msg">{order}</p>
              </div>):
              (<div className="login-box-body" style={{backgroundColor:'inherit'}}>
                <p className="login-box-msg">Masukan email aktif anda untuk memulihkan kata sandi</p>
                <form onSubmit={this.submitRecovery}>
                    <div className="form-group has-feedback">
                      <input type="text" value={email} name='loginEmail' className="form-control" placeholder="Masukan Email..." onChange={this.handleChange} />
                    </div>
                    <div className="row">
                      <div className="col-xs-12">
                        {
                          button ? <button type="submit" className="no-border btn btn-primary btn-block btn-flat">Kirim</button> :
                          <button disabled={true} type="submit" className="no-border btn btn-primary btn-block btn-flat">Harap Tunggu... </button>
                        }
                      </div>
                    </div>
                 </form>
                  <a href="#" style={{color:'rgb(50,50,50)'}}></a><br />
                </div>)
             }
         </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchRecoveryPassword: (email,cb) => dispatch(dispatchRecoveryPassword(email,cb)),
  }
}

export default connect(null, mapDispatchToProps)(Recovery);
