// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import { Map, Marker, Popup, ZoomControl } from 'react-leaflet';
import L from 'leaflet'
import ReactTooltip from 'react-tooltip'

// Common Component
import QmapLayers from '../../../../../../../common/Layers'
import SearchBox from '../../../../../../../common/SearchBox'

// Map Icons
import homeIcon from '../../../../../../../../../../public/imgs/icon/home_marker.png'
import home from '../../../../../../../../../../public/imgs/marker/Area_Selection_Home_Green@1x.png'

// Utility
import { greenLakeCity } from '../../../../../../../../../util/mapConfig'

const marker = L.icon({
  iconUrl: homeIcon,
  iconAnchor: [15,45],
  popupAnchor: [0,-35]
})

const existingCluster = L.icon({
  iconUrl: home,
  iconSize: [26,37],
  iconAnchor: [13,37],
  popupAnchor: [0,-20]
})

class MapCluster extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.toggle && !this.props.toggle) {
      setTimeout(() => this.refs['qmap'].leafletElement.invalidateSize(true), 500)
    }
    if (nextProps.tabPanel === "rumah" || nextProps.tabPanel === "cluster" || nextProps.tabPanel === "township") {
      setTimeout(() => this.refs['qmap'].leafletElement.invalidateSize(true), 500)
    }
  }

  onClickMap=(e) => {
    if (this.props.formHouse[0].latitude !== "" || this.props.formHouse[0].longitude !== "" ){
      this.props._onClickAddHouseRow()
    }
    const newLat = e.latlng.lat
    const newLng = e.latlng.lng
    this.changeInput("lat", newLat)
    this.changeInput("lng", newLng)
  }

  changeInput = (field, value) => {
    if (field === "lat"){
      this.props._onChangeMapLatLng("latitude", value)
    } else if (field === "lng"){
      this.props._onChangeMapLatLng("longitude", value)
    } else {
      this.props._onChangeMapLatLng(field, value)
    }
  }

  render() {
    const { clusterList, _onChangeCluster, cluster , _submitFormClusterButton, _reloadCluster } = this.props
    const newPosition = [+cluster.latitude, +cluster.longitude]
    return (
      <div>
        { _submitFormClusterButton }
        <h4 className="text-center" style={{marginBottom: 30}}>Pendaftaran Cluster Baru</h4>
        <ReactTooltip id="reload-daftar-cluster" place={"bottom"} />
        <button
          data-for="reload-daftar-cluster"
          data-tip="Reload Lokasi Cluster"
          data-iscapture="true"
          onClick={() => _reloadCluster()}
          type="submit"
          className="btn btn-qluster-primary pull-right"
          style={{position:'absolute', right:40, top:12, border:'none'}}>
          <span className="pe-7s-refresh" style={{paddingRight: 6}}></span>
           Reload
        </button>
        <Map id="mapQlusterRegister" ref='qmap' center={[greenLakeCity.latitude, greenLakeCity.longitude]} style={{ height: 500, marginBottom:'2rem' }} maxZoom={22} zoom={greenLakeCity.zoom} zoomControl={false} onClick={(evt)=>_onChangeCluster(evt.latlng, 'coords')}>
          <QmapLayers />
          <SearchBox />
          <ZoomControl position="bottomleft" />
          { cluster.latitude === "" || cluster.longitude === "" ? null :
            <Marker position={newPosition} icon={marker}>
              <Popup>
                <div>
                  <input
                    type="text"
                    name="name"
                    style={{width:'27.5rem', margin:'1rem'}}
                    value={cluster.name}
                    placeholder="Nama Cluster"
                    className="form-control"
                    onChange={_onChangeCluster}
                  />
                  <textarea
                    name="address"
                    value={cluster.address}
                    style={{width:'27.5rem', margin:'1rem'}}
                    placeholder="Alamat"
                    className="form-control"
                    rows="1"
                    onChange={_onChangeCluster}
                   />
                   <div className="row">
                     <div className="col-xs-6" style={{paddingRight: 0}}>
                       <input
                         type="text"
                         name="latitude"
                         value={cluster.latitude}
                         placeholder="Latitude"
                         style={{width:'92%', margin:'0px 10px'}}
                         className="form-control"
                         onChange={_onChangeCluster}
                       />
                     </div>
                     <div className="col-xs-6" style={{paddingLeft: 0}}>
                       <input
                         type="text"
                         name="longitude"
                         id="longitude"
                         value={cluster.longitude}
                         placeholder="Longitude"
                         style={{width:'92%', margin:'0px 0px'}}
                         className="form-control"
                         onChange={_onChangeCluster}
                       />
                     </div>
                  </div>
                 </div>
              </Popup>
            </Marker>
          }
          { clusterList.fetched ?
            clusterList.result.map((item, index) => {
              const position = [item.latitude, item.longitude]
              return (
                <Marker position={position} icon={existingCluster} key={index}>
                  <Popup>
                    <div style={{width: 300}}>
                      <div className="row" style={{marginBottom: 4}}>
                        <div className="col-xs-4">
                          <b>Cluster: </b>
                        </div>
                        <div className="col-xs-8">
                          <em>{item.name}</em>
                        </div>
                      </div>
                      <div className="row" style={{marginBottom: 4}}>
                        <div className="col-xs-4">
                          <b>Alamat: </b>
                        </div>
                       <div className="col-xs-8">
                         <em>{item.address}</em>
                       </div>
                     </div>
                     <div className="row" style={{marginBottom: 4}}>
                       <div className="col-xs-4">
                         <b>Latitude: </b>
                       </div>
                       <div className="col-xs-8">
                         <em>{item.longitude}</em>
                       </div>
                     </div>
                     <div className="row" style={{marginBottom: 4}}>
                       <div className="col-xs-4">
                         <b>Longitude: </b>
                       </div>
                       <div className="col-xs-8">
                         <em>{item.longitude}</em>
                       </div>
                     </div>
                   </div>
                  </Popup>
                </Marker>
              )
            }) : null
          }
        </Map>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    toggle: state.sidebarToggle,
    clusterList: state.clusterList,
    tabPanel: state.tabPanel
  }
}

export default connect(mapStateToProps, null)(MapCluster);
