// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import { Map, Marker, Popup, ZoomControl } from 'react-leaflet';
import L from 'leaflet'
import Select from 'react-select'
import { SingleDatePicker } from 'react-dates'
import moment from 'moment'

// Common Component
import QmapLayers from '../../../../../../../common/Layers'
import SearchBox from '../../../../../../../common/SearchBox'

// Map Icons
import homeIcon from '../../../../../../../../../../public/imgs/icon/home_marker.png'
import home from '../../../../../../../../../../public/imgs/marker/Area_Selection_Home_Green@1x.png'

// Dispatcher
import {
  dispatchHouseByCluster,
  dispatchRemoveHouseByCluster,
  dispatchLoadHouseByCluster,
  dispatchResetHouseByCluster
} from '../../../../../../../../../dispatcher'

// Utility
import { greenLakeCity } from '../../../../../../../../../util/mapConfig'

const marker = L.icon({
  iconUrl: homeIcon,
  iconAnchor: [15,45],
  popupAnchor: [0,-35]
})

const existingHome = L.icon({
  iconUrl: home,
  iconSize: [26,37],
  iconAnchor: [13,37],
  popupAnchor: [0,-20]
})

class MapHouse extends React.Component {
  constructor(props){
    super(props)
    this.state = {
			options: '',
			value: [],
      clusterArray : [],
      isFocused:false
    }
  }

  componentWillUnmount() {
    this.props.dispatchResetHouseByCluster()
  }

  _handleSelectChange = value => {
    const clusterArray = value.split(',')
    if (clusterArray[0] === "") {
      this.setState({ clusterArray: [], value })
    } else {
      this.setState({ clusterArray, value })
    }
	}

  componentDidUpdate = (prevProps, prevState) => {
    const { clusterArray } = this.state
    if (prevState.clusterArray.length !== clusterArray.length){
      if (prevState.clusterArray.length < clusterArray.length){
        this.props.dispatchLoadHouseByCluster()
        this.props.dispatchHouseByCluster(clusterArray[clusterArray.length - 1])
      } else {
        const getRemovedClusterId = prevState.clusterArray.filter(val => !clusterArray.includes(val))
        this.props.dispatchRemoveHouseByCluster(getRemovedClusterId[0])
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.toggle && !this.props.toggle) {
      setTimeout(() => this.refs['qmap'].leafletElement.invalidateSize(true), 500)
    }
    if (nextProps.tabPanel === "rumah" || nextProps.tabPanel === "cluster" || nextProps.tabPanel === "township") {
      setTimeout(() => this.refs['qmap'].leafletElement.invalidateSize(true), 500)
    }
  }

  onClickMap=(e) => {
    if (this.props.formHouse[0].latitude !== "" || this.props.formHouse[0].longitude !== "" ){
      this.props._onClickAddHouseRow()
    }
    const newLat = e.latlng.lat
    const newLng = e.latlng.lng
    this.changeInput("lat", newLat)
    this.changeInput("lng", newLng)
  }

  changeInput = (field, value) => {
    if (field === "lat"){
      this.props._onChangeMapLatLng("latitude", value)
    } else if (field === "lng") {
      this.props._onChangeMapLatLng("longitude", value)
    } else {
      this.props._onChangeMapLatLng(field, value)
    }
  }

  _handleClickDate = () =>{
    this.setState({isFocused:!this.state.isFocused})
  }

  _handleChange = (id,name,value,_onChangeHouseRow) => {
    _onChangeHouseRow({ id,name },value)
    this.setState({isFocused:!this.state.isFocused})
  }

  _renderCalender = (_onChangeHouseRow,house) =>{
    return(<SingleDatePicker
      isOutsideRange={() => false}
      date={house.handoverDate ? moment(house.handoverDate) : house.handoverDate }
      onDateChange={handoverDate => this._handleChange(house.id,'handoverDate',handoverDate,_onChangeHouseRow)}
      focused={this.state.isFocused}
      displayFormat={"DD MMMM YY"}
      placeholder={'Tanggal Penyerahan'}
      numberOfMonths={1}
      onFocusChange={({ focused}) => _onChangeHouseRow({ id: house.id, name: 'handoverDate'}, focused)}
    />)
  }

  render() {
    const {
      formHouse,
      clusterOptions,
      _onChangeHouseRow,
      _onClickAddHouseRow,
      filteredHouse,
      _onSubmitFormHouseButton,
      _onClickDeleteHouseRow
    } = this.props

    // const clusterOptions = clusterList.result.map(item => ({ label: item.name, value: item.id }))

    return (
      <div style={{position: 'relative'}}>
        { _onSubmitFormHouseButton }
        <div style={{position: 'absolute', top:'10px', right: '16px', zIndex:999, width:'200px', height: '50px'}}>
          <Select multi simpleValue
            clearable={false}
            value={this.state.value}
            placeholder="Cluster..."
            options={clusterOptions}
            onChange={this._handleSelectChange} />
        </div>
        <Map id="mapQlusterRegister" ref='qmap' center={[greenLakeCity.latitude, greenLakeCity.longitude]} style={{ height: 500, marginBottom:'2rem' }} maxZoom={22} zoom={greenLakeCity.zoom} zoomControl={false} onClick={(evt)=>this.onClickMap(evt)}>
          <QmapLayers />
          <SearchBox />
          <ZoomControl position="bottomleft" />
          { formHouse.map((house, index) => {
            const position = [+house.latitude, +house.longitude]
            return (
              <div key={house.id}>
              { house.latitude === "" || house.longitude === "" ? null :
                <Marker position={position} icon={marker}>
                  <Popup>
                    <div>
                       <select
                         onChange={(e) => _onChangeHouseRow({ id: house.id, name: 'ClusterIdMap'}, e)}
                         value={house.ClusterId}
                         style={{width:'27.5rem', margin:'1rem'}}
                         className="form-control">
                         <option value="" style={{fontWeight: 'bold'}} disabled selected>Pilih cluster</option>
                         { clusterOptions.map((item, index) => (
                           <option
                             key={index}
                             style={{fontWeight: 'bold'}}
                             value={item.value}>{item.label}</option>
                           ))
                         }
                       </select>
                      <input
                        type="text"
                        name="HouseName"
                        style={{width:'27.5rem', margin:'1rem'}}
                        value={house.HouseName}
                        placeholder="Nama Rumah"
                        className="form-control"
                        onChange={(e) => _onChangeHouseRow({id: house.id}, e)}
                      />
                      <textarea
                        name="address"
                        value={house.address}
                        style={{width:'27.5rem', margin:'1rem'}}
                        placeholder="Alamat Lengkap"
                        className="form-control"
                        rows="1"
                        onChange={(e) => _onChangeHouseRow({id: house.id}, e)}
                       />
                       <div style={{width:'27.5rem', margin:'1rem'}} onClick={() => this._handleClickDate()}>
                        { this._renderCalender(_onChangeHouseRow, house) }
                       </div>
                       <div className="row">
                         <div className="col-xs-6" style={{paddingRight: 0}}>
                           <input
                             type="text"
                             name="latitude"
                             value={house.latitude}
                             placeholder="Latitude"
                             style={{width:'92%', margin:'0px 10px'}}
                             className="form-control"
                             onChange={(e) => _onChangeHouseRow({id: house.id}, e)}
                           />
                         </div>
                         <div className="col-xs-6" style={{paddingLeft: 0}}>
                           <input
                             type="text"
                             name="longitude"
                             value={house.longitude}
                             placeholder="Longitude"
                             style={{width:'92%', margin:'0px 0px'}}
                             className="form-control"
                             onChange={(e) => _onChangeHouseRow({id: house.id}, e)}
                           />
                         </div>
                       </div>
                       <div style={{width:'27.5rem', margin:'1rem', textAlign:'center'}}>
                         <i
                           className="fa fa-trash fa-1-7x report-table-icon"
                           onClick={() => _onClickDeleteHouseRow(house.id)}>
                         </i>
                       </div>
                     </div>
                   </Popup>
                 </Marker>
               }
             </div>
              )
            })
          }
          { filteredHouse.result.map((house, index) => {
            const position = [+house.latitude, +house.longitude]
            return (
              <div key={house.id}>
                <Marker position={position} icon={existingHome}>
                  <Popup>
                    <div style={{width: 280}}>
                      <div className="row" style={{marginBottom: 4}}>
                        <div className="col-xs-4">
                          <b>Rumah: </b>
                        </div>
                        <div className="col-xs-8">
                          <em>{house.HouseName}</em>
                        </div>
                      </div>
                      <div className="row" style={{marginBottom: 4}}>
                        <div className="col-xs-4">
                          <b>Alamat: </b>
                        </div>
                       <div className="col-xs-8">
                         <em>{house.address}</em>
                       </div>
                     </div>
                     <div className="row" style={{marginBottom: 4}}>
                       <div className="col-xs-4">
                         <b>Longitude: </b>
                       </div>
                       <div className="col-xs-8">
                         <em>{house.longitude}</em>
                       </div>
                     </div>
                     <div className="row" style={{marginBottom: 4}}>
                       <div className="col-xs-4">
                         <b>Latitude: </b>
                       </div>
                       <div className="col-xs-8">
                         <em>{house.latitude}</em>
                       </div>
                     </div>
                    <hr style={{margin: '10px 0px'}} />
                    <div className="row" style={{marginBottom: 4}}>
                      <div className="col-xs-4">
                        <b>Township: </b>
                      </div>
                      <div className="col-xs-8">
                        <em>{house.TownName}</em>
                      </div>
                    </div>
                    <div className="row" style={{marginBottom: 4}}>
                      <div className="col-xs-4">
                        <b>Cluster: </b>
                      </div>
                      <div className="col-xs-8">
                        <em>{house.ClusterName}</em>
                      </div>
                    </div>
                   </div>
                  </Popup>
                </Marker>
              </div>
              )
            })
          }
        </Map>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    toggle: state.sidebarToggle,
    tabPanel: state.tabPanel,
    clusterList: state.clusterList,
    filteredHouse: state.filteredHouse,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchHouseByCluster: id => dispatch(dispatchHouseByCluster(id)),
    dispatchRemoveHouseByCluster: id => dispatch(dispatchRemoveHouseByCluster(id)),
    dispatchLoadHouseByCluster: () => dispatch(dispatchLoadHouseByCluster()),
    dispatchResetHouseByCluster: () => dispatch(dispatchResetHouseByCluster())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MapHouse);
