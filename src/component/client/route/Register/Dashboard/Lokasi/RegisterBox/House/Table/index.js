// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Plugin Dependencies
import $ from 'jquery'
import moment from 'moment'
// Table Component
import Row from './Row'

class Table extends Component {
  static propTypes = {
    formHouse: PropTypes.array,
    townshipOptions: PropTypes.array,
    clusterOptions: PropTypes.array,
    moment: PropTypes.func,
    filterTownshipOptions: PropTypes.func,
    _onClickDeleteHouseRow: PropTypes.func,
    _onChangeHouseRow: PropTypes.func
  }

  componentDidMount(){
    $(document).ready(function() {
      $('#table-daftar-rumah').DataTable({ "bSort" : false, "iDisplayLength": 10000, "retrieve": true })
      $('#table-daftar-rumah_wrapper .row').first().css('display','none')
      $('#table-daftar-rumah_wrapper .row').last().css('display','none')
    })

    // moment.locale("id")
  }

  render() {
    const { formHouse, townshipOptions, clusterOptions, filterTownshipOptions, _onClickDeleteHouseRow, _onChangeHouseRow } = this.props
    return (
      <div className="row">
        <div className="col-xs-12">
          <table id="table-daftar-rumah" className="table tableAuto registerBoxRumah">
            <thead className="billingTitle">
              <tr>
                <th style={{textAlign:'center'}}>No. </th>
                <th style={{width:'5%', textAlign:'center'}}>Cluster</th>
                <th style={{textAlign:'center'}}>No. Rumah</th>
                <th style={{textAlign:'center'}}>Alamat Rumah</th>
                <th style={{textAlign:'center'}}>Tanggal Penyerahan</th>
                <th style={{textAlign:'center'}}>Lokasi</th>
                <th style={{textAlign:'center'}}></th>
              </tr>
            </thead>
            <tbody>
              {
                formHouse.map((item, index) => (
                  <Row
                    key={index}
                    house={item}
                    moment={moment}
                    townshipOptions={townshipOptions}
                    clusterOptions={clusterOptions}
                    filterTownshipOptions={filterTownshipOptions}
                    index={index}
                    _onClickDeleteHouseRow={_onClickDeleteHouseRow}
                    _onChangeHouseRow={_onChangeHouseRow} />
                ))
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default Table
