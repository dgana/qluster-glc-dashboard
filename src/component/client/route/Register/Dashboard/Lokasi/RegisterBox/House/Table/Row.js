// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Plugin Dependencies
import { SingleDatePicker } from 'react-dates'
// import moment from 'moment'
import Select from 'react-select'
import ReactTooltip from 'react-tooltip'

// Component Style
const selectStyle = { width:"100%",textAlign:'left', cursor:'pointer', }

class Row extends Component {
  static propTypes = {
    _onChangeHouseRow: PropTypes.func,
    moment: PropTypes.func,
  }

  render() {

    const {
      house,
      moment,
      clusterOptions,
      _onClickDeleteHouseRow,
      _onChangeHouseRow,
      index,
    } = this.props

    return (
      <tr>
        <td
          style={{textAlign:'center', width:'5%', padding:'15px 15px'}}>
          <div style={{width:'100%'}}>{index + 1}</div>
        </td>
        <td style={{width: '2%'}}>
          <div style={{width: '100%'}}>
            <div className="col-xs-12" style={{padding:'0rem 0.3rem'}}>
            <Select
              name="ClusterId"
              style={selectStyle}
              value={house.ClusterId}
              placeholder="Pilih cluster"
              options={clusterOptions}
              clearable={false}
              onChange={(e) => _onChangeHouseRow({ id: house.id, name: 'ClusterId' }, e)} />
            </div>
          </div>
        </td>
        <td style={{width:'10%'}}>
          <div style={{width:'100%'}}>
            <div className="col-xs-12"style={{padding:'0rem 0.3rem'}}>
              <input
                type="text"
                name="HouseName"
                style={{width:'100%'}}
                value={house.HouseName}
                placeholder="A-001"
                className="form-control"
                onChange={(e) => _onChangeHouseRow({id: house.id}, e)} />
            </div>
          </div>
        </td>
        <td style={{width:'20%'}}>
          <div style={{width:'100%'}}>
            <div className="col-xs-12"style={{padding:'0rem 0.3rem'}}>
              <textarea
                name="address"
                value={house.address}
                style={{width:'100%'}}
                placeholder="Alamat"
                className="form-control"
                rows="1"
                onChange={(e) => _onChangeHouseRow({id: house.id}, e)}>
              </textarea>
          </div>
        </div>
      </td>
      {/*      <td style={{width:'5%'}}>
        <div style={{width:'100%'}}>
          <div className="col-xs-12"style={{padding:'0rem 0.3rem'}}>
            <input
              type="text"
              name="size"
              style={{width:'100%'}}
              value={house.size}
              placeholder="45"
              className="form-control"
              onChange={(e) => _onChangeHouseRow({id: house.id}, e)} />
          </div>
        </div>
      </td>*/}
      <td style={{width:'15%'}}>
      <div style={{width:'100%'}}>
        <div className="col-xs-12"style={{padding:'0rem 0.3rem'}}>
          <SingleDatePicker
            isOutsideRange={() => false}
            date={house.handoverDate ? moment(house.handoverDate) : house.handoverDate }
            onDateChange={handoverDate => _onChangeHouseRow({ id: house.id, name: 'handoverDate'}, handoverDate)}
            focused={house.focused}
            displayFormat={"DD MMMM YY"}
            placeholder={'Tanggal'}
            numberOfMonths={1}
            onFocusChange={({ focused }) => _onChangeHouseRow({ id: house.id, name: 'handoverDate'}, focused)}
          />
        </div>
      </div>
      </td>
      <td style={{width:'15%'}}>
        <div className="row" style={{display:'block', width:'100%'}}>
          <div className="col-xs-12" style={{padding:'0rem 0.3rem'}}>
            <div style={{width:'100%', marginLeft:'1.5rem'}}>
                <input
                  type="text"
                  name="latitude"
                  value={house.latitude}
                  placeholder="Latitude"
                  style={{width:'100%'}}
                  className="form-control"
                  onChange={(e) => _onChangeHouseRow({id: house.id}, e)} />
            </div>
            <div style={{width:'100%', marginLeft:'1.5rem', marginTop:10}}>
              <input
                type="text"
                name="longitude"
                value={house.longitude}
                placeholder="Longitude"
                style={{width:'100%'}}
                className="form-control"
                onChange={(e) => _onChangeHouseRow({id: house.id}, e)} />
            </div>
          </div>
        </div>
      </td>
      <td style={{width:'5%', padding: '18px 5px'}}>
        <div style={{width:'100%', textAlign:'center'}}>
          <ReactTooltip id={"delete-daftar-rumah-baris-" + house.id} place={"bottom"} />
          <i
            data-for={"delete-daftar-rumah-baris-" + house.id}
            data-tip={"Hapus Baris No. " + Number(index + 1)}
            data-iscapture="true"
            style={{paddingLeft: 10}}
            className="fa fa-trash fa-1-7x report-table-icon"
            onClick={() => _onClickDeleteHouseRow(house.id)}>
          </i>
        </div>
      </td>
    </tr>
    )
  }
}

export default Row
