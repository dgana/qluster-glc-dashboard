// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Cluster extends Component {
  static propTypes = {
    cluster: PropTypes.object,
    _submitFormClusterButton: PropTypes.node,
    _onChangeCluster: PropTypes.func
  }

  render() {
    const { cluster, _onChangeCluster, _submitFormClusterButton } = this.props
    return (
      <div>
        <div className="form-horizontal">
          <div className="form-group">
            <label className="control-label col-sm-2 col-xs-6" htmlFor="name">Nama Cluster:</label>
            <div className="col-sm-3 col-xs-6">
              <input
                type="text"
                name="name"
                id="name"
                className="form-control"
                value={cluster.name}
                placeholder="Nama Cluster"
                onChange={_onChangeCluster} />
            </div>
            <label className="control-label col-sm-2 col-xs-6 margin-top-form" htmlFor="latitude">Latitude:</label>
            <div className="col-sm-4 col-xs-6 margin-top-form">
              <input
                style={{width: '75%'}}
                type="text"
                name="latitude"
                id="latitude"
                className="form-control"
                value={cluster.latitude}
                placeholder="Latitude"
                onChange={_onChangeCluster} />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2 col-xs-6" htmlFor="address">Alamat:</label>
            <div className="col-sm-3 col-xs-6">
              <input
                type="text"
                name="address"
                id="address"
                className="form-control"
                value={cluster.address}
                placeholder="Alamat"
                onChange={_onChangeCluster} />
            </div>
            <label className="control-label col-sm-2 col-xs-6 margin-top-form" htmlFor="longitude">Longitude:</label>
            <div className="col-sm-4 col-xs-6 margin-top-form">
              <input
                style={{width: '75%'}}
                type="text"
                name="longitude"
                id="longitude"
                className="form-control"
                value={cluster.longitude}
                placeholder="Longitude"
                onChange={_onChangeCluster} />
            </div>
          </div>
          {_submitFormClusterButton}
        </div>
      </div>
    )
  }
}

export default Cluster
