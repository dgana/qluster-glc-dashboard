// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
// import PropTypes from 'prop-types'

// Bootstrap Popup Message
import popup from '../../../../../../../../../../public/js/bootstrap-notify/popupMessage'

// Plugin Dependencies
import FileReaderInput from 'react-file-reader-input'
import Workbook from 'react-excel-workbook'
import ReactTooltip from 'react-tooltip'

import { addHouseData } from '../../../../../../../../../util/excelData'

const formStyle = {
  borderRadius: 2,
  border: '1px solid rgb(230,230,230)',
  boxShadow: '0 2px 6px 0 rgba(0,0,0,0.1)',
  marginBottom: 10,
  height:35,
}

const labelStyle = {
  paddingTop:6,
  width:'100%',
  cursor:'pointer',
  marginBottom: 30
}

const templateStyle = {
  cursor: 'pointer',
  paddingLeft: 5,
  fontSize: 11
}

class Register extends Component {
  handleChange = (e, results) => {
    let filename = results[0][1].name
    let result = results[0][0].target.result
    let object = { filename, result }
    this.props._onUploadHouse(object)
    popup.message('pe-7s-check',`<span class="alert-popup-text-message">${filename} berhasil diupload</span>`,'success', 1000 ,'bottom','right')
  }

  render() {
    const { title, _onUploadHouse, _reloadHouse } = this.props
    return (
      <div>
        <ReactTooltip id="reload-daftar-rumah" place={"bottom"} />
        <button
          data-for="reload-daftar-rumah"
          data-tip="Reload Pilihan Cluster"
          data-iscapture="true"
          onClick={() => _reloadHouse()}
          type="submit"
          className="btn btn-qluster-primary pull-right"
          style={{position:'absolute', right:40, top:12, border:'none'}}>
          <span className="pe-7s-refresh" style={{paddingRight: 6}}></span>
           Reload
        </button>
        <h4 className="text-center" style={{marginBottom: 15}}>{title}</h4>
        <div className="row">
          <div className="col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8" style={formStyle}>
            <div className="form-group">
            <form>
             <label htmlFor="my-file-input" style={labelStyle}>Unggah File ( .xlsx / .csv)</label>
             <FileReaderInput as="binary" id="my-file-input" value="" onChange={this.handleChange}>
               <button style={{display:"none"}}>Pilih File!</button>
             </FileReaderInput>
           </form>
            </div>
          </div>
        </div>
        <div className="row" style={{marginBottom: 25}}>
          <div className="col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8">
            <p className="text-center" style={{marginTop:8, fontSize:11}}>Belum punya template?
              <Workbook filename="tambah-rumah.xlsx" element={<u style={templateStyle}>Unduh Sekarang</u>} >
                <Workbook.Sheet data={addHouseData} name="Sheet1">
                  <Workbook.Column label="ClusterId" value="ClusterId" />
                  <Workbook.Column label="HouseName" value="HouseName" />
                  <Workbook.Column label="address" value="address" />
                  <Workbook.Column label="handoverDate" value="handoverDate" />
                  <Workbook.Column label="latitude" value="latitude" />
                  <Workbook.Column label="longitude" value="longitude" />
                </Workbook.Sheet>
              </Workbook>
            </p>
          </div>
        </div>
      </div>
    )
  }
}

export default Register
