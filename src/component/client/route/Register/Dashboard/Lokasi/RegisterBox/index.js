// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

// Common Component
import Box from '../../../../../common/Box'
import Body from '../../../../../common/Box/Body'

// RegisterBox Component
import UploadHouse from './House/Upload'
import TableHouse from './House/Table'
import ButtonHouse from './House/Button'
import MapHouse from './House/Map'
import MapCluster from './Cluster/Map'
import ClusterForm from './Cluster/'

// Plugin Dependencies
import classNames from 'classnames'
import Confirm from 'react-confirm-bootstrap'

// Dispatcher
import { dispatchTabPanel } from '../../../../../../../dispatcher'

class RegisterBox extends React.Component {
  static propTypes = {
    formHouse: PropTypes.array,
    _onSubmitFormHouse: PropTypes.func,
    _onUploadHouse: PropTypes.func,
    _onClickAddHouseRow: PropTypes.func,
    _onClickDeleteAllHouseRow: PropTypes.func,
    _onClickDeleteHouseRow: PropTypes.func,
    _onChangeHouseRow: PropTypes.func,
    _reloadHouse: PropTypes.func,
    moment:PropTypes.func,
    formCluster: PropTypes.object,
    _onSubmitFormCluster: PropTypes.func,
    _onChangeCluster: PropTypes.func,
    _reloadCluster: PropTypes.func,
  }

  render() {
    const {
      _reloadHouse,
      _reloadCluster,
      _reloadTownship,
      dispatchTabPanel,
      tabPanel,
      clusterOptions,
      formHouse,
      _onUploadHouse,
      _onSubmitFormHouse,
      _onClickAddHouseRow,
      _onClickDeleteAllHouseRow,
      _onClickDeleteHouseRow,
      _onChangeHouseRow,
      _onChangeMapLatLng,
      formCluster,
      _onSubmitFormCluster,
      _onChangeCluster,
    } = this.props

    return (
      <Box size="col-md-12" padding={0}>
        <Body padding='0px'>
          <div className="row">
            <div className="col-md-12" style={{marginBottom:0}}>
              <div className="nav-tabs-custom">
                <ul className="nav nav-tabs">
                  <li className={classNames({"active": tabPanel === 'rumah'})}><a href="#rumah" onClick={() => dispatchTabPanel("rumah")} data-toggle="tab" style={{borderTopLeftRadius: 4}}>Rumah</a></li>
                  <li className={classNames({"active": tabPanel === 'cluster'})}><a href="#cluster" onClick={() => dispatchTabPanel("cluster")} data-toggle="tab">Cluster</a></li>
                </ul>
                <div className="tab-content">
                  <div className={classNames("tab-pane", {"active": tabPanel === 'rumah'})} id="rumah">
                    <UploadHouse
                      title={"Pendaftaran Rumah Baru"}
                      _reloadHouse={_reloadHouse}
                      _onUploadHouse={(file) => _onUploadHouse(file)} />

                    <MapHouse
                      formHouse={formHouse}
                      clusterOptions={clusterOptions}
                      _onClickDeleteHouseRow={_onClickDeleteHouseRow}
                      _onChangeHouseRow={_onChangeHouseRow}
                      _onClickAddHouseRow={_onClickAddHouseRow}
                      _onChangeMapLatLng={_onChangeMapLatLng}
                      _onSubmitFormHouseButton={
                        <Confirm
                          onConfirm={_onSubmitFormHouse}
                          body="Daftarkan Perumahan?"
                          confirmText="Kirim"
                          cancelText="Batal"
                          confirmBSStyle="success"
                          title={'Pendaftaran Rumah Baru'}>
                          <div className="btn-group add-house-map-button-container">
                            <button className="btn btn-qluster-success add-house-map-button">
                              Daftarkan
                            </button>
                          </div>
                        </Confirm>
                      } />

                    <TableHouse
                      formHouse={formHouse}
                      clusterOptions={clusterOptions}
                      _onClickDeleteHouseRow={_onClickDeleteHouseRow}
                      _onChangeHouseRow={_onChangeHouseRow} />

                    <ButtonHouse
                      _onClickAddHouseRow={_onClickAddHouseRow}
                      _submitFormHouseButton={
                        <Confirm
                          onConfirm={_onSubmitFormHouse}
                          body="Daftarkan Perumahan?"
                          confirmText="Kirim"
                          cancelText="Batal"
                          confirmBSStyle="success"
                          title={'Pendaftaran Rumah Baru'}>
                          <button type="submit" className="btn btn-qluster-success pull-right margin-top-register-submit">
                            Daftarkan
                          </button>
                        </Confirm>
                      }
                      _deleteHouseRowButton={
                        <Confirm
                          onConfirm={_onClickDeleteAllHouseRow}
                          body="Hapus Seluruh Baris?"
                          confirmText="Hapus"
                          cancelText="Keluar"
                          title={'Pendaftaran Rumah Baru'}>
                          <button className="btn btn-default btn-sm">
                            <span className="fa fa-refresh" style={{marginRight: 8}}></span>
                            Hapus Semua Baris
                          </button>
                        </Confirm>
                      } />
                  </div>

                  <div className={classNames("tab-pane", {"active": tabPanel === 'cluster'})} id="cluster" style={{height:220}}>
                    <MapCluster
                      _reloadCluster={_reloadCluster}
                      cluster={formCluster}
                      _onChangeCluster={_onChangeCluster}
                      _submitFormClusterButton={
                        <Confirm
                          onConfirm={_onSubmitFormCluster}
                          body="Daftarkan Cluster?"
                          confirmText="Kirim"
                          cancelText="Batal"
                          confirmBSStyle="success"
                          title={'Pendaftaran Cluster Baru'}>
                          <div className="btn-group add-house-map-button-container-short">
                            <button className="btn btn-qluster-success add-house-map-button">
                              Daftarkan
                            </button>
                          </div>
                        </Confirm>
                      } />
                    <ClusterForm
                      cluster={formCluster}
                      _onChangeCluster={_onChangeCluster}
                      _submitFormClusterButton={
                        <div style={{marginBottom:10}}>
                          <Confirm
                            onConfirm={_onSubmitFormCluster}
                            body="Daftarkan Cluster?"
                            confirmText="Kirim"
                            cancelText="Batal"
                            confirmBSStyle="success"
                            title={'Pendaftaran Cluster Baru'}>
                            <button type="submit" className="btn btn-qluster-success pull-right button-margin-bottom">
                              Daftarkan
                            </button>
                          </Confirm>
                        </div>
                      } />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Body>
      </Box>
    )
  }
}

const mapStateToProps = state => {
  return {
    tabPanel: state.tabPanel
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchTabPanel: location => dispatch(dispatchTabPanel(location))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterBox);
