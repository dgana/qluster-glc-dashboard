// React, React-Router, PropTypes & Redux Dependencies
import React from 'react';
import PropTypes from 'prop-types'

const Button = props => (
  <div className="row" style={{marginTop:15}}>
    <div className="col-xs-12">
      <button className="btn btn-default btn-sm" style={{marginRight: 12}} onClick={props._onClickAddHouseRow}>
        <span className="fa fa-plus" style={{marginRight: 8}}></span>
        Baris Baru
      </button>
      {props._deleteHouseRowButton}
      {props._submitFormHouseButton}
    </div>
  </div>
)

Button.propTypes = {
  _onClickAddHouseRow: PropTypes.func.isRequired,
  _submitFormHouseButton: PropTypes.node.isRequired,
  _deleteHouseRowButton: PropTypes.node.isRequired
}

export default Button
