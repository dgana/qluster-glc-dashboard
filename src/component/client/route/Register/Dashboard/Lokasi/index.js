// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Lokasi Component
import RegisterBox from './RegisterBox'

// Plugin Dependencies
import Scroll from 'react-scroll'
import xlsx from 'xlsx'
import popup from '../../../../../../../public/js/bootstrap-notify/popupMessage'
const scroll = Scroll.animateScroll
import $ from 'jquery'

// Common Component
import EmptyBox from '../../../../common/EmptyBox'
import Loading from '../../../../common/Loading'

// Dispatcher
import {
  dispatchAllClusterList,
  dispatchRegisterHouse,
  dispatchRegisterCluster,
  dispatchAddClusterList,
  dispatchAddHouseList,
  dispatchResetClusterList,
} from '../../../../../../dispatcher'

let currentState = {
  formHouse: [
    {
      id: 1,
      ClusterId: '',
      HouseName: '',
      address: '',
      latitude: '',
      longitude: '',
      handoverDate:null,
      focused:null,
      houseList: []
    }
  ],
  formCluster: {
    name: '',
    address: '',
    latitude: '',
    longitude: ''
  },
  clusterOptions: []
}

class Lokasi extends React.Component {
  constructor(props) {
    super(props);
    this.state = currentState
    this._callbackClusterOptions = this._callbackClusterOptions.bind(this)
  }

  componentWillUnmount() {
    currentState = this.state
  }

  componentDidMount() {
    this.props.dispatchAllClusterList(this._callbackClusterOptions)
  }

  _reloadHouse = () => {
    this.props.dispatchResetClusterList()
    this.props.dispatchAllClusterList(this._callbackClusterOptions)
  }

  _reloadCluster = () => {
    this.props.dispatchResetClusterList()
    this.props.dispatchAllClusterList(this._callbackClusterOptions)
}



  // *************************************************************************//
  /* Add House
  // *************************************************************************/

  _onUploadHouse = file => {
    const { formHouse } = this.state
    const { filename, result } = file

    // If the filetype xlsx
    const workbook = xlsx.read(btoa(result), {type: 'base64'})
    let xlsxObjects = xlsx.utils.sheet_to_json(workbook.Sheets.Sheet1)

    // If the filetype csv trim and Split file
    let arr = result.trim().split('\n')
    let array = []
    arr.forEach(item => {
      array.push(item.split('|'))
    })

    // Convert Multidimensional CSV array into array of objects
    let keys = array.shift();
    let csvObjects = array.map(values => {
      return keys.reduce((o, k, i) => {
        o[k] = values[i]
        return o
      }, {})
    })

    // Check the filetype xlsx / csv and insert into arrayObjectFile
    const getFileExt = filename.slice(filename.length-3, filename.length)
    const arrayObjectFile = getFileExt === 'csv' ? csvObjects : xlsxObjects

    // Filter the previous state array whether the form is empty or not
    const newFilteredArray = formHouse.filter(x => x.ClusterId && x.HouseName  && x.address && x.handoverDate && x.latitude && x.longitude)

    const newArrayObjectFile = arrayObjectFile.map((item, index) => {
      return {
        id: formHouse[formHouse.length - 1].id + (index + 1),
        handoverDate:new Date(item.handoverDate),
        ...item,
      }
    })

    // Scroll to bottom after upload file
    scroll.scrollToBottom()

    // If the newFilteredArray are exist then spread it with the new uploaded file
    if(newFilteredArray) {
      this.setState({
        formHouse: [...newFilteredArray, ...newArrayObjectFile]
      })
    } else {
      this.setState({
        formHouse: [...newArrayObjectFile]
      })
    }
  }

  _onSubmitFormHouse = () => {
    const { formHouse } = this.state
    const { dispatchRegisterHouse } = this.props

    const filteredClusterId = formHouse.filter(item => !item.ClusterId)
    const filteredHouseName = formHouse.filter(item => !item.HouseName)
    const filteredAddress = formHouse.filter(item => !item.address)
    const filteredHandoverDate = formHouse.filter(item => !item.handoverDate)
    const filteredLatitude = formHouse.filter(item => !item.latitude)
    const filteredLongitude = formHouse.filter(item => !item.longitude)

    let html = '<h5>Harap lengkapi form pendaftaran rumah: </h5><ol>'

    if(filteredClusterId.length !== 0) {
      formHouse.map((item, index) => !item.ClusterId ? html += `<li> Cluster - Baris ${index + 1}</li>` : null)
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })

    } else if(filteredHouseName.length !== 0) {
      formHouse.map((item, index) => !item.HouseName ? html += `<li> No. Rumah - Baris ${index + 1}</li>` : null)
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })

    } else if(filteredAddress.length !== 0) {
      formHouse.map((item, index) => !item.address ? html += `<li> Alamat Rumah	 - Baris ${index + 1}</li>` : null)
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })

    } else if(filteredHandoverDate.length !== 0) {
      formHouse.map((item, index) => !item.handoverDate ? html += `<li> Tanggal Penyerahan - Baris ${index + 1}</li>` : null)
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })

    } else if(filteredLatitude.length !== 0) {
      formHouse.map((item, index) => !item.latitude ? html += `<li> Lokasi: Latitude - Baris ${index + 1}</li>` : null)
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })

    } else if(filteredLongitude.length !== 0) {
      formHouse.map((item, index) => !item.longitude ? html += `<li> Lokasi: Longitude - Baris ${index + 1}</li>` : null)
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })
    } else {
      formHouse.forEach(x => delete x.id )
      return dispatchRegisterHouse(formHouse, this._registerHouseCallback)
    }
  }

  _registerHouseCallback = (err, result) => {
    if (result) {
      const newFailed = result.failed.map((obj, index) => {
        return {
          id: index + 1,
          ClusterId: '',
          HouseName: obj.HouseName,
          address: obj.address,
          latitude: obj.latitude,
          longitude: obj.longitude,
          handoverDate:null,
          focused:null
        }
      })

      if (result.failed.length === 0) {
        this.setState({
          formHouse: [
            {
              id: 1,
              ClusterId: '',
              HouseName: '',
              address: '',
              latitude: '',
              longitude: '',
              handoverDate:null,
              focused:null
            }
          ]
        })
      } else {
        this.setState({
          formHouse: newFailed
        })
      }

      if(result.failed.length === 0) {
        popup.message('pe-7s-like2',`<span class="alert-popup-text-message">Pendaftaran rumah berhasil</span>`,'success', 5000 ,'bottom','right')

        this.props.dispatchAddHouseList(result.succeed)

      } else if (result.succeed.length === 0) {
        let html = '<h5>Gagal dalam pendaftaran rumah: </h5><ol>'
        result.failed.map((item, index) => {
          return html += `<li><span style="width:50%;display:inline-block">${item.ClusterId} </span><em>${item.message}</em></li>`
        })
        popup.message('', html+'</ol>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else {
        let html = '<h5>Gagal dalam pendaftaran rumah: </h5><ol>'
        result.failed.map((item, index) => {
          return html += `<li><span style="width:50%;display:inline-block">${item.ClusterId} </span> <em>${item.message}</em></li>`
        })
        popup.message('pe-7s-like2',`<span class="alert-popup-text-message">${result.succeed.length} rumah berhasil didaftarkan</span>`,'success', 5000 ,'bottom','right')
        popup.message('', html+'</ol>','danger', 5000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })

        this.props.dispatchAddHouseList(result.succeed)

      }
    } else {
      console.log(err);
    }
  }

  _onChangeHouseRow = (obj, e) => {

    const { formHouse } = this.state
    const index = formHouse.findIndex(x => x.id === obj.id)

    if (obj.name === 'ClusterId') {
      const newCluster = [
        ...formHouse.slice(0, index),
        Object.assign({}, formHouse[index], { ClusterId: e.value } ),
        ...formHouse.slice(index + 1)
      ]
      this.setState({
        formHouse: newCluster,
      })

    } else if (obj.name === 'ClusterIdMap') {
      const newCluster = [
        ...formHouse.slice(0, index),
        Object.assign({}, formHouse[index], { ClusterId: e.target.value } ),
        ...formHouse.slice(index + 1)
      ]
      this.setState({
        formHouse: newCluster,
      })
    } else if (obj.name === 'coords') {
      const newCoords = [
        ...formHouse.slice(0, index),
        Object.assign({}, formHouse[index], { latitude: e.lat(), longitude: e.lng() } ),
        ...formHouse.slice(index + 1)
      ]
      this.setState({
        formHouse: newCoords
      })
    } else if (obj.name === 'handoverDate') {
      if(typeof e === 'object') {
        if(e === null) {
          return null
        } else {
          const newForm = [
            ...formHouse.slice(0, index),
            Object.assign({}, formHouse[index], { handoverDate: e._d, focused: false }),
            ...formHouse.slice(index + 1)
          ]
          this.setState({
            formHouse: newForm
          })
        }
      } else {
        const newForm = [
          ...formHouse.slice(0, index),
          Object.assign({}, formHouse[index], {focused: e}),
          ...formHouse.slice(index + 1)
        ]
        this.setState({
          formHouse: newForm
        })
      }
    }else {
      const newHouse = [
        ...formHouse.slice(0, index),
        Object.assign({}, formHouse[index], {[e.target.name]: e.target.value}),
        ...formHouse.slice(index + 1)
      ]
      this.setState({
        formHouse: newHouse
      })
    }
  }

  _onChangeMapLatLng = (name, value) => {
    const { formHouse } = this.state
    const newHouse = [
      ...formHouse.slice(0, formHouse.length-1),
      Object.assign({}, formHouse[formHouse.length-1], {[name]: value}),
      ...formHouse.slice(formHouse.length-1 + 1)
    ]
    this.setState({formHouse:newHouse})
  }

  _onClickDeleteAllHouseRow = () => {
    this.setState({
      formHouse: [
        {
          id: 1,
          ClusterId: '',
          HouseName: '',
          address: '',
          latitude: '',
          longitude: '',
          handoverDate:null,
          focused:null
        }
      ]
    })
  }

  _onClickAddHouseRow = () => {
    const { formHouse } = this.state
    this.setState(prevState => ({
      formHouse: [
        ...formHouse,
        {
          id: prevState.formHouse[prevState.formHouse.length-1].id + 1,
          ClusterId: '',
          HouseName: '',
          address: '',
          latitude: '',
          longitude: '',
          handoverDate:null,
          focused:null
        }
      ]
    }))
  }

  _onClickDeleteHouseRow = id => {
    const { formHouse } = this.state
    if(formHouse.length === 1) {
      this.setState({
        formHouse: [
          {
            id: 1,
            ClusterId: '',
            HouseName: '',
            address: '',
            latitude: '',
            longitude: '',
            handoverDate:null,
            focused:null
          }
        ]
      })
    } else {
      this.setState(prevState => ({
        formHouse: prevState.formHouse.filter(item => item.id !== id)
      }))
    }
  }


  // *************************************************************************//
  /* Add Cluster
  // *************************************************************************/

  _onSubmitFormCluster = () => {
    const { formCluster } = this.state
    const { dispatchRegisterCluster } = this.props

    if (!formCluster.name || !formCluster.latitude || !formCluster.longitude || !formCluster.address) {
      popup.message('pe-7s-attention', '<p style="display:inline-block; padding-left:10px">Harap lengkapi form pendaftaran cluster</p>','danger', 2000 ,'top','center')
    } else {
      return dispatchRegisterCluster(formCluster, this._registerClusterCallback)
    }
  }

  _registerClusterCallback = (err, result) => {
    if(result) {
      popup.message('pe-7s-like2',`<span class="alert-popup-text-message">Pendaftaran cluster berhasil</span>`,'success', 2000 ,'bottom','right')
      this.setState({
        formCluster: {
          name: '',
          address: '',
          latitude: '',
          longitude: '',
          handoverDate:null,
          focused:false
        }
      })
      this.props.dispatchAddClusterList(result.succeed)
    } else {
      popup.message('pe-7s-attention', '<p style="display:inline-block; padding-left:10px">Gagal dalam pendaftaran cluster</p>','danger', 2000 ,'top','center')
    }
  }

  _onChangeCluster = (e, name) => {
    const { formCluster } = this.state

     if (name === 'coords') {
        const newCoords = {
          ...formCluster,
          latitude: e.lat,
          longitude: e.lng
        }
        this.setState({
          formCluster: newCoords
        })
    } else {
      const newCluster = {
        ...formCluster,
        [e.target.name]: e.target.value
      }
      this.setState({
        formCluster: newCluster
      })
    }
  }

  _fetchClusterListCallback = (err, result) => {
    if (result) {
      this.setState(prevState => ({
        formCluster: {
          ...prevState.formCluster,
          clusterList: result
        }
      }))
    } else {
      console.log(err)
    }
  }

  _callbackClusterOptions = (err,result) => {
    if(result){
      const clusterOptions = result.map( cluster => ({label:cluster.name,value:cluster.id}))
      this.setState({clusterOptions})
    }else console.log(err);
  }

  render() {
    const { formHouse, formCluster,clusterOptions } = this.state
    const { clusterList, filteredHouse } = this.props
    return (
      <div>
        <RegisterBox
          _reloadHouse={this._reloadHouse}
          _reloadCluster={this._reloadCluster}
          formHouse={formHouse}
          clusterOptions={clusterOptions}
          _onUploadHouse={this._onUploadHouse}
          _onSubmitFormHouse={this._onSubmitFormHouse}
          _onClickDeleteAllHouseRow={this._onClickDeleteAllHouseRow}
          _onClickAddHouseRow={this._onClickAddHouseRow}
          _onClickDeleteHouseRow={this._onClickDeleteHouseRow}
          _onChangeHouseRow={this._onChangeHouseRow}
          _onChangeMapLatLng={this._onChangeMapLatLng}
          formCluster={formCluster}
          _onSubmitFormCluster={this._onSubmitFormCluster}
          _onChangeCluster={this._onChangeCluster} />

          { !filteredHouse.fetched || !clusterList.fetched ?
            <Loading marginBottom="80%">
              <EmptyBox />
            </Loading> : null
          }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    houseList: state.houseList,
    clusterList: state.clusterList,
    filteredHouse: state.filteredHouse,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchRegisterHouse: (form, cb) => dispatch(dispatchRegisterHouse(form, cb)),
    dispatchRegisterCluster: (form, cb) => dispatch(dispatchRegisterCluster(form, cb)),
    dispatchAddClusterList: newList => dispatch(dispatchAddClusterList(newList)),
    dispatchAddHouseList: newList => dispatch(dispatchAddHouseList(newList)),
    dispatchAllClusterList: (cb) => dispatch(dispatchAllClusterList(cb)),
    dispatchResetClusterList: () => dispatch(dispatchResetClusterList()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Lokasi)
