// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Lokasi Component
import RegisterBox from './RegisterBox'

// Plugin Dependencies
import popup from '../../../../../../../public/js/bootstrap-notify/popupMessage'
import $ from 'jquery'

// Dispatcher
import {
  dispatchCctvQmap,
  dispatchRegisterCctv,
  dispatchResetCctvQmap,
} from '../../../../../../dispatcher'

let currentState = {
  form: [
    {
      id: 1,
      name: '',
      url: '',
      latitude: '',
      longitude: ''
    }
  ],
  disabled: false
}

class Cctv extends React.Component {
  constructor(props) {
    super(props)
    this.state = currentState
  }

  componentWillUnmount = () =>  {
    currentState = this.state
  }

  componentDidMount = () => {
    if (!this.props.cctvList.fetched) {
      this.props.dispatchCctvQmap(null, this._callbackCctvQmap)
    }
  }

  _reloadCctv = () => {
    this.props.dispatchResetCctvQmap()
    this.props.dispatchCctvQmap(null, this._callbackCctvQmap)
  }

  _callbackCctvQmap = (err, result) => {
    if (err) console.log(err);
  }

  _onSubmitForm = () => {
    const { form } = this.state
    const { dispatchRegisterCctv } = this.props

    // const filteredTownId = form.filter(item => !item.townId)
    const filteredName = form.filter(item => !item.name)
    const filteredUrl = form.filter(item => !item.url)
    const filteredLatitude = form.filter(item => !item.latitude)
    const filteredLongitude = form.filter(item => !item.longitude)

    let html = '<h5>Harap lengkapi form pendaftaran CCTV: </h5><ol>'

    if (filteredName.length !== 0) {
      form.map((item, index) => !item.name ? html += `<li> Nama - Baris ${index + 1}</li>` : null)
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })

    } else if (filteredUrl.length !== 0) {
      form.map((item, index) => !item.url ? html += `<li> Link - Baris ${index + 1}</li>` : null)
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })

    } else if(filteredLatitude.length !== 0) {
      form.map((item, index) => !item.latitude ? html += `<li> Latitude - Baris ${index + 1}</li>` : null)
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })

    } else if(filteredLongitude.length !== 0) {
      form.map((item, index) => !item.longitude ? html += `<li> Longitude - Baris ${index + 1}</li>` : null)
      popup.message('', html+'</ol>','danger', 5000 ,'top','right')
      $('.close').css({ top: '15px', fontSize: '35px' })

    } else {
      dispatchRegisterCctv(form, this._callbackRegisterCctv)
      this.setState({ disabled: true })
    }
  }

  _callbackRegisterCctv = (err, result) => {
    if (result) {
      result.failed.forEach((obj, index) => obj.id = index + 1 )
      if (result.failed.length === 0) {
        this.setState({
          form: [
            {
              id: 1,
              name: '',
              url: '',
              latitude: '',
              longitude: ''
            }
          ],
          disabled: false
        })
      } else {
        this.setState({
          disabled: false,
          form: result.failed
        })
      }

      if(result.failed.length === 0) {
        popup.message('pe-7s-like2',`<span class="alert-popup-text-message">Pendaftaran CCTV berhasil</span>`,'success', 2000 ,'bottom','right')
      } else if (result.succeed.length === 0) {
        let html = '<h5>Gagal dalam pendaftaran: </h5><ol>'
        result.failed.map(item => {
          return html += `<li>${item.name}: &nbsp&nbsp<em>${item.message}</em></li>`
        })
        popup.message('', html+'</ol>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else {
        let html = '<h5>Gagal dalam pendaftaran: </h5><ol>'
        result.failed.map(item => {
          return html += `<li>${item.name}: &nbsp&nbsp<em>${item.message}</em></li>`
        })
        popup.message('pe-7s-like2',`<span class="alert-popup-text-message">${result.succeed.length} CCTV berhasil didaftarkan</span>`,'success', 5000 ,'bottom','right')
        popup.message('', html+'</ol>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      }
    } else {
      console.log(err);
    }
  }

  _onChangeCctv = (id, e) => {
    const { form } = this.state
    const index = form.findIndex(x => x.id === id)

    const newForm = [
      ...form.slice(0, index),
      Object.assign({}, form[index], {[e.target.name]: e.target.value}),
      ...form.slice(index + 1),
    ]

    this.setState({
      form: newForm
    })
  }

  _onChangeMapLatLng = (latitude, longitude) => {
    const { form } = this.state
    const newCctv = [
      ...form.slice(0, form.length-1),
      Object.assign({}, form[form.length-1], { latitude, longitude }),
      ...form.slice(form.length-1 + 1)
    ]
    this.setState({ form: newCctv })
  }

  _onClickDeleteAllRow = () => {
    this.setState({
      form: [
        {
          name: '',
          url: '',
          latitude: '',
          longitude: ''
        }
      ]
    })
  }

  _onClickAddRow = () => {
    const { form } = this.state
    this.setState(prevState => ({
      form: [
        ...form,
        {
          id: prevState.form[prevState.form.length-1].id + 1,
          name: '',
          url: '',
          latitude: '',
          longitude: ''
        }
      ]
    }))
  }

  _onClickDeleteRow = id => {
    const { form } = this.state
    if(form.length === 1) {
      this.setState({
        form: [
          {
            id: 1,
            name: '',
            url: '',
            latitude: '',
            longitude: ''
          }
        ]
      })
    } else {
      this.setState(prevState => ({
        form: prevState.form.filter(item => item.id !== id)
      }))
    }
  }

  render() {
    const { form, disabled } = this.state

    return (
      <div>
        <RegisterBox
          form={form}
          disabled={disabled}
          _reloadCctv={this._reloadCctv}
          _onSubmitForm={this._onSubmitForm}
          _onChangeCctv={this._onChangeCctv}
          _onChangeMapLatLng={this._onChangeMapLatLng}
          _onClickDeleteAllRow={this._onClickDeleteAllRow}
          _onClickAddRow={this._onClickAddRow}
          _onClickDeleteRow={this._onClickDeleteRow} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    id: state.showClient.companyId,
    cctvList: state.cctvList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchCctvQmap: (registerId,cb) => dispatch(dispatchCctvQmap(registerId,cb)),
    dispatchRegisterCctv: (form, cb) => dispatch(dispatchRegisterCctv(form, cb)),
    dispatchResetCctvQmap: () => dispatch(dispatchResetCctvQmap()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cctv)
