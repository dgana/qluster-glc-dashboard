// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

// Common Component
import Box from '../../../../../common/Box'
import BoxHeader from '../../../../../common/Box/Header'
import Body from '../../../../../common/Box/Body'

// RegisterBox Component
import MapCctv from './Map'
import FormTable from './FormTable'
import Button from './Button'

// Plugin Dependencies
import Confirm from 'react-confirm-bootstrap'
import ReactTooltip from 'react-tooltip'

class RegisterBox extends React.Component {
  static propTypes = {
    form: PropTypes.array,
    _onSubmitForm: PropTypes.func,
    _reloadCctv: PropTypes.func,
    _onChangeMapLatLng: PropTypes.func,
    _onChangeCctv: PropTypes.func,
    _onClickDeleteAllRow: PropTypes.func,
    _onClickAddRow: PropTypes.func,
    _onClickDeleteRow: PropTypes.func
  }

  render() {
    const {
      form,
      cctvList,
      disabled,
      _onSubmitForm,
      _reloadCctv,
      _onChangeMapLatLng,
      _onChangeCctv,
      _onClickDeleteAllRow,
      _onClickAddRow,
      _onClickDeleteRow
    } = this.props

    return (
      <Box size="col-md-12">
        <BoxHeader title="Pendaftaran CCTV" />
        <Body>
          <ReactTooltip id="reload-daftar-cctv" place={"bottom"} />
          <button
            data-for="reload-daftar-cctv"
            data-tip="Reload List CCTV"
            data-iscapture="true"
            onClick={() => _reloadCctv()}
            type="submit"
            className="btn btn-qluster-primary pull-right"
            style={{position:'absolute', right:24, top:10, border:'none'}}>
            <span className="pe-7s-refresh"></span> Reload
          </button>
          <hr style={{marginTop: 0}} />

          <MapCctv
            form={form}
            _onChangeMapLatLng={_onChangeMapLatLng}
            _onChangeCctv={_onChangeCctv}
            _onClickAddRow={_onClickAddRow}
            _onClickDeleteRow={_onClickDeleteRow}
            _onSubmitFormButton={
              <Confirm
                onConfirm={_onSubmitForm}
                body="Daftarkan CCTV?"
                confirmText="Kirim"
                cancelText="Batal"
                confirmBSStyle="success"
                title={'Pendaftaran CCTV'}>
                <div className="btn-group add-cctv-map-button-container">
                  <button className="btn btn-qluster-success add-cctv-map-button">
                    Daftarkan
                  </button>
                </div>
              </Confirm>
            } />

          { cctvList.fetched ?
            <FormTable
              form={form}
              _onClickDeleteRow={_onClickDeleteRow}
              _onChangeCctv={_onChangeCctv} /> : null
          }

          <Button
            _onClickAddRow={_onClickAddRow}
            _onSubmitFormButton={
              <Confirm
                onConfirm={_onSubmitForm}
                body="Daftarkan CCTV?"
                confirmText="Kirim"
                cancelText="Batal"
                confirmBSStyle="success"
                title={'Pendaftaran CCTV'}>
                <button type="submit" className="btn btn-qluster-success pull-right margin-top-register-submit">
                  { disabled ? "Harap Tunggu..." : "Daftarkan" }
                </button>
              </Confirm>
            }
            _deleteAllRowButton={
              <Confirm
                onConfirm={_onClickDeleteAllRow}
                body="Hapus Seluruh Baris?"
                confirmText="Hapus"
                cancelText="Keluar"
                title={'Pendaftaran CCTV'}>
                <button className="btn btn-default btn-sm">
                  <span className="fa fa-refresh" style={{marginRight: 8}}></span>
                  Hapus Semua Baris
                </button>
              </Confirm>
            } />

        </Body>
      </Box>
    )
  }
}

const mapStateToProps = state => {
  return {
    cctvList: state.cctvList
  }
}

export default connect(mapStateToProps, null)(RegisterBox)
