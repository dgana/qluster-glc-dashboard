// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import { Map, Marker, Popup, ZoomControl } from 'react-leaflet';
import L from 'leaflet'

// Common Component
import QmapLayers from '../../../../../../common/Layers'
import SearchBox from '../../../../../../common/SearchBox'

// Map Icons
import homeIcon from '../../../../../../../../../public/imgs/icon/home_marker.png'
import cctv from '../../../../../../../../../public/imgs/marker/cctv.png'
import loadingImg from '../../../../../../../../../public/imgs/loading/loading_qluster.gif'

// Utility
import { greenLakeCity } from '../../../../../../../../util/mapConfig'

const marker = L.icon({
  iconUrl: homeIcon,
  iconAnchor: [15,45],
  popupAnchor: [0,-35]
})

const existingCctv = L.icon({
  iconUrl: cctv,
  iconSize: [26, 36],
  iconAnchor: [13,36],
  popupAnchor: [0,-26]
})

class MapCctv extends React.Component {

  componentWillReceiveProps(nextProps) {
    if (nextProps.sidebarToggle !== this.props.sidebarToggle) {
      setTimeout(() => this.refs['qmap'].leafletElement.invalidateSize(true), 500)
    }
  }

  _onClickMap = e => {
    const { form } = this.props
    if (form[0].latitude !== "" || form[0].longitude !== "" ) {
      this.props._onClickAddRow()
    }
    const newLat = e.latlng.lat
    const newLng = e.latlng.lng
    this.props._onChangeMapLatLng(newLat, newLng)
  }

  render() {

    const {
      form,
      cctvList,
      _onChangeMapLatLng,
      _onChangeCctv,
      _onSubmitFormButton,
      _onClickDeleteRow
    } = this.props

    return (
      <div>
        { _onSubmitFormButton }
        <Map
          id="mapQlusterRegister"
          ref='qmap'
          center={[greenLakeCity.latitude, greenLakeCity.longitude]}
          style={{ height: 500, marginBottom:'2rem' }}
          maxZoom={22}
          zoom={greenLakeCity.zoom}
          zoomControl={false}
          onClick={evt => this._onClickMap(evt)}>

          <QmapLayers />
          <SearchBox />

          <ZoomControl position="bottomleft" />
          { form.map((cctv, index) => {
            const position = [cctv.latitude, cctv.longitude]
            return (
              <div key={cctv.index}>
                { cctv.latitude === "" || cctv.longitude === "" ? null :
                  <Marker position={position} icon={marker}>
                    <Popup>
                      <div>
                        <input
                          type="text"
                          name="name"
                          style={{width:'25rem', margin:'1rem'}}
                          value={cctv.name}
                          placeholder="Nama CCTV"
                          className="form-control"
                          onChange={(e) => _onChangeCctv(cctv.id, e)} />
                        <input
                          type="text"
                          name="url"
                          style={{width:'25rem', margin:'1rem'}}
                          value={cctv.url}
                          placeholder="Link CCTV"
                          className="form-control"
                          onChange={(e) => _onChangeCctv(cctv.id, e)} />
                         <div className="row">
                           <div className="col-xs-6" style={{paddingRight: 0}}>
                             <input
                               type="text"
                               name="latitude"
                               value={cctv.latitude}
                               placeholder="Latitude"
                               style={{width:'92%', margin:'0px 10px'}}
                               className="form-control"
                               onChange={(e) => _onChangeCctv(cctv.id, e)}
                             />
                           </div>
                           <div className="col-xs-6" style={{paddingLeft: 0}}>
                             <input
                               type="text"
                               name="longitude"
                               id="longitude"
                               value={cctv.longitude}
                               placeholder="Longitude"
                               style={{width:'92%', margin:'0px 0px'}}
                               className="form-control"
                               onChange={(e) => _onChangeCctv(cctv.id, e)}
                             />
                           </div>
                        </div>
                        <div style={{width:'25rem', margin:'1rem', textAlign:'center'}}>
                          <i
                            className="fa fa-trash fa-1-7x report-table-icon"
                            onClick={() => _onClickDeleteRow(cctv.id)}>
                          </i>
                        </div>
                      </div>
                    </Popup>
                  </Marker>
                }
              </div>
            )
          })
          }
          { cctvList.result.map((cctv, index) => {
            const position = [cctv.latitude, cctv.longitude]
            return (
              <div key={cctv.index}>
                <Marker position={position} icon={existingCctv}>
                  <Popup className="popupCctv">
                    <div style={{width: '530px', marginLeft: '-19px'}}>
                    <div>
                      <div className="content-margin" style={{background: 'rgb(220,220,220)', padding: 12, marginTop: -10, borderRadius: '10px 10px 0 0'}}>
                        <div className="row">
                          <div className="col-md-2 text-center">
                            <h7>ID :</h7>
                          </div>
                          <div className="col-md-1 text-center">
                            <em>{cctv.id}</em>
                          </div>
                          <div className="col-md-2 text-right">
                            <h7>Nama :</h7>
                          </div>
                          <div className="col-md-3 text-center">
                            <em>{cctv.name}</em>
                          </div>
                          <div className="col-md-2">
                            <h7>Status :</h7>
                          </div>
                          <div className="col-md-2">
                            <em>{cctv.isActive ? "Aktif" : "Tidak Aktif"}</em>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="qmap-popup" style={{width:'530px', height:'300px', padding: 0}}>
                      <div className="content-margin" style={{margin: 0}}>
                        <img src={loadingImg} style={{width:'150px', zIndex:999, marginLeft:'35%', marginTop:'12%'}} alt="Loading" />
                        <a href={cctv.url} target="_blank"><img id={cctv.id} src={cctv.url} style={{maxWidth:'530px', zIndex:1000, position:'absolute', borderRadius: '0 0 10px 10px', top:'43px', display:'block'}} alt="CCTV" /></a>
                      </div>
                    </div>
                  </div>
                  </Popup>
                </Marker>
              </div>
              )
            })
          }
        </Map>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    cctvList: state.cctvList,
    sidebarToggle: state.sidebarToggle
  }
}

export default connect(mapStateToProps, null)(MapCctv);
