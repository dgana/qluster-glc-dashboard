// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Plugin Dependencies
import $ from 'jquery'

// Table Component
import Row from './Row'

class FormTable extends Component {
  static propTypes = {
    form: PropTypes.array,
    _onChangeCctv: PropTypes.func,
    _onClickAddRow: PropTypes.func,
    _onClickDeleteRow: PropTypes.func,
    _onClickDeleteAllRow: PropTypes.func,
    _onSubmitFormButton: PropTypes.node
  }

  componentDidMount(){
    $(document).ready(function() {
      $('#table-daftar-cctv').DataTable({ "bSort" : false, "iDisplayLength": 10000, "retrieve": true  })
      $('#table-daftar-cctv_wrapper .row').first().css('display','none')
      $('#table-daftar-cctv_wrapper .row').last().css('display','none')
    })
  }

  render() {
    const {
      form,
      _onChangeCctv,
      _onClickAddRow,
      _onClickDeleteRow,
      _onClickDeleteAllRow,
      _onSubmitFormButton
    } = this.props

    return (
      <div className="row">
        <div className="col-xs-12">
          <table id="table-daftar-cctv" className="table tableAuto">
            <thead className="">
              <tr>
                <th style={{width:'5%', textAlign:'center'}}>No. </th>
                <th style={{width:'20%', textAlign:'center'}}>Nama</th>
                <th style={{width:'25%', textAlign:'center'}}>Link</th>
                <th style={{width:'25%', textAlign:'center'}}>Lokasi</th>
                <th style={{width:'5%', textAlign:'center'}}></th>
              </tr>
            </thead>
            <tbody>
            { form.map((item, index) => (
              <Row
                key={index}
                cctv={item}
                index={index}
                _onClickDeleteRow={_onClickDeleteRow}
                _onChangeCctv={_onChangeCctv} />
              ))
            }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default FormTable
