// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'

// Plugin Dependencies
import ReactTooltip from 'react-tooltip'

const Row = props => (
  <tr>
    <td style={{textAlign: 'center'}}>
      {props.index + 1}
    </td>
    <td style={{textAlign: 'center'}}>
      <input
        type="text"
        name="name"
        style={{width: '100%'}}
        className="form-control"
        value={props.cctv.name}
        placeholder="Nama CCTV"
        onChange={(e) => props._onChangeCctv(props.cctv.id, e)} />
    </td>
    <td style={{textAlign: 'center'}}>
      <input
        type="text"
        name="url"
        style={{width: '100%'}}
        className="form-control"
        value={props.cctv.url}
        placeholder="Link CCTV"
        onChange={(e) => props._onChangeCctv(props.cctv.id, e)} />
    </td>
    <td style={{textAlign: 'center'}}>
      <input
        type="text"
        name="latitude"
        style={{width: '100%'}}
        className="form-control"
        value={props.cctv.latitude}
        placeholder="Latitude"
        onChange={(e) => props._onChangeCctv(props.cctv.id, e)} />
      <input
        type="text"
        name="longitude"
        style={{width: '100%', marginTop: 10}}
        className="form-control"
        value={props.cctv.longitude}
        placeholder="Longitude"
        onChange={(e) => props._onChangeCctv(props.cctv.id, e)} />
    </td>
    <td style={{textAlign: 'center'}}>
      <ReactTooltip id={"delete-daftar-cctv-baris-" + props.cctv.id} place={"bottom"} />
      <i
        data-for={"delete-daftar-cctv-baris-" + props.cctv.id}
        data-tip={"Hapus Baris No. " + Number(props.index + 1)}
        data-iscapture="true"
        style={{paddingLeft: 10, width: "100%"}}
        className="fa fa-trash fa-1-7x report-table-icon"
        onClick={() => props._onClickDeleteRow(props.cctv.id)}>
      </i>
    </td>
  </tr>
)

export default Row;
