// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Plugin Dependencies
import $ from 'jquery'
import { Checkbox } from 'react-icheck'
import classNames from 'classnames'

const checkboxStyle = { padding:'10px 5%', width:'10%', cursor:'pointer' }

class TableRole extends Component {
  static propTypes = {
    features: PropTypes.array.isRequired,
    access: PropTypes.array.isRequired,
    _onClick: PropTypes.func.isRequired,
    _onSubmit: PropTypes.func.isRequired,
    width: PropTypes.number,
  }

  componentDidMount(){
    $(document).ready(function() {
      $('#table-register-peran').DataTable({
        "iDisplayLength": 50,
        "bSort": false ,
        "retrieve": true,
      })
      $('#table-register-peran_length').last().css('display','none')
      $('#table-register-peran_wrapper .row').last().css('display','none')
      $("td:nth-child(2)").attr("style","text-align:left; padding-left:30px")
    })
  }

  _renderPenghuni(features, access, _onClick){
    const penghuniFeatures = features.filter((item) =>(item.isMobile))
    return penghuniFeatures.map((item, index) => (
      <tr key={index}>
        <td>{index + 1}</td>
        <td style={{textAlign:'left', paddingLeft:30}}><p>{item.name}</p></td>
        <td style={{textAlign:'left', paddingLeft:30}}><p>mobile</p></td>
        <td onClick={() => _onClick(item.id)} style={checkboxStyle}>
          <Checkbox
            checkboxClass="icheckbox_square-green"
            increaseArea="-100%"
            checked={access.find((d)=>(d===item.id)) ? true : false} />
        </td>
      </tr>
    ))
  }

  _renderPengelola(features, access, _onClick){
    return features.map((item, index) => (
      <tr key={index}>
        <td>{index + 1}</td>
        <td style={{textAlign:'left', paddingLeft:30}}><p>{item.name}</p></td>
        <td style={{textAlign:'left', paddingLeft:30}}><p>{item.name[0] === '/' ? "dashboard" : "mobile"}</p></td>
        <td onClick={() => _onClick(item.id)} style={checkboxStyle}>
          <Checkbox
            checkboxClass="icheckbox_square-green"
            increaseArea="-100%"
            checked={access.find((d)=>(d===item.id)) ? true : false} />
        </td>
      </tr>
    ))
  }

  render() {
    const { features, category, access, _onClick, _onSubmit, width, buttonDisable } = this.props

    return (
      <div className="row">
        <div className={classNames({"col-xs-10 col-xs-offset-1": width > 768 ? true : false, "col-xs-12": width < 768 ? true : false })}>
          <p style={{position: width > 768 ? "absolute" : "relative" }}>Hak Akses : </p>
          <table id="table-register-peran" className="table text-center">
            <thead>
              <tr>
                <th width="50">No. </th>
                <th style={{textAlign:'left', paddingLeft:30}}>Fungsi</th>
                <th style={{textAlign:'left', paddingLeft:30}}>Platform</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            { category === 'manager' ? this._renderPengelola(features, access, _onClick) :
              this._renderPenghuni(features, access, _onClick)
            }
            </tbody>
          </table>
          { buttonDisable ?
            <button
              disabled={true}
              className="btn btn-qluster-success btn-md pull-right"
              style={{width:200, marginTop: 25}}
              onClick={_onSubmit}>Harap Tunggu...
            </button> :
            <button
              className="btn btn-qluster-success btn-md pull-right"
              style={{width:200, marginTop: 25}}
              onClick={_onSubmit}>Kirim
            </button>
          }
        </div>
      </div>
    )
  }
}

export default TableRole
