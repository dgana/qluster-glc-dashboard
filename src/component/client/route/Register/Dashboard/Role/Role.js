// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'

// Plugin Dependencies
import classNames from 'classnames'
import Select from 'react-virtualized-select'

const Role = props => {
  return (
    <div>
      <div className="row">
        <div className={classNames({"col-xs-5 col-sm-2 col-xs-offset-1": props.width > 768 ? true : false, "col-xs-6": props.width < 768 ? true : false })}>
          Nama Peran:
        </div>
        <div
          className={classNames({"col-xs-6 col-sm-4": props.width > 768 ? true : false, "col-xs-6": props.width < 768 ? true : false })}
          style={{marginBottom: 10}}>
          <input
            type="text"
            name="name"
            id="rolename"
            className="form-control"
            value={props.role.name}
            placeholder="Masukkan peran baru"
            onChange={props._onChange} />
        </div>
      </div>
      <div className="row">
        <div className={classNames({"col-xs-5 col-sm-2 col-xs-offset-1": props.width > 768 ? true : false, "col-xs-6": props.width < 768 ? true : false })}>
          Kategori Peran:
        </div>
        <div
          className={classNames({"col-xs-6 col-sm-4": props.width > 768 ? true : false, "col-xs-6": props.width < 768 ? true : false })}
          style={{marginBottom: 10}}>
          <Select
            clearable={false}
            searchable={false}
            options={[{ label: "Pengelola", value: 'manager' },{ label: "Penghuni", value: 'resident' },]}
            onChange={props._selectOnChange.bind(this)}
            value={props.category} />
        </div>
      </div>
    </div>
  )
}

export default Role
