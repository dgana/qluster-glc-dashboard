// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

// RegisterRole component
import Role from './Role'
import Table from './Table'

// Common component
import Body from '../../../../common/Content/Body'
import Box from '../../../../common/Box'
import Header from '../../../../common/Box/Header'
import BoxBody from '../../../../common/Box/Body'
import Loading from '../../../../common/Loading'
import EmptyBox from '../../../../common/EmptyBox'

// Dispatcher
import { dispatchAddRole, dispatchGetFeatures, dispatchResetFeatures } from '../../../../../../dispatcher'

// Plugin Dependencies
import _ from 'lodash'
import popup from '../../../../../../../public/js/bootstrap-notify/popupMessage'
import ReactTooltip from 'react-tooltip'

class RegisterRole extends Component {
  constructor(props){
    super(props)
    this.state = {
      width: 0,
      role: {
        name: '',
        access: []
      },
      buttonDisable: false,
      category: 'manager',
    }
  }

  static propTypes = {
    pathList: PropTypes.array
  }

  componentDidMount() {
    this.props.dispatchGetFeatures()
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  _reloadFeatures = () => {
    this.props.dispatchResetFeatures()
    this.props.dispatchGetFeatures()
  }

  updateWindowDimensions = () => {
    this.setState({ width: window.innerWidth})
  }

  _onChange = (e) => {
    let newForm = Object.assign({}, this.state.role, {[e.target.name]: e.target.value})
    this.setState({ role: newForm })
  }

  _selectOnChange = (e) => {
    this.setState({ category:e.value })
  }

  _onClick = (id,i) => {
    const { role } = this.state
    const { name,access } = role
    const available = access.find((item)=>(item===id))

    if(available){
      const newAcces = access.filter(item =>(item!==id))
      const newRole = {name:name,access:newAcces}
      this.setState({role:newRole})
    }else{
      const newRole = {name:name,access:[...access,id]}
      this.setState({role:newRole})
    }
  }

  _onSubmit = () => {
    const { role,category } = this.state
    const { dispatchAddRole } = this.props

    if (role.name === "") {
      popup.message('pe-7s-attention',`<span class="alert-popup-text-message">Masukkan nama peran</span>`,'danger', 2000 ,'bottom','right')
    } else {
      this.setState({ buttonDisable: true })
      const newRole = {name:role.name,features:role.access,role:category}
      dispatchAddRole(newRole, this._callbackFromDispatcher)
    }
  }

  _callbackFromDispatcher = (err, result) => {
    if(result) {
      popup.message('pe-7s-like2',`<span class="alert-popup-text-message">Peran <p class="popup-text">${''}</p> berhasil didaftarkan dengan ${''} akses</span>`,'success', 2000 ,'bottom','right')
      this.setState({
        role: {
          name: '',
          access: []
        },
        buttonDisable: false
      })
    }else {
      popup.message('pe-7s-attention',`<span class="alert-popup-text-message">API Bermasalah! <p class="popup-text">${err.message}</p></span>`,'danger', 2000 ,'bottom','right')
      this.setState({
        buttonDisable: false
      })
    }
  }

  render() {
    const { role, width, buttonDisable,category } = this.state
    const { roleFeatures } = this.props
    return (
      <div>
      { roleFeatures.fetched ? (
        <Body>
          <Box size="col-md-12">
            <Header title="Daftar Peranan">
              <hr style={{marginTop:14, marginBottom:2}} />
              <ReactTooltip id="reload-daftar-peran" place={"bottom"} />
              <button
                data-for="reload-daftar-peran"
                data-tip="Reload Daftar Peran"
                data-iscapture="true"
                onClick={() => this._reloadFeatures()}
                type="submit"
                className="btn btn-qluster-primary pull-right"
                style={{position:'absolute', right:24, top:2, border:'none'}}>
                <span className="pe-7s-refresh" style={{paddingRight: 6}}></span>
                 Reload
              </button>
            </Header>
              <BoxBody>
                <Role
                  _onChange={this._onChange}
                  role={role}
                  category={category}
                  _selectOnChange={this._selectOnChange}
                  width={width} />
                <Table
                  category={category}
                  features={roleFeatures.features}
                  _onClick={this._onClick}
                  width={width}
                  _onSubmit={this._onSubmit}
                  access={role.access}
                  buttonDisable={buttonDisable} />
              </BoxBody>
          </Box>
        </Body>
        ) :
        <Loading>
          <EmptyBox />
        </Loading>
      }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    id: state.showClient.companyId,
    roleFeatures:state.roleFeatures
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchAddRole: (obj, cb) => dispatch(dispatchAddRole(obj, cb)),
    dispatchGetFeatures: () => dispatch(dispatchGetFeatures()),
    dispatchResetFeatures: () => dispatch(dispatchResetFeatures())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterRole)
