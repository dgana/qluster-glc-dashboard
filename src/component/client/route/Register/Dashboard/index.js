// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

// Common component
import DashboardLayout from '../../../common/DashboardLayout'
import Wrapper from '../../../common/Content/Wrapper'
import Body from '../../../common/Content/Body'
import Loading from '../../../common/Loading'
import EmptyBox from '../../../common/EmptyBox'

// RegisterDashboard Component
import RegisterRole from './Role'
import Lokasi from './Lokasi'
import RegisterCctv from './Cctv'

// Dispatcher
import { dispatchRoute, dispatchResetRoleOption } from '../../../../../dispatcher'

// Utility
import clientRoute from '../../../../../util/clientRoute'

// Plugin Dependencies
import _ from 'lodash'

class RegisterDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }

  static propTypes = {
    route: PropTypes.object,
    pathList: PropTypes.array
  }

  _reload = () => {
    this.setState({ loading: true })
  }

  _show = () => {
    this.setState({ loading: false })
  }

  componentDidMount() {
    this.props.dispatchRoute('client', clientRoute)
  }

  render() {
    const { route, pathList, cctvList } = this.props
    return (
      <DashboardLayout>
        <Wrapper>
        { _.has(route, 'pathname') ?
          route.pathname === '/daftar-peranan' ?
          <RegisterRole pathList={pathList} /> :
          route.pathname === '/daftar-cctv' ?
          <div>
          <Body>
            <RegisterCctv _reload={this._reload} />
          </Body>
          { !cctvList.fetched ?
            <Loading marginBottom="65%">
            </Loading> : null
          }
          </div> :
          <Body>
            <Lokasi />
          </Body> :
          <Loading>
            <EmptyBox />
          </Loading>
        }
        </Wrapper>
      </DashboardLayout>
    )
  }
}

const mapStateToProps = state => {
  return {
    route: state.router.location,
    pathList: state.path,
    cctvList: state.cctvList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchRoute: (role, route) => dispatch(dispatchRoute(role, route)),
    dispatchResetRoleOption: () => dispatch(dispatchResetRoleOption())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterDashboard)
