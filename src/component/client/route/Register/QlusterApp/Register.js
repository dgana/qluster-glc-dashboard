// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Bootstrap Popup Message
import popup from '../../../../../../public/js/bootstrap-notify/popupMessage'

// Plugin Dependencies
import FileReaderInput from 'react-file-reader-input'
import Workbook from 'react-excel-workbook'
import ReactTooltip from 'react-tooltip'

import { occupantData, managerData } from '../../../../../util/excelData'

const formStyle = {
  borderRadius: 2,
  border: '1px solid rgb(230,230,230)',
  boxShadow: '0 2px 6px 0 rgba(0,0,0,0.1)',
  marginBottom: 10,
  height:35,
}
const labelStyle = {
  paddingTop:6,
  width:'100%',
  cursor:'pointer',
  marginBottom: 30
}
const templateStyle = {
  cursor: 'pointer',
  paddingLeft: 5,
  fontSize: 11
}

class Register extends Component {
  static propTypes = {
    title: PropTypes.string,
    route: PropTypes.string,
    _onUpload: PropTypes.func,
    _reloadOccupantCode: PropTypes.func,
    _reloadManagerCode: PropTypes.func
  }

  handleChange = (e, results) => {
    let filename = results[0][1].name
    let result = results[0][0].target.result
    let object = { filename, result }
    this.props._onUpload(object)
    popup.message('pe-7s-check',`<span class="alert-popup-text-message">${filename} berhasil diupload</span>`,'success', 1000 ,'bottom','right')
  }

  render() {
    const { title, _onUpload, route, _reloadOccupantCode, _reloadManagerCode } = this.props
    return (
      <div>
        <h4 className="text-center" style={{marginBottom: 15}}>Daftarkan {title} Baru !</h4>
        <ReactTooltip id="reload-daftar-pengelola" place={"bottom"} />
        <ReactTooltip id="reload-daftar-penghuni" place={"bottom"} />
        <button
          data-for={route === '/daftar-penghuni' ? "reload-daftar-penghuni" : "reload-daftar-pengelola"}
          data-tip={route === '/daftar-penghuni' ? "Reload Kode Rumah & Peran" : "Reload Kode Divisi & Peran"}
          data-iscapture="true"
          onClick={() => route === '/daftar-penghuni' ? _reloadOccupantCode() : _reloadManagerCode()}
          type="submit"
          className="btn btn-qluster-primary pull-right"
          style={{position:'absolute', right:24, top:16, border:'none'}}>
          <span className="pe-7s-refresh" style={{marginRight: 8}}></span>
           Reload
        </button>
        <div className="row">
          <div className="col-sm-offset-3 col-sm-6 col-xs-offset-1 col-xs-10" style={formStyle}>
            <div className="form-group">
            <form>
             <label htmlFor="my-file-input" style={labelStyle}>Unggah File (.xlsx / .csv)</label>
             <FileReaderInput as="binary" id="my-file-input" value="" onChange={this.handleChange}>
               <button style={{display:"none"}}>Select a file!</button>
             </FileReaderInput>
           </form>
            </div>
          </div>
        </div>

        <div className="row" style={{marginBottom: 25}}>
          <div className="col-sm-offset-3 col-sm-6">
            <p className="text-center" style={{marginTop:8, fontSize:11}}>Belum punya template?
            { route === '/daftar-penghuni' ?
              <Workbook filename="penghuni.xlsx" element={<u style={templateStyle}>Unduh Sekarang</u>} >
                <Workbook.Sheet data={occupantData} name="Sheet1">
                  <Workbook.Column label="name" value="name"/>
                  <Workbook.Column label="email" value="email"/>
                  <Workbook.Column label="phone" value="phone"/>
                  <Workbook.Column label="code" value="code"/>
                  <Workbook.Column label="status" value="status"/>
                </Workbook.Sheet>
              </Workbook> :
              <Workbook filename="pengelola.xlsx" element={<u style={templateStyle}>Unduh Sekarang</u>} >
                <Workbook.Sheet data={managerData} name="Sheet1">
                  <Workbook.Column label="name" value="name"/>
                  <Workbook.Column label="email" value="email"/>
                  <Workbook.Column label="phone" value="phone"/>
                  <Workbook.Column label="code" value="code"/>
                  <Workbook.Column label="status" value="status"/>
                </Workbook.Sheet>
              </Workbook>
            }
            </p>
          </div>
        </div>
      </div>
    )
  }
}

export default Register
