// React, React-Router, PropTypes & Redux Dependencies
import React from 'react';
import PropTypes from 'prop-types'

const Button = props => (
  <div className="row">
    <div className="col-xs-12" style={{marginTop: 12}}>

      <button className="btn btn-default btn-sm" style={{marginRight: 12}} onClick={props._onClickAddRow}>
        <span className="fa fa-plus" style={{marginRight: 8}}></span>
        Baris Baru
      </button>
      {props._Confirm}
      {
        props.route === '/daftar-penghuni' ? props._onSubmitPenghuni : props._onSubmitPengelola
      }

    </div>
  </div>
)

Button.propTypes = {
  _onClickAddRow: PropTypes.func.isRequired,
  _onSubmitPenghuni: PropTypes.node.isRequired,
  _onSubmitPengelola: PropTypes.node.isRequired,
  _Confirm: PropTypes.node.isRequired,
  route: PropTypes.string.isRequired
}

export default Button
