// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Plugin Dependencies
import $ from 'jquery'
import classNames from 'classnames'

// Table Component
import Row from './Row'

class Table extends Component {
  static propTypes = {
    formPenghuni: PropTypes.array,
    formPengelola: PropTypes.array,
    tablePenghuni: PropTypes.array,
    tablePengelola: PropTypes.array,
    managerCode: PropTypes.array,
    managerStatusCode: PropTypes.array,
    houseCode: PropTypes.array,
    houseStatusCode: PropTypes.array,
    _onChange: PropTypes.func,
    _onDeleteRow: PropTypes.func,
    route: PropTypes.string,
  }

  constructor(props) {
    super(props);
    this.state = {
      penghuniSelected: false,
      pengelolaSelected: false
    }
  }

  componentWillReceiveProps(nextProps) {
    const { route } = this.props
    if (route !== nextProps.route){
      if (route === '/daftar-penghuni'){
        this.setState({ pengelolaSelected: true, penghuniSelected: false })
      } else{
        this.setState({ penghuniSelected: true, pengelolaSelected: false})
      }
    }
  }

  componentDidMount(){
    const { route } = this.props
    $(document).ready(function() {
      $('#table-daftar-qluster').DataTable({ "bSort" : false })
      $('#table-daftar-qluster_wrapper .row').first().css('display','none')
      $('#table-daftar-qluster_wrapper .row').last().css('display','none')
    })
    if (route === '/daftar-penghuni'){
      this.setState({ penghuniSelected: true, pengelolaSelected: false })
    } else {
      this.setState({ pengelolaSelected: true, penghuniSelected: false })
    }
  }

  render() {

    const {
      penghuniSelected,
      pengelolaSelected
    } = this.state

    const {
      formPenghuni,
      formPengelola,
      tablePenghuni,
      tablePengelola,
      managerCode,
      managerStatusCode,
      houseCode,
      houseStatusCode,
      _onChange,
      _onDeleteRow,
      route
    } = this.props

    return (
      <div className="row">
        <div className="col-xs-12">
          <table
            id="table-daftar-qluster"
            className={
              classNames("table", "text-center",
              { daftarPenghuniTable:  penghuniSelected },
              { daftarPengelolaTable: pengelolaSelected } )
            }>
            <thead>
            { route === '/daftar-penghuni' ? (
              <tr>
                { tablePenghuni.map((item, index) => <th key={index}>{item}</th> ) }
              </tr>
              ) : (
              <tr>
                { tablePengelola.map((item, index) => <th key={index}>{item}</th> ) }
              </tr>
              )
            }
            </thead>
            <tbody>
            { route === '/daftar-penghuni' ?
              formPenghuni.map((item, index) => (
                <Row
                  key={index}
                  route={route}
                  penghuni={item}
                  houseStatusCode={houseStatusCode}
                  index={index}
                  options={houseCode}
                  _onChange={(id, e) => _onChange(id, e)}
                  _onDeleteRow={(id) => _onDeleteRow(id)} />
              )) :
              formPengelola.map((item, index) => (
                <Row
                  key={index}
                  route={route}
                  pengelola={item}
                  managerStatusCode={managerStatusCode}
                  index={index}
                  options={managerCode}
                  _onChange={(id, e) => _onChange(id, e)}
                  _onDeleteRow={(id) => _onDeleteRow(id)}
                  className="daftarPengelolaTable" />
              ))
            }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default Table
