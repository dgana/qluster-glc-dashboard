// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Plugin Dependencies
import Select from 'react-virtualized-select'
import ReactTooltip from 'react-tooltip'

const selectStyle = {
  textAlign:'left',
  cursor:'pointer'
}

class Row extends Component {
  render() {

    const {
      penghuni,
      pengelola,
      houseStatusCode,
      managerStatusCode,
      _onChange,
      _onDeleteRow,
      index,
      options,
      route
    } = this.props

    const statusOptions = route === '/daftar-penghuni' ?
    houseStatusCode.map(data =>({
      value: data.label,
      label: data.label,
      name: 'status'
    })) :
    managerStatusCode.map(data =>({
      value: data.label,
      label: data.label,
      name: 'status'
    }))

    const row = route === '/daftar-penghuni' ? penghuni : pengelola

    return (
      <tr>
        <td style={{width: '1%'}}>{index + 1}</td>
        <td style={{width: '20%'}}>
          <input
            type="name"
            style={{width: '100%'}}
            value={row.name === undefined ? "" : row.name}
            className="form-control"
            name="name"
            onChange={(e) => _onChange({ id:row.id },  e)} />
        </td>
        <td style={{width: '20%'}}>
          <input
            type="email"
            style={{width: '100%'}}
            value={row.email === undefined ? "" : row.email}
            className="form-control"
            name="email"
            onChange={(e) => _onChange({ id:row.id },  e)} />
        </td>
        <td style={{width: '18%'}}>
          <input
            type="text"
            style={{width: '100%'}}
            value={row.phone === undefined ? "" : row.phone}
            className="form-control"
            name="phone"
            onChange={(e) => _onChange({ id:row.id },  e)} />
        </td>

        { route === '/daftar-penghuni' ? (
          <td style={{width: '15%'}}>
          <Select
            name="code"
            style={selectStyle}
            value={row.code}
            placeholder="Kode"
            options={options}
            clearable={false}
            onChange={(e) => _onChange({ id: row.id, name: 'code' }, e)} />
          </td>
          ) : (
            <td style={{width: '15%'}}>
            <Select
              name="code"
              style={selectStyle}
              value={row.code}
              placeholder="Kode..."
              options={options}
              clearable={false}
              onChange={(e) => _onChange({ id: row.id, name: 'code' }, e)} />
            </td>
            )
        }

        { route === '/daftar-penghuni' ? (
          <td style={{width: '5%'}}>
            <Select
              name="status"
              style={selectStyle}
              value={row.status}
              placeholder="Peran..."
              options={statusOptions}
              clearable={false}
              onChange={(e) => _onChange({ id: row.id, name: 'status' }, e )} />
          </td>
          ) : (
            <td style={{width: '5%'}}>
              <Select
                name="status"
                style={selectStyle}
                value={row.status}
                placeholder="Peran..."
                options={statusOptions}
                clearable={false}
                onChange={(e) => _onChange({ id: row.id, name: 'status' }, e )} />
            </td>
            )
        }
        <td style={{width: '1%'}}>
          <ReactTooltip id={"delete-row-daftar-pengelola-" + row.id} place={"bottom"} />
          <ReactTooltip id={"delete-row-daftar-penghuni-" + row.id} place={"bottom"} />
          <i
            data-for={route === '/daftar-penghuni' ? "delete-row-daftar-penghuni-"+row.id : "delete-row-daftar-pengelola-"+row.id}
            data-tip={"Hapus Baris No. " + row.id}
            data-iscapture="true"
            style={{paddingLeft: 10, width: "100%"}}
            className="fa fa-trash fa-1-7x report-table-icon"
            onClick={() => _onDeleteRow(row.id)}>
          </i>
        </td>
      </tr>
    )
  }
}

Row.propTypes = {
  penghuni: PropTypes.object,
  pengelola: PropTypes.object,
  options: PropTypes.array.isRequired,
  route: PropTypes.string.isRequired,
  _onChange: PropTypes.func.isRequired,
  _onDeleteRow: PropTypes.func.isRequired
}

export default Row
