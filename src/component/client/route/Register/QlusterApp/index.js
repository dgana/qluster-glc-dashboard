// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

// Common component
import DashboardLayout from '../../../common/DashboardLayout'
import Wrapper from '../../../common/Content/Wrapper'
import Body from '../../../common/Content/Body'
import Box from '../../../common/Box'
import BoxBody from '../../../common/Box/Body'
import Loading from '../../../common/Loading'
import EmptyBox from '../../../common/EmptyBox'

// Daftar Component
import Register from './Register'
import Table from './Table/'
import Button from './Button'

// Plugin Dependencies
import Scroll from 'react-scroll'
import xlsx from 'xlsx'
import Confirm from 'react-confirm-bootstrap'
const scroll = Scroll.animateScroll
import popup from '../../../../../../public/js/bootstrap-notify/popupMessage'
import $ from 'jquery'
import _ from 'lodash'

// Dispatcher
import {
  dispatchAddOccupant,
  dispatchAddManager,
  dispatchOccupantCode,
  dispatchManagerCode,
  dispatchResetManagerCode,
  dispatchResetOccupantCode
} from '../../../../../dispatcher'

let currentState = {
  formPenghuni: [
    {
      id: 1,
      name: '',
      email: '',
      phone: '',
      code: '',
      status: ''
    }
  ],
  formPengelola: [
    {
      id: 1,
      name: '',
      email: '',
      phone: '',
      code: '',
      status: ''
    }
  ],
  buttonDisable: false,
  tablePenghuni: ["No.", "Nama", "ID/Email", "No. Ponsel", "Kode Rumah", "Peran", "Aksi"],
  tablePengelola: ["No.", "Nama", "ID/Email", "No. Ponsel", "Divisi","Peran", "Aksi"],
}

class Pendaftaran extends Component {
  constructor(props) {
    super(props);
    this.state = currentState
  }

  static propTypes = {
    path: PropTypes.object.isRequired // Pathname Props
  }

  componentWillUnmount() {
    currentState = this.state
  }

  componentDidMount() {
    this.props.dispatchOccupantCode(this._occupantCodeCallback)
    this.props.dispatchManagerCode(this._managerCodeCallback)
  }

  _reloadOccupantCode = () => {
    this.props.dispatchResetOccupantCode()
    this.props.dispatchOccupantCode(this._occupantCodeCallback)
  }

  _occupantCodeCallback = (err, result) => {
    if (!result)
      popup.message('pe-7s-attention',`<span class="alert-popup-text-message">API kode rumah sedang mengalami gangguan</span>`,'danger', 5000 ,'bottom','right')
  }

  _reloadManagerCode = () => {
    this.props.dispatchResetManagerCode()
    this.props.dispatchManagerCode(this._managerCodeCallback)
  }

  _managerCodeCallback = (err, result) => {
    if (!result)
      popup.message('pe-7s-attention',`<span class="alert-popup-text-message">API kode Divisi sedang mengalami gangguan</span>`,'danger', 5000 ,'bottom','right')
  }

  _onSubmitForm = () => {
    const { path, dispatchAddOccupant, dispatchAddManager } = this.props
    const { formPenghuni, formPengelola } = this.state

    if (path.title === 'Penghuni') {
      const filteredName = formPenghuni.filter(item => item.name.length === 0)
      const filteredEmail = formPenghuni.filter(item => item.email.length === 0)
      const filteredPhone = formPenghuni.filter(item => item.phone.length === 0)
      const filteredCode = formPenghuni.filter(item => item.code.length === 0)
      const filteredStatus = formPenghuni.filter(item => item.status.length === 0)
      let html = '<h5>Harap lengkapi form pendaftaran dibawah ini: </h5><ul>'

      if(filteredName.length !== 0) {
        formPenghuni.map((item, index) => {
          if (item.name === '') {
            html += `<li> Nama - Baris ${index + 1}</li>`
          }
          return html
        })
        popup.message('', html+'</ul>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else if (filteredEmail.length !== 0) {
        formPenghuni.map((item, index) => {
          if (item.email === '') {
            html += `<li> ID/Email - Baris ${index + 1}</li>`
          }
          return html
        })
        popup.message('', html+'</ul>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else if (filteredPhone.length !== 0) {
        formPenghuni.map((item, index) => {
          if (item.phone === '') {
            html += `<li> No. Ponsel - Baris ${index + 1}</li>`
          }
          return html
        })
        popup.message('', html+'</ul>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else if (filteredCode.length !== 0) {
        formPenghuni.map((item, index) => {
          if (item.code === '') {
            html += `<li> Kode Rumah - Baris ${index + 1}</li>`
          }
          return html
        })
        popup.message('', html+'</ul>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else if (filteredStatus.length !== 0) {
        formPenghuni.map((item, index) => {
          if (item.status === '') {
            html += `<li> Peran - Baris ${index + 1}</li>`
          }
          return html
        })
        popup.message('', html+'</ul>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else {
        // formPenghuni.forEach(x => {delete x.id delete x.message})
        this.setState({ buttonDisable: true })
        return dispatchAddOccupant(formPenghuni, this._callbackFromDispatcherPenghuni)
      }
    } else {
      const filteredName = formPengelola.filter(item => item.name.length === 0)
      const filteredEmail = formPengelola.filter(item => item.email.length === 0)
      const filteredPhone = formPengelola.filter(item => item.phone.length === 0)
      const filteredCode = formPengelola.filter(item => item.code.length === 0)
      const filteredStatus = formPengelola.filter(item => item.status.length === 0)

      let html = '<h5>Harap lengkapi form pendaftaran dibawah ini: </h5><ul>'

      if (filteredName.length !== 0) {
        formPengelola.map((item, index) => {
          if (item.name === '') {
            html += `<li> Nama - Baris ${index + 1}</li>`
          }
          return html
        })
        popup.message('', html+'</ul>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else if (filteredEmail.length !== 0) {
        formPengelola.map((item, index) => {
          if (item.email === '') {
            html += `<li> ID/Email - Baris ${index + 1}</li>`
          }
          return html
        })
        popup.message('', html+'</ul>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else if (filteredPhone.length !== 0) {
        formPengelola.map((item, index) => {
          if (item.phone === '') {
            html += `<li> No. Ponsel - Baris ${index + 1}</li>`
          }
          return html
        })
        popup.message('', html+'</ul>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else if (filteredCode.length !== 0) {
        formPengelola.map((item, index) => {
          if (item.code === '') {
            html += `<li> Kode Divisi - Baris ${index + 1}</li>`
          }
          return html
        })
        popup.message('', html+'</ul>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else if (filteredStatus.length !== 0) {
        formPengelola.map((item, index) => {
          if (item.status === '') {
            html += `<li> Peran - Baris ${index + 1}</li>`
          }
          return html
        })
        popup.message('', html+'</ul>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else {
        formPengelola.forEach(x => delete x.id)
        this.setState({ buttonDisable: true })
        return dispatchAddManager(formPengelola, this._callbackFromDispatcherPengelola)
      }
    }
  }

  _callbackFromDispatcherPenghuni = (err, result) => {
    if (result) {
      result.failed.forEach((obj, index) => obj.id = index + 1 )
      if (result.failed.length === 0) {
        this.setState({
          buttonDisable: false,
          formPenghuni: [
            {
              id: 1,
              name: '',
              email: '',
              phone: '',
              code: '',
              status: ''
            }
          ]
        })
      } else {
        this.setState({
          buttonDisable: false,
          formPenghuni: result.failed
        })
      }

      if(result.failed.length === 0) {
        popup.message('pe-7s-like2',`<span class="alert-popup-text-message">Penghuni berhasil didaftarkan</span>`,'success', 5000 ,'bottom','right')
      } else if (result.succeed.length === 0) {
        let html = '<h5>Gagal dalam pendaftaran: </h5><ol>'
        result.failed.map(item => {
          return html += `<li><span style="width:50%;display:inline-block">${item.email}:</span><em>${item.message}</em></li>`
        })
        popup.message('', html+'</ol>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else {
        let html = '<h5>Gagal dalam pendaftaran: </h5><ol>'
        result.failed.map(item => {
          return html += `<li><span style="width:50%;display:inline-block">${item.email}:</span> <em>${item.message}</em></li>`
        })
        popup.message('pe-7s-like2',`<span class="alert-popup-text-message">${result.succeed.length} penghuni berhasil didaftarkan</span>`,'success', 5000 ,'bottom','right')
        popup.message('', html+'</ol>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      }
    } else {
      console.log(err);
    }
  }

  _callbackFromDispatcherPengelola = (err, result) => {
    if (result) {
      result.failed.forEach((obj, index) => obj.id = index + 1 )
      if (result.failed.length === 0) {
        this.setState({
          buttonDisable: false,
          formPengelola: [
            {
              id: 1,
              name: '',
              email: '',
              phone: '',
              code: '',
              status: ''
            }
          ]
        })
      } else {
        this.setState({
          buttonDisable: false,
          formPengelola: result.failed
        })
      }

      if(result.failed.length === 0) {
        popup.message('pe-7s-like2',`<span class="alert-popup-text-message">${result.succeed.length} pengelola berhasil didaftarkan</span>`,'success', 5000 ,'bottom','right')
      } else if (result.succeed.length === 0) {
        let html = '<h5>Gagal dalam pendaftaran: </h5><ol>'
        result.failed.map(item => {
          return html += `<li>${item.email}: &nbsp&nbsp<em>${item.message}</em></li>`
        })
        popup.message('', html+'</ol>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      } else {
        let html = '<h5>Gagal dalam pendaftaran: </h5><ol>'
        result.failed.map(item => {
          return html += `<li>${item.email}: &nbsp&nbsp<em>${item.message}</em></li>`
        })
        popup.message('pe-7s-like2',`<span class="alert-popup-text-message">${result.succeed.length} pengelola berhasil didaftarkan</span>`,'success', 5000 ,'bottom','right')
        popup.message('', html+'</ol>','danger', 10000 ,'top','right')
        $('.close').css({ top: '15px', fontSize: '35px' })
      }
    } else {
      console.log(err);
    }
  }

  _onClickAddRow = (e) => {
    const { formPenghuni, formPengelola } = this.state
    const { path } = this.props

    if (path.title === 'Penghuni') {
      if(this.state.formPenghuni.length === 0) {
        this.setState({
          formPenghuni: [
            {
              id: 1,
              name: '',
              email: '',
              phone: '',
              code: '',
              status: ''
            }
          ]
        })
      } else {
        this.setState(prevState => ({
          formPenghuni: [...formPenghuni,
            {
              id: prevState.formPenghuni[prevState.formPenghuni.length-1].id + 1,
              name: '',
              email: '',
              phone: '',
              code: '',
              status: ''
            }
          ]
        }))
      }
    } else {
      if(this.state.formPengelola.length === 0) {
        this.setState({
          formPengelola: [
            {
              id: 1,
              name: '',
              email: '',
              phone: '',
              code: '',
              status:''
            }
          ]
        })
      } else {
        this.setState(prevState => ({
          formPengelola: [
            ...formPengelola,
            {
              id: prevState.formPengelola[prevState.formPengelola.length-1].id + 1,
              name: '',
              email: '',
              phone: '',
              code: '',
              status:''
            }
          ]
        }))
      }
    }
  }

  _onChange = (obj, e) => {
    const { formPenghuni, formPengelola } = this.state
    const { path } = this.props

    // Find the the index with the current _onChange id
    if(path.title === 'Penghuni') {
      const index = formPenghuni.findIndex(x => x.id === obj.id)
      // Handle the select option changes for penghuni
      if (obj.name === 'status') {
        let newForm = [
          ...formPenghuni.slice(0, index),
          Object.assign({}, formPenghuni[index], { status: e.value, id: obj.id}),
          ...formPenghuni.slice(index + 1)
        ]

        this.setState({
          formPenghuni: newForm
        })
      } else if (obj.name === 'code') {
        let newForm = [
          ...formPenghuni.slice(0, index),
          Object.assign({}, formPenghuni[index], { code: e.value, id: obj.id}),
          ...formPenghuni.slice(index + 1)
        ]

        this.setState({
          formPenghuni: newForm
        })

      } else {

        let newForm = [
          ...formPenghuni.slice(0, index),
          Object.assign({}, formPenghuni[index], {[e.target.name]: e.target.value, id: obj.id}),
          ...formPenghuni.slice(index + 1)
        ]

        this.setState({
          formPenghuni: newForm
        })
      }
    } else {
      const index = formPengelola.findIndex(x => x.id === obj.id)
      // Handle the select option changes for penghuni
      if (obj.name === 'status') {
        let newForm = [
          ...formPengelola.slice(0, index),
          Object.assign({}, formPengelola[index], { status: e.value, id: obj.id}),
          ...formPengelola.slice(index + 1)
        ]

        this.setState({
          formPengelola: newForm
        })
      } else if (obj.name === 'code') {
        let newForm = [
          ...formPengelola.slice(0, index),
          Object.assign({}, formPengelola[index], { code: e.value, id: obj.id}),
          ...formPengelola.slice(index + 1)
        ]

        this.setState({
          formPengelola: newForm
        })
      } else {

        let newForm = [
          ...formPengelola.slice(0, index),
          Object.assign({}, formPengelola[index], {[e.target.name]: e.target.value, id: obj.id}),
          ...formPengelola.slice(index + 1)
        ]

        this.setState({
          formPengelola: newForm
        })
      }
    }
  }

  _onDeleteRow = id => {
    const { formPenghuni, formPengelola } = this.state
    const { path } = this.props
    if(path.title === 'Penghuni') {
      if(formPenghuni.length === 1) {
        this.setState({
          formPenghuni: [
            {
              id: 1,
              name: '',
              email: '',
              phone: '',
              code: '',
              status: ''
            }
          ]
        })
      } else {
        this.setState(prevState => ({
          formPenghuni: prevState.formPenghuni.filter(item => item.id !== id)
        }))
      }
    } else {
      if(formPengelola.length === 1) {
        this.setState({
          formPengelola: [
            {
              id: 1,
              name: '',
              email: '',
              phone: '',
              code: '',
              status: ''
            }
          ]
        })
      } else {
        this.setState(prevState => ({
          formPengelola: prevState.formPengelola.filter(item => item.id !== id)
        }))
      }
    }
  }

  _onUpload = file => {
    const { formPenghuni, formPengelola } = this.state
    const { path } = this.props
    const { filename, result } = file

    // Scroll to bottom after upload file
    scroll.scrollToBottom()

    // If the filetype xlsx
    const workbook = xlsx.read(btoa(result), {type: 'base64'})
    let xlsxObjects = xlsx.utils.sheet_to_json(workbook.Sheets.Sheet1)

    // If the filetype csv
    // Trim and Split file
    let arr = result.trim().split('\n')
    let array = []
    arr.forEach(item => {
      array.push(item.split('|'))
    })

    // Convert Multidimensional array into array of objects
    let keys = array.shift();
    let csvObjects = array.map(values => {
      return keys.reduce((o, k, i) => {
        o[k] = values[i]
        return o
      }, {})
    })

    // Check the filetype xlsx / csv and insert into arrayObjectFile
    const getFileExt = filename.slice(filename.length-3, filename.length)
    const arrayObjectFile = getFileExt === 'csv' ? csvObjects : xlsxObjects

    if(path.title === 'Penghuni') {
      // Add ID to the array of objects and UPPERCASE code with conditional
      const newArrayObjectFile =  arrayObjectFile.map((item, index) => {
        return {
          ...item,
          id: formPenghuni[formPenghuni.length - 1].id + (index + 1),
          code: item.code.toUpperCase()
        }
      })
      // Filter the previous state array whether the form is empty or not
      const newFilteredArray = formPenghuni.filter(x => x.email && x.phone && x.status && x.code)
      // If the newFilteredArray are exist then spread it with the new uploaded file
      if(newFilteredArray) {
        this.setState(prevState => ({
          formPenghuni: [...newFilteredArray, ...newArrayObjectFile]
        }))
      } else {
        this.setState(prevState => ({
          formPenghuni: [...newArrayObjectFile]
        }))
      }
    } else {
      // Add ID to the array of objects and UPPERCASE code with conditional
      const newArrayObjectFile =  arrayObjectFile.map((item, index) => {
        return {
          ...item,
          id: formPengelola[formPengelola.length - 1].id + (index + 1),
          code: item.code,
          status: item.status
        }
      })
      // Filter the previous state array whether the form is empty or not
      const newFilteredArray = formPengelola.filter(x => x.email && x.code && x.phone &&x.status)
      // If the newFilteredArray are exist then spread it with the new uploaded file
      if(newFilteredArray) {
        this.setState(prevState => ({
          formPengelola: [...newFilteredArray, ...newArrayObjectFile]
        }))
      } else {
        this.setState(prevState => ({
          formPengelola: [...newArrayObjectFile]
        }))
      }
    }
  }

  _onConfirmDeleteAllRow = () => {
    const { path } = this.props
    if(path.title === 'Penghuni') {
      this.setState({
        formPenghuni: [
          {
            id: 1,
            name: '',
            email: '',
            phone: '',
            code: '',
            status: ''
          }
        ]
      })
    } else {
      this.setState({
        formPengelola: [
          {
            id: 1,
            name: '',
            email: '',
            phone: '',
            code: '',
            status: ''
          }
        ]
      })
    }
  }

  render() {
    const { formPenghuni, formPengelola, tablePenghuni, tablePengelola, buttonDisable } = this.state
    const { path, houseCode, managerCode, route } = this.props
        console.log('kode rumah',houseCode);
    return (
      <DashboardLayout>
        <Wrapper>
        { _.has(route, 'pathname') ?
          !managerCode.fetched || !houseCode.fetched ?

          <Loading>
            <EmptyBox />
          </Loading> :

          <Body>
            <Box size="col-md-12">
              <BoxBody>
                <Register
                  _reloadOccupantCode={this._reloadOccupantCode}
                  _reloadManagerCode={this._reloadManagerCode}
                  title={route.pathname === '/daftar-pengelola' ? 'Pengelola' : 'Penghuni'}
                  _onUpload={(file) => this._onUpload(file)}
                  route={route.pathname} />
                <Table
                  formPenghuni={formPenghuni}
                  formPengelola={formPengelola}
                  tablePenghuni={tablePenghuni}
                  tablePengelola={tablePengelola}
                  houseCode={houseCode.result}
                  houseStatusCode={houseCode.resultStatus}
                  managerCode={managerCode.result}
                  managerStatusCode={managerCode.resultStatus}
                  divisionCode={managerCode.divisionCode}
                  route={route.pathname}
                  _onDeleteRow={(id) => this._onDeleteRow(id)}
                  _onChange={(id, e) => this._onChange(id, e)} />
                <Button
                  route={route.pathname}
                  _onClickAddRow={this._onClickAddRow}
                  _onClickDeleteAllRow={this._onClickDeleteAllRow}
                  _onSubmitPenghuni={
                    <Confirm
                      onConfirm={this._onSubmitForm}
                      body="Daftarkan Penghuni"
                      confirmText="Daftarkan"
                      cancelText="Batal"
                      confirmBSStyle="success"
                      title={'Pendaftaran Penghuni'}>
                      { buttonDisable ?
                        <button
                          disabled={true}
                          style={{width: 150}}
                          type="submit"
                          className="btn btn-qluster-success pull-right margin-top-register-submit">
                          Harap Tunggu...
                        </button> :
                        <button
                          type="submit"
                          className="btn btn-qluster-success pull-right margin-top-register-submit">
                          Daftarkan
                        </button>
                      }
                    </Confirm>
                  }
                  _onSubmitPengelola={
                    <Confirm
                      onConfirm={this._onSubmitForm}
                      body="Daftarkan Pengelola"
                      confirmText="Daftarkan"
                      cancelText="Batal"
                      confirmBSStyle="success"
                      title={'Pendaftaran Pengelola'}>
                      {
                        buttonDisable ?
                        <button
                          type="submit"
                          disabled={true}
                          style={{width: 150}}
                          className="btn btn-qluster-success pull-right margin-top-register-submit">
                          Harap Tunggu...
                        </button> :
                        <button
                          type="submit"
                          className="btn btn-qluster-success pull-right margin-top-register-submit">
                          Daftarkan
                        </button>
                      }

                    </Confirm>
                  }
                  _Confirm={
                    <Confirm
                      onConfirm={this._onConfirmDeleteAllRow}
                      body="Hapus Seluruh Baris?"
                      confirmText="Hapus"
                      cancelText="Keluar"
                      title={path.title === 'Penghuni' ? 'Pendaftaran Penghuni' : 'Pendaftaran Pengelola'}>
                      <button className="btn btn-default btn-sm" style={{marginLeft: 15}}>
                        <span className="fa fa-refresh" style={{marginRight: 8}}></span>
                        Hapus Semua Baris
                      </button>
                    </Confirm>
                  } />
              </BoxBody>
            </Box>
          </Body> :

          <Loading>
            <EmptyBox />
          </Loading>
        }
        </Wrapper>
      </DashboardLayout>
    )
  }
}

const mapStateToProps = state => {
  return {
    path: state.sidebar,
    id: state.showClient.companyId,
    houseCode: state.houseCode,
    houseStatusCode: state.houseStatusCode,
    managerCode: state.managerCode,
    managerStatusCode: state.managerStatusCode,
    route: state.router.location,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchOccupantCode: (cb) => dispatch(dispatchOccupantCode(cb)),
    dispatchManagerCode: (cb) => dispatch(dispatchManagerCode(cb)),
    dispatchAddOccupant: (occupant, cb) => dispatch(dispatchAddOccupant(occupant, cb)),
    dispatchAddManager: (manager, cb) => dispatch(dispatchAddManager(manager, cb)),
    dispatchResetManagerCode: () => dispatch(dispatchResetManagerCode()),
    dispatchResetOccupantCode: () => dispatch(dispatchResetOccupantCode())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pendaftaran);
