// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Common Component
import Box from '../../common/Box'
import Body from '../../common/Box/Body'

//Dependencies
import popup from '../../../../../public/js/bootstrap-notify/popupMessage'

// Dispather
import { dispatchChangePassword } from '../../../../dispatcher'

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: {
        oldPassword: '',
        newPassword: '',
        confirmNewPassword: ''
      },
    }
  }

  submit = () => {
    const { input } = this.state
    const newForm = input
    if (input.oldPassword === "" || input.newPassword === "" || input.confirmNewPassword === ""){
      popup.message('pe-7s-attention', `<span class="alert-popup-text-message">Field tidak boleh kosong</span>`, 'danger', 1000, 'bottom', 'right')
    } else if (input.newPassword !== input.confirmNewPassword) {
      popup.message('pe-7s-attention', `<span class="alert-popup-text-message">Password baru dan konfirmasi password baru harus sama</span>`, 'danger', 1000, 'bottom', 'right')
    } else {
      this.props.dispatchChangePassword(newForm, this._callbackDispatchChangePassword)
    }
  }

  _callbackDispatchChangePassword = (err, result) => {
    if(result) {
      popup.message('pe-7s-like2', `<span class="alert-popup-text-message">${result.message}</span>`, 'success', 1000, 'bottom', 'right')
      this.props._changeLoadingState()
      this.props._changeLoadingState()
    } else {
      popup.message('pe-7s-attention', `<span class="alert-popup-text-message">${err.result.message}</span>`, 'danger', 1000, 'bottom', 'right')
    }

  }

  changeInput(field, value) {
    const newInput = this.state.input;
    newInput[field] = value;
    this.setState({ input: newInput });
  }

  render() {
    const labelStyle = {
      fontSize: 10
    }
    const formGroupStyle = {
      marginBottom: 5,
      textAlign:'left'
    }
    const buttonStyle = {
      width: 'auto',
      height: 25,
      fontSize: 15,
      paddingTop: 1,
      marginTop: 20
    }
    return (
      <Box size={this.props.size} padding={0} display={this.props.display}>
        <Body padding="16px 20px">
          <div className="row">
            <div className="col-xs-12" style={{marginTop:20, display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
              <div className="form-group" style={formGroupStyle}>
                <label style={labelStyle}>Password Lama</label>
                <input type="password" className="form-control" value={ this.state.input.oldPassword } onChange={(evt) => this.changeInput('oldPassword', evt.target.value)} />
              </div>
              <div className="form-group" style={formGroupStyle}>
                <label style={labelStyle}>Password Baru</label>
                <input type="password" className="form-control" value={ this.state.input.newPassword } onChange={(evt) => this.changeInput('newPassword', evt.target.value)} />
              </div>
              <div className="form-group" style={formGroupStyle}>
                <label style={labelStyle}>Konfirmasi Password Baru</label>
                <input type="password" className="form-control" value={ this.state.input.confirmNewPassword } onChange={(evt) => this.changeInput('confirmNewPassword', evt.target.value)} />
              </div>
              <button className="btn btn-default" style={buttonStyle} onClick={this.submit}>Ganti Password</button>
            </div>
          </div>
        </Body>
      </Box>
    )
  }
}

const mapStateToProps = state => {
  return {
    client: state.showClient
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchChangePassword: (form, cb) => dispatch(dispatchChangePassword(form, cb))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
