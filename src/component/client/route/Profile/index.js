// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Common Component
import DashboardLayout from '../../common/DashboardLayout'
import Wrapper from '../../common/Content/Wrapper'
import Body from '../../common/Content/Body'
import Loading from '../../common/Loading'

import Content from './Content'
import EditProfile from './EditProfile'
import ChangePassword from './ChangePassword'

// Dependencies
import _ from 'lodash'

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }

  _changeLoadingState = () => {
    this.setState(prevState => ({
      loading: prevState.loading ? false : true
    }))
  }

  render() {
    const { loading } = this.state
    const { route } = this.props
    return (
      <DashboardLayout>
        <Wrapper>
          {
            _.has(route, 'pathname') ?
            route.pathname === "/profile" ?
            <Body>
              <Content size="col-md-12" />
            </Body> :
            route.pathname === "/edit-profile" ?
            loading ?
            <Loading>
              <EditProfile display="none" _changeLoadingState={this._changeLoadingState} size="col-md-12" />
            </Loading> :
            <Body>
              <EditProfile display="block" _changeLoadingState={this._changeLoadingState} size="col-md-12" />
            </Body> :
            loading ?
            <Loading>
              <ChangePassword display="none" _changeLoadingState={this._changeLoadingState} size="col-md-12" />
            </Loading> :
            <Body>
              <ChangePassword display="block" _changeLoadingState={this._changeLoadingState} size="col-md-12" />
            </Body> :
            <Loading>
              <ChangePassword display="none" _changeLoadingState={this._changeLoadingState} size="col-md-12" />
            </Loading>
          }
        </Wrapper>
      </DashboardLayout>
    )
  }
}

const mapStateToProps = state => {
  return {
    route: state.router.location
  }
}

export default connect(mapStateToProps)(Profile);
