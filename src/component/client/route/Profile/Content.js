// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

// Common Component
import Box from '../../common/Box'
import Body from '../../common/Box/Body'

const formGroupStyle = {
  marginBottom: 5
}

const buttonStyle = {
  width: 90,
  height: 25,
  fontSize: 15,
  paddingTop: 1,
  marginTop: 20
}

class Content extends Component {
  render() {
    const { client } = this.props
    return (
      <Box size={this.props.size} padding={0}>
        <Body padding="16px 20px">
          <div className="row">
            <div className="col-xs-12" style={{marginTop:20, display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
            <img className='text-center' src={client.picture} alt="profile" style={{borderRadius: 100, width: 150, height:150, background:'white'}} />
              <h3>{client.name}</h3>
              <p className="text-center" style={{fontSize:16}}>{client.role}</p>
              <div className="form-group" style={{ ...formGroupStyle, marginTop: 6}}>
                <Link
                  to="/edit-profile">
                  <button className="btn btn-default" style={buttonStyle}>Sunting</button>
                </Link>
              </div>
            </div>
          </div>
        </Body>
      </Box>
    )
  }
}

const mapStateToProps = state => {
  return {
    client: state.client.user
  }
}

export default connect(mapStateToProps)(Content);
