// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Common Component
import Box from '../../common/Box'
import Body from '../../common/Box/Body'

// Dependencies
import { Link } from 'react-router-dom'
import Dropzone from 'react-dropzone'
import popup from '../../../../../public/js/bootstrap-notify/popupMessage'

// Dispather
import { dispatchEditProfile } from '../../../../dispatcher'

class EditProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      input: {
        name: '',
        picture: null,
        editDropClass: 'editBeforeDrop'
      },
    }
    this.submit = this.submit.bind(this )
  }

  componentDidMount() {
    const newInput = this.state.input
    newInput['name'] = this.props.client.name
    console.log(newInput);
    this.setState({ input: newInput })
  }

  onDrop = (imgFile) => {
    const newInput = this.state.input
    newInput.picture= imgFile
    newInput.editDropClass= 'editOndrop'
    this.setState({
      input: newInput
    })
  }

  changeInput(field, value) {
    const newInput = this.state.input;
    newInput[field] = value;
    this.setState({ input: newInput });
  }

  removePicture = () => {
    const newInput = this.state.input
    newInput.picture = null
    this.setState({ input: newInput })
  }

  submit = () => {
    const { input } = this.state
    const {client } = this.props
    const currentInput = {name:input.name,picture:client.picture}
    const newForm = input.picture ? input:currentInput
    this.props.dispatchEditProfile(newForm, this._callbackDispatchEditProfile)

  }

  _callbackDispatchEditProfile = (err, result) => {
    if(result) {
      if (result) {
        this.setState({
          showModal:false,
          input: {
            name:result.name,
            oldPassword: '',
            newPassword: '',
            confirmNewPassword: '',
            picture: result.picture,
            editDropClass: 'editBeforeDrop',
          }
        })
        setTimeout(() => this.props._changeLoadingState(), 2000)
        popup.message('pe-7s-like2', `<span class="alert-popup-text-message">Profil berhasil disunting</span>`, 'success', 1000, 'bottom', 'right')
      } else {
        this.setState({
          showModal:false,
          input: {
            name: this.props.client.name,
            oldPassword: '',
            newPassword: '',
            confirmNewPassword: '',
            picture: this.props.client.picture,
            editDropClass: 'editBeforeDrop'
          }
        })
        setTimeout(() => this.props._changeLoadingState(), 2000)
        popup.message('pe-7s-attention', `<span class="alert-popup-text-message">Profil gagal disunting</span>`, 'danger', 1000, 'bottom', 'right')
      }
    } else {
      console.log(err);
    }
  }

  render() {
    const { client, size } = this.props
    const { input } = this.state
    const labelStyle = {
      fontSize: 10
    }
    const formGroupStyle = {
      marginBottom: 5
    }
    const buttonStyle = {
      width: 90,
      height: 25,
      fontSize: 15,
      paddingTop: 1,
      marginTop: 10
    }
    const buttonStyleLeft = {
      marginRight: 27,
      width: 90,
      height: 25,
      fontSize: 15,
      paddingTop: 1,
      marginTop: 10
    }
    return (
      <Box size={size} padding={0} display={this.props.display}>
        <Body padding="16px 20px">
          <div className="row">
            <div className="col-xs-12" style={{marginTop:20, display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
              {
                input.picture ?
                <div style={{display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                  <img src={input.picture[0].preview} alt="profile" style={{borderRadius: 100, width: 100, height:100, objectFit:'contain' }} />
                  <button className="removePicture" onClick={this.removePicture}>Hapus Foto</button>
                </div> :
                <Dropzone
                  style={{backgroundImage: `url('${client.picture}')`, backgroundSize:'cover', opacity:1}}
                  multiple={false}
                  onDrop={this.onDrop}
                  className="editDropZone"
                  activeClassName='active-dropzone text-center'>
                  <div className="fa fa-plus fa-2x dropzone-box-add" style={{width: '100%', height: '100%', background:'rgba(255,255,255,0.5)', borderRadius:'100%', paddingTop:10}}></div>
                </Dropzone>
              }
              <p className="text-center" style={{fontSize:13, marginTop:5}}>{client.role}</p>
              <div className="form-group" style={formGroupStyle}>
                <label style={labelStyle}>Nama</label>
                <input type="text" className="form-control col-md-3" value={ input.name } onChange={(evt) => this.changeInput('name', evt.target.value)} />
              </div>
              <div className="form-group" style={formGroupStyle}>
                <Link
                  to="/profile">
                  <button className="btn btn-default" style={buttonStyleLeft}>Batal</button>
                </Link>

                  <button className="btn btn-default" style={buttonStyle} onClick={this.submit}>Simpan</button>:


              </div>
            </div>
          </div>
        </Body>
      </Box>
    )
  }
}

const mapStateToProps = state => {
  return {
    client: state.client.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchEditProfile: (form, cb) => dispatch(dispatchEditProfile(form, cb))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
