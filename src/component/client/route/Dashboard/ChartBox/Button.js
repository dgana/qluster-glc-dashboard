// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import PropTypes from 'prop-types'

const Button = props => (
  <div>
    <button className="btn btn-default btn-sm" onClick={props.add}>
      Bandingkan
      <span className="fa fa-plus" style={{marginLeft:8}}></span>
    </button>
    <button className="btn btn-default btn-sm" disabled={props.disable} style={{marginLeft:10}} onClick={props.del}>
      Hapus Semua Baris
      <span className="fa fa-refresh" style={{marginLeft:8}}></span>
    </button>
    <button className="btn btn-qluster-success btn-sm pull-right" style={{fontSize:16, padding:'3px 17px', height:30}} onClick={props._onSubmit}>Tampilkan
    </button>
  </div>
)

Button.propTypes = {
  add: PropTypes.func.isRequired,
  del: PropTypes.func.isRequired,
  disable: PropTypes.bool.isRequired,
  _onSubmit: PropTypes.func.isRequired
}

export default Button
