// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import { connect } from 'react-redux'

// Common Component
import Box from '../../../common/Box'
import ToolBox from '../../../common/Box/ToolBox'
import Header from '../../../common/Box/Header'
import Body from '../../../common/Box/Body'

// ChartBox Component
import Select from './Select'
import Chart from './Chart'
import Button from './Button'

class ChartBox extends Component {
  constructor(props) {
    super(props)
    this.state = {
      township: [{ options: [] }],
      cluster: [{ options: []}],
      topic: [{ options: [] }],
      buttonDisabled: true
    }
  }

  static propTypes = {
    size: PropTypes.string.isRequired
  }

  componentDidMount() {

    // *************************************************************************//
    /* TODO: Fetch Township API
    // *************************************************************************/
    // *************************************************************************//
    /* TODO: Reduxify state
    // *************************************************************************/
    const fetchTownship = [
      { value: 'Jakarta', label: 'Jakarta' },
      { value: 'Tangerang', label: 'Tangerang' },
      { value: 'Bekasi', label: 'Bekasi' }
    ]
    // const fetchCluster = [
    //   { value: 'Cluster A', label: 'Cluster A' },
    //   { value: 'Cluster B', label: 'Cluster B' },
    //   { value: 'Cluster C', label: 'Cluster C' },
    //   { value: 'Cluster D', label: 'Cluster D' }
    // ]

    this.setState({
      township: [
        {
          options: [...fetchTownship],
          selected: false
        }
      ]
    })
  }

  addSelect = () => {
    this.setState(prevState => ({
      township: prevState.township.concat({ options: prevState.township[0].options, selected: false }),
      buttonDisabled: false
    }))
  }

  deleteSelect = () => {
    this.setState(prevState => ({
      township: prevState.township.filter((item, index) => index + 1 !== prevState.township.length),
      buttonDisabled: prevState.township.length === 2 ? true : false
    }))
  }

  _onSubmit = () => {
    alert('Submit')
  }

  render() {
    const { size } = this.props
    const { buttonDisabled, township } = this.state
    return (
      <Box size={size}>
        <Header title="Line Chart">
          <ToolBox />
        </Header>
        <Body>
        { township.map((item, index) => (<Select key={index} township={item} />)) }
          <Button add={this.addSelect} del={this.deleteSelect} disable={buttonDisabled} _onSubmit={this._onSubmit} />
          <Chart />
        </Body>
      </Box>
    )
  }
}

export default ChartBox
