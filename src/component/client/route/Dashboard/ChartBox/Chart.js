// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react';
import { connect } from 'react-redux'

// Plugin Dependencies
import { LineChart } from 'react-d3-components';
import $ from 'jquery'

class Chart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      width: window.innerWidth - 300,
      // height: window.innerHeight - 101
      sidebarWidth: 0
    }
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillReceiveProps(nextProps) {
    nextProps.toggle ? this.setState({sidebarWidth:230}) : this.setState({sidebarWidth:0})
    $('.area').css('transition','0.5s')
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions = () => {
    this.setState({
      width: window.innerWidth - 300,
      // height: window.innerHeight - 101
    })
  }

  render() {
    const { width } = this.state
    const data = [
      {
        customLabel: 'somethingA',
        customValues: [[0, 3], [1.3, 4], [3, 7], [3.5, 8], [4, 7], [4.5, 7],  [5, 7.8]]
      },
      {
        customLabel: 'somethingB',
        customValues: [[0, 2], [1.3, 4], [3,4.8], [3.5, 5.9], [4, 7], [4.5, 7.2],  [5, 7.8]]
      },
    ]

    let labelAccessor = stack => stack.customLabel
    let valuesAccessor = stack => stack.customValues
    let xAccessor = element => element[0]
    let yAccessor = element => element[1]
    let tooltipArea = (label, val) => "Label: " + label + " Value: " + val

    return (
      <div className="row">
        <div className="col-xs-12">
          <LineChart
            data={data}
            width={width + this.state.sidebarWidth}
            height={350}
            margin={{top: 30, bottom: 30, left: 50, right: 30}}
            interpolate={"basis"}
            label={labelAccessor}
            x={xAccessor}
            y={yAccessor}
            values={valuesAccessor}
            tooltipHtml={tooltipArea} />
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    toggle: state.sidebarToggle
  }
}

export default connect(mapStateToProps)(Chart);
