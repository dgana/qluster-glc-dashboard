// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'

// Plugin Dependencies
import Select from 'react-virtualized-select'
import createFilterOptions from 'react-select-fast-filter-options'
import { DateRangePicker } from 'react-dates'
import moment from 'moment'

// Chart Select Component
import CustomDateInfo from '../../../common/CustomDateInfo'

// Chart Select Utility
import isInclusivelyAfterDay from '../../../../../util/isInclusivelyAfterDay'

class ChartSelect extends Component {
  constructor(){
    super()
    this.state = {
      form: {
        startDate: null,
        endDate: null,
        town: '',
        cluster: '',
        topic: ''
      },
      focusedInput: null
    }
  }

  static PropTypes = {
    township: PropTypes.array.isRequired,
    index: PropTypes.number.isRequired,
    _onDeleteRow: PropTypes.func.isRequired
  }

  componentDidMount() {
    moment.locale("id") // Ubah tanggal menjadi bahasa indonesia
  }

  selectOnChange = (val, name) => {
    let newForm = Object.assign({}, this.state.form, {[name]: val.value})
    this.setState({ form: newForm })
  }

  render() {
    const { township, _onDeleteRow, index } = this.props
    const { form, focusedInput } = this.state
    const filterOptions = createFilterOptions({ options: township })
    const paddingRight = { paddingRight: 8 }
    const paddingLeft = { paddingLeft: 8 }
    const padding = { paddingLeft: 8, paddingRight:8 }
    return (
      <div className="row" style={{marginBottom: 16, marginLeft:0}}>
        <div className="col-dashboard-xs col-dashboard-md" style={paddingRight}>
          <DateRangePicker
            startDate={form.startDate}
            endDate={form.endDate}
            onDatesChange={({ startDate, endDate }) => {
              const newForm = Object.assign({}, this.state.form, { startDate, endDate })
              this.setState({ form: newForm })
            }}
            focusedInput={focusedInput}
            onFocusChange={focusedInput => this.setState({ focusedInput })}
            startDatePlaceholderText={"Start Date"}
            endDatePlaceholderText={"End Date"}
            numberOfMonths={3}
            withPortal={true}
            displayFormat={"D/M/Y"}
            showClearDates={true}
            daySize={35}
            isOutsideRange={day =>
              !isInclusivelyAfterDay(day, moment('2015-10-03')) || // Set The Start Date
              isInclusivelyAfterDay(day, moment()) // Set The End Time
            }
            initialVisibleMonth={() => moment('2015-10-03')}
            renderCalendarInfo={() => (<CustomDateInfo />)}
          />
        </div>
        <div className="col-dashboard-xs col-dashboard-md" style={padding}>
          <Select
            name="topic"
            value={form.topic}
            disabled={false}
            placeholder="Topic..."
            options={township.options}
            filterOptions={filterOptions}
            clearable={false}
            onChange={(val, name) => this.selectOnChange(val, 'topic')} />
        </div>
        <div className="col-dashboard-xs col-dashboard-md" style={padding}>
          <Select
            name="town"
            value={form.town}
            disabled={true}
            placeholder="Township..."
            options={township.options}
            filterOptions={filterOptions}
            clearable={false}
            onChange={(val, name) => this.selectOnChange(val, 'town')} />
        </div>
        <div className="col-dashboard-xs col-dashboard-md" style={paddingLeft}>
          <Select
            name="cluster"
            value={form.cluster}
            disabled={true}
            placeholder="Cluster..."
            options={township.options}
            filterOptions={filterOptions}
            clearable={false}
            onChange={(val, name) => this.selectOnChange(val, 'cluster')} />
        </div>
        <div
          className="col-dashboard-button-xs col-dashboard-button-md"
          style={{paddingLeft:16, paddingTop:2, cursor:'pointer'}}
          onClick={(e) => _onDeleteRow(index, e)}>
          <span className="fa fa-trash fa-1-7x" style={{color:'#939393'}}></span>
        </div>
      </div>
    )
  }
}

export default ChartSelect
