// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react';
import { connect } from 'react-redux'

// Common Component
import DashboardLayout from '../../common/DashboardLayout'
import Wrapper from '../../common/Content/Wrapper'
import Body from '../../common/Content/Body'
import Loading from '../../common/Loading'

// Dashboard Component
import Summary from './Summary'
// import ChartBox from './ChartBox'

// Plugin Dependencies
import moment from 'moment'

// Utility
import { monthYear } from '../../../../util'

// Dispatcher
import { dispatchAllClusterList, dispatchShowSummary } from '../../../../dispatcher'


class Dashboard extends Component {
  componentDidMount() {
    const { clusterList } = this.props

    if (!clusterList.fetched) {
      this.props.dispatchAllClusterList(this._allClusterListCallback)
    }
    this.props.dispatchShowSummary('all', monthYear(moment().add(0, 'months')).split(' ').join('') + ',' + monthYear(moment().add(1, 'months')).split(' ').join(''), this._showSummaryCallback)
  }

  _allClusterListCallback = (err, result) => {
    if (err) console.log(err)
  }

  _showSummaryCallback = (err, result) => {
    if (err) console.log(err);
  }

  render() {
    return (
      <DashboardLayout>
        <Wrapper>
        { !this.props.summary.fetched ?
          <Loading>
            <Summary display="none" />
          </Loading> :
          <Body>
            <Summary />
          </Body>
        }
        </Wrapper>
      </DashboardLayout>
    )
  }
}

const mapStateToProps = state => {
  return {
    clusterList: state.clusterList,
    summary: state.summary
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchAllClusterList: id => dispatch(dispatchAllClusterList(id)),
    dispatchShowSummary: (id, month, cb) => dispatch(dispatchShowSummary(id, month, cb))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
