// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import PropTypes from 'prop-types'

const Progress = props => {
  let progressColor = props.percentage <= '30' ? "red" :
  props.percentage >= '30' && props.percentage <= '75' ? "yellow" : "green"
  if (props.percentage === 'percentage') {
    return (
      <div style={{height: 20}}></div>
    )
  } else {
    return (
      <div>
        <p className="text-center" style={{marginBottom:10, fontSize:11}}>Pencapaian: <b>{props.min}/{props.max}</b></p>
        <div className="progress sm" style={{margin:'auto', width:'90%'}}>
          <div className={"progress-bar progress-bar-" + progressColor } style={{width: props.percentage + "%"}}></div>
        </div>
      </div>
    )
  }
}

Progress.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  percentage: PropTypes.number
}

export default Progress
