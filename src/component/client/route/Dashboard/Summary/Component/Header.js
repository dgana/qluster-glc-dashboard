// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import PropTypes from 'prop-types'

const Header = props => (
  <div>
    <h5 className="text-center small-box-mainheader">Performa {props.title}</h5>
    <p className="text-center small-box-subheader" style={{fontSize:12}}>{props.cluster}</p>
    <p className="text-center small-box-subheader" style={{fontSize:12}}>{props.date.split(/(\d+)/).join(' ')}</p>
  </div>
);

Header.propTypes = {
  title: PropTypes.string,
  cluster: PropTypes.string,
  date: PropTypes.string
}

export default Header
