// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import PropTypes from 'prop-types'

const Percentage = props => (
  <h3 className="text-center" style={{marginBottom:0, marginTop: props.display === 'none' ? 15 : 0}}>
    {props.percentage === null ? 0 : props.percentage}
    <span style={{fontSize:20, marginLeft: 6}}>
    { props.display === 'none' ? null :
      <i className="fa fa-percent" aria-hidden="true"></i>
    }
    </span>
  </h3>
)

Percentage.propTypes = {
  percentage: PropTypes.number
}

export default Percentage
