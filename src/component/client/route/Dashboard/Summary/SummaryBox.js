// React, React-Router, PropTypes & Redux Dependencies
import React from 'react';
import PropTypes from 'prop-types'

const SummaryBox = props => (
  <div className="col-md-3 col-xs-12">
    <div className="small-box">
      <div className="small-box-header">
        {props.header}
      </div>
      <div className="inner">
        {props.percentage}
      </div>
      <div className="icon"><i className={props.icon}></i></div>
      <div className="progress-group" style={{paddingBottom: 15}}>
        {props.progress}
      </div>
    </div>
  </div>
)

SummaryBox.propTypes = {
  header: PropTypes.node.isRequired,
  percentage: PropTypes.node.isRequired,
  icon: PropTypes.string.isRequired,
  progress: PropTypes.node.isRequired
}

export default SummaryBox;
