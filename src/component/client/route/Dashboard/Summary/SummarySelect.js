// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Chart Select Dependencies
import Select from 'react-virtualized-select'
import createFilterOptions from 'react-select-fast-filter-options'
import moment from 'moment'
import popup from '../../../../../../public/js/bootstrap-notify/popupMessage'

// Utility
import { monthYear, getYear, getMonth } from '../../../../../util'

// Dispather
import { dispatchShowSummary } from '../../../../../dispatcher'

class SummarySelect extends Component {
  constructor() {
    super()
    this.state = {
      month: {},
      cluster: {
        selected: false,
        value: ''
      },
      buttonDisable: false
    }
  }

  componentDidMount() {

    let presentYear = getYear(moment())
    let presentMonth = getMonth(moment())

    const startDate = moment([2017, 5]).diff(moment([presentYear, presentMonth]), 'months', true)
    let newDate = []
    for(let i = startDate; i <= 0; i++) {
      newDate.push({
        value: monthYear(moment().add(i, 'months')) + ',' + monthYear(moment().add(i+1, 'months')),
        label: monthYear(moment().add(i, 'months'))
      })
    }

    this.setState({
      month: {
        options: [...newDate],
        selected: false,
        value: ''
      },
    })
  }

  _onSubmitSummary = () => {
    const { month, cluster } = this.state

    if(month.value === "" || cluster.value === "") {
      popup.message('pe-7s-attention', `<span class="alert-popup-text-message">Harap lengkapi form terlebih dahulu</span>`, 'danger', 1000, 'bottom', 'right')
    } else {
      this.setState({ buttonDisable: true })
      this.props.dispatchShowSummary(cluster.value, month.value, this._showSummaryCallback)
    }
  }

  _showSummaryCallback = (err, result) => {
    if (result) {
      this.setState({ buttonDisable: false })
    } else {
      console.log(err);
    }
  }

  selectOnChange = (val, name, state) => {
    let newForm = Object.assign({}, state , { value: val.value, selected: val.value ? true : false  })
    this.setState({ [name]: newForm })
  }

  render() {
    const { month, cluster, buttonDisable } = this.state
    const { clusterList } = this.props

    const clusterOptions = clusterList.result.map(item => {
      return {
        value: item.id,
        label: item.name
      }
    })

    const filterMonthOptions = createFilterOptions({ options: month.options })
    const filterClusterOptions = createFilterOptions({ options: clusterOptions })

    return (
      <div className="row" style={{marginBottom:18}}>
        <div className="col-md-3 col-xs-12 summarySelect">
          <Select
            name="month"
            value={month.value}
            disabled={false}
            placeholder="Tanggal..."
            options={month.options}
            filterOptions={filterMonthOptions}
            clearable={false}
            onChange={(val, name) => this.selectOnChange(val, 'month', month)} />
        </div>
        <div className="col-md-3 col-xs-12 summarySelect summarySelectWidth">
          <Select
            name="cluster"
            value={month.selected ? cluster.value : ""}
            disabled={month.selected ? false : true}
            placeholder={month.selected ? "Cluster..." : "Cluster"}
            options={clusterOptions}
            filterOptions={filterClusterOptions}
            clearable={false}
            onChange={(val, name) => this.selectOnChange(val, 'cluster', cluster)} />
        </div>
        <div className="col-md-3 col-xs-12 summarySelect">
        { buttonDisable ?
          <button
            className="btn btn-qluster-success"
            onClick={this._onSubmitSummary}
            disabled={true}
            style={{height: '33px', width:'150px', alignItems: 'center'}}>
            Harap Tunggu...
          </button> :
          <button
            className="btn btn-qluster-success"
            onClick={this._onSubmitSummary}
            style={{height: '33px', width:'120px', alignItems: 'center'}}>
            Tampilkan
          </button>
        }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    clusterList: state.clusterList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchShowSummary: (id, month, cb) => dispatch(dispatchShowSummary(id, month, cb))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SummarySelect)
