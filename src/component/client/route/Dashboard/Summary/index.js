// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Common Component
import SummaryBox from './SummaryBox'
import Header from './Component/Header'

// Summary Component
import Percentage from './Component/Percentage'
import Progress from './Component/Progress'
import SummarySelect from './SummarySelect'

// Plugin Dependencies
import moment from 'moment'

// Utility
import { monthYear } from '../../../../../util'

class Summary extends Component {
  constructor(props) {
    super(props)
    this.state = {
      summary: []
    }
  }

  render() {
    const { summary } = this.props
    const { result } = summary
    return (
      <div style={{display: this.props.display}}>
        <SummarySelect />
        <div className="row">
          { summary.fetched ?
            <div>
              <SummaryBox
                header={
                  <Header
                    title={"Pembayaran"}
                    cluster={result.Cluster}
                    date={result.Bulan} />
                  }
                percentage={
                  <Percentage
                    percentage={result.Pembayaran.Summary} />
                  }
                progress={
                  <Progress
                    percentage={result.Pembayaran.Summary}
                    min={result.Pembayaran.Selesai}
                    max={result.Pembayaran.Total} />
                  }
                icon={"ion ion-card"} />

              <SummaryBox
                header={
                  <Header
                    title={"Keluhan"}
                    cluster={result.Cluster}
                    date={result.Bulan} />
                  }
                percentage={
                  <Percentage
                    percentage={result.Keluhan.Summary} />
                  }
                progress={
                  <Progress
                    percentage={result.Keluhan.Summary}
                    min={result.Keluhan.Selesai}
                    max={result.Keluhan.Total} />
                  }
                icon={"ion-speakerphone"} />

              <SummaryBox
                header={
                  <Header
                    title={"Permintaan Jasa"}
                    cluster={result.Cluster}
                    date={result.Bulan} />
                  }
                percentage={
                  <Percentage
                    percentage={result.Jasa.Summary} />
                  }
                progress={
                  <Progress
                    percentage={result.Jasa.Summary}
                    min={result.Jasa.Selesai}
                    max={result.Jasa.Total} />
                  }
                icon={"ion ion-wrench"} />

              <SummaryBox
                header={
                  <Header
                    title={"Penghuni"}
                    cluster={result.Cluster}
                    date={"Juni - " + monthYear(moment())} />
                  }
                percentage={
                  <Percentage
                    percentage={result.Penghuni.Total}
                    display={'none'} />
                  }
                progress={
                  <Progress
                    percentage={'percentage'}
                    min={'min'}
                    max={'max'} />
                  }
                icon={"ion-ios-home"} />
            </div> : null
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    summary: state.summary
  }
}

export default connect(mapStateToProps)(Summary)
