// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import PropTypes from 'prop-types'

// Common Component
import Box from '../../common/Box'
import Body from '../../common/Box/Body'

// Images
import img1 from '../../../../../public/imgs/app/avatars/user3-128x128.jpg'
import img2 from '../../../../../public/imgs/app/avatars/user4-128x128.jpg'
import img3 from '../../../../../public/imgs/app/avatars/user5-128x128.jpg'
import img4 from '../../../../../public/imgs/app/avatars/user6-128x128.jpg'

const boxCommentsStyle = {
  padding: "16px 24px 2px",
  background: '#FFF'
}

const Content = props => (
  <Box size={props.size} padding={0}>
    <Body padding="16px 20px">
      <div className="notification-comments" style={boxCommentsStyle}>
        <div className="notification-comment">
          <img className="img-circle img-md" src={img1} alt="User" />
          <div className="notification-text">
            <span className="username">
              Maria Gonzales
            </span>
            It is a long established fact that a reader.
            <p className="text-muted" style={{marginTop: 4}}>8:03 PM Today</p>
          </div>
        </div>
        <div className="notification-comment">
          <img className="img-circle img-md" src={img2} alt="User" />
          <div className="notification-text">
            <span className="username">
              Luna Stark
            </span>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            <p className="text-muted" style={{marginTop: 4}}>11:09 PM Yesterday</p>
          </div>
        </div>
        <div className="notification-comment">
          <img className="img-circle img-md" src={img3} alt="User" />
          <div className="notification-text">
            <span className="username">
              Luna Stark
            </span>
            Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.
            <p className="text-muted" style={{marginTop: 4}}>8:01 PM Yesterday</p>
          </div>
        </div>
        <div className="notification-comment">
          <img className="img-circle img-md" src={img4} alt="User" />
          <div className="notification-text">
            <span className="username">
              Luna Stark
            </span>
            Sunt in culpa qui officia deserunt mollit anim id est laborum.
            <p className="text-muted" style={{marginTop: 4}}>1:53 PM Yesterday</p>
          </div>
        </div>
      </div>
    </Body>
  </Box>
)

Content.propTypes = {
  size: PropTypes.string.isRequired
}

export default Content
