// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'

// Common Component
import DashboardLayout from '../../common/DashboardLayout'
import Wrapper from '../../common/Content/Wrapper'
import Body from '../../common/Content/Body'

import Content from './Content'

class Notifications extends Component {
  render() {
    return (
      <DashboardLayout>
        <Wrapper>
          <Body>
            <Content size="col-md-12" />
          </Body>
        </Wrapper>
      </DashboardLayout>
    )
  }
}

export default Notifications;
