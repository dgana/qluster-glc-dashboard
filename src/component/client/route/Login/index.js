// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

import Logo from '../../../../../public/imgs/logo/logo_sedayu.png'
// import { Checkbox } from 'react-icheck';

// Dispatcher
import { dispatchUserLogin } from '../../../../dispatcher'

class CompanyLogin extends Component {
  constructor() {
    super()
    this.state = {
      loginId: '',
      loginPassword: '',
      forgetPassword: '',
      loggedIn: false,
      button: true
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }

  submitLogin = (e) => {
    const { loginId, loginPassword } = this.state

    e.preventDefault()
    this.setState({ button: false })
    this.props.dispatchUserLogin(loginId, loginPassword, this._callbackLogout)
  }

  _callbackLogout = () => this.setState({ button: true })

  render() {
    const { loginId, loginPassword, button } = this.state

    return (
      <div className="login-background" style={{height: window.innerHeight}}>
        <div className="login-box" style={{marginTop: '-4%'}}>
          <div className="login-logo">
            <img style={{width:'40%', position:'relative', top:25}} src={Logo} alt='Qluster Logo'></img>
          </div>
          <div className="login-box-body" style={{backgroundColor:'inherit'}}>
            <p className="login-box-msg"></p>
            <form onSubmit={this.submitLogin}>
              <div className="form-group has-feedback">
                <input type="text" value={loginId} name='loginId' className="form-control" placeholder="Username" onChange={this.handleChange} />
                <span className="fa fa-university form-control-feedback" style={{borderLeft: '1px solid rgb(210,210,210)', width:'13%', height:'100%'}}></span>
              </div>
              <div className="form-group has-feedback">
                <input type="password" value={loginPassword} name='loginPassword' className="form-control" placeholder="Sandi" onChange={this.handleChange} />
                <span className="glyphicon glyphicon-lock form-control-feedback" style={{borderLeft: '1px solid rgb(210,210,210)', width:'13%', height:'100%'}}></span>
              </div>
              <div className="row">
                {/* <div className="col-xs-7">
                  <div className="checkbox icheck">
                    <Checkbox
                      value="remember-me"
                      style={{paddingLeft:10}}
                      checkboxClass="icheckbox_square-blue"
                      increaseArea="40%"
                      label="<span style='padding:10px; color:rgb(60,60,60); font-size:12px'>Remember Me</span>"
                    />
                  </div>
                </div> */}
                <div className="col-xs-7 col-xs-offset-5">
                  { button ? <button type="submit" className="no-border btn btn-primary btn-block btn-flat">Masuk</button> :
                    <button disabled={true} type="submit" className="no-border btn btn-primary btn-block btn-flat">Harap Tunggu... </button>
                  }
                </div>
              </div>
            </form>
            <a href="#" style={{color:'rgb(50,50,50)'}}></a><br />
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchUserLogin: (loginId, password, cb) => dispatch(dispatchUserLogin(loginId, password, cb)),
  }
}

export default connect(null, mapDispatchToProps)(CompanyLogin);
