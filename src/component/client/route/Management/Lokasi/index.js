// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Lokasi Component
import SearchRumah from './SearchRumah'
import TableRumah from './Table/Rumah'
import TableCluster from './Table/Cluster'
// import TableTownship from './Table/Township'
import Loading from '../../../common/Loading'
import Box from '../../../common/Box'
import Body from '../../../common/Box/Body'

// Plugin Dependencies
import popup from '../../../../../../public/js/bootstrap-notify/popupMessage'
import ClassNames from 'classnames'

// Dispatcher
import {
  dispatchAllClusterList,
  dispatchTabPanel,
  dispatchSearchHouseByIdAndName,
  dispatchResetHouseByIdAndName,
  dispatchCurrentHouseSearch
} from '../../../../../dispatcher'

let currentState = {
  clusterOptions: [],
  searchButtonDisabled: false,
  search: '',
  loading: false
}

class Lokasi extends React.Component {
  constructor(props) {
    super(props);
    this.state = currentState
    this._clusterOptionsCallback = this._clusterOptionsCallback.bind(this)
  }

  componentWillUnmount() {
    currentState = this.state
  }

  componentDidMount(){
    this.props.dispatchAllClusterList(this._clusterOptionsCallback)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.tabPanel !== this.props.tabPanel) {
      this.setState({ loading: true })
      setTimeout(() => this.setState({ loading: false }), 500)
    }
  }

  _showLoader = () => {
    this.setState({ loading: true })
  }

  _hideLoader = () => {
    this.setState({ loading: false })
  }

  _onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  _onClickAndKeypressSearch = val => {
    const { search } = this.state
    if (search === "") {
      popup.message('pe-7s-attention', '<p style="display:inline-block; padding-left:10px">Form pencarian tidak boleh kosong</p>','danger', 5000 ,'bottom','right')
    } else {
      this.setState({ searchButtonDisabled: true })
      this.props.dispatchResetHouseByIdAndName()
      this.props.dispatchCurrentHouseSearch(search)
      this.props.dispatchSearchHouseByIdAndName(val, this._searchHouseByIdAndNameCallback)
    }
  }

  _onClickSearch = val => {
    this._onClickAndKeypressSearch(val)
  }

  _handleKeyPress = (e, val) => {
    if (e.key === "Enter") {
      this._onClickAndKeypressSearch(val)
    }
  }

  _searchHouseByIdAndNameCallback = (err, result) => {
    if (result) {
      this._hideLoader()
      if (result.length === 0) {
        popup.message('pe-7s-attention', `<p style="display:inline-block; padding-left:10px">Hasil Pencarian ${this.props.houseSearch} tidak ditemukan</p>`,'danger', 5000 ,'bottom','right')
      } else {
        popup.message('pe-7s-success', `<p style="display:inline-block; padding-left:10px">Hasil Pencarian ${this.props.houseSearch}: ${result.length} data rumah</p>`,'success', 5000 ,'bottom','right')
      }
      this.setState({
        searchButtonDisabled: false,
        search: ""
      })
    } else {
      console.log(err);
    }
  }

  _reloadHouse = () => {
    const { houseSearch } = this.props
    this._showLoader()
    if (houseSearch === "") {
      setTimeout(() => this._hideLoader(), 500)
    } else {
      this.setState({
          clusterOptions: [],
          searchButtonDisabled: false,
          search: '',
          loading: false
        })
      this.props.dispatchResetHouseByIdAndName()
      this.props.dispatchSearchHouseByIdAndName(houseSearch, this._searchHouseByIdAndNameCallback)
    }
  }

  _clusterOptionsCallback = (err,result) => {
    if (result) {
      const clusterOptions = result.map( cluster => ({label:cluster.name,value:cluster.id}))
      this.setState({ clusterOptions })
    } else console.log(err);
  }

  render() {
    const {
      clusterOptions,
      loading,
      searchButtonDisabled,
      search,
    } = this.state

    const {
      tabPanel,
      clusterList,
      dispatchTabPanel,
      houseManagementList
    } = this.props

    return (
      <Box size="col-md-12" padding={0}>
        <Body padding='0px'>
          <div className="row">
            <div className="col-md-12" style={{marginBottom:0}}>
              <div className="nav-tabs-custom">
                <ul className="nav nav-tabs">
                  <li className={ClassNames({"active": tabPanel === 'rumah'})}><a href="#rumah" onClick={() => dispatchTabPanel("rumah")} data-toggle="tab" style={{borderTopLeftRadius: 4}}>Rumah</a></li>
                  <li className={ClassNames({"active": tabPanel === 'cluster'})}><a href="#cluster" onClick={() => dispatchTabPanel("cluster")} data-toggle="tab">Cluster</a></li>
                </ul>
                <div className="tab-content" style={{padding:'16px 24px'}}>
                { tabPanel === 'rumah' && loading ?
                  <Loading paddingLeft={0}>
                    <TableRumah display="none" />
                  </Loading> :

                  tabPanel === 'rumah' && !loading ?
                  !houseManagementList.fetched || houseManagementList.result.length === 0 ?
                  <SearchRumah
                    _onChange={this._onChange}
                    _onClickSearch={this._onClickSearch}
                    _handleKeyPress={this._handleKeyPress}
                    _reloadHouse={this._reloadHouse}
                    search={search}
                    searchButtonDisabled={searchButtonDisabled} /> :
                  <div>
                    <SearchRumah
                      _onChange={this._onChange}
                      _onClickSearch={this._onClickSearch}
                      _handleKeyPress={this._handleKeyPress}
                      _reloadHouse={this._reloadHouse}
                      search={search}
                      searchButtonDisabled={searchButtonDisabled} />
                    <TableRumah
                      _showLoader={this._showLoader}
                      _hideLoader={this._hideLoader}
                      clusterOptions={clusterOptions} />
                  </div> :

                  (tabPanel === 'cluster' && !clusterList.fetched) || loading ?
                  <Loading paddingLeft={0}>
                    <TableCluster display="none" />
                  </Loading> : <TableCluster />
                }
                </div>
              </div>
            </div>
          </div>
      </Body>
    </Box>
    )
  }
}

const mapStateToProps = state => {
  return {
    tabPanel: state.tabPanel,
    clusterList: state.clusterList,
    houseSearch: state.houseSearch.result,
    houseManagementList: state.houseManagementList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchAllClusterList: cb => dispatch(dispatchAllClusterList(cb)),
    dispatchTabPanel: location => dispatch(dispatchTabPanel(location)),
    dispatchCurrentHouseSearch: search => dispatch(dispatchCurrentHouseSearch(search)),
    dispatchSearchHouseByIdAndName: (val, cb) => dispatch(dispatchSearchHouseByIdAndName(val, cb)),
    dispatchResetHouseByIdAndName: () => dispatch(dispatchResetHouseByIdAndName())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Lokasi)
