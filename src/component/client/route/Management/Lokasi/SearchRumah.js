// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

// Plugin Dependencies
import ReactTooltip from 'react-tooltip'

const formStyle = {
  borderRadius: 2,
  marginBottom: 10,
  height:45,
  fontSize: 14
}

const buttonSearch = {
  position: 'absolute',
  top: '0%',
  right: '2%',
  height: '81%',
  width: '16%',
  borderTopLeftRadius: 2,
  borderBottomLeftRadius: 2,
  border: '1px solid rgb(242,242,242)'
}

const buttonSearchData = {
  position: 'absolute',
  marginLeft: '3%',
  top: '0%',
  left: '0%',
  width: '8%',
  padding: 0,
  border: 0
}

class SearchRumah extends React.Component {
  render() {

    const {
      searchButtonDisabled,
      search,
      houseSearch,
      houseManagementList
    } = this.props

    return (
      <div style={{height: 210}}>
        <p className='box-title' style={{margin: '6px 0px 28px', fontSize:'16px'}}>Manajemen Rumah</p>
        <ReactTooltip id="reload-manajemen-rumah" place={"bottom"} />
        <button
          data-for="reload-manajemen-rumah"
          data-tip="Reload Pencarian Terakhir"
          data-iscapture="true"
          onClick={() => this.props._reloadHouse()}
          type="submit"
          className="btn btn-qluster-primary pull-right"
          style={{position:'absolute', right:40, top:12, border:'none'}}>
          <span className="pe-7s-refresh" style={{paddingRight: 6}}></span>
           Reload
        </button>
        <div className="col-sm-offset-2 col-sm-8 col-xs-12">
          <input
            onKeyPress={(e) => this.props._handleKeyPress(e, search)}
            onChange={this.props._onChange}
            className="form-control"
            name="search"
            type="text"
            value={search}
            style={formStyle}
            placeholder="Masukkan ID / No. Rumah" />
          <button
            onClick={() => this.props._onClickSearch(search)}
            type="submit"
            style={buttonSearch}
            className="btn btn-qluster-success">
            { searchButtonDisabled ?
              <i className="fa fa-ellipsis-h fa-1-5x"></i> :
              <i className="fa fa-home fa-1-5x"></i>
            }
          </button>
        </div>
        <div className="col-sm-offset-2 col-sm-8 col-xs-12">
          <Link
            style={{color: 'rgb(125,125,125)', marginLeft: '10%'}}
            to="/laporan-rumah"
            target="_blank">
            <button
              type="submit"
              style={buttonSearchData}
              className="btn btn-default">
              <i className="fa fa-home"></i>
            </button>
            Daftar Laporan ID & No. Rumah
          </Link>
        </div>
        { houseManagementList.fetched && houseManagementList.result.length > 0 ?
          <div className="col-xs-12" style={{paddingLeft: 0, marginTop: 24}}>
            <hr style={{margin: '0px 0px 12px'}} />
            <p style={{marginBottom: 0}}>Hasil Pencarian <h5 style={{display: 'inline-block'}}>{houseSearch}</h5> ditemukan {houseManagementList.result.length} data rumah</p>
          </div> : null
        }

      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    houseManagementList: state.houseManagementList,
    houseSearch: state.houseSearch.result
  }
}

export default connect(mapStateToProps)(SearchRumah)
