// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Common Component
import QmapLayers from '../../../../common/Layers'
import SearchBox from '../../../../common/SearchBox'
import ExtendedMarker from '../../../../common/ExtendedMarker'

// Plugin Dependencies
import $ from 'jquery'
import popup from '../../../../../../../public/js/bootstrap-notify/popupMessage'
import Select from 'react-select'
import Confirm from 'react-confirm-bootstrap'
import { Modal } from 'react-bootstrap'
import { Map, Marker, ZoomControl, Popup } from 'react-leaflet'
import L from 'leaflet'
import ReactTooltip from 'react-tooltip'
import { SingleDatePicker } from 'react-dates'
import moment from 'moment'

// Home Icons
import homeIcon from '../../../../../../../public/imgs/icon/home_marker.png'
import home from '../../../../../../../public/imgs/marker/Area_Selection_Home_Green@1x.png'

// Dispatcher
import {
  dispatchDeleteHouse,
  dispatchEditHouse,
  dispatchHouseByCluster,
  dispatchRemoveHouseByCluster,
  dispatchLoadHouseByCluster,
} from '../../../../../../dispatcher'

// Utility
import { newsDate } from '../../../../../../util'
import { greenLakeCity } from '../../../../../../util/mapConfig'

// Style
const popupLabel = {
  display: 'block',
  lineHeight: '34px',
  marginLeft: 12
}

const marker = L.icon({
  iconUrl: homeIcon,
  iconAnchor: [15,45],
  popupAnchor: [0,-35]
})

const existingHome = L.icon({
  iconUrl: home,
  iconSize: [26,37],
  iconAnchor: [13,37],
  popupAnchor: [0,-20]
})

let currentState = {
  form: {
    clusterId: "",
    houseId:"",
    number: "",
    address: "",
    handoverDate: null,
    clusterOptions: [],
    latitude: "",
    longitude: "",
    zoom: 12,
    houseList: [],
    filteredHouseList: false
  },
  disabled: false,
  options: '',
  value: [],
  clusterArray : [],
  showModal:false,
  isFocused:false,
}

class Rumah extends React.Component {
  constructor(props) {
    super(props)
    this.state = currentState
    this._renderCalender = this._renderCalender.bind(this)
    this._handleClickDate = this._handleClickDate.bind(this)
  }

  componentWillUnmount() {
    currentState = this.state
  }

  componentDidMount() {
    $(document).ready(function() {
      $('#table-manajemen-rumah').DataTable({
        iDisplayLength: 25,
        retrieve: true
      })
    })
    moment.locale("id")
  }

  // Filter House
  _handleSelectChange = value => {
    const clusterArray = value.split(',')
    if (clusterArray[0] === "") {
      this.setState({ clusterArray: [], value })
    } else {
      this.setState({ clusterArray, value })
    }
	}

  // Edit Modal
  onClickModal = () => {
    const currentModalState = this.state.showModal
    this.setState({showModal: !currentModalState})
  }

  onClickMap = (e) => {
    this._onChangeForm('coords',e.latlng)
  }


  _onChangeForm = (name, e) => {

    // If you want to access the event properties in an asynchronous way
    if(typeof name === 'object') {
      name.persist()
    }

   if (name === 'cluster') {
      this.setState(prevState => ({
        form: {
          ...prevState.form,
          clusterId: e.value
        }
      }))
    } else if (name === 'coords') {
      this.setState(prevState => ({
        form: {
          ...prevState.form,
          latitude: +e.lat,
          longitude: +e.lng
        }
      }))
    } else if(name === 'handoverDate'){

      if(typeof e === 'object'){
        if(e === null){
          this.setState(prevState => ({
            isFocused:false
          }))
          return null
        }
        else {
          this.setState(prevState => ({
            form:{
              ...prevState.form,
              handoverDate: e._d
            }
          }))
        }
      }

    } else {
      this.setState(prevState => ({
        form: {
          ...prevState.form,
          [name.target.name]: name.target.value
        }
      }))
    }
  }

  _deleteHouseCallback = (err, result) => {
    this.props._hideLoader()
    if(result)
      popup.message('pe-7s-like2',`<span class="alert-popup-text-message">Rumah berhasil dihapus</span>`,'success', 5000 ,'bottom','right')
    else popup.message('pe-7s-attention',`<span class="alert-popup-text-message">Rumah tidak dapat dihapus karena sudah ada penghuninya</span>`,'danger', 5000 ,'bottom','right')
  }

  componentDidUpdate = (prevProps, prevState) => {
    const { clusterArray } = this.state
    if (prevState.clusterArray.length !== clusterArray.length){
      if (prevState.clusterArray.length < clusterArray.length){
        this.props.dispatchLoadHouseByCluster()
        this.props.dispatchHouseByCluster(clusterArray[clusterArray.length - 1])
      } else {
        const getRemovedClusterId = prevState.clusterArray.filter(val => !clusterArray.includes(val))
        this.props.dispatchRemoveHouseByCluster(getRemovedClusterId[0])
      }
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if(nextState.form.houseList) {
      const newFilteredHouseList = nextState.form.houseList.filter(item => item.latitude !== nextState.form.latitude && item.longitude !== nextState.form.longitude)
      if(newFilteredHouseList.length !== nextState.form.houseList.length) {
        this.setState(prevState => ({
          form: {
            ...prevState.form,
            houseList: newFilteredHouseList,
            filteredHouseList: true
          }
        }))
      }
    }
  }

  _onSubmitEditForm = () => {
    const { form } = this.state
    if(form.clusterId === "" || form.name === "" || form.address === "" || form.latitude === "" || form.longitude === "") {
      popup.message('pe-7s-attention',`<span class="alert-popup-text-message">Harap lengkapi form</span>`,'danger', 5000 ,'top','right')
    } else {
      this.props.dispatchEditHouse(form, this._editHouseFormCallback)
      this.setState({ disabled: true })
    }
  }

  _editHouseFormCallback = (err, result) => {
    if(result){
      popup.message('pe-7s-like2',`<span class="alert-popup-text-message">Sunting rumah berhasil</span>`,'success', 5000 ,'bottom','right')
      this.onClickModal()
      this.setState({ disabled: false })
    } else {
      console.log(err);
    }
  }

  _onConfirmDeleteHouse = (id) => {
    this.props._showLoader()
    this.props.dispatchDeleteHouse(id, this._deleteHouseCallback)
  }

  _editRumah(house){
    const newForm = {...this.state.form,...house}
    this.setState({
      form:newForm,
      showModal:true
    })
  }

  _handleClickDate = () =>{
    this.setState({isFocused:!this.state.isFocused})
  }

  _renderCalender = (house,_onChangeForm) =>{
    return(<SingleDatePicker
      isOutsideRange={() => false}
      date={house.handoverDate ? moment(house.handoverDate):house.handoverDate}
      onDateChange={(handoverDate) =>_onChangeForm('handoverDate',handoverDate)}
      focused={this.state.isFocused}
      displayFormat={"DD MMMM YY"}
      placeholder={'Tanggal Penyerahan'}
      numberOfMonths={1}
      onFocusChange={({focused}) =>_onChangeForm('handoverDate',focused)}
    />)
  }

  render() {

    const {
      houseManagementList,
      filteredHouse,
      clusterList
    } = this.props

    const {
      form,
      disabled
    } = this.state

    const current = [form.latitude, form.longitude]
    const clusterOptions = clusterList.result.map(item => ({ label: item.name, value: item.id }))
    const existingHouses = filteredHouse.result.filter(item => item.id !== form.houseId)
    console.log('tampil data form edit---------',form);
    console.log('tampil data form showModal---------',this.state.showModal);
    return (
      <div style={{display:this.props.display}}>
        <div className="row">
          <div className="col-xs-12">
            <table id="table-manajemen-rumah" className="table text-center daftarRumahTable">
              <thead>
                <tr>
                  <th>House ID</th>
                  <th>Nomor Rumah</th>
                  <th>Alamat Rumah</th>
                  <th>Waktu Penyerahan</th>
                  <th>Cluster</th>
                  <th>Tindakan</th>
                </tr>
              </thead>
              <tbody>
              { houseManagementList.result.map(item => (
                  <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.number}</td>
                    <td>{item.address}</td>
                    <td>{newsDate(item.handoverDate)}</td>
                    <td>{item.clusterName}</td>
                    <td style={{textAlign:'center'}}>
                      <ReactTooltip id={"edit-manajemen-rumah-baris-" + item.id} place={"left"} />
                      <ReactTooltip id={"delete-manajemen-rumah-baris-" + item.id} place={"left"} />
                      <i className="fa fa-edit report-table-icon"
                        data-for={"edit-manajemen-rumah-baris-" + item.id}
                        data-tip={"Sunting No. Rumah " + item.number}
                        data-iscapture="true"
                        onClick={() => {
                          const house = {
                            houseId: item.id,
                            clusterId: item.ClusterId,
                            number: item.number,
                            address: item.address,
                            handoverDate:moment(new Date(item.handoverDate)),
                            latitude: item.latitude,
                            longitude: item.longitude,
                          }
                          this._editRumah(house)}
                        }
                      />
                      <Confirm
                        onConfirm={() => this._onConfirmDeleteHouse(item.id)}
                        body={`Hapus No. Rumah ${item.number}?`}
                        confirmText="Hapus"
                        cancelText="Batal"
                        confirmBSStyle="danger"
                        title={'Manajemen Rumah'}>
                        <i
                          data-for={"delete-manajemen-rumah-baris-" + item.id}
                          data-tip={"Hapus No. Rumah " + item.number}
                          data-iscapture="true"
                          className="fa fa-trash report-table-icon">
                        </i>
                      </Confirm>
                    </td>
                  </tr>
                ))
              }
              </tbody>
            </table>
          </div>
        </div>
        <Modal show={this.state.showModal} onHide={this.onClickModal} dialogClassName="modalEdit">
          <Modal.Header bsClass="modalHeaderContainer">
            <p style={{margin:'4px 0px'}}>Sunting Rumah</p>
          </Modal.Header>
          <div style={{position: 'absolute', top:'14%', right: '36px', zIndex:999, width:'180px'}}>
            <Select multi simpleValue
              disabled={this.state.filterDisabled}
              value={this.state.value}
              placeholder="Cluster..."
              options={clusterOptions}
              clearable={false}
              onChange={this._handleSelectChange}
            />
          </div>
          <Modal.Body bsClass="modalBodyContainer">
            <div className='container-fluid' style={{padding: 0}}>
              { form.latitude !== '' ?
                <Map id="mapManajemenHouse" center={[greenLakeCity.latitude, greenLakeCity.longitude]} zoomControl={false} style={{ height: 450 }} zoom={greenLakeCity.zoom} onClick={(e) => this.onClickMap(e)} >
                  <QmapLayers />
                  <SearchBox position="bottomleft" />
                  <ZoomControl position="bottomleft" />
                  { existingHouses.map((house, index) => {
                    const position = [+house.latitude, +house.longitude]
                    return (
                      <div key={index}>
                        <Marker position={position} icon={existingHome}>
                          <Popup>
                            <div style={{width: 280}}>
                              <div className="row" style={{marginBottom: 4}}>
                                <div className="col-xs-4">
                                  <b>Rumah: </b>
                                </div>
                                <div className="col-xs-8">
                                  <em>{house.number}</em>
                                </div>
                              </div>
                              <div className="row" style={{marginBottom: 4}}>
                                <div className="col-xs-4">
                                  <b>Alamat: </b>
                                </div>
                               <div className="col-xs-8">
                                 <em>{house.address}</em>
                               </div>
                             </div>
                             <div className="row" style={{marginBottom: 4}}>
                               <div className="col-xs-4">
                                 <b>Longitude: </b>
                               </div>
                               <div className="col-xs-8">
                                 <em>{house.longitude}</em>
                               </div>
                             </div>
                             <div className="row" style={{marginBottom: 4}}>
                               <div className="col-xs-4">
                                 <b>Latitude: </b>
                               </div>
                               <div className="col-xs-8">
                                 <em>{house.latitude}</em>
                               </div>
                             </div>
                            <hr style={{margin: '10px 0px'}} />
                            <div className="row" style={{marginBottom: 4}}>
                              <div className="col-xs-4">
                                <b>Township: </b>
                              </div>
                              <div className="col-xs-8">
                                <em>{house.townName}</em>
                              </div>
                            </div>
                            <div className="row" style={{marginBottom: 4}}>
                              <div className="col-xs-4">
                                <b>Cluster: </b>
                              </div>
                              <div className="col-xs-8">
                                <em>{house.clusterName}</em>
                              </div>
                            </div>
                           </div>
                          </Popup>
                        </Marker>
                      </div>
                    )
                  })
                }
                { form.latitude !== '' ?
                  <ExtendedMarker position={current} icon={marker}>
                    <Popup>
                      <div style={{width: 400}}>
                        <div className="row" style={{marginBottom: 8}}>
                          <div className="col-xs-3">
                            <b style={popupLabel}>Cluster: </b>
                          </div>
                          <div className="col-xs-9">
                            <select
                              disabled={form.disabled}
                              onChange={(e) => this._onChangeForm('cluster', e.target)}
                              value={form.clusterId}
                              style={{width:'100%'}}
                              className="form-control">
                              <option value="" style={{fontWeight: 'bold'}} disabled defaultValue>Pilih cluster</option>
                              { clusterOptions.map((item, index) => (
                                 <option
                                   key={index}
                                   style={{fontWeight: 'bold'}}
                                   value={item.value}>{item.label}
                                 </option>
                               ))
                              }
                            </select>
                          </div>
                        </div>
                        <div className="row" style={{marginBottom: 8}}>
                          <div className="col-xs-3">
                            <b style={popupLabel}>Nomor: </b>
                          </div>
                          <div className="col-xs-9">
                            <input
                              type="text"
                              name="number"
                              style={{width:'100%'}}
                              value={form.number}
                              placeholder="Nomor Rumah"
                              className="form-control"
                              onChange={this._onChangeForm}
                            />
                          </div>
                        </div>
                        <div className="row" style={{marginBottom: 8}}>
                          <div className="col-xs-3">
                            <b style={popupLabel}>Alamat: </b>
                          </div>
                          <div className="col-xs-9">
                            <textarea
                              name="address"
                              value={form.address}
                              style={{width:'100%'}}
                              placeholder="Alamat Lengkap"
                              className="form-control"
                              rows="1"
                              onChange={this._onChangeForm}
                             />
                          </div>
                        </div>
                        <div className="row" style={{marginBottom: 8}}>
                          <div className="col-xs-3">
                            <b style={popupLabel}>Penyerahan: </b>
                          </div>
                          <div className="col-xs-9" onClick={() =>this._handleClickDate()}>
                           { this._renderCalender(form,this._onChangeForm) }
                          </div>
                        </div>
                        <div className="row" style={{marginBottom: 8}}>
                          <div className="col-xs-3">
                            <b style={popupLabel}>Lat/Lng: </b>
                          </div>
                          <div className="col-xs-4" style={{paddingRight: 0, width: '35.6%'}}>
                            <input
                              type="text"
                              name="latitude"
                              value={form.latitude}
                              placeholder="Latitude"
                              style={{width:'100%'}}
                              className="form-control"
                              onChange={this._onChangeForm}
                            />
                          </div>
                          <div className="col-xs-4" style={{paddingRight: 0, width: '35.6%'}}>
                            <input
                              type="text"
                              name="longitude"
                              value={form.longitude}
                              placeholder="Longitude"
                              style={{width:'100%'}}
                              className="form-control"
                              onChange={this._onChangeForm}
                            />
                          </div>
                        </div>
                      </div>
                     </Popup>
                  </ExtendedMarker> : null
                }
              </Map> : null
              }
              <Modal.Footer>
                <button
                  className="btn btn-qluster-success pull-right"
                  disabled={disabled}
                  onClick={() => this._onSubmitEditForm()}>
                   { disabled ? "Harap Tunggu..." : "Sunting" }
                </button>
                <button
                  className="btn btn-default pull-right modalCancelButton"
                  style={{marginRight:10}}
                  onClick={() => this.onClickModal()}>Batal</button>
              </Modal.Footer>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    houseManagementList: state.houseManagementList,
    clusterList: state.clusterList,
    filteredHouse: state.filteredHouse,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchDeleteHouse: (id, cb) => dispatch(dispatchDeleteHouse(id, cb)),
    dispatchEditHouse: (form, cb) => dispatch(dispatchEditHouse(form, cb)),
    dispatchHouseByCluster: id => dispatch(dispatchHouseByCluster(id)),
    dispatchRemoveHouseByCluster: id => dispatch(dispatchRemoveHouseByCluster(id)),
    dispatchLoadHouseByCluster: () => dispatch(dispatchLoadHouseByCluster()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Rumah)
