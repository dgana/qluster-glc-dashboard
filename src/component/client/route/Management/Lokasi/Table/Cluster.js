// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Common Component
import QmapLayers from '../../../../common/Layers'
import SearchBox from '../../../../common/SearchBox'
import ExtendedMarker from '../../../../common/ExtendedMarker'

// Plugin Dependencies
import { Modal } from 'react-bootstrap'
import { Map, Marker, ZoomControl, Popup } from 'react-leaflet'
import L from 'leaflet'
import $ from 'jquery'
import Confirm from 'react-confirm-bootstrap'
import popup from '../../../../../../../public/js/bootstrap-notify/popupMessage'
import ReactTooltip from 'react-tooltip'

// Dispatcher
import {
  dispatchResetClusterList,
  dispatchDeleteCluster,
  dispatchEditCluster,
  dispatchAllClusterList
} from '../../../../../../dispatcher'

// Map icons
import homeIcon from '../../../../../../../public/imgs/icon/home_marker.png'
import home from '../../../../../../../public/imgs/marker/Area_Selection_Home_Green@1x.png'

// Utility
import { greenLakeCity } from '../../../../../../util/mapConfig'

// Style
const popupLabel = {
  display: 'block',
  lineHeight: '34px',
  marginLeft: 12
}

// L.Icon
const marker = L.icon({
  iconUrl: homeIcon,
  iconAnchor: [15,45],
  popupAnchor: [0,-35]
})

const existingHome = L.icon({
  iconUrl: home,
  iconSize: [26,37],
  iconAnchor: [13,37],
  popupAnchor: [0,-20]
})

class Cluster extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        name: "",
        address: "",
        latitude: "",
        longitude: "",
        id: ""
      },
      disabled: false,
    }
  }

  componentDidMount() {
    $(document).ready(function() {
      $('#table-register-cluster').DataTable({
        iDisplayLength: 25,
        retrieve: true
      })
    })
  }

  // Edit Modal
  onClickModal = () => {
    const currentModalState = this.state.showModal
    this.setState({showModal: !currentModalState})
  }

  onClickMap = (e) => {
    this._onChangeForm('coords', e.latlng)
  }

  _onChangeForm = (field, value) => {
    let newForm = this.state.form;
    if (field === 'coords'){
      newForm.latitude = value.lat
      newForm.longitude = value.lng
    } else {
      newForm[field] = value.value
    }
    this.setState({ form: newForm });
  }

  _reloadCluster = () => {
    this.props.dispatchResetClusterList()
    this.props.dispatchAllClusterList(this._clusterOptionsCallback)
  }

  _clusterOptionsCallback = (err,result) => {
    if (err) console.log(err);
  }

  _deleteClusterCallback = (err, result) => {
    if(result) {
      popup.message('pe-7s-like2',`<span class="alert-popup-text-message">Cluster berhasil dihapus</span>`,'success', 5000 ,'bottom','right')
    } else popup.message('pe-7s-attention',`<span class="alert-popup-text-message">Cluster sudah ada penghuninya</span>`,'danger', 5000 ,'bottom','right')
  }

  _onSubmitEditForm = () => {
    const { form } = this.state
    if(form.townId === "" || form.name === "" || form.address === "" || form.latitude === "" || form.longitude === "") {
      popup.message('pe-7s-attention',`<span class="alert-popup-text-message">Input tidak boleh kosong</span>`,'danger', 5000 ,'top','right')
    } else {
      this.props.dispatchEditCluster(form, this._editClusterFormCallback)
      this.setState({ disabled: true })
    }
  }

  _editClusterFormCallback = (err, result) => {
    if(result){
      popup.message('pe-7s-like2',`<span class="alert-popup-text-message">Sunting cluster berhasil</span>`,'success', 5000 ,'bottom','right')
      this.setState({ disabled: false })
      this.onClickModal()
    } else {
      console.log(err);
    }
  }

  render() {
    const { clusterList } = this.props
    const { form, disabled } = this.state

    const existingClusters = clusterList.result.filter(item => item.id !== form.id)
    const current = [form.latitude, form.longitude]

    return (
      <div style={{display:this.props.display}}>
          <p className='box-title' style={{margin: '6px 0px 16px', fontSize:'16px'}}>Manajemen Cluster</p>
          <ReactTooltip id="reload-manajemen-cluster" place={"bottom"} />
          <button
            data-for="reload-manajemen-cluster"
            data-tip="Reload Cluster"
            data-iscapture="true"
            onClick={() => this._reloadCluster()}
            type="submit"
            className="btn btn-qluster-primary pull-right"
            style={{position:'absolute', right:40, top:12, border:'none'}}>
            <span className="pe-7s-refresh" style={{paddingRight: 6}}></span>
             Reload
          </button>
          <div className="row">
            <div className="col-xs-12">
              <table id="table-register-cluster" className="table text-center daftarClusterTable">
                <thead>
                  <tr>
                    <th>Cluster ID</th>
                    <th>Nama Cluster</th>
                    <th>Alamat Cluster</th>
                    <th>Tindakan</th>
                  </tr>
                </thead>
                <tbody>
                { clusterList.result.map((item, index) => (
                  <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.address}</td>
                    <td style={{textAlign:'center'}}>
                      <ReactTooltip id={"edit-manajemen-cluster-baris-" + item.id} place={"left"} />
                      <ReactTooltip id={"delete-manajemen-cluster-baris-" + item.id} place={"left"} />
                      <i
                        data-for={"edit-manajemen-cluster-baris-" + item.id}
                        data-tip={"Sunting Cluster " + item.name}
                        data-iscapture="true"
                        className="fa fa-edit report-table-icon"
                        onClick={() => {
                          const newObj = {
                            name: item.name,
                            address: item.address,
                            latitude: item.latitude,
                            longitude: item.longitude,
                            id: item.id
                          }
                          this.setState(prevState => ({
                            form: {
                              ...prevState.form,
                              ...newObj
                            }
                          }))
                          this.onClickModal()
                        }}>
                      </i>
                      <Confirm
                        onConfirm={() => this.props.dispatchDeleteCluster(item.id, this._deleteClusterCallback)}
                        body={`Hapus Cluster ${item.name}?`}
                        confirmText="Hapus"
                        cancelText="Batal"
                        confirmBSStyle="danger"
                        title={'Manajemen Cluster'}>
                        <i
                          data-for={"delete-manajemen-cluster-baris-" + item.id}
                          data-tip={"Hapus Cluster " + item.name}
                          data-iscapture="true"
                          className="fa fa-trash report-table-icon">
                        </i>
                      </Confirm>
                    </td>
                  </tr>
                ))
                }
                </tbody>
              </table>
            </div>
          </div>
          <Modal show={this.state.showModal} onHide={this.onClickModal} dialogClassName="modalEdit">
            <Modal.Header bsClass="modalHeaderContainer">
              <p style={{margin:'4px 0px'}}>Sunting Cluster</p>
            </Modal.Header>
            <Modal.Body bsClass="modalBodyContainer">
              { form.latitude !== '' ?
                <Map id="mapManajemenCluster" center={[greenLakeCity.latitude, greenLakeCity.longitude]} style={{ height: 450 }} zoomControl={false} zoom={greenLakeCity.zoom} maxZoom={19} onClick={(e) => this.onClickMap(e)} >
                  <QmapLayers />
                  <SearchBox position="bottomleft" />
                  <ZoomControl position="bottomleft" />
                  { existingClusters.map((cluster, index) => {
                    const position = [cluster.latitude, cluster.longitude]
                    return (
                      <div key={index}>
                        <Marker position={position} icon={existingHome}>
                          <Popup>
                            <div style={{width: 400}}>
                              <div className="row" style={{marginBottom: 4}}>
                                <div className="col-xs-4">
                                  <b>Cluster: </b>
                                </div>
                                <div className="col-xs-8">
                                  <em>{cluster.name}</em>
                                </div>
                              </div>
                              <div className="row" style={{marginBottom: 4}}>
                                <div className="col-xs-4">
                                  <b>Alamat: </b>
                                </div>
                               <div className="col-xs-8">
                                 <em>{cluster.address}</em>
                               </div>
                             </div>
                             <div className="row" style={{marginBottom: 4}}>
                               <div className="col-xs-4">
                                 <b>Latitude: </b>
                               </div>
                               <div className="col-xs-8">
                                 <em>{cluster.longitude}</em>
                               </div>
                             </div>
                             <div className="row" style={{marginBottom: 4}}>
                               <div className="col-xs-4">
                                 <b>Longitude: </b>
                               </div>
                               <div className="col-xs-8">
                                 <em>{cluster.longitude}</em>
                               </div>
                             </div>
                             <hr style={{margin: '10px 0px'}} />
                              <div className="row" style={{marginBottom: 4}}>
                                <div className="col-xs-4">
                                  <b>Township: </b>
                                </div>
                                <div className="col-xs-8">
                                  <em>{cluster.TownName}</em>
                                </div>
                              </div>
                           </div>
                          </Popup>
                        </Marker>
                      </div>
                    )
                  })
                }
                { form.latitude !== '' ?
                  <ExtendedMarker position={current} icon={marker}>
                    <Popup>
                      <div style={{width: 400}}>
                        <div className="row" style={{marginBottom: 8}}>
                          <div className="col-xs-3">
                            <b style={popupLabel}>Cluster: </b>
                          </div>
                          <div className="col-xs-9">
                            <input
                              type="text"
                              name="name"
                              style={{width:'100%'}}
                              value={form.name}
                              placeholder="Cluster"
                              className="form-control"
                              onChange={(e) => this._onChangeForm('name', e.target)}
                            />
                          </div>
                        </div>
                        <div className="row" style={{marginBottom: 8}}>
                          <div className="col-xs-3">
                            <b style={popupLabel}>Alamat: </b>
                          </div>
                          <div className="col-xs-9">
                            <textarea
                              name="address"
                              value={form.address}
                              style={{width:'100%'}}
                              placeholder="Alamat"
                              className="form-control"
                              rows="3"
                              onChange={(e) => this._onChangeForm('address', e.target)}
                             />
                          </div>
                        </div>
                        <div className="row" style={{marginBottom: 8}}>
                          <div className="col-xs-3">
                            <b style={popupLabel}>Lat/Lng: </b>
                          </div>
                          <div className="col-xs-4" style={{paddingRight: 0, width: '35.6%'}}>
                            <input
                              type="text"
                              name="latitude"
                              value={form.latitude}
                              placeholder="Latitude"
                              style={{width:'100%'}}
                              className="form-control"
                              onChange={(e) => this._onChangeForm('latitude', e.target)}
                            />
                          </div>
                          <div className="col-xs-4" style={{paddingRight: 0, width: '35.6%'}}>
                            <input
                              type="text"
                              name="longitude"
                              id="longitude"
                              value={form.longitude}
                              placeholder="Longitude"
                              style={{width:'100%'}}
                              className="form-control"
                              onChange={(e) => this._onChangeForm('longitude', e.target)}
                            />
                          </div>
                        </div>
                      </div>
                    </Popup>
                  </ExtendedMarker> : null
                }
              </Map> : null
              }
            </Modal.Body>
            <Modal.Footer>
              <button
                className="btn btn-qluster-success pull-right"
                disabled={disabled}
                onClick={() => this._onSubmitEditForm()}>
                 { disabled ? "Harap Tunggu..." : "Sunting" }
              </button>
              <button
                className="btn btn-default pull-right modalCancelButton"
                style={{marginRight:10}}
                onClick={() => this.onClickModal()}>Batal
              </button>
            </Modal.Footer>
          </Modal>
        </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    clusterList: state.clusterList,
    id: state.showClient.companyId
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchAllClusterList: cb => dispatch(dispatchAllClusterList(cb)),
    dispatchDeleteCluster: (id, cb) => dispatch(dispatchDeleteCluster(id, cb)),
    dispatchResetClusterList: () => dispatch(dispatchResetClusterList()),
    dispatchEditCluster: (form, cb) => dispatch(dispatchEditCluster(form, cb))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cluster)
