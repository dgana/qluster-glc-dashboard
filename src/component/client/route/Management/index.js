// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import _ from 'lodash'

// Management Component
import AdminManagement from './Admin'
import LocationManagement from './Lokasi'
import RoleManagement from './Peranan'
import CctvManagement from './CCTV'

// Common Component
import DashboardLayout from '../../common/DashboardLayout'
import Wrapper from '../../common/Content/Wrapper'
import Body from '../../common/Content/Body'
import Loading from '../../common/Loading'

class Management extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    const { route, cctvList, adminManagementList, roleList } = this.props
    const loading = _.has(route, 'pathname') ?
    route.pathname === '/manajemen-admin' && adminManagementList.fetched ? false :
    route.pathname === '/manajemen-cctv' && cctvList.fetched ? false :
    route.pathname === '/manajemen-peranan' && roleList.fetched ? false : true : null

    this.setState({ loading })
  }

  _showLoadingImage = () => this.setState({ loading: true })
  _hideLoadingImage = () => this.setState({ loading: false })

  render() {
    const { loading } = this.state
    const { route } = this.props
    return (
      <DashboardLayout>
        <Wrapper>
          { _.has(route, 'pathname') ?
            route.pathname === '/manajemen-admin' ? (
              <div>
                { loading ?
                  <Loading>
                    <AdminManagement
                      display="none"
                      _hideLoadingImage={this._hideLoadingImage}
                      _showLoadingImage={this._showLoadingImage}
                      loading={loading} />
                  </Loading> :
                  <Body>
                    <AdminManagement
                      display="block"
                      _hideLoadingImage={this._hideLoadingImage}
                      _showLoadingImage={this._showLoadingImage}
                      loading={loading} />
                  </Body>
                }
              </div>
            ) :
            route.pathname === '/manajemen-cctv' ? (
              <div>
              { loading ?
                <Loading>
                  <CctvManagement
                    display="none"
                    _hideLoadingImage={this._hideLoadingImage}
                    _showLoadingImage={this._showLoadingImage}
                    loading={loading} />
                </Loading> :
                <Body>
                  <CctvManagement
                    display="block"
                    _hideLoadingImage={this._hideLoadingImage}
                    _showLoadingImage={this._showLoadingImage}
                    loading={loading} />
                </Body>
              }
              </div>
            ) :
            route.pathname === '/manajemen-lokasi' ? (
              <Body>
                <LocationManagement />
              </Body>
            ) : (
              <div>
                { loading ?
                  <Loading>
                    <RoleManagement
                      display="none"
                      _hideLoadingImage={this._hideLoadingImage}
                      _showLoadingImage={this._showLoadingImage}
                      loading={loading} />
                  </Loading> :
                  <Body>
                    <RoleManagement
                      display="block"
                      _hideLoadingImage={this._hideLoadingImage}
                      _showLoadingImage={this._showLoadingImage}
                      loading={loading} />
                  </Body>
                }
              </div>
            ) : null
          }
        </Wrapper>
      </DashboardLayout>
    )
  }
}

const mapStateToProps = state => {
  return {
    route: state.router.location,
    cctvList: state.cctvList,
    adminManagementList: state.adminManagementList,
    roleList: state.roleList
  }
}

export default connect(mapStateToProps)(Management);
