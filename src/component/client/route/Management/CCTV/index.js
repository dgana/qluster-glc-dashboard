// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Common Component
import QmapLayers from '../../../common/Layers'
import Box from '../../../common/Box'
import Body from '../../../common/Box/Body'
import BoxHeader from '../../../common/Box/Header'
import SearchBox from '../../../common/SearchBox'

// Plugin Dependencies
import { Modal } from 'react-bootstrap'
import { Map, Marker, ZoomControl, Popup } from 'react-leaflet'
import L from 'leaflet'
import $ from 'jquery'
import Confirm from 'react-confirm-bootstrap'
import popup from '../../../../../../public/js/bootstrap-notify/popupMessage'
import { Checkbox } from 'react-icheck'
import ReactTooltip from 'react-tooltip'

// Utility
import { limitString30 } from '../../../../../util'
import { greenLakeCity } from '../../../../../util/mapConfig'

// Dispatcher
import {
  dispatchEditCctv,
  dispatchResetCctvQmap,
  dispatchCctvQmap,
  dispatchDeleteCctv
} from '../../../../../dispatcher'

// Loading gif
import loadingImg from '../../../../../../public/imgs/loading/loading_qluster.gif'

// Map Icons
import homeIcon from '../../../../../../public/imgs/icon/home_marker.png'
import cctv from '../../../../../../public/imgs/marker/cctv.png'

const marker = L.icon({
  iconUrl: homeIcon,
  iconAnchor: [15,45],
  popupAnchor: [0,-35]
})

const existingCctvIcon = L.icon({
  iconUrl: cctv,
  iconSize: [26,37],
  iconAnchor: [13,37],
  popupAnchor: [0,-20]
})

class Cctv extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        name: '',
        url: '',
        latitude: '',
        longitude: '',
        isActive: false,
        cctvId:'',
      },
      showModal: false,
      disabled: false,
    }
  }

  //Edit Modal
  onClickModal = () => {
    const currentModalState = this.state.showModal
    this.setState({showModal: !currentModalState})
  }

  onClickMap = (e) => {
    this._onChangeForm('coords',e.latlng)
  }

  _onChangeForm = (field, value) => {
    const newForm = this.state.form;
    if (field === 'coords'){
      newForm.latitude = +value.lat
      newForm.longitude = +value.lng
    } else {
      newForm[field] = value.value
    }
    this.setState({ form: newForm });
  }

  componentDidMount = () => {
    if (!this.props.cctvList.fetched) {
      this.props.dispatchCctvQmap(null,this._drawTable)
    } else {
      $(document).ready(function() {
        $('#table-edit-cctv').DataTable({
          iDisplayLength: 25,
          retrieve: true
        })
      })
    }
  }

  _reloadCctv = () => {
    this.props._showLoadingImage()
    this.props.dispatchResetCctvQmap()
  }

  _onSubmitEditCctv = () => {
    const { form } = this.state
    const { dispatchEditCctv } = this.props

    if (form.name === "" || form.url === "" || form.latitude === "" || form.longitude === "") {
      popup.message('pe-7s-attention',`<span class="alert-popup-text-message">Harap lengkapi form pendaftaran CCTV</span>`,'danger', 2000 ,'bottom','right')
    } else {
      dispatchEditCctv(form, this._callbackRegisterCctv)
      this.setState({ disabled: true })
    }
  }

  _callbackRegisterCctv = (err, result) => {
    if (result) {
      popup.message('pe-7s-like2',`<span class="alert-popup-text-message">CCTV berhasil disunting</span>`,'success', 2000 ,'bottom','right')
      this.setState({
        form: {
          name: '',
          url: '',
          latitude: '',
          longitude: '',
          isActive: '',
          cctvId:'',
        },
        showModal: false,
        disabled: false,
      })
    } else {
      console.log(err);
    }
  }

  _deleteCctvCallback = (err, result) => {
    if (result) {
      popup.message('pe-7s-like2',`<span class="alert-popup-text-message">CCTV berhasil dihapus</span>`,'success', 2000 ,'bottom','right')
    } else {
      console.log(err);
    }
  }

  _onChangeCctv = (val, name) => {
    const { form } = this.state
    const newForm = {
      ...form,
      [val.target.name]: val.target.value
    }
    this.setState({
      form: newForm
    })
  }

  _onChangeMapLatLng = (lat, lng) => {
    const { form } = this.state
    const newCctv = {
      ...form,
      latitude: lat,
      longitude: lng
    }
    this.setState({ form: newCctv })
  }

  _drawTable = (err, result) => {
    if(result) {
      $(document).ready(function() {
        $('#table-edit-cctv').DataTable({
          iDisplayLength: 25,
          retrieve: true
        })
      })
      this.props._hideLoadingImage()
    } else {
      console.log(err);
    }
  }

  handleIsActive = (value) =>{
    const newForm = {...this.state.form,isActive:!value}
    this.setState({form:newForm})
  }

  render() {
    const { form } = this.state
    const { cctvList, loading, display } = this.props

    const current = [form.latitude, form.longitude]
    const existingCctv = cctvList.result.filter(item => item.id !== form.cctvId)

    return (
      <div>
        <Box size="col-md-12" display={display}>
          <BoxHeader title="Manajemen CCTV" />
          <Body padding="4px 20px 6px">
          { loading ? null :
            <div className="row">
              <ReactTooltip id="reload-manajemen-cctv" place={"bottom"} />
              <button
                data-for="reload-manajemen-cctv"
                data-tip="Reload List CCTV"
                data-iscapture="true"
                onClick={() => this._reloadCctv()}
                type="submit"
                className="btn btn-qluster-primary pull-right"
                style={{ position:'absolute', right:24, top:8, border:'none' }}>
                <span className="pe-7s-refresh"></span> Reload
              </button>
              <div className="col-xs-12">
                <table id="table-edit-cctv" className="table text-center">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Nama</th>
                      <th>Link</th>
                      <th>Latitude</th>
                      <th>Longitude</th>
                      <th>Status</th>
                      <th>Tindakan</th>
                    </tr>
                  </thead>
                  <tbody>
                  { !cctvList.fetched ? null :
                    cctvList.result.map((item, index) => (
                    <tr key={index}>
                      <td>{item.id}</td>
                      <td>{item.name}</td>
                      <td><a href={item.url} target="_blank">{limitString30(item.url)}</a></td>
                      <td>{item.latitude}</td>
                      <td>{item.longitude}</td>
                      <td>{item.isActive ? "Aktif" : "Tidak Aktif"}</td>
                      <td style={{textAlign:'center'}}>
                        <ReactTooltip id={"edit-manajemen-cctv-baris-" + item.id} place={"left"} />
                        <ReactTooltip id={"delete-manajemen-cctv-baris-" + item.id} place={"left"} />
                        <i
                          data-for={"edit-manajemen-cctv-baris-" + item.id}
                          data-tip={"Sunting CCTV " + item.name}
                          data-iscapture="true"
                          className="fa fa-edit report-table-icon"
                          onClick={() => {
                            // this.props._showLoadingImage()
                            const newObj = {
                              name: item.name,
                              url: item.url,
                              latitude: item.latitude,
                              longitude: item.longitude,
                              isActive: item.isActive,
                              cctvId: item.id,
                            }
                            this.setState({ form: { ...newObj }})
                            // this.props._hideLoadingImage()
                            this.onClickModal()
                          }}>
                        </i>
                        <Confirm
                          onConfirm={() => this.props.dispatchDeleteCctv(item.id,this._deleteCctvCallback)}
                          body={`Hapus CCTV ${item.name}?`}
                          confirmText="Hapus"
                          cancelText="Batal"
                          confirmBSStyle="danger"
                          title={'Manajemen CCTV'}>
                          <i
                            data-for={"delete-manajemen-cctv-baris-" + item.id}
                            data-tip={"Hapus CCTV " + item.name}
                            data-iscapture="true"
                            className="fa fa-trash report-table-icon">
                          </i>
                        </Confirm>
                      </td>
                    </tr>
                  ))
                  }
                  </tbody>
                </table>
              </div>
            </div>
          }
            <Modal show={this.state.showModal} onHide={this.onClickModal} dialogClassName="modalEdit">
              <Modal.Header bsClass="modalHeaderContainer">
                <p style={{margin:'4px 0px'}}>Sunting CCTV</p>
              </Modal.Header>
              <Modal.Body bsClass="modalBodyContainer">
                <div className="form-horizontal">
                  <div className="form-group">
                    <label className="control-label col-sm-2 col-xs-6" htmlFor="name">Nama:</label>
                    <div className="col-sm-4 col-xs-6">
                      <input
                        type="text"
                        name="name"
                        id="name"
                        className="form-control"
                        value={form.name}
                        placeholder="Nama Cluster"
                        onChange={(e) => this._onChangeForm('name', e.target)} />
                    </div>
                  </div>
                  <div className="form-group">
                    <label className="control-label col-sm-2 col-xs-6" htmlFor="address">Link:</label>
                    <div className="col-sm-4 col-xs-6">
                      <textarea
                        name="url"
                        id="url"
                        rows="1"
                        className="form-control"
                        value={form.url}
                        placeholder="Link CCTV"
                        onChange={(e) => this._onChangeForm('url', e.target)}></textarea>
                    </div>
                    <label className="control-label col-sm-2 col-xs-6" htmlFor="id">Aktif:</label>
                    <div className="col-sm-4 col-xs-6" style={{marginTop:6}}>
                      <span onClick={()=>{this.handleIsActive(form.isActive)}}>
                        <Checkbox
                          checkboxClass="icheckbox_square-green"
                          increaseArea="-100%"
                          checked={form.isActive} />
                      </span>
                    </div>
                  </div>
                  <div className="form-group">
                    <label className="control-label col-sm-2 col-xs-6" htmlFor="latitude">Lat:</label>
                    <div className="col-sm-4 col-xs-6">
                      <input
                        type="text"
                        name="latitude"
                        id="latitude"
                        className="form-control"
                        value={form.latitude}
                        placeholder="Latitude"
                        onChange={(e) => this._onChangeForm('latitude', e.target)} />
                    </div>
                    <label className="control-label col-sm-2 col-xs-6" htmlFor="longitude">Lng:</label>
                    <div className="col-sm-4 col-xs-6">
                      <input
                        type="text"
                        name="longitude"
                        id="longitude"
                        className="form-control"
                        value={form.longitude}
                        placeholder="Longitude"
                        onChange={(e) => this._onChangeForm('longitude', e.target)} />
                    </div>
                  </div>
                </div>
                { form.latitude !== '' ?
                  <Map id="mapQluster" center={[greenLakeCity.latitude, greenLakeCity.longitude]} style={{ height: 300 }} zoomControl={false} zoom={greenLakeCity.zoom} onClick={(e) => this.onClickMap(e)}>
                    <QmapLayers />
                    <SearchBox />
                    <ZoomControl position="bottomleft" />
                    { existingCctv.map((cctv, index) => {
                      const position = [cctv.latitude, cctv.longitude]
                      return (
                        <div key={index}>
                          <Marker position={position} icon={existingCctvIcon}>
                            <Popup>
                              <div className="qmap-popup" style={{width:'250px', height:'130px'}}>
                                <div>
                                  <img src={loadingImg} style={{width:'35%', zIndex:999, marginLeft:'35%', marginTop:'12%'}} alt="Loading" />
                                  <a href={cctv.url}><img src={cctv.url} style={{width:'250px', height:'130px', zIndex:1000, position:'absolute', top:'10px', display:'block'}} alt="CCTV" /></a>
                                </div>
                              </div>
                            </Popup>
                          </Marker>
                        </div>
                      )
                    })
                  }
                  { form.latitude !== '' ?
                    <Marker position={current} icon={marker}>
                    </Marker> : null
                  }
                </Map> : null
                }
              </Modal.Body>
              <Modal.Footer>
                <button
                  className="btn btn-qluster-success pull-right"
                  disabled={this.state.disabled}
                  onClick={() => {
                    this._onSubmitEditCctv()
                  }}>{this.state.disabled ? "Harap Tunggu..." : "Sunting"}
                </button>
              <button
                className="btn btn-default pull-right modalCancelButton"
                style={{marginRight:10}}
                onClick={() => this.onClickModal()}>Batal</button>
              </Modal.Footer>
            </Modal>
          </Body>
        </Box>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    cctvList: state.cctvList,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchCctvQmap: (registerId,cb) => dispatch(dispatchCctvQmap(registerId,cb)),
    dispatchEditCctv: (form, cb) => dispatch(dispatchEditCctv(form, cb)),
    dispatchResetCctvQmap: () => dispatch(dispatchResetCctvQmap()),
    dispatchDeleteCctv: (cctvId, cb) => dispatch(dispatchDeleteCctv(cctvId, cb))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cctv)
