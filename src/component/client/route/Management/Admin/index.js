// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Common Component
import Box from '../../../common/Box'
import Body from '../../../common/Box/Body'
import BoxHeader from '../../../common/Box/Header'

// Plugin Dependencies
import $ from 'jquery'
import { Modal } from 'react-bootstrap'
import Select from 'react-virtualized-select'
import { Checkbox } from 'react-icheck'
import popup from '../../../../../../public/js/bootstrap-notify/popupMessage'
import _ from 'lodash'
import ReactTooltip from 'react-tooltip'

// Dispatcher
import {
  dispatchAdminManagementList,
  dispatchEditUserDashboard,
  dispatchEditUserDashboardPassword
} from '../../../../../dispatcher'

class AdminManagement extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showModalAdmin: false,
      showModalPassword: false,
      formAdmin: {
        userId: '',
        name: '',
        email:'',
        role: '',
        status: ''
      },
      formPass: {
        password: '',
        confirmPassword: '',
        userId: ''
      }
    }
  }

  componentDidMount() {
    if (!this.props.adminManagementList.fetched) {
      this.props.dispatchAdminManagementList(this._drawTable)
    } else {
      $(document).ready(function() {
        $('#table-laporan-admin').DataTable({
          iDisplayLength: 25,
          retrieve: true
        })
      })
    }
  }

  _openModalAdmin = (userId, name, role, email, status) => {
    this.setState({
      showModalAdmin: true,
      formAdmin: { userId, name, role,email, status }
    })
  }

  _openModalPassword = userId => {
    this.setState(prevState => ({
      showModalPassword: true,
      formPass: { ...prevState.formPass, userId }
    }))
  }

  _closeModalAdmin = () => this.setState({ showModalAdmin: false })
  _closeModalPassword = () => this.setState({ showModalPassword: false, formPass:{password:'',confirmPassword:'',userId:''} })

  _onChangeAdmin = (val, name) => {
    const form = this.state.formAdmin
    if (name === 'role') {
      const newForm = { ...form, [name]: val.value, roleId: val.id }
      this.setState({ formAdmin: newForm })
    } else {
      const newForm = { ...form, [val.target.name]: val.target.value }
      this.setState({ formAdmin: newForm })
    }
  }

  _onChangeAdminPass = e => {
    const formPass = { ...this.state.formPass, [e.target.name]: e.target.value }
    this.setState({ formPass })
  }

  _submitEditForm = () => {
    const { formAdmin } = this.state
    if (formAdmin.name === '') {
      popup.message('pe-7s-attention', `<span class="alert-popup-text-message">Nama tidak boleh kosong!</span>`, 'danger', 2000, 'bottom', 'right')
    } else {
      this.props._showLoadingImage()
      this.props.dispatchEditUserDashboard(formAdmin, this._callbackEditUserDashboard)
    }
  }

  _callbackEditUserDashboard = (err, result) => {
    if(result) {
      this._drawTable(null, "DRAW")
      popup.message('pe-7s-like2', `<span class="alert-popup-text-message">Admin berhasil disunting</span>`, 'success', 2000, 'bottom', 'right')
      this.setState({
        formAdmin: {
          name: '',
          role: '',
          roleId: '',
          status: '',
          userId: ''
        }
      })
    } else {
      console.log(err);
    }
  }

  _submitEditFormPassword = () => {
    const { formPass } = this.state
    if (formPass.password === '' || formPass.confirmPassword === '') {
      popup.message('pe-7s-attention', `<span class="alert-popup-text-message">Lengkapi form password</span>`, 'danger', 2000, 'bottom', 'right')
    } else if(formPass.password !== formPass.confirmPassword) {
      popup.message('pe-7s-attention', `<span class="alert-popup-text-message">Password tidak sama!</span>`, 'danger', 2000, 'bottom', 'right')
    } else {
      // this.props._showLoadingImage()
      this.props.dispatchEditUserDashboardPassword(formPass, this._callbackEditUserDashboardPassword)
      this._closeModalPassword()
    }
  }

  _callbackEditUserDashboardPassword = (err, result) => {
    if(result) {
      popup.message('pe-7s-like2', `<span class="alert-popup-text-message">Password admin berhasil disunting</span>`, 'success', 2000, 'bottom', 'right')
    }else {
      popup.message('pe-7s-like2', `<span class="alert-popup-text-message">Terjadi Kesalahan!</span>`, 'danger', 2000, 'bottom', 'right')
    }
  }

  _onClickActive = (status) => {
    const form = this.state.formAdmin
    if (status === 'aktif') {
      this.setState({ formAdmin: { ...form, status: 'tidak aktif' }})
    } else {
      this.setState({ formAdmin: { ...form, status: 'aktif' }})
    }
  }

  _fetchAdminReport = () => {
    this.props._showLoadingImage()
    this.props.dispatchAdminManagementList(this._drawTable)
  }

  _drawTable = (err, result) => {
    if(result) {
      $(document).ready(function() {
        $('#table-laporan-admin').DataTable({
          iDisplayLength: 25,
          retrieve: true
        })
      })
      this.props._hideLoadingImage()
    } else {
      console.log(err);
    }
  }

  _modalAdmin = (formAdmin, roleOptions, showModalAdmin) => (
    <Modal
      show={showModalAdmin}
      onHide={this._closeModalAdmin}
      aria-labelledby="ModalHeader">
      <Modal.Header>
        <p style={{margin:'4px 0px'}}>Manajemen Admin</p>
      </Modal.Header>
      <Modal.Body>
        <div className="form-horizontal" style={{marginTop: 12}}>
          <div className="form-group">
            <label className="control-label col-sm-2 col-xs-4" htmlFor="id">Nama:</label>
            <div className="col-sm-6 col-xs-8">
              <input
                type="text"
                name="name"
                id="name"
                className="form-control"
                value={formAdmin.name}
                placeholder="Nama Admin"
                onChange={(val, name) => this._onChangeAdmin(val, 'name')} />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-2 col-xs-4" htmlFor="id">Peranan:</label>
            <div className="col-sm-6 col-xs-8" style={{marginBottom: 8}}>
              <Select
                name="role"
                value={formAdmin.role}
                disabled={false}
                placeholder="Peranan..."
                options={roleOptions}
                clearable={false}
                onChange={(val, name) => this._onChangeAdmin(val, 'role')} />
            </div>
            <label className="control-label col-sm-2 col-xs-4" htmlFor="id">Aktif:</label>
            <div className="col-sm-2 col-xs-8" style={{marginTop:6}} onClick={() => this._onClickActive(formAdmin.status)}>
              <Checkbox
                checkboxClass="icheckbox_square-green"
                increaseArea="-100%"
                checked={formAdmin.status === 'aktif'} />
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <button className="btn btn-qluster-success" onClick={this._submitEditForm}>
          Sunting
        </button>
      </Modal.Footer>
    </Modal>
  )

  _modalPassword = (formPass, showModalPassword) => (
    <Modal
      show={showModalPassword}
      onHide={this._closeModalPassword}
      aria-labelledby="ModalHeader">
      <Modal.Header>
        <p style={{margin:'4px 0px'}}>Manajemen Admin</p>
      </Modal.Header>
      <Modal.Body>
        <div className="form-horizontal" style={{marginTop: 12}}>
          <div className="form-group">
            <label className="control-label col-sm-4 col-xs-4" htmlFor="password">Password Baru:</label>
            <div className="col-sm-6 col-xs-8">
              <input
                type="password"
                name="password"
                id="password"
                className="form-control"
                value={formPass.password}
                placeholder="Password minimal 6 karakter"
                onChange={this._onChangeAdminPass} />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label col-sm-4 col-xs-4" htmlFor="confirmPassword">Konfirmasi Password:</label>
            <div className="col-sm-6 col-xs-8">
              <input
                type="password"
                name="confirmPassword"
                id="confirmPassword"
                className="form-control"
                value={formPass.confirmPassword}
                placeholder="Password minimal 6 karakter"
                onChange={this._onChangeAdminPass} />
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <button className="btn btn-qluster-success" onClick={this._submitEditFormPassword}>
          Sunting
        </button>
      </Modal.Footer>
    </Modal>
  )

  render() {
    const { formAdmin, formPass, showModalAdmin, showModalPassword } = this.state
    const { adminManagementList, loading } = this.props
    const { managers,roleOptions } = adminManagementList.result

    return (
      <Box size="col-md-12" display={this.props.display}>

        { this._modalAdmin(formAdmin, roleOptions, showModalAdmin) }
        { this._modalPassword(formPass, showModalPassword)}

        <BoxHeader title="Manajemen Admin" />
        <Body padding="4px 20px 6px">
        { loading ? null :
          <div className="row">
            <ReactTooltip id="reload-manajemen-admin" place={"bottom"} />
            <button
              data-for="reload-manajemen-admin"
              data-tip="Reload Admin"
              data-iscapture="true"
              onClick={() => this._fetchAdminReport()}
              type="submit"
              className="btn btn-qluster-primary pull-right"
              style={{position:'absolute', right:24, top:8, border:'none'}}>
              <span className="pe-7s-refresh"></span>
               Reload
            </button>
            <div className="col-xs-12">
              <table id="table-laporan-admin" className="table text-center adminTable">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Email</th>
                    <th>Nama</th>
                    <th>Peranan</th>
                    <th>Tanggal Perubahan Terakhir</th>
                    <th>Status</th>
                    <th>Tindakan</th>
                  </tr>
                </thead>
                <tbody>
                { managers.length > 0 ? managers.map((item, index) => (
                  <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.email}</td>
                    <td>{item.name}</td>
                    <td>{item.role}</td>
                    <td>{item.updatedAt}</td>
                    <td>{item.status}</td>
                    <td>
                      <ReactTooltip id={"edit-manajemen-admin-baris-" + item.id} place={"left"} />
                      <span
                        data-for={"edit-manajemen-admin-baris-" + item.id}
                        data-tip={"Sunting Admin " + item.name}
                        data-iscapture="true"
                        className="fa fa-pencil-square-o"
                        style={{marginRight: 15, cursor: 'pointer'}}
                        onClick={() => this._openModalAdmin(item.id, item.name, item.role, item.email, item.status)}></span>
                      <ReactTooltip id={"password-manajemen-admin-baris-" + item.id} place={"left"} />
                      <span
                        data-for={"password-manajemen-admin-baris-" + item.id}
                        data-tip={"Ubah Sandi " + item.name}
                        data-iscapture="true"
                        className="fa fa-key"
                        style={{cursor: 'pointer'}}
                        onClick={() => this._openModalPassword(item.id)}></span>
                    </td>
                  </tr>
                  )) : null
                }
                </tbody>
              </table>
            </div>
          </div>
        }
        </Body>
      </Box>
    )
  }
}

const mapStateToProps = state => {
  return {
    adminManagementList: state.adminManagementList,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchAdminManagementList: (cb) => dispatch(dispatchAdminManagementList(cb)),
    dispatchEditUserDashboard: (form, cb) => dispatch(dispatchEditUserDashboard(form, cb)),
    dispatchEditUserDashboardPassword: (form, cb) => dispatch(dispatchEditUserDashboardPassword(form, cb))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminManagement)
