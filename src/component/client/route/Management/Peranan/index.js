// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Common Component
import Box from '../../../common/Box'
import Body from '../../../common/Box/Body'
import BoxHeader from '../../../common/Box/Header'

// Plugin Dependencies
import $ from 'jquery'
import classNames from 'classnames'
import { Modal } from 'react-bootstrap'
import { Checkbox } from 'react-icheck'
import popup from '../../../../../../public/js/bootstrap-notify/popupMessage'
import _ from 'lodash'
import ReactTooltip from 'react-tooltip'

// Dispatcher
import {
  dispatchRoleList,
  dispatchEditRole,
  dispatchRoute
} from '../../../../../dispatcher'

// Utility
import clientRoute from '../../../../../util/clientRoute'

const checkboxStyle = { padding:'10px 5%', width:'10%', cursor:'pointer' }

class RoleManagement extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tableIsEmpty: false,
      showModalRole: false,
      formRole: {
        id: Number,
        name: '',
        access: []
      },
      buttonDisable: false
    }
  }

  componentDidMount() {
    this.props.dispatchRoute('client', clientRoute)
    if (!this.props.roleList.fetched) {
      this.props.dispatchRoleList(this._drawTable)
    } else {
      $(document).ready(function() {
        $('#table-laporan-peranan').DataTable({
          iDisplayLength: 25,
          retrieve: true
        })
      })
    }
  }

  _onClick = (item) => {
    const { formRole } = this.state
    const newAccess = formRole.access.map(data => (data.id===item.id)?{id:item.id,feature:item.feature,picked:!item.picked,platform:item.platform}:data)
    this.setState({formRole:{...formRole,access:newAccess}})
  }

  _openModalRole = (id, name, access) => {
    $(document).ready(function() {
      $('#table-manajemen-peran').DataTable({ "iDisplayLength": 50, "bSort": false })
      $('#table-manajemen-peran_length').last().css('display','none')
      $('#table-manajemen-peran_wrapper .row').last().css('display','none')
      $("td:nth-child(2)").attr("style","text-align:left; padding-left:30px")
    })

    this.setState({
      showModalRole: true,
      formRole: {
        id,
        name,
        access
      }
    })

  }

  _closeModalRole = () => {
    this.setState({
      showModalRole: false
    })
  }

  _onChangeRole = e => {
    const newRole = {
      ...this.state.formRole,
      name: e.target.value
    }

    this.setState(prevState => ({
      formRole: newRole
    }))
  }

  _submitEditForm = () => {
    const { formRole } = this.state
    const { access } = formRole
    if (formRole.name === "") {
      popup.message('pe-7s-attention', `<span class="alert-popup-text-message">Nama peran tidak boleh kosong</span>`, 'danger', 2000, 'bottom', 'right')
    } else {
      const newFeature = access.map(data =>({id:data.id,status:data.picked}))
      const newRole = {
        newName:formRole.name,
        roleFeature:{roleId:formRole.id,feature:newFeature}
      }
      this.props.dispatchEditRole(newRole, this._callbackEditRole)
      this.setState({ buttonDisable: true })
    }
  }

  _callbackEditRole = (err, result) => {
    if (result) {
      popup.message('pe-7s-like2', `<span class="alert-popup-text-message">Peran berhasil disunting</span>`, 'success', 2000, 'bottom', 'right')
      this.setState({ showModalRole: false, buttonDisable: false })
    } else {
      console.log(err);
    }
  }

  _fetchRoleList = () => {
    this.props._showLoadingImage()
    this.props.dispatchRoleList(this._drawTable)
  }

  _drawTable = (err, result) => {
    if(result) {
      $(document).ready(function() {
        $('#table-laporan-peranan').DataTable({
          iDisplayLength: 25,
          retrieve: true
        })
      })
      this.props._hideLoadingImage()
    } else {
      console.log(err);
    }
  }

  render() {

    const { tableIsEmpty, showModalRole, formRole, buttonDisable } = this.state
    const { display, loading, roleList } = this.props
    const { access } = formRole
    return (
      <Box size="col-md-12" display={display}>
        <Modal
          show={showModalRole}
          onHide={this._closeModalRole}
          aria-labelledby="ModalHeader"
          dialogClassName="modalEdit">
          <Modal.Header>
            <p style={{margin:'4px 0px'}}>Manajemen Peranan</p>
          </Modal.Header>
          <Modal.Body bsClass="modalBodyPadding">
            <div className="form-horizontal" style={{marginTop: 12, marginBottom: 24}}>
              <div className="form-group">
                <label className="control-label col-sm-2 col-xs-4" style={{textAlign: 'left'}} htmlFor="name">Nama Peran:</label>
                <div className="col-sm-6 col-xs-8">
                  <input
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                    value={formRole.name}
                    placeholder="Sunting nama peran"
                    onChange={this._onChangeRole}/>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-12">
                <p style={{position: 'absolute'}}>Hak Akses : </p>
                <table id="table-manajemen-peran" className="table text-center">
                  <thead>
                    <tr>
                      <th width="50">No. </th>
                      <th style={{textAlign:'left', paddingLeft:20}}>Fungsi</th>
                      <th style={{textAlign:'left', paddingLeft:20}}>Platform</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  { access.length>0 ?
                    access.map((item, index) => {
                      return (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td style={{textAlign:'left', paddingLeft:20}}><p>{item.feature}</p></td>
                          <td style={{textAlign:'left', paddingLeft:20}}><p>{item.platform}</p></td>
                          <td onClick={() => this._onClick(item)} style={checkboxStyle}>
                            <Checkbox
                              checkboxClass="icheckbox_square-green"
                              increaseArea="-100%"
                              checked={item.picked}
                            />
                          </td>
                        </tr>
                      )
                    }): <tr><td></td></tr>
                  }
                  </tbody>
                </table>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <button className="btn btn-default" onClick={this._closeModalRole}>
              Batal
            </button>
            { buttonDisable ?
              <button  className="btn btn-qluster-success" disabled="true">Harap Tunggu...</button> :
              <button className="btn btn-qluster-success" onClick={this._submitEditForm}>Sunting</button>
            }
          </Modal.Footer>
        </Modal>

        <BoxHeader title="Manajemen Peranan" />
        <Body padding="4px 20px 6px">
          { loading ? null :
            <div className="row">
              <ReactTooltip id="reload-manajemen-peran" place={"bottom"} />
              <button
                data-for="reload-manajemen-peran"
                data-tip="Reload List Peran"
                data-iscapture="true"
                onClick={() => this._fetchRoleList()}
                type="submit"
                className="btn btn-qluster-primary pull-right"
                style={{position:'absolute', right:24, top:8, border:'none'}}>
                <span className="pe-7s-refresh"></span> Reload
              </button>
              <div className="col-xs-12">
                <table id="table-laporan-peranan" className="table text-center perananTable">
                  <thead>
                    <tr>
                      <th className={classNames({ emptyTable: tableIsEmpty })}>ID</th>
                      <th className={classNames({ emptyTable: tableIsEmpty })}>Nama Peran</th>
                      <th className={classNames({ emptyTable: tableIsEmpty })}>Tanggal Perubahan Terakhir</th>
                      <th className={classNames({ emptyTable: tableIsEmpty })}>Tindakan</th>
                    </tr>
                  </thead>
                  <tbody>
                  { roleList.result.length > 0 ? roleList.result.map((item, index) => (
                    <tr key={index}>
                      <td>{item.id}</td>
                      <td>{item.role}</td>
                      <td>{item.updatedAt}</td>
                      <td>
                        <ReactTooltip id={"edit-manajemen-peran-"+index} place={"bottom"} />
                        <span
                          data-for={"edit-manajemen-peran-"+index}
                          data-tip={"Sunting peran "+item.role}
                          data-iscapture="true"
                          className="fa fa-pencil-square-o"
                          style={{marginRight: 15, cursor: 'pointer'}}
                          onClick={() => this._openModalRole(item.id, item.role, item.features)}></span>
                      </td>
                    </tr>
                  )) : null
                  }
                  </tbody>
                </table>
              </div>
            </div>
          }
        </Body>
      </Box>
    )
  }
}

const mapStateToProps = state => {
  return {
    roleList: state.roleList,
    route: state.router.location,
    pathList: state.path
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchRoleList: (cb) => dispatch(dispatchRoleList(cb)),
    dispatchEditRole: (form, cb) => dispatch(dispatchEditRole(form, cb)),
    dispatchRoute: (role, route) => dispatch(dispatchRoute(role, route)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RoleManagement)
