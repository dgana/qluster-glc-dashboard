// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Common Component
import DashboardLayout from '../../common/DashboardLayout'
import Wrapper from '../../common/Content/Wrapper'
import EmptyBox from '../../common/EmptyBox'
import Loading from '../../common/Loading'

// Plugin Dependencies
import { Map, Marker, Popup } from 'react-leaflet'
import L from 'leaflet'
import MarkerClusterGroup from 'react-leaflet-markercluster'

import _ from 'lodash'
// import { EditControl } from "react-leaflet-draw"
import firebase from "firebase"
import {
  REACT_APP_FIREBASE_API_KEY,
  REACT_APP_FIREBASE_PROJECT_ID,
  REACT_APP_FIREBASE_DATABASE_URL
} from '../../../../../config'

// Qmap Component
import MapMenu from './MapMenu'
import QmapLayers from '../../common/Layers'
import SearchBox from '../../common/SearchBox'

// Utility
import { fullDate } from '../../../../util'
import { greenLakeCity } from '../../../../util/mapConfig'

// Loading GIF
import loadingImg from '../../../../../public/imgs/loading/loading_qluster.gif'

// Markers
import marker from './markers'

// Images
import noImage from '../../../../../public/imgs/icon/no_image.jpg'
import star from '../../../../../public/imgs/icon/star.png'

// Dispatcher
import {
  dispatchAllCategories,
  dispatchComplaintQmap,
  // dispatchServiceQmap,
  dispatchBillingQmap,
  // dispatchSosQmap,
  dispatchCctvQmap,
  // dispatchResetCctvQmap,
  dispatchAllClusterList,
  dispatchHouseByCluster,
  dispatchRemoveHouseByCluster,
  dispatchLoadHouseByCluster,
  // dispatchResetHouseByCluster,
  dispatchShowMapMenu
} from '../../../../dispatcher'

// let polyline;

let currentState = {
  loading: false,
  height: window.innerHeight - 100,
  expandMapMenu: false,
  open: {
    keluhan: false,
    jasa: false,
    tagihan: false,
    sos: false,
    rumah: false,
    cctv: false,
    gps: false
  },
  category: {
    keluhan: [],
    jasa: [],
    tagihan: [
      { checked: false }
    ],
    sos: [
      { checked: false }
    ],
    rumah: [
      { checked: false }
    ],
    cctv: [
      { checked: false }
    ],
    gps: [
      { checked: false }
    ]
  },
  markers: [],
  gpsMarker: [],
  clusterArray : [],
  keluhanArray: [],
  keluhanValue: [],
  clusterValue: [],
  currentClusterId: '',
  currentKeluhanId: ''
}

class Qmap extends Component {
  constructor() {
    super()
    this.state = currentState
  }

  componentDidMount() {
    const { keluhan } = this.state.category
    this.props.dispatchShowMapMenu()
    this.props.dispatchAllCategories(this._getAllCategories)
    this.props.dispatchAllClusterList(this._callbackAllClusterList)

    if (firebase.apps.length) {
      this.setState({
        gpsTracker: firebase.database().ref('/asg/glc/gps')
      })
    } else {
      const config = {
        apiKey: process.env.REACT_APP_FIREBASE_API_KEY || REACT_APP_FIREBASE_API_KEY,
        authDomain: `${process.env.REACT_APP_FIREBASE_PROJECT_ID || REACT_APP_FIREBASE_PROJECT_ID}.firebaseapp.com`,
        databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL || REACT_APP_FIREBASE_DATABASE_URL,
        // storageBucket: "<BUCKET>.appspot.com",
      }
      firebase.initializeApp(config)
      this.setState({
        gpsTracker: firebase.database().ref('/asg/glc/gps')
      })
    }
  }

  _callbackAllClusterList = (err, result) => {
    if (err) console.log(err);
  }

  _getAllCategories = (err, result) => {
    if (result) {

      const keluhan = result.Keluhan.map(item => ({ ...item, checked: false }))
      const tagihan = result.Tagihan.map(item => ({ ...item, checked: false }))
      const cctv = result.CCTV.map(item => ({ ...item, checked: false }))
      const gps = result.gps.map(item => ({ ...item, checked: false }))

      console.log(keluhan)

      const category = {
        keluhan,
        // jasa,
        tagihan,
        // sos,
        // rumah,
        cctv,
        gps
      }
      console.log(category);

      this.setState(prevState => ({
        category: {
          ...prevState.category,
          ...category
        }
      }))

    } else {
      console.log(err);
    }
  }

  _houseSelectChange = clusterValue => {
    const clusterArray = clusterValue.split(',')
    if (clusterArray[0] === "") {
      this.setState({ clusterArray: [], clusterValue })
    } else {
      this.setState({ clusterArray, clusterValue })
    }
	}

  _keluhanSelectChange = keluhanValue => {
    const keluhanArray = keluhanValue.split(',')
    if (keluhanArray[0] === "") {
      this.setState({ keluhanArray: [], keluhanValue })
    } else {
      this.setState({ keluhanArray, keluhanValue })
    }
  }

  componentDidUpdate = (prevProps, prevState) => {
    const { clusterArray, keluhanArray, category } = this.state

    if (prevState.clusterArray.length !== clusterArray.length) {
      if (prevState.clusterArray.length < clusterArray.length) {
        this.props.dispatchLoadHouseByCluster()
        const rumah = category.rumah.map(item => {
          if (item.id === clusterArray[clusterArray.length - 1]) {
            return {
              ...item,
              checked: true
            }
          } else {
            return {
              ...item
            }
          }
        })
        this.setState(prevState => ({
          category: {
            ...prevState.category,
            rumah
          }
        }))
        this.props.dispatchHouseByCluster(clusterArray[clusterArray.length - 1])
      } else {
        const getRemovedClusterId = prevState.clusterArray.filter(val => !clusterArray.includes(val))
        const rumah = category.rumah.map(item => {
          if (item.id === getRemovedClusterId[0]) {
            return {
              ...item,
              checked: false
            }
          } else {
            return {
              ...item
            }
          }
        })
        console.log(getRemovedClusterId[0]);
        this.props.dispatchRemoveHouseByCluster(getRemovedClusterId[0])
        this.setState(prevState => ({
          currentClusterId: getRemovedClusterId[0] ,
          category: {
            ...prevState.category,
            rumah
          }
        }))
      }
    }

    if (prevState.keluhanArray.length !== keluhanArray.length) {
      if (prevState.keluhanArray.length < keluhanArray.length) {
        const keluhan = category.keluhan.map(item => {
          if (item.id === keluhanArray[keluhanArray.length - 1]) {
            return {
              ...item,
              checked: true
            }
          } else {
            return {
              ...item
            }
          }
        })
        this.setState(prevState => ({
          loading: true,
          category: {
            ...prevState.category,
            keluhan
          }
        }))
        this.props.dispatchComplaintQmap(keluhanArray[keluhanArray.length - 1], this._complaintCallback)
      } else {
        const getRemovedKeluhanId = prevState.keluhanArray.filter(val => !keluhanArray.includes(val))
        const keluhan = category.keluhan.map(item => {
          if (item.id === getRemovedKeluhanId[0]) {
            return {
              ...item,
              checked: false
            }
          } else {
            return {
              ...item
            }
          }
        })
        this._complaintCallback("reset", null, getRemovedKeluhanId[0])
        this.setState(prevState => ({
          currentKeluhanId: getRemovedKeluhanId[0],
          category: {
            ...prevState.category,
            keluhan
          }
        }))
      }
    }
  }

  componentWillUnmount = () => {
    // this.props.dispatchResetCctvQmap()
    // this.props.dispatchResetHouseByCluster()
    currentState = this.state
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.toggle !== this.props.toggle) {
      setTimeout(() => this.refs['qmap'].leafletElement.invalidateSize(true), 500)
    }

    if (nextProps.filteredHouse.result.length > this.props.filteredHouse.result.length) {
      const markers = nextProps.filteredHouse.result.map(item => {
        let icon = item.isActive ? marker.rumahDone : marker.rumahWait
        return {
          id: item.ClusterId,
          lat: item.latitude,
          lng: item.longitude,
          popup: this._getLeafletPopupOccupant(item),
          options: { icon }
        }
      })
      this.setState({ markers })
    }
  }

  _expandMapMenu = () => {
    this.setState(prevState => ({
      expandMapMenu: prevState.expandMapMenu ? false : true
    }))
  }

  _activeMapBar = topic => {
    const { open } = this.state
    const newActive = topic === 'keluhan' ? { ...open, keluhan: open.keluhan ? false : true  } :
    topic === 'jasa' ? { ...open, jasa: open.jasa ? false : true  } :
    topic === 'tagihan' ? { ...open, tagihan: open.tagihan ? false : true  } :
    topic === 'sos' ? { ...open, sos: open.sos ? false : true  } :
    topic === 'rumah' ? { ...open, rumah: open.rumah ? false : true  } :
    topic === 'cctv' ? { ...open, cctv: open.cctv ? false : true  } :
    topic === 'gps' ? { ...open, gps: open.gps ? false : true  } : null
    this.setState({ open: newActive })
  }

  _checkCategory = (type, categoryId, index) => {
    const { category } = this.state

    const newCategory = [
      ...category[type].slice(0, index),
      { ...category[type][index], checked: category[type][index].checked ? false : true, id: categoryId },
      ...category[type].slice(index + 1)
    ]
    const setCategory = Object.assign({}, category, { [type]: newCategory })

    this.setState({
      category: setCategory
    })
  }

  componentWillUpdate(nextProps, nextState) {
    const {
      dispatchBillingQmap,
      // dispatchServiceQmap,
      // dispatchSosQmap,
      dispatchCctvQmap
    } = this.props

    const {
      // jasa: nextJasa,
      tagihan: nextTagihan,
      // sos: nextSos,
      cctv: nextCctv,
      gps: nextGps
    } = nextState.category

    const {
      // keluhan,
      // jasa,
      tagihan,
      // sos,
      // rumah,
      cctv,
      gps
    } = this.state.category

    const compareBillingLength = nextTagihan.filter(item => item.checked).length !== tagihan.filter(item => item.checked).length
    const compareCctvLength = nextCctv.filter(item => item.checked).length !== cctv.filter(item => item.checked).length

    if (compareBillingLength || compareCctvLength ) {

      // // Category Jasa
      // nextJasa.forEach((item, index) => {
      //   if (item.checked && !jasa[index].checked) {
      //     return dispatchServiceQmap(compId, item.id, this._serviceCallback)
      //   } else if (!item.checked && jasa[index].checked) {
      //     this._serviceCallback("reset", null, item.id)
      //   }
      // })

      // Category Tagihan
      nextTagihan.forEach((item, index) => {
        if (item.checked && !tagihan[index].checked) {
          return dispatchBillingQmap('T001', this._billingCallback)
        } else if (!item.checked && tagihan[index].checked) {
          this._billingCallback("reset", null, 'T001')
        }
      })

      // // Category Sos
      // nextSos.forEach((item, index) => {
      //   if (item.checked && !sos[index].checked) {
      //     return dispatchSosQmap(compId, 'S001', this._sosCallback)
      //   } else if (!item.checked && sos[index].checked) {
      //     this._sosCallback("reset", null, 'S001')
      //   }
      // })


      // Category CCTV
      nextCctv.forEach((item, index) => {
        if (item.checked && !cctv[index].checked) {
          return dispatchCctvQmap('C001', this._cctvCallback)
        } else if (!item.checked && cctv[index].checked) {
          this._cctvCallback("reset", null, 'C001')
        }
      })

      // Category GPS

    }

    nextGps.forEach((item, index) => {
      if (item.checked && !gps[index].checked) {
        let snapshots
        this.state.gpsTracker.on('value', snapshotGps => {
          this._gpsCallback(null, snapshotGps.val(), 'G001')
        })
      } else if (!item.checked && gps[index].checked) {
        this.state.gpsTracker.off()
        this._gpsCallback("reset", null, 'G001')
      }
    })

    if (nextProps.filteredHouse.result.length < this.props.filteredHouse.result.length) {
      this.setState(prevState => ({
        markers: prevState.markers.filter(item => item.id !== nextState.currentClusterId)
      }))
    }
  }

  _gpsCallback = (err, result, categoryId) => {
    let gpsMarker
    if (result) {

      var newArrayDataOfOjbect = Object.values(result)

      console.log(newArrayDataOfOjbect);
      gpsMarker = newArrayDataOfOjbect.map(item => {
        return {
          id: 'G001',
          address: item.address,
          createdAt: fullDate(item.createdAt),
          device_gsm_number: item.device_gsm_number,
          device_identity_number: item.device_identity_number,
          device_name: item.device_name,
          ignition: item.ignition,
          ignition_by_speed: item.ignition_by_speed,
          speed_kph: item.speed_kph,
          time_zone: item.time_zone,
          updatedAt: fullDate(item.updatedAt),
          position: [Number(item.latitude), Number(item.longitude)],
        }
      })

      console.log(err, result, categoryId)
      console.log(gpsMarker);
      // this._gpsCallback("reset", null, 'G001')

      this.setState(prevState => ({
        gpsMarker: [
          ...gpsMarker
        ]
      }))
    } else {
      this.setState(prevState => ({
        gpsMarker: prevState.markers.filter(item => item.id !== categoryId)
      }))
    }
  }

  _getLeafletPopupGps = obj => {
    return L.popup({minWidth: 200, closeButton: true, 'className': 'outer-popup'})
      .setContent(`
        <div class="qmap-popup" style="min-height:50px">
          <p>GPS</p>
        </div>
      `);
  }

  _complaintCallback = (err, result, reportCategoryId) => {
    if(result) {
      let markers, icon, id
      markers = result.map(item => {
        id = item.labelId
        if (item.status === "waiting") {
          icon = id === 'j6lo1m8e' ? marker.foundationsWait :
          id === 'j6lo1m8f' ? marker.superstructureWait :
          id === 'j6lo1m8g' ? marker.exteriorVerticalWait :
          id === 'j6lo1m8h' ? marker.exteriorHorizontalWait :
          id === 'j6lo1m8i' ? marker.interiorConstructionWait :
          id === 'j6lo1m8j' ? marker.interiorFinishesWait :
          id === 'j6lo1m8k' ? marker.conveyingWait :
          id === 'j6lo1m8l' ? marker.plumbingWait :
          id === 'j6lo1m8m' ? marker.hvacWait :
          id === 'j6lo1m8n' ? marker.fireProtectionWait :
          id === 'j6lo1m8o' ? marker.electricalWait :
          id === 'j6lo1m8p' ? marker.electronicWait :
          id === 'j6lo1m8q' ? marker.equipmentWait :
          id === 'j6lo1m8r' ? marker.furnishingsWait :
          id === 'j6lo1m8s' ? marker.specialConstructionWait :
          id === 'j6lo1m8t' ? marker.sitePreparationWait :
          id === 'j6lo1m8u' ? marker.siteImprovementsWait :
          id === 'j6lo1m8v' ? marker.liquidAndGasWait :
          id === 'j6lo1m8w' ? marker.electricalSiteWait :
          id === 'j6lo1m8x' ? marker.constructionSiteWait :
          id === 'j6lo1m8y' ? marker.houseKeepingWait :
          id === 'j6lo1m8z' ? marker.securityWait :
          id === 'j6lo1m90' ? marker.pestControlWait :
          id === 'j6lo1m91' ? marker.parkirWait :
          id === 'j6lo1m92' ? marker.othersWait : marker.othersWait
        } else if (item.status === 'progress') {
          icon = id === 'j6lo1m8e' ? marker.foundationsProgress :
          id === 'j6lo1m8f' ? marker.superstructureProgress :
          id === 'j6lo1m8g' ? marker.exteriorVerticalProgress :
          id === 'j6lo1m8h' ? marker.exteriorHorizontalProgress :
          id === 'j6lo1m8i' ? marker.interiorConstructionProgress :
          id === 'j6lo1m8j' ? marker.interiorFinishesProgress :
          id === 'j6lo1m8k' ? marker.conveyingProgress :
          id === 'j6lo1m8l' ? marker.plumbingProgress :
          id === 'j6lo1m8m' ? marker.hvacProgress :
          id === 'j6lo1m8n' ? marker.fireProtectionProgress :
          id === 'j6lo1m8o' ? marker.electricalProgress :
          id === 'j6lo1m8p' ? marker.electronicProgress :
          id === 'j6lo1m8q' ? marker.equipmentProgress :
          id === 'j6lo1m8r' ? marker.furnishingsProgress :
          id === 'j6lo1m8s' ? marker.specialConstructionProgress :
          id === 'j6lo1m8t' ? marker.sitePreparationProgress :
          id === 'j6lo1m8u' ? marker.siteImprovementsProgress :
          id === 'j6lo1m8v' ? marker.liquidAndGasProgress :
          id === 'j6lo1m8w' ? marker.electricalSiteProgress :
          id === 'j6lo1m8x' ? marker.constructionSiteProgress :
          id === 'j6lo1m8y' ? marker.houseKeepingProgress :
          id === 'j6lo1m8z' ? marker.securityProgress :
          id === 'j6lo1m90' ? marker.pestControlProgress :
          id === 'j6lo1m91' ? marker.parkirProgress :
          id === 'j6lo1m92' ? marker.othersProgress : marker.othersProgress
        } else if (item.status === 'done') {
          icon = id === 'j6lo1m8e' ? marker.foundationsDone :
          id === 'j6lo1m8f' ? marker.superstructureDone :
          id === 'j6lo1m8g' ? marker.exteriorVerticalDone :
          id === 'j6lo1m8h' ? marker.exteriorHorizontalDone :
          id === 'j6lo1m8i' ? marker.interiorConstructionDone :
          id === 'j6lo1m8j' ? marker.interiorFinishesDone :
          id === 'j6lo1m8k' ? marker.conveyingDone :
          id === 'j6lo1m8l' ? marker.plumbingDone :
          id === 'j6lo1m8m' ? marker.hvacDone :
          id === 'j6lo1m8n' ? marker.fireProtectionDone :
          id === 'j6lo1m8o' ? marker.electricalDone :
          id === 'j6lo1m8p' ? marker.electronicDone :
          id === 'j6lo1m8q' ? marker.equipmentDone :
          id === 'j6lo1m8r' ? marker.furnishingsDone :
          id === 'j6lo1m8s' ? marker.specialConstructionDone :
          id === 'j6lo1m8t' ? marker.sitePreparationDone :
          id === 'j6lo1m8u' ? marker.siteImprovementsDone :
          id === 'j6lo1m8v' ? marker.liquidAndGasDone :
          id === 'j6lo1m8w' ? marker.electricalSiteDone :
          id === 'j6lo1m8x' ? marker.constructionSiteDone :
          id === 'j6lo1m8y' ? marker.houseKeepingDone :
          id === 'j6lo1m8z' ? marker.securityDone :
          id === 'j6lo1m90' ? marker.pestControlDone :
          id === 'j6lo1m91' ? marker.parkirDone :
          id === 'j6lo1m92' ? marker.othersDone : marker.othersDone
        } else {
          icon = id === 'j6lo1m8e' ? marker.foundationsInvalid :
          id === 'j6lo1m8f' ? marker.superstructureInvalid :
          id === 'j6lo1m8g' ? marker.exteriorVerticalInvalid :
          id === 'j6lo1m8h' ? marker.exteriorHorizontalInvalid :
          id === 'j6lo1m8i' ? marker.interiorConstructionInvalid :
          id === 'j6lo1m8j' ? marker.interiorFinishesInvalid :
          id === 'j6lo1m8k' ? marker.conveyingInvalid :
          id === 'j6lo1m8l' ? marker.plumbingInvalid :
          id === 'j6lo1m8m' ? marker.hvacInvalid :
          id === 'j6lo1m8n' ? marker.fireProtectionInvalid :
          id === 'j6lo1m8o' ? marker.electricalInvalid :
          id === 'j6lo1m8p' ? marker.electronicInvalid :
          id === 'j6lo1m8q' ? marker.equipmentInvalid :
          id === 'j6lo1m8r' ? marker.furnishingsInvalid :
          id === 'j6lo1m8s' ? marker.specialConstructionInvalid :
          id === 'j6lo1m8t' ? marker.sitePreparationInvalid :
          id === 'j6lo1m8u' ? marker.siteImprovementsInvalid :
          id === 'j6lo1m8v' ? marker.liquidAndGasInvalid :
          id === 'j6lo1m8w' ? marker.electricalSiteInvalid :
          id === 'j6lo1m8x' ? marker.constructionSiteInvalid :
          id === 'j6lo1m8y' ? marker.houseKeepingInvalid :
          id === 'j6lo1m8z' ? marker.securityInvalid :
          id === 'j6lo1m90' ? marker.pestControlInvalid :
          id === 'j6lo1m91' ? marker.parkirInvalid :
          id === 'j6lo1m92' ? marker.othersInvalid : marker.othersInvalid
        }

        return {
          id,
          lat: item.latitude,
          lng: item.longitude,
          popup: this._getLeafletPopupComplaint(item),
          options: { icon }
        }
      })
      this.setState(prevState => ({
        markers: [
          ...prevState.markers,
          ...markers
        ],
        loading: false
      }))
    } else {
      console.log(reportCategoryId)
      this.setState(prevState => ({
        markers: prevState.markers.filter(item => item.id !== reportCategoryId)
      }))
    }
  }

  _rating = starCount => {
    let rows = ''
    if(starCount === 0) {
      rows = '-'
    } else {
      for (let i=0; i < starCount; i++) {
        rows += `<img src=${star} alt="rating" style='width: 20px; margin-right: 4px' />`
      }
    }
    return rows
  }

  _handlerPicture = handlerPic => {
    if (handlerPic === '-') return noImage
    else return handlerPic
  }

  _pictures = pic => {
    if (pic === '-') return '-'
    else {
      return (`
        <img
          style='width: 100px'
          src=${pic}
          alt="resident"
          onclick="window.open(this.src)" />
      `)
    }
  }

  _getLeafletPopupComplaint = obj => {
    console.log(obj);
    return L.popup({minWidth: 600, closeButton: true, 'className': 'outer-popup'})
      .setContent(`
        <div class="qmap-popup" style='background: white; height: 450px; border-radius: 8px'>
          <div class='col-md-12 content-margin'>
            <div>
              <div style='padding: 0px' class="col-xs-2">
                <p>ID</p>
              </div>
              <div style='padding: 0px' class="col-xs-10">
                <p>${obj.id}</p>
              </div>
            </div>
            <div>
              <div style='padding: 0px' class="col-xs-2">
                <p>Status</p>
              </div>
              <div style='padding: 0px' class="col-xs-10">
                <p>${obj.status}</p>
              </div>
            </div>
            <div>
              <div style='padding: 0px' class="col-xs-2">
                <p>Judul</p>
              </div>
              <div style='padding: 0px' class="col-xs-10">
                <p>${obj.title}</p>
              </div>
            </div>
            <div>
              <div style='padding: 0px' class="col-xs-2">
                <p>Isi</p>
              </div>
              <div style='padding: 0px' class="col-xs-10">
                <p>${obj.content}</p>
              </div>
            </div>
            <div>
              <div style='padding: 0px' class="col-xs-2">
                <p>Lokasi</p>
              </div>
              <div style='padding: 0px' class="col-xs-10">
                <p>${obj.googleAddress} (${obj.locationDetail})</p>
              </div>
            </div>
            <div>
              <div style='padding: 0px' class="col-xs-2">
                <p>Garansi</p>
              </div>
              <div style='padding: 0px' class="col-xs-10">
                <p>${obj.isGuarantee ? "Ya" : 'Tidak'}</p>
              </div>
            </div>
            <div>
              <div style='padding: 0px' class="col-xs-2">
                <p>Foto</p>
              </div>
              <div style='padding: 0px' class="col-xs-10">
                <p>
                  ${this._pictures(obj.picture)}
                </p>
              </div>
            </div>
            <div>
              <div style='padding: 0px' class="col-xs-2">
                <p>Rating</p>
              </div>
              <div style='padding: 0px' class="col-xs-10">
                <p>${this._rating(obj.rating)}</p>
              </div>
            </div>

            <div style='padding-right:16px; padding-left: 0px;' class="col-xs-6">
              <hr />
              <h4 style='margin-bottom: 24px'>Pelapor</h4>
              <div style='padding:0px' class="col-xs-8">
                <div style='padding:0px' class="col-xs-4">
                  <p>Nama</p>
                  <p>Telpon</p>
                  <p>Rumah</p>
                </div>
                <div style='padding:0px' class="col-xs-8">
                  <p>${obj.residentName}</p>
                  <p>${obj.residentPhone}</p>
                  <p>${obj.residentHouse}</p>
                </div>
              </div>
              <div class="col-xs-4">
                <img
                  style='width: 50px'
                  src=${obj.residentPicture}
                  alt="Penghuni"
                  onclick="window.open(this.src)" />
              </div>
            </div>
            <div style='padding-left:16px; padding-right:0px;' class="col-xs-6">
              <hr />
              <h4 style='margin-bottom: 24px'>Pelaksana</h4>
              <div style='padding:0px' class="col-xs-8">
                <div style='padding:0px' class="col-xs-4">
                  <p>Nama</p>
                  <p>Telpon</p>
                  <p>Divisi</p>
                </div>
                <div style='padding:0px' class="col-xs-8">
                  <p>${obj.handlerName}</p>
                  <p>${obj.handlerPhone}</p>
                  <p>${obj.handlerDivision}</p>
                </div>
              </div>
              <div class="col-xs-4">
                <img
                  style='width: 50px'
                  src=${this._handlerPicture(obj.handlerPicture)}
                  alt="Pelaksana"
                  onclick="window.open(this.src)" />
              </div>
            </div>
          </div>
        </div>
      `);
  }

  _serviceCallback = (err, result, categoryId) => {
    let markers, icon, id

    if (result) {
      markers = result.map(item => {
        id = item.ServiceCategoryId
        if (item.status === "WAITING") {
          icon = id === 'SCATBSD122' ? marker.acWait :
          id === 'SCATBSD123' ? marker.tukangWait :
          id === 'SCATTAR123' ? marker.cucianWait : marker.lainnyaWait
        } else if (item.status === "ON PROGRESS") {
          icon = id === 'SCATBSD122' ? marker.acProgress :
          id === 'SCATBSD123' ? marker.tukangProgress :
          id === 'SCATTAR123' ? marker.cucianProgress : marker.lainnyaWait
        } else if (item.status === "DONE") {
          icon = id === 'SCATBSD122' ? marker.acDone :
          id === 'SCATBSD123' ? marker.tukangDone :
          id === 'SCATTAR123' ? marker.cucianDone : marker.lainnyaWait
        }

        return {
          id: item.ServiceCategoryId,
          lat: item.latitude,
          lng: item.longitude,
          popup: this._getLeafletPopupService(item),
          options: { icon }
        }
      })
      this.setState(prevState => ({
        markers: [
          ...prevState.markers,
          ...markers
        ]
      }))
    } else {
      this.setState(prevState => ({
        markers: prevState.markers.filter(item => item.id !== categoryId)
      }))
    }
  }

  _getLeafletPopupService = obj => {
    return L.popup({minWidth: 200, closeButton: true, 'className': 'outer-popup'})
      .setContent(`
        <div class="qmap-popup" style="min-height:300px">
          <img src=${obj.beforePictures} class="before-pictures" alt="Before Pictures" />
          <div class="content-margin">
            <a class='username'>${obj.User.name}</a><br>
            <p>${obj.id}</p>
            <hr class='style-one'>
            <div class='popup-content'>${obj.content}</div>
          </div>
        </div>
      `);
  }

  _billingCallback = (err, result, categoryId) => {
    let markers, icon

    if (result) {
      markers = result.map(item => {
        icon = item.status === 'paid' ? marker.tagihanDone : marker.tagihanWait

        return {
          id: categoryId,
          lat: item.latitude,
          lng: item.longitude,
          popup: this._getLeafletPopupBilling(item),
          options: { icon }
        }
      })

      this.setState(prevState => ({
        markers: [
          ...prevState.markers,
          ...markers
        ]
      }))
    } else {
      this.setState(prevState => ({
        markers: prevState.markers.filter(item => item.id !== categoryId)
      }))
    }
  }

  _getLeafletPopupBilling = obj => {
    console.log(obj);
    return L.popup({minWidth: 300, closeButton: true, 'className': 'outer-popup'})
      .setContent(`
        <div class="qmap-popup" style="width:300px;">
          <div class="content-margin">
            <a class='username'>${obj.houseName}</a><br>
            <p>${obj.id}</p>
            <p>${obj.address}</p>
            <hr class='style-one'>
            <div class='popup-content' style="width:300px;">
              <p>No. Invoice: ${obj.invoiceNumber}</p>
              <p>Tenggat Waktu: ${obj.dueDate}</p>
            </div>
          </div>
        </div>
      `);
  }

  _sosCallback = (err, result, categoryId) => {
    let markers, icon

    if (result) {
      markers = result.map(item => {
        icon = item.status === "CALLING" ? marker.sosWait :
        item.status === "ON PROGRESS" ? marker.sosProgress : marker.sosDone

        return {
          id: 'S001',
          lat: item.latitude,
          lng: item.longitude,
          popup: this._getLeafletPopupSos(item),
          options: { icon }
        }
      })
      this.setState(prevState => ({
        markers: [
          ...prevState.markers,
          ...markers
        ]
      }))
    } else {
      this.setState(prevState => ({
        markers: prevState.markers.filter(item => item.id !== categoryId)
      }))
    }
  }

  // _getLeafletPopupSos = obj => {
  //   return L.popup({minWidth: 200, closeButton: true, 'className': 'outer-popup'})
  //     .setContent(`
  //       <div class="qmap-popup">
  //         <div class="content-margin">
  //           <a class='username'>${obj.namaPenghuni}</a><br>
  //           <p>${obj.sosId}</p>
  //           <p>${obj.houseAddress}</p>
  //           <p>Tanggal: ${fullDate(obj.createdAt)}</p>
  //           <hr class='style-one'>
  //             <div class='popup-content' style="width:230px;">
  //               Status: ${obj.status}
  //             </div>
  //         </div>
  //       </div>
  //     `);
  // }

  _getLeafletPopupOccupant = obj => {
    return L.popup({minWidth: 300, closeButton: true, 'className': 'outer-popup'})
      .setContent(`
        <div class="qmap-popup" style="width:300px;">
          <div class="content-margin">
            <a class='username'>${obj.HouseName}</a><br>
            <p>${obj.id}</p>
            <p>${obj.address}</p>
            <hr class='style-one'>
            <div class='popup-content' style="width:300px;">
              <p>Tanggal Bergabung : ${fullDate(obj.createdAt)}</p>
              <p>Tanggal Sunting : ${fullDate(obj.updatedAt)}</p>
              <p>Cluster : ${obj.ClusterName}</p>
            </div>
          </div>
        </div>
      `);
  }

  _cctvCallback = (err, result, categoryId) => {
    let markers, icon
    if (result) {
      markers = result.map(item => {
        icon = marker.cctv

        return {
          id: 'C001',
          lat: item.latitude,
          lng: item.longitude,
          popup: this._getLeafletPopupCctv(item),
          options: { icon }
        }
      })

      this.setState(prevState => ({
        markers: [
          ...prevState.markers,
          ...markers
        ]
      }))
    } else {
      this.setState(prevState => ({
        markers: prevState.markers.filter(item => item.id !== categoryId)
      }))
    }
  }

  _getLeafletPopupCctv = obj => {
    const isActive = obj.isActive ? "Aktif" : "Non:Aktif"
    return L.popup({minWidth: 530, closeButton: true, 'className': 'outer-popup'})
      .setContent(`
        <div class="qmap-popup">
          <div class="content-margin" style="background: rgb(220,220,220); margin: 0; padding: 10px; border-radius: 10px 10px 0 0;">
            <div class="row">
              <div class="col-md-2 text-center">
                <p>ID :</p>
              </div>
              <div class="col-md-1 text-center">
                <em>${obj.id}</em>
              </div>
              <div class="col-md-2 text-right">
                <p>Nama :</p>
              </div>
              <div class="col-md-3 text-center">
                <em>${obj.name}</em>
              </div>
              <div class="col-md-2">
                <p>Status :</p>
              </div>
              <div class="col-md-2">
                <em>${isActive}</em>
              </div>
            </div>
          </div>
        </div>
        <div class="qmap-popup" style="width:530px; height:300px"; padding: 0;>
          <div class="content-margin" style="margin: 0">
            <img src=${loadingImg} style="width:35%; z-index:999; margin-left:35%; margin-top:12%" alt="Loading" />
            <a href="${obj.url}"><img id="cctv-popup-${obj.id}" src="${obj.url}" style="max-width:530px; z-index:1000; position:absolute; border-radius: 0 0 10px 10px; top:40px; display:block" alt="CCTV" /></a>
          </div>
        </div>
      `);
  }

  // _onEditPath = (e) => {
  //   console.log('Path edited !');
  // }
  //
  // _onCreate = (e) => {
  //   polyline = e.layer;
  //   // To edit this polyline call : polyline.handler.enable()
  //   console.log('Path created !');
  //
  //   console.log(polyline._latlngs);
  //   console.log(polyline._latlngs[0][0].lat);
  //   console.log(polyline._latlngs[0][0].lng);
  // }
  //
  // _onDeleted = (e) => {
  //   console.log('Path deleted !');
  // }

  render() {

    const {
      height,
      loading,
      expandMapMenu,
      open,
      markers,
      clusterArray,
      keluhanArray,
      category,
      keluhanValue,
      clusterValue,
      gpsMarker
    } = this.state

    const {
      route,
      clusterList,
      filteredHouse,
      mapMenuDisplay
    } = this.props

    const clusterOptions = clusterList.result.map(item => ({ label: item.name, value: item.id }))

    return (
      <DashboardLayout>
        <Wrapper>
        { _.has(route, 'pathname') ?
          <div>
            <MapMenu
              open={open}
              category={category}
              expandMapMenu={expandMapMenu}
              clusterOptions={clusterOptions}
              mapMenuDisplay={mapMenuDisplay}
              keluhanArray={keluhanArray}
              clusterArray={clusterArray}
              keluhanValue={keluhanValue}
              clusterValue={clusterValue}
              _keluhanSelectChange={this._keluhanSelectChange}
              _houseSelectChange={this._houseSelectChange}
              _expandMapMenu={this._expandMapMenu}
              _activeMapBar={this._activeMapBar}
              _checkCategory={this._checkCategory} />
            <Map
              id="mapQluster"
              // bounds={[jakarta, this.state.getTownLocations]}
              // boundsOptions={{padding: [50, 50]}}
              ref="qmap"
              className="qmap"
              center={[greenLakeCity.latitude, greenLakeCity.longitude]}
              style={{ height }}
              zoom={greenLakeCity.zoom}
              maxZoom={19}
              zoomControl={false}>
              <QmapLayers />
              <SearchBox />
              { gpsMarker.map((item, index) => (
                <div key={index}>
                  <Marker icon={marker.bus} position={item.position}>
                    <Popup>
                      <div>
                        <p>ID: {item.id}</p>
                        <p>Device GSM Number: {item.device_gsm_number}</p>
                        <p>Device Identity Number: {item.device_identity_number}</p>
                        <p>Nama Device: {item.device_name}</p>
                        <hr />
                        <p>Address: {item.address}</p>
                        <p>Created At: {item.createdAt}</p>
                        <p>ignition: {item.ignition}</p>
                        <p>Ignition By Speed: {item.ignition_by_speed}</p>
                        <p>Speed KPH: {item.speed_kph}</p>
                        <p>Timezone: {item.time_zone}</p>
                      </div>
                    </Popup>
                  </Marker>
                </div>
              ))
              }
              {/* <FeatureGroup>
                <EditControl
                  position='bottomright'
                  onEdited={this._onEditPath}
                  onCreated={this._onCreate}
                  onDeleted={this._onDeleted}
                  draw={{
                    rectangle: false
                  }}
                />
                <Circle center={[51.51, -0.06]} radius={200} />
              </FeatureGroup>
              <ZoomControl position="bottomleft" />
              */ }
              { markers.length !== 0 ?
                <MarkerClusterGroup
                  markers={markers}
                  wrapperOptions={{enableDefaultStyle: true}}
                  options={{
                    disableClusteringAtZoom: 17,
                    // spiderfyOnMaxZoom: false,
                    maxClusterRadius: 40
                  }} /> : null
              }
            </Map>
            { !filteredHouse.fetched || loading ?
              <Loading marginBottom="60%">
                <EmptyBox />
              </Loading> : null
            }
          </div> :
          <Loading>
            <EmptyBox />
          </Loading>
        }
        </Wrapper>
      </DashboardLayout>
    )
  }
}

const mapStateToProps = state => {
  return {
    toggle: state.sidebarToggle,
    complaintReport: state.complaintReport,
    route: state.router.location,
    clusterList: state.clusterList,
    filteredHouse: state.filteredHouse,
    mapMenuDisplay: state.mapMenuDisplay,
    qmapCategories: state.qmapCategories
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchComplaintQmap: (categoryId, cb) => dispatch(dispatchComplaintQmap(categoryId, cb)),
    // dispatchServiceQmap: (categoryId, cb) => dispatch(dispatchServiceQmap(categoryId, cb)),
    dispatchBillingQmap: (categoryId, cb) => dispatch(dispatchBillingQmap(categoryId, cb)),
    // dispatchSosQmap: (categoryId, cb) => dispatch(dispatchSosQmap(categoryId, cb)),
    dispatchCctvQmap: (categoryId, cb) => dispatch(dispatchCctvQmap(categoryId, cb)),
    // dispatchResetCctvQmap: () => dispatch(dispatchResetCctvQmap()),
    dispatchAllCategories: cb => dispatch(dispatchAllCategories(cb)),
    dispatchAllClusterList: cb => dispatch(dispatchAllClusterList(cb)),
    dispatchHouseByCluster: id => dispatch(dispatchHouseByCluster(id)),
    dispatchRemoveHouseByCluster: id => dispatch(dispatchRemoveHouseByCluster(id)),
    dispatchLoadHouseByCluster: () => dispatch(dispatchLoadHouseByCluster()),
    // dispatchResetHouseByCluster: () => dispatch(dispatchResetHouseByCluster()),
    dispatchShowMapMenu: () => dispatch(dispatchShowMapMenu())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Qmap);
