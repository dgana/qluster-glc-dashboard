// React, React-Router, PropTypes & Redux Dependencies
import React from 'react';
import { connect } from 'react-redux'

// Plugin Dependencies
import classNames from 'classnames'
import { Checkbox } from 'react-icheck'
import Select from 'react-select'

const MapMenu = props => {
  const {
    expandMapMenu,
    _expandMapMenu,
    _activeMapBar,
    _checkCategory,
    open,
    category,
    keluhanValue,
  } = props

  const complaintLength = category.keluhan.filter(item => item.checked).length
  // const serviceLength = category.jasa.filter(item => item.checked).length
  const billingLength = category.tagihan.filter(item => item.checked).length
  // const sosLength = category.sos.filter(item => item.checked).length
  const occupantLength = category.rumah.filter(item => item.checked).length
  const cctvLength = category.cctv.filter(item => item.checked).length
  const gpsLength = category.gps.filter(item => item.checked).length

  const keluhanOptions = category.keluhan.map(item => ({ label: item.name, value: item.id }))

  const labelStyle = {
    right: 30,
    position: 'absolute',
    top: 10,
    padding: '3px 12px 0px 7px',
    fontSize: 9,
    width: 10,
    height: 14
  }

  let expandClass = 'fa fa-angle-down'
  let shadowClass = ""
  if (expandMapMenu) {
    expandClass = 'fa fa-angle-up'
    shadowClass = 'shadow-button'
  } else {
    expandClass = 'fa fa-angle-down'
    shadowClass = 'no-shadow-button'
  }

  const showHideMenu = props.mapMenuDisplay ? "none" : "block"

  return (
    <div className="btn-group map-button">
      <button onClick={_expandMapMenu} type="button" style={{paddingLeft:10, paddingRight: 13, position:'absolute', right: 0, width: 140, display: showHideMenu}} className={classNames("btn btn-qluster-success dropdown-toggle", shadowClass)} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Map Menu <i style={{paddingLeft:2, fontSize:12, lineHeight: '20px', marginLeft:10}} className={classNames(expandClass, "pull-right")}></i>
      </button>
      { expandMapMenu ? (
          <aside className="map-menu">
            <section className="sidebar-map">
              <ul className="sidebar-menu" style={{display: showHideMenu}} data-widget="tree">
                <li className={classNames('treeview', {'active': open.keluhan})}>
                  <div
                    onClick={() => _activeMapBar('keluhan')}
                    className={classNames('map-menu-item map-menu-text', { 'map-menu-item-focus': open.keluhan })}>
                    Keluhan
                    <i className="fa fa-bullhorn pull-left text-center" style={{lineHeight:'20px', marginRight:20}} aria-hidden="true"></i>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-down pull-right"></i>
                    </span>
                    { complaintLength !== 0 ?
                      <span style={labelStyle} className="label label-success pull-right">{complaintLength}</span> : null
                    }
                  </div>
                  <ul className="treeview-menu">
                    <li className="map-menu-subitem" style={{height: 50, padding: '10px 20px'}}>
                      <div style={{position: 'absolute', zIndex:999, width:'85%'}}>
                        <Select multi simpleValue
                          value={keluhanValue}
                          placeholder="Keluhan..."
                          clearable={false}
                          options={keluhanOptions}
                          onChange={props._keluhanSelectChange} />
                      </div>
                    </li>
                  </ul>
                </li>
                {/* <li className={classNames('treeview', {'active': open.jasa})}>
                  <div
                    onClick={() => _activeMapBar('jasa')}
                    className={classNames('map-menu-item map-menu-text', { 'map-menu-item-focus': open.jasa })}>
                    Jasa
                    <i className="fa fa-wrench pull-left text-center" style={{lineHeight:'20px', marginRight:20}} aria-hidden="true"></i>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-down pull-right"></i>
                    </span>
                    { serviceLength !== 0 ?
                      <span style={labelStyle} className="label label-success pull-right">{serviceLength}</span> : null
                    }
                  </div>
                  <ul className="treeview-menu">
                  { category.jasa.map((item, index) => (
                    <li className="map-menu-subitem" key={index}>
                      <Checkbox
                        onClick={() => _checkCategory('jasa', item.id, index)}
                        checkboxClass="icheckbox_square-green"
                        label={item.name}
                        increaseArea="-100%"
                        checked={category.jasa[index].checked ? true : false} />
                    </li>
                    ))
                  }
                  </ul>
                </li> */}
                <li className={classNames('treeview', {'active': open.tagihan})}>
                  <div
                    onClick={() => _activeMapBar('tagihan')}
                    className={classNames('map-menu-item map-menu-text', { 'map-menu-item-focus': open.tagihan })}>
                    Tagihan
                    <i className="fa fa-credit-card-alt pull-left text-center" style={{lineHeight:'20px', marginRight:15}} aria-hidden="true"></i>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-down pull-right"></i>
                    </span>
                    { billingLength !== 0 ?
                      <span style={labelStyle} className="label label-success pull-right">{billingLength}</span> : null
                    }
                  </div>
                  <ul className="treeview-menu">
                    <li className="map-menu-subitem">
                      <Checkbox
                        onClick={() => _checkCategory('tagihan', null, 0)}
                        checkboxClass="icheckbox_square-green"
                        label="Tagihan"
                        increaseArea="-100%"
                        checked={category.tagihan[0].checked ? true : false} />
                    </li>
                  </ul>
                </li>
                {/* <li className={classNames('treeview', {'active': open.sos})}>
                  <div
                    onClick={() => _activeMapBar('sos')}
                    className={classNames('map-menu-item map-menu-text', { 'map-menu-item-focus': open.sos })}>
                    SOS
                    <i className="fa fa-exclamation pull-left text-center" style={{lineHeight:'20px', marginRight:'2em'}} aria-hidden="true"></i>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-down pull-right"></i>
                    </span>
                    { sosLength !== 0 ?
                      <span style={labelStyle} className="label label-success pull-right">{sosLength}</span> : null
                    }
                  </div>
                  <ul className="treeview-menu">
                    <li className="map-menu-subitem">
                      <Checkbox
                        onClick={() => _checkCategory('sos', null, 0)}
                        checkboxClass="icheckbox_square-green"
                        label="SOS"
                        increaseArea="-100%"
                        checked={category.sos[0].checked ? true : false} />
                    </li>
                  </ul>
                </li> */}
                <li className={classNames('treeview', {'active': open.rumah})}>
                  <div
                    onClick={() => _activeMapBar('rumah')}
                    className={classNames('map-menu-item map-menu-text', { 'map-menu-item-focus': open.rumah })}>
                    Rumah
                    <i className="fa fa-home pull-left text-center" style={{lineHeight:'20px', marginRight:18}} aria-hidden="true"></i>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-down pull-right"></i>
                    </span>
                    { occupantLength !== 0 ?
                      <span style={labelStyle} className="label label-success pull-right">{occupantLength}</span> : null
                    }
                  </div>
                  <ul className="treeview-menu">
                    <li className="map-menu-subitem" style={{height: 50, padding: '10px 20px'}}>
                      <div style={{position: 'absolute', zIndex:999, width:'85%'}}>
                        <Select multi simpleValue
                          value={props.clusterValue}
                          placeholder="Cluster..."
                          clearable={false}
                          options={props.clusterOptions}
                          onChange={props._houseSelectChange} />
                      </div>
                    </li>
                  </ul>
                </li>
                <li className={classNames('treeview', {'active': open.cctv})}>
                  <div
                    onClick={() => _activeMapBar('cctv')}
                    className={classNames('map-menu-item map-menu-text', { 'map-menu-item-focus': open.cctv })}>
                    CCTV
                    <i className="fa fa-video-camera pull-left text-center" style={{lineHeight:'20px', marginRight:18}} aria-hidden="true"></i>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-down pull-right"></i>
                    </span>
                    { cctvLength !== 0 ?
                      <span style={labelStyle} className="label label-success pull-right">{cctvLength}</span> : null
                    }
                  </div>
                  <ul className="treeview-menu">
                    <li className="map-menu-subitem">
                      <Checkbox
                        onClick={() => _checkCategory('cctv', null, 0)}
                        checkboxClass="icheckbox_square-green"
                        label="CCTV"
                        increaseArea="-100%"
                        checked={category.cctv[0].checked ? true : false} />
                    </li>
                  </ul>
                </li>
                <li className={classNames('treeview', {'active': open.gps})}>
                  <div
                    onClick={() => _activeMapBar('gps')}
                    className={classNames('map-menu-item map-menu-text', { 'map-menu-item-focus': open.gps })}>
                    GPS
                    <i className="fa fa-bus pull-left text-center" style={{lineHeight:'20px', marginRight:18}} aria-hidden="true"></i>
                    <span className="pull-right-container">
                      <i className="fa fa-angle-down pull-right"></i>
                    </span>
                    { gpsLength !== 0 ?
                      <span style={labelStyle} className="label label-success pull-right">{gpsLength}</span> : null
                    }
                  </div>
                  <ul className="treeview-menu">
                    <li className="map-menu-subitem">
                      <Checkbox
                        onClick={() => _checkCategory('gps', null, 0)}
                        checkboxClass="icheckbox_square-green"
                        label="GPS"
                        increaseArea="-100%"
                        checked={category.gps[0].checked ? true : false} />
                    </li>
                  </ul>
                </li>
              </ul>
            </section>
          </aside>
        ) : null
      }
    </div>
  )
}

const mapStateToProps = state => {
  return {
    clusterList: state.clusterList
  }
}

export default connect(mapStateToProps)(MapMenu)
