import L from 'leaflet'

import foundationsWait              from   '../../../../../public/imgs/marker/pointer-red/Pekerjaan-Fondasi.png'
import superstructureWait           from   '../../../../../public/imgs/marker/pointer-red/Pekerjaan-Struktur-Atas.png'
import exteriorVerticalWait         from   '../../../../../public/imgs/marker/pointer-red/Fasad-Eksterior-Vertical.png'
import exteriorHorizontalWait       from   '../../../../../public/imgs/marker/pointer-red/Fasad-Eksterior-Horizontal.png'
import interiorConstructionWait     from   '../../../../../public/imgs/marker/pointer-red/Kontruksi-Interior.png'
import interiorFinishesWait         from   '../../../../../public/imgs/marker/pointer-red/Finishing-Interior.png'
import conveyingWait                from   '../../../../../public/imgs/marker/pointer-red/Conveying.png'
import plumbingWait                 from   '../../../../../public/imgs/marker/pointer-red/Pemipaan.png'
import hvacWait                     from   '../../../../../public/imgs/marker/pointer-red/HVAC.png'
import fireProtectionWait           from   '../../../../../public/imgs/marker/pointer-red/Perlindungan-Bahaya-Kebakaran.png'
import electricalWait               from   '../../../../../public/imgs/marker/pointer-red/Sistem-Elektrikal.png'
import electronicWait               from   '../../../../../public/imgs/marker/pointer-red/Alat-alat-Elektronik.png'
import equipmentWait                from   '../../../../../public/imgs/marker/pointer-red/Perlengkapan-&-Peralatan.png'
import furnishingsWait              from   '../../../../../public/imgs/marker/pointer-red/Funiture.png'
import specialConstructionWait      from   '../../../../../public/imgs/marker/pointer-red/Pekerjaan-Kontruksi-Khusus.png'
import sitePreparationWait          from   '../../../../../public/imgs/marker/pointer-red/Persiapan-Lahan.png'
import siteImprovementsWait         from   '../../../../../public/imgs/marker/pointer-red/Perbaikan-lahan.png'
import liquidAndGasWait             from   '../../../../../public/imgs/marker/pointer-red/Utilitas-Lingkungan-(Gas-&-Cairan).png'
import electricalSiteWait           from   '../../../../../public/imgs/marker/pointer-red/Perbaikan-Sistem-Elektrikal-Lingkungan.png'
import constructionSiteWait         from   '../../../../../public/imgs/marker/pointer-red/Kontruksi-Lahan-(Lain-lain).png'
import houseKeepingWait             from   '../../../../../public/imgs/marker/pointer-red/Perawatan-Rumah.png'
import securityWait                 from   '../../../../../public/imgs/marker/pointer-red/Keamanan-Lingkungan.png'
import pestControlWait              from   '../../../../../public/imgs/marker/pointer-red/Pengendalian-Hama.png'
import parkingWait                  from   '../../../../../public/imgs/marker/pointer-red/Parkir.png'
import othersWait                   from   '../../../../../public/imgs/marker/pointer-red/Lain---lain.png'

import foundationsProgress          from   '../../../../../public/imgs/marker/pointer-yellow/Pekerjaan-Fondasi.png'
import superstructureProgress       from   '../../../../../public/imgs/marker/pointer-yellow/Pekerjaan-Struktur-Atas.png'
import exteriorVerticalProgress     from   '../../../../../public/imgs/marker/pointer-yellow/Fasad-Eksterior-Vertikal.png'
import exteriorHorizontalProgress   from   '../../../../../public/imgs/marker/pointer-yellow/Fasad-Eksterior-Horizontal.png'
import interiorConstructionProgress from   '../../../../../public/imgs/marker/pointer-yellow/Kontruksi-Interior.png'
import interiorFinishesProgress     from   '../../../../../public/imgs/marker/pointer-yellow/Finishing-Interior.png'
import conveyingProgress            from   '../../../../../public/imgs/marker/pointer-yellow/Conveying.png'
import plumbingProgress             from   '../../../../../public/imgs/marker/pointer-yellow/Pemipaan.png'
import hvacProgress                 from   '../../../../../public/imgs/marker/pointer-yellow/HVAC.png'
import fireProtectionProgress       from   '../../../../../public/imgs/marker/pointer-yellow/Perlindungan-Bahaya-Kebakaran.png'
import electricalProgress           from   '../../../../../public/imgs/marker/pointer-yellow/Sistem-Elektrikal.png'
import electronicProgress           from   '../../../../../public/imgs/marker/pointer-yellow/Alat-alat-Elektronik.png'
import equipmentProgress            from   '../../../../../public/imgs/marker/pointer-yellow/Perlengkapan-&-Peralatan.png'
import furnishingsProgress          from   '../../../../../public/imgs/marker/pointer-yellow/Funiture.png'
import specialConstructionProgress  from   '../../../../../public/imgs/marker/pointer-yellow/Pekerjaan-Kontruksi-Khusus.png'
import sitePreparationProgress      from   '../../../../../public/imgs/marker/pointer-yellow/Persiapan-Lahan.png'
import siteImprovementsProgress     from   '../../../../../public/imgs/marker/pointer-yellow/Perbaikan-lahan.png'
import liquidAndGasProgress         from   '../../../../../public/imgs/marker/pointer-yellow/Utilitas-Lingkungan-(Gas-&-cairan).png'
import electricalSiteProgress       from   '../../../../../public/imgs/marker/pointer-yellow/Perbaikan-Sistem-Elektrikal-Lingkungan.png'
import constructionSiteProgress     from   '../../../../../public/imgs/marker/pointer-yellow/Kontruksi-Lahan-(lain-lain).png'
import houseKeepingProgress         from   '../../../../../public/imgs/marker/pointer-yellow/Perawatan-Rumah.png'
import securityProgress             from   '../../../../../public/imgs/marker/pointer-yellow/Keamanan-Lingkungan.png'
import pestControlProgress          from   '../../../../../public/imgs/marker/pointer-yellow/Pengendalian-Hama.png'
import parkingProgress              from   '../../../../../public/imgs/marker/pointer-yellow/Parkir.png'
import othersProgress               from   '../../../../../public/imgs/marker/pointer-yellow/Lain---lain.png'

import foundationsDone              from   '../../../../../public/imgs/marker/pointer-green/Pekerjaan-Fondasi.png'
import superstructureDone           from   '../../../../../public/imgs/marker/pointer-green/Pekerjaan-Struktur Atas.png'
import exteriorVerticalDone         from   '../../../../../public/imgs/marker/pointer-green/Fasad-Eksterior-Vertical.png'
import exteriorHorizontalDone       from   '../../../../../public/imgs/marker/pointer-green/Fasad-Eksterior-Horizontal.png'
import interiorConstructionDone     from   '../../../../../public/imgs/marker/pointer-green/Kontruksi-Interior.png'
import interiorFinishesDone         from   '../../../../../public/imgs/marker/pointer-green/Finishing-Interior.png'
import conveyingDone                from   '../../../../../public/imgs/marker/pointer-green/Conveying.png'
import plumbingDone                 from   '../../../../../public/imgs/marker/pointer-green/Pemipaan.png'
import hvacDone                     from   '../../../../../public/imgs/marker/pointer-green/HVAC.png'
import fireProtectionDone           from   '../../../../../public/imgs/marker/pointer-green/Perlindungan-Bahaya-Kebakaran.png'
import electricalDone               from   '../../../../../public/imgs/marker/pointer-green/Sistem-Elektrikal.png'
import electronicDone               from   '../../../../../public/imgs/marker/pointer-green/Alat-alat-Elektronik.png'
import equipmentDone                from   '../../../../../public/imgs/marker/pointer-green/Perlengkapan.png'
import furnishingsDone              from   '../../../../../public/imgs/marker/pointer-green/Funiture.png'
import specialConstructionDone      from   '../../../../../public/imgs/marker/pointer-green/Pekerjaan-Kontruksi-Khusus.png'
import sitePreparationDone          from   '../../../../../public/imgs/marker/pointer-green/Persiapan-Lahan.png'
import siteImprovementsDone         from   '../../../../../public/imgs/marker/pointer-green/Perbaikan-lahan.png'
import liquidAndGasDone             from   '../../../../../public/imgs/marker/pointer-green/Utilitas-Lingkungan-(Gas-&-cairan).png'
import electricalSiteDone           from   '../../../../../public/imgs/marker/pointer-green/Perbaikan-Sistem-Elektrikal-Lingkungan.png'
import constructionSiteDone         from   '../../../../../public/imgs/marker/pointer-green/Kontruksi-Lahan.png'
import houseKeepingDone             from   '../../../../../public/imgs/marker/pointer-green/Perawatan-Rumah.png'
import securityDone                 from   '../../../../../public/imgs/marker/pointer-green/Keamanan.png'
import pestControlDone              from   '../../../../../public/imgs/marker/pointer-green/Pengendalian-Hama.png'
import parkingDone                  from   '../../../../../public/imgs/marker/pointer-green/Parkir.png'
import othersDone                   from   '../../../../../public/imgs/marker/pointer-green/Lain---lain.png'

import foundationsInvalid           from   '../../../../../public/imgs/marker/pointer-white/Invalid_Pekerjaan Fondasi.png'
import superstructureInvalid        from   '../../../../../public/imgs/marker/pointer-white/Invalid_Pekerjaan Struktur At.png'
import exteriorVerticalInvalid      from   '../../../../../public/imgs/marker/pointer-white/Invalid_Fasad Eksterior Verti.png'
import exteriorHorizontalInvalid    from   '../../../../../public/imgs/marker/pointer-white/Invalid_Fasad Eksterior Horiz.png'
import interiorConstructionInvalid  from   '../../../../../public/imgs/marker/pointer-white/Invalid_Kontruksi Interior.png'
import interiorFinishesInvalid      from   '../../../../../public/imgs/marker/pointer-white/Invalid_Finishing Interior.png'
import conveyingInvalid             from   '../../../../../public/imgs/marker/pointer-white/Invalid_Conveying.png'
import plumbingInvalid              from   '../../../../../public/imgs/marker/pointer-white/Invalid_Pemipaan.png'
import hvacInvalid                  from   '../../../../../public/imgs/marker/pointer-white/Invalid_HVAC.png'
import fireProtectionInvalid        from   '../../../../../public/imgs/marker/pointer-white/Invalid_Perlindungan Bahaya K.png'
import electricalInvalid            from   '../../../../../public/imgs/marker/pointer-white/Invalid_Sistem Elektrikal.png'
import electronicInvalid            from   '../../../../../public/imgs/marker/pointer-white/Invalid_Alat- alat Elektronik.png'
import equipmentInvalid             from   '../../../../../public/imgs/marker/pointer-white/Invalid_Perlengkapan & Perala.png'
import furnishingsInvalid           from   '../../../../../public/imgs/marker/pointer-white/Invalid_Funiture & fit out.png'
import specialConstructionInvalid   from   '../../../../../public/imgs/marker/pointer-white/Invalid_Pekerjaan Konstruksi .png'
import sitePreparationInvalid       from   '../../../../../public/imgs/marker/pointer-white/Invalid_Persiapan Lahan.png'
import siteImprovementsInvalid      from   '../../../../../public/imgs/marker/pointer-white/Invalid_Perbaikan Lahan.png'
import liquidAndGasInvalid          from   '../../../../../public/imgs/marker/pointer-white/Invalid_Utilitas Lingkungan (.png'
import electricalSiteInvalid        from   '../../../../../public/imgs/marker/pointer-white/Invalid_Perbaikan Elektrikan .png'
import constructionSiteInvalid      from   '../../../../../public/imgs/marker/pointer-white/Invalid_Kontruksi Lahan (Lain.png'
import houseKeepingInvalid          from   '../../../../../public/imgs/marker/pointer-white/Invalid_Perawatan Rumah.png'
import securityInvalid              from   '../../../../../public/imgs/marker/pointer-white/Invalid_Keamanan Lingkungan.png'
import pestControlInvalid           from   '../../../../../public/imgs/marker/pointer-white/Invalid_Pengendalian Hama.png'
import parkingInvalid               from   '../../../../../public/imgs/marker/pointer-white/Invalid_Parkir.png'
import othersInvalid                from   '../../../../../public/imgs/marker/pointer-white/Invalid_Lain - lain.png'

import cctv                         from   '../../../../../public/imgs/marker/cctv.png'
import bus                          from   '../../../../../public/imgs/marker/BusMarker.png'

import rumahDone                    from   '../../../../../public/imgs/marker/Area_Selection_Home_Green@1x.png'
import rumahWait                    from   '../../../../../public/imgs/marker/Area_Selection_Home_Red@1x.png'

import tagihanDone                  from   '../../../../../public/imgs/marker/tagihan-done.png'
import tagihanWait                  from   '../../../../../public/imgs/marker/tagihan-wait.png'

module.exports = {
  foundationsWait: L.icon({
    iconUrl: foundationsWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  superstructureWait: L.icon({
    iconUrl: superstructureWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  exteriorVerticalWait: L.icon({
    iconUrl: exteriorVerticalWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  exteriorHorizontalWait: L.icon({
    iconUrl: exteriorHorizontalWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  interiorConstructionWait: L.icon({
    iconUrl: interiorConstructionWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  interiorFinishesWait: L.icon({
    iconUrl: interiorFinishesWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  conveyingWait: L.icon({
    iconUrl: conveyingWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  plumbingWait: L.icon({
    iconUrl: plumbingWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  hvacWait: L.icon({
    iconUrl: hvacWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  fireProtectionWait: L.icon({
    iconUrl: fireProtectionWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  electricalWait: L.icon({
    iconUrl: electricalWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  electronicWait: L.icon({
    iconUrl: electronicWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  equipmentWait: L.icon({
    iconUrl: equipmentWait,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  furnishingsWait: L.icon({
    iconUrl: furnishingsWait,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  specialConstructionWait: L.icon({
    iconUrl: specialConstructionWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  sitePreparationWait: L.icon({
    iconUrl: sitePreparationWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  siteImprovementsWait: L.icon({
    iconUrl: siteImprovementsWait,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  liquidAndGasWait: L.icon({
    iconUrl: liquidAndGasWait,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  electricalSiteWait: L.icon({
    iconUrl: electricalSiteWait,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  constructionSiteWait: L.icon({
    iconUrl: constructionSiteWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  houseKeepingWait: L.icon({
    iconUrl: houseKeepingWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  securityWait: L.icon({
    iconUrl: securityWait,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  pestControlWait: L.icon({
    iconUrl: pestControlWait,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  parkingWait: L.icon({
    iconUrl: parkingWait,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  othersWait: L.icon({
    iconUrl: othersWait,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),

  foundationsProgress: L.icon({
    iconUrl: foundationsProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  superstructureProgress: L.icon({
    iconUrl: superstructureProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  exteriorVerticalProgress: L.icon({
    iconUrl: exteriorVerticalProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  exteriorHorizontalProgress: L.icon({
    iconUrl: exteriorHorizontalProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  interiorConstructionProgress: L.icon({
    iconUrl: interiorConstructionProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  interiorFinishesProgress: L.icon({
    iconUrl: interiorFinishesProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  conveyingProgress: L.icon({
    iconUrl: conveyingProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  plumbingProgress: L.icon({
    iconUrl: plumbingProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  hvacProgress: L.icon({
    iconUrl: hvacProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  fireProtectionProgress: L.icon({
    iconUrl: fireProtectionProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  electricalProgress: L.icon({
    iconUrl: electricalProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  electronicProgress: L.icon({
    iconUrl: electronicProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  equipmentProgress: L.icon({
    iconUrl: equipmentProgress,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  furnishingsProgress: L.icon({
    iconUrl: furnishingsProgress,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  specialConstructionProgress: L.icon({
    iconUrl: specialConstructionProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  sitePreparationProgress: L.icon({
    iconUrl: sitePreparationProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  siteImprovementsProgress: L.icon({
    iconUrl: siteImprovementsProgress,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  liquidAndGasProgress: L.icon({
    iconUrl: liquidAndGasProgress,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  electricalSiteProgress: L.icon({
    iconUrl: electricalSiteProgress,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  constructionSiteProgress: L.icon({
    iconUrl: constructionSiteProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  houseKeepingProgress: L.icon({
    iconUrl: houseKeepingProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  securityProgress: L.icon({
    iconUrl: securityProgress,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  pestControlProgress: L.icon({
    iconUrl: pestControlProgress,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  parkingProgress: L.icon({
    iconUrl: parkingProgress,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  othersProgress: L.icon({
    iconUrl: othersProgress,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),

  foundationsDone: L.icon({
    iconUrl: foundationsDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  superstructureDone: L.icon({
    iconUrl: superstructureDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  exteriorVerticalDone: L.icon({
    iconUrl: exteriorVerticalDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  exteriorHorizontalDone: L.icon({
    iconUrl: exteriorHorizontalDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  interiorConstructionDone: L.icon({
    iconUrl: interiorConstructionDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  interiorFinishesDone: L.icon({
    iconUrl: interiorFinishesDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  conveyingDone: L.icon({
    iconUrl: conveyingDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  plumbingDone: L.icon({
    iconUrl: plumbingDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  hvacDone: L.icon({
    iconUrl: hvacDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  fireProtectionDone: L.icon({
    iconUrl: fireProtectionDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  electricalDone: L.icon({
    iconUrl: electricalDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  electronicDone: L.icon({
    iconUrl: electronicDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  equipmentDone: L.icon({
    iconUrl: equipmentDone,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  furnishingsDone: L.icon({
    iconUrl: furnishingsDone,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  specialConstructionDone: L.icon({
    iconUrl: specialConstructionDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  sitePreparationDone: L.icon({
    iconUrl: sitePreparationDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  siteImprovementsDone: L.icon({
    iconUrl: siteImprovementsDone,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  liquidAndGasDone: L.icon({
    iconUrl: liquidAndGasDone,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  electricalSiteDone: L.icon({
    iconUrl: electricalSiteDone,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  constructionSiteDone: L.icon({
    iconUrl: constructionSiteDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  houseKeepingDone: L.icon({
    iconUrl: houseKeepingDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  securityDone: L.icon({
    iconUrl: securityDone,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  pestControlDone: L.icon({
    iconUrl: pestControlDone,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  parkingDone: L.icon({
    iconUrl: parkingDone,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  othersDone: L.icon({
    iconUrl: othersDone,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),

  foundationsInvalid: L.icon({
    iconUrl: foundationsInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  superstructureInvalid: L.icon({
    iconUrl: superstructureInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  exteriorVerticalInvalid: L.icon({
    iconUrl: exteriorVerticalInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  exteriorHorizontalInvalid: L.icon({
    iconUrl: exteriorHorizontalInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  interiorConstructionInvalid: L.icon({
    iconUrl: interiorConstructionInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  interiorFinishesInvalid: L.icon({
    iconUrl: interiorFinishesInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  conveyingInvalid: L.icon({
    iconUrl: conveyingInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  plumbingInvalid: L.icon({
    iconUrl: plumbingInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  hvacInvalid: L.icon({
    iconUrl: hvacInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  fireProtectionInvalid: L.icon({
    iconUrl: fireProtectionInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  electricalInvalid: L.icon({
    iconUrl: electricalInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  electronicInvalid: L.icon({
    iconUrl: electronicInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  equipmentInvalid: L.icon({
    iconUrl: equipmentInvalid,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  furnishingsInvalid: L.icon({
    iconUrl: furnishingsInvalid,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  specialConstructionInvalid: L.icon({
    iconUrl: specialConstructionInvalid,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  sitePreparationInvalid: L.icon({
    iconUrl: sitePreparationInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  siteImprovementsInvalid: L.icon({
    iconUrl: siteImprovementsInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  liquidAndGasInvalid: L.icon({
    iconUrl: liquidAndGasInvalid,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  electricalSiteInvalid: L.icon({
    iconUrl: electricalSiteInvalid,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  constructionSiteInvalid: L.icon({
    iconUrl: constructionSiteInvalid,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  houseKeepingInvalid: L.icon({
    iconUrl: houseKeepingInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  securityInvalid: L.icon({
    iconUrl: securityInvalid,
    iconSize: [28, 38],
    iconAnchor: [14,38],
    popupAnchor: [0,-26]
  }),
  pestControlInvalid: L.icon({
    iconUrl: pestControlInvalid,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  parkingInvalid: L.icon({
    iconUrl: parkingInvalid,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),
  othersInvalid: L.icon({
    iconUrl: othersInvalid,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),

  cctv: L.icon({
    iconUrl: cctv,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-26]
  }),
  bus: L.icon({
    iconUrl: bus,
    iconSize: [29, 37],
    iconAnchor: [14,37],
    popupAnchor: [0,-26]
  }),

  rumahDone: L.icon({
    iconUrl: rumahDone,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20],
  }),
  rumahWait: L.icon({
    iconUrl: rumahWait,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-20]
  }),

  tagihanWait: L.icon({
    iconUrl: tagihanWait,
    iconSize: [26, 37],
    iconAnchor: [16,37],
    popupAnchor: [0,-26]
  }),
  tagihanDone: L.icon({
    iconUrl: tagihanDone,
    iconSize: [26, 37],
    iconAnchor: [13,37],
    popupAnchor: [0,-26]
  })
}
