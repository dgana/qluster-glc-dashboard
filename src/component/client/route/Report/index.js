// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Common Component
import DashboardLayout from '../../common/DashboardLayout'
import Wrapper from '../../common/Content/Wrapper'
import Body from '../../common/Content/Body'
import Loading from '../../common/Loading'
import EmptyBox from '../../common/EmptyBox'

// Laporan Component
import Table from './Table/'
// import Jasa from './Table/Jasa'
import Keluhan from './Table/Keluhan'
import Penghuni from './Table/Penghuni'
import Pengelola from './Table/Pengelola'
import Peranan from './Table/Peranan'
// import Sos from './Table/Sos'
import Tagihan from './Table/Tagihan'
import Gps from './Table/Gps'
import Cctv from './Table/Cctv'
import Rumah from './Table/Rumah'
import Cluster from './Table/Cluster'

// Plugin Dependencies
import _ from 'lodash'
import popup from '../../../../../public/js/bootstrap-notify/popupMessage'
import Workbook from 'react-excel-workbook'
import ReactTooltip from 'react-tooltip'
import { autoTable } from 'jspdf-autotable'

// Images
import excelIcon from '../../../../../public/imgs/icon/xls-24.png'

// Dispatcher
import {
  dispatchCctvReport,
  // dispatchServiceReport,
  dispatchComplaintReport,
  dispatchOccupantReport,
  dispatchGpsReport,
  dispatchManagerReport,
  dispatchRoleReport,
  // dispatchSosReport,
  dispatchBillingReport,
  dispatchHouseReport,
  dispatchClusterReport,
} from '../../../../dispatcher'

class Report extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
    }
  }

  componentDidMount() {
    const {
      route,
      // serviceReport,
      complaintReport,
      // sosReport,
      billingReport,
      occupantReport,
      managerReport,
      roleReport,
      cctvReport,
      gpsReport,
      houseReport,
      clusterReport
    } = this.props

    // route.pathname === '/laporan-jasa' ?
    //   serviceReport.fetched ? this._reportCallback(null, 'FETCHED') :
    //   this.props.dispatchServiceReport(compId, this._reportCallback) :

    route.pathname === '/laporan-keluhan' ?
      complaintReport.fetched ? this._reportCallback(null, 'FETCHED') :
      this.props.dispatchComplaintReport(this._reportCallback) :

    // route.pathname === '/laporan-sos' ?
    //   sosReport.fetched ? this._reportCallback(null, 'FETCHED') :
    //   this.props.dispatchSosReport(compId, this._reportCallback) :

    route.pathname === '/laporan-tagihan' ?
      billingReport.fetched ? this._reportCallback(null, 'FETCHED') :
      this.props.dispatchBillingReport(this._reportCallback) :

    route.pathname === '/laporan-penghuni' ?
      occupantReport.fetched ? this._reportCallback(null, 'FETCHED') :
      this.props.dispatchOccupantReport(this._reportCallback) :

    route.pathname === '/laporan-pengelola' ?
      managerReport.fetched ? this._reportCallback(null, 'FETCHED') :
      this.props.dispatchManagerReport(this._reportCallback) :

    route.pathname === '/laporan-peranan' ?
      roleReport.fetched ? this._reportCallback(null, 'FETCHED') :
      this.props.dispatchRoleReport(this._reportCallback) :

    route.pathname === '/laporan-cctv' ?
      cctvReport.fetched ? this._reportCallback(null, 'FETCHED') :
      this.props.dispatchCctvReport(this._reportCallback) :

    route.pathname === '/laporan-gps' ?
      gpsReport.fetched ? this._reportCallback(null, 'FETCHED') :
      this.props.dispatchGpsReport(this._reportCallback) :

    route.pathname === '/laporan-rumah' ?
      houseReport.fetched ? this._reportCallback(null, 'FETCHED') :
      this.props.dispatchHouseReport(this._reportCallback) :

      clusterReport.fetched ? this._reportCallback(null, 'FETCHED') :
      this.props.dispatchClusterReport(this._reportCallback)
  }

  _showLoadingImage = () => this.setState({ loading: true })
  _hideLoadingImage = () => this.setState({ loading: false })

  _fetchReport = report => {
    this._showLoadingImage()
    // report === 'jasa' ? this.props.dispatchServiceReport(this._reportCallback) :
    report === 'keluhan' ? this.props.dispatchComplaintReport(this._reportCallback) :
    // report === 'sos' ? this.props.dispatchSosReport(this._reportCallback) :
    report === 'tagihan' ? this.props.dispatchBillingReport(this._reportCallback) :
    report === 'penghuni' ? this.props.dispatchOccupantReport(this._reportCallback) :
    report === 'pengelola' ? this.props.dispatchManagerReport(this._reportCallback) :
    report === 'peranan' ? this.props.dispatchRoleReport(this._reportCallback) :
    report === 'cctv' ? this.props.dispatchCctvReport(this._reportCallback) :
    report === 'gps' ? this.props.dispatchGpsReport(this._reportCallback) :
    report === 'rumah' ? this.props.dispatchHouseReport(this._reportCallback) :
    this.props.dispatchClusterReport(this._reportCallback)
  }

  _reportCallback = (err, result) => {
    if (result) {
      this._hideLoadingImage()
    } else {
      popup.message('pe-7s-attention', `<span class="alert-popup-text-message">API sedang mengalami gangguan</span>`, 'danger', 1000, 'bottom', 'right')
    }
  }

  _onExportExcel = (reportData, topic) => (
    <div>
      <ReactTooltip id={topic} place={"bottom"} />
      <Workbook
        filename={"laporan-" + topic + ".xlsx"}
        element={
          <button
            data-for={topic}
            data-tip="Eksport ke Excel (.xlsx)"
            data-iscapture="true"
            type="submit"
            className="btn btn-default btn-sm report-excel-button">
            <img alt="Excel Icon" src={excelIcon}></img>
           </button>
         }>
        <Workbook.Sheet data={reportData} name="Sheet1">
          { Object.keys(reportData[0]).map((item, index) => (
              <Workbook.Column key={index} label={item} value={item}/>
            ))
          }
        </Workbook.Sheet>
      </Workbook>
    </div>
  )

  _onExportPdf = (reportData, topic, docType) => {
    let columns = Object.keys(reportData[0]).map((item, index) => ({ title: item, dataKey: item }))
    let rows = reportData.map(item => ({ ...item }))

    docType.autoTable([], [], { margin: { top: 50 }})
    const header = data => {
      docType.setFontSize(18);
      docType.setTextColor(40);
      docType.setFontStyle('normal');
      // docType.addImage('http://icons.iconarchive.com/icons/elegantthemes/beautiful-flat/128/document-icon.png', 'PNG', data.settings.margin.left, 20, 50, 50);
      docType.text("Laporan " + topic, data.settings.margin.left, 50);
    }

    const options = {
      beforePageContent: header,
      margin: { top: 70 },
      startY: docType.autoTableEndPosY() + 20
    }

    docType.autoTable(columns, rows, options);
    docType.save('laporan-' + topic + '.pdf');
  }

  render() {
    const { route } = this.props
    const { loading } = this.state
    return (
      <DashboardLayout>
        <Wrapper>
        { _.has(route, 'pathname') ?
          /*
          route.pathname === '/laporan-jasa' ? (
            <div>
            { loading ?
              <Loading>
                <EmptyBox />
              </Loading> :
              <Body>
                <Table
                  title={"Jasa"}
                  table={<Jasa
                    _onExportExcel={this._onExportExcel}
                    _onExportPdf={this._onExportPdf}
                    _fetchReport={this._fetchReport}
                    loading={loading} />
                  } />
              </Body>
            }
            </div>
          ) :
          */
          route.pathname === '/laporan-keluhan' ? (
            <div>
            { loading ?
              <Loading>
                <EmptyBox />
              </Loading> :
              <Body>
                <Table
                  title={"Keluhan"}
                  table={<Keluhan
                    _onExportExcel={this._onExportExcel}
                    _onExportPdf={this._onExportPdf}
                    _fetchReport={this._fetchReport}
                    loading={loading} />
                  } />
              </Body>
            }
            </div>
          ) :
          /*
          route.pathname === '/laporan-sos' ? (
            <div>
            { loading ?
              <Loading>
                <EmptyBox />
              </Loading> :
              <Body>
                <Table
                  title={"SOS"}
                  table={<Sos
                    _onExportExcel={this._onExportExcel}
                    _onExportPdf={this._onExportPdf}
                    _fetchReport={this._fetchReport}
                    loading={loading} />
                  } />
              </Body>
            }
            </div>
          ) : */
          route.pathname === '/laporan-tagihan' ? (
            <div>
            { loading ?
              <Loading>
                <EmptyBox />
              </Loading> :
              <Body>
                <Table
                  title={"Tagihan"}
                  table={<Tagihan
                    _onExportExcel={this._onExportExcel}
                    _onExportPdf={this._onExportPdf}
                    _fetchReport={this._fetchReport}
                    loading={loading} />
                  } />
              </Body>
            }
            </div>
          ) :
          route.pathname === '/laporan-penghuni' ? (
            <div>
            { loading ?
              <Loading>
                <EmptyBox />
              </Loading> :
              <Body>
                <Table
                  title={"Penghuni"}
                  table={<Penghuni
                    _onExportExcel={this._onExportExcel}
                    _onExportPdf={this._onExportPdf}
                    _fetchReport={this._fetchReport}
                    loading={loading} />
                  } />
              </Body>
            }
            </div>
          ) :
          route.pathname === '/laporan-pengelola' ? (
            <div>
            { loading ?
              <Loading>
                <EmptyBox />
              </Loading> :
              <Body>
                <Table
                  title={"Pengelola"}
                  table={<Pengelola
                    _onExportExcel={this._onExportExcel}
                    _onExportPdf={this._onExportPdf}
                    _fetchReport={this._fetchReport}
                    loading={loading} />
                  } />
              </Body>
            }
            </div>
          ) :
          route.pathname === '/laporan-peranan' ? (
            <div>
            { loading ?
              <Loading>
                <EmptyBox />
              </Loading> :
              <Body>
                <Table
                  title={"Peranan"}
                  table={<Peranan
                    _onExportExcel={this._onExportExcel}
                    _onExportPdf={this._onExportPdf}
                    _fetchReport={this._fetchReport}
                    loading={loading} />
                  } />
              </Body>
            }
            </div>
          ) :
          route.pathname === '/laporan-cctv' ? (
            <div>
            { loading ?
              <Loading>
                <EmptyBox />
              </Loading> :
              <Body>
                <Table
                  title={"CCTV"}
                  table={<Cctv
                    _onExportExcel={this._onExportExcel}
                    _onExportPdf={this._onExportPdf}
                    _fetchReport={this._fetchReport}
                    loading={loading} />
                  } />
              </Body>
            }
            </div>
          ) :
          route.pathname === '/laporan-gps' ? (
            <div>
            { loading ?
              <Loading>
                <EmptyBox />
              </Loading> :
              <Body>
                <Table
                  title={"GPS"}
                  table={<Gps
                    _onExportExcel={this._onExportExcel}
                    _onExportPdf={this._onExportPdf}
                    _fetchReport={this._fetchReport}
                    loading={loading} />
                  } />
              </Body>
            }
            </div>
          ) :
          route.pathname === '/laporan-rumah' ? (
            <div>
            { loading ?
              <Loading>
                <EmptyBox />
              </Loading> :
              <Body>
                <Table
                  title={"Rumah"}
                  table={<Rumah
                    _onExportExcel={this._onExportExcel}
                    _onExportPdf={this._onExportPdf}
                    _fetchReport={this._fetchReport}
                    loading={loading} />
                  } />
              </Body>
            }
            </div>
          ) : (
            <div>
            { loading ?
              <Loading>
                <EmptyBox />
              </Loading> :
              <Body>
                <Table
                  title={"Cluster"}
                  table={<Cluster
                    _onExportExcel={this._onExportExcel}
                    _onExportPdf={this._onExportPdf}
                    _fetchReport={this._fetchReport}
                    loading={loading} />
                  } />
              </Body>
            }
            </div>
          ) :
          <Loading>
            <EmptyBox />
          </Loading>
        }
        </Wrapper>
      </DashboardLayout>
    )
  }
}

const mapStateToProps = state => {
  return {
    route: state.router.location,
    // serviceReport: state.serviceReport,
    complaintReport: state.complaintReport,
    // sosReport: state.sosReport,
    billingReport: state.billingReport,
    occupantReport: state.occupantReport,
    managerReport: state.managerReport,
    roleReport: state.roleReport,
    cctvReport: state.cctvReport,
    gpsReport: state.gpsReport,
    houseReport: state.houseReport,
    clusterReport: state.clusterReport,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // dispatchServiceReport: (compId, cb) => dispatch(dispatchServiceReport(compId, cb)),
    dispatchComplaintReport: cb => dispatch(dispatchComplaintReport(cb)),
    // dispatchSosReport: (compId, cb) => dispatch(dispatchSosReport(compId, cb)),
    dispatchGpsReport: cb => dispatch(dispatchGpsReport(cb)),
    dispatchBillingReport: cb => dispatch(dispatchBillingReport(cb)),
    dispatchOccupantReport: cb => dispatch(dispatchOccupantReport(cb)),
    dispatchManagerReport: cb => dispatch(dispatchManagerReport(cb)),
    dispatchRoleReport: cb => dispatch(dispatchRoleReport(cb)),
    dispatchCctvReport: cb => dispatch(dispatchCctvReport(cb)),
    dispatchHouseReport: cb => dispatch(dispatchHouseReport(cb)),
    dispatchClusterReport: cb => dispatch(dispatchClusterReport(cb)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Report);
