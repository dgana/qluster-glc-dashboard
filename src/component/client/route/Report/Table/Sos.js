// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import $ from 'jquery'
import classNames from 'classnames'

// SOS Component
import ReportEmpty from './ReportEmpty'
import ReloadButton from './Button/ReloadButton'
import ExportPdfButton from './Button/ExportPdfButton'

class Sos extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tableIsEmpty : false
    }
  }

  componentDidMount() {
    $(document).ready(function() {
      $('#table-laporan-sos').DataTable({
        iDisplayLength: 25,
        retrieve: true
      })
    })
  }

  render() {
    const { tableIsEmpty } = this.state

    const {
      sosReport,
      loading,
      compId,
      _fetchReport,
      _onExportExcel,
      _onExportPdf
     } = this.props

    return (
      <div>
      { loading ? null :
        sosReport.result.length === 0 ?
        <ReportEmpty
          _fetchReport={() => _fetchReport('sos')}
          report="sos"
          compId={compId} /> :
        <div className="row">
          { sosReport.fetched ? _onExportExcel(sosReport.result, 'sos') : null }
          <ReloadButton
            _fetchReport={() => _fetchReport('sos')}
            report="sos"
            compId={compId} />
          <ExportPdfButton
            _onExportPdf={_onExportPdf}
            adminReport={sosReport.result}
            report="sos"
            orientation={'landscape'} />
          <div className="col-xs-12">
            <table id="table-laporan-sos" className="table text-center sosTable">
              <thead>
                <tr>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>ID</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Penghuni</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Township</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Cluster</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Rumah</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Waktu Kirim</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Waktu Selesai</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Status</th>
                </tr>
              </thead>
              <tbody>
              { sosReport.result.length > 0 ? sosReport.result.map((item, index) => (
                <tr key={index}>
                  <td>{item.id}</td>
                  <td>{item.penghuni}</td>
                  <td>{item.township}</td>
                  <td>{item.cluster}</td>
                  <td>{item.house}</td>
                  <td>{item.createdAt}</td>
                  <td>
                  { item.updatedAt === 'STILL IN PROGRESS' ?
                    <div>
                      <i style={{color:'rgb(27, 187, 153)'}} className="fa fa-clock-o fa-1-7x" aria-hidden="true"></i>
                    </div> : item.updatedAt
                  }
                  </td>
                  <td>{item.status}</td>
                </tr>
                )) : null
              }
              </tbody>
            </table>
          </div>
        </div>
      }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    sosReport: state.sosReport,
    compId: state.showClient.companyId
  }
}

export default connect(mapStateToProps)(Sos)
