// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'

// Plugin Dependencies
import ReactTooltip from 'react-tooltip'

// No Data Image
import NoData from '../../../../../../public/imgs/icon/nodata.png'

// Utility
import { jsUcfirst } from '../../../../../util'

const ReportEmpty = props => {

  const dataTip = props.report === "sos" || props.report === 'cctv' ?
  "Reload Laporan " + props.report.toUpperCase() : "Reload Laporan " + jsUcfirst(props.report)

  const headline = props.report === "sos" || props.report === 'cctv' ?
  "Laporan " + props.report.toUpperCase() + " Kosong" : "Laporan " + jsUcfirst(props.report) + " Kosong"

  return (
    <center>
      <ReactTooltip id={"reload-laporan-kosong-" + props.report} place={"bottom"} />
      <img alt="NoData" src={NoData} className="noData"/>
      <p>{headline}
        <button
          data-for={"reload-laporan-kosong-" + props.report}
          data-tip={dataTip}
          data-iscapture="true"
          onClick={() => props._fetchReport(props.compId, props.report)}
          type="submit"
          className="btn btn-qluster-primary pull-right"
          style={{position:'absolute', right:24, top:8, border:'none'}}>
          <span className="pe-7s-refresh" style={{marginRight: 8}}></span>
           Reload
        </button>
      </p>
    </center>
  )
}

export default ReportEmpty
