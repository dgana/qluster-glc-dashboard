// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import $ from 'jquery'
import ReactTable from "react-table"
import "react-table/react-table.css"
import { Checkbox } from 'react-icheck'

// Peranan Component
import ReportEmpty from './ReportEmpty'
import ReloadButton from './Button/ReloadButton'
import ExportPdfButton from './Button/ExportPdfButton'
import ResetTableState from './Button/ResetTableState'

// Dispatcher
import { dispatchGetFeatures, dispatchResetFeatures } from '../../../../../dispatcher'

// Utility
import columns from '../../../../../util/Report/peranan'

// Styles
const checkboxStyle = { padding:'10px 5%', width:'10%', cursor:'pointer' }

const initialState = {
  sorted: [],
  page: 0,
  pageSize: 10,
  expanded: {},
  resized: [],
  filtered: []
}

let currentState = () => ({
  sorted: [],
  page: 0,
  pageSize: 10,
  expanded: {},
  resized: [],
  filtered: []
})

class Peranan extends Component {
  constructor(props) {
    super(props)
    this.state = currentState()
  }

  componentWillUnmount() {
    currentState = () => this.state
  }

  componentDidMount() {
    this.props.dispatchGetFeatures()
    $(document).ready(function() {
      $('#table_laporan_peran').DataTable({ "iDisplayLength": 50, "bSort": false , "retrieve": true })
      $('#table_laporan_peran_length').last().css('display','none')
      $('#table_laporan_peran_wrapper .row').last().css('display','none')
      $("td:nth-child(2)").attr("style","text-align:left; padding-left:30px")
    })
  }

  _resetState = () => {
    this.setState({ ...initialState })
  }

  _renderPengelola = (features, access) => {
    access = access.map(item => item.id)
    return features.map((item, index) => (
      <tr key={index}>
        <td>{index + 1}</td>
        <td style={{textAlign:'left', paddingLeft:30}}><p>{item.name}</p></td>
        <td style={{textAlign:'left', paddingLeft:30}}><p>{item.name[0] === '/' ? "dashboard" : "mobile"}</p></td>
        <td>
          <Checkbox
            checkboxClass="icheckbox_square-green"
            increaseArea="-100%"
            checked={access.find(d => d === item.id) ? true : false} />
        </td>
      </tr>
    ))
  }

  render() {

    const {
      roleReport,
      loading,
      _fetchReport,
      _onExportExcel,
      _onExportPdf,
      roleFeatures
     } = this.props

    const newRoleReportPdf = roleReport.result.map(item => {
      return {
        "ID": item.id,
        "Peran": item.role,
        "Tanggal Perubahan Terakhir": item.updatedAt,
      }
    })

    const newRoleReportExcel = roleReport.result.map(item => {
      return {
        "id": item.id,
        "role": item.role,
        "updatedAt": item.updatedAt,
      }
    })

    const data = roleReport.result.map((item, index) => {
      return {
        number: index + 1,
        ...item
      }
    })

    return (
      <div>
      { loading ? null :
        roleReport.result.length === 0 ?
        <ReportEmpty
          _fetchReport={() => _fetchReport('peranan')}
          report="peranan" /> :
        <div className="row">
          { roleReport.fetched ? _onExportExcel(newRoleReportExcel, 'peranan') : null }
          <ReloadButton
            _fetchReport={() => _fetchReport('cctv')}
            report="peranan" />
          <ExportPdfButton
            _onExportPdf={_onExportPdf}
            adminReport={newRoleReportPdf}
            report="peranan"
            orientation={'portrait'} />
          <ResetTableState
            report="peranan"
            _resetState={this._resetState} />
          <div className="col-xs-12">
            <ReactTable
              data={data}
              columns={columns}
              sortable={true}
              resizable={true}
              filterable={true}

              // Controlled Props
              sorted={this.state.sorted}
              page={this.state.page}
              pageSize={this.state.pageSize}
              expanded={this.state.expanded}
              resized={this.state.resized}
              filtered={this.state.filtered}
              onSortedChange={sorted => this.setState({ sorted })}
              onPageChange={page => this.setState({ page })}
              onPageSizeChange={(pageSize, page) => this.setState({ page, pageSize })}
              onExpandedChange={expanded => this.setState({ expanded })}
              onResizedChange={resized => this.setState({ resized })}
              onFilteredChange={filtered => this.setState({ filtered })}

              // Change Text Props
              previousText={"Sebelum"}
              nextText={"Berikutnya"}
              noDataText={"Data Kosong"}
              pageText={"Halaman"}
              ofText={'dari'}
              rowsText={"baris"}
              defaultPageSize={10}
              defaultFilterMethod={(filter, row, column) => {
                const id = filter.pivotId || filter.id
                return row[id] !== undefined ?
                String(row[id]).toLowerCase().startsWith(filter.value.toLowerCase()) : true
              }}
              SubComponent={data => {
                const access = data.original.features.map(item => item.id)
                return (
                  <div className='col-xs-12' style={{margin: '12px 24px 30px', width: '95%'}}>
                    <h3>{"Hak Akses " + data.original.role}</h3>
                    <hr />
                    <table id="table_laporan_peran" className="table text-center">  
                      <thead>
                        <tr>
                          <th width="50">No. </th>
                          <th style={{textAlign:'left', paddingLeft:30}}>Fungsi</th>
                          <th style={{textAlign:'left', paddingLeft:30}}>Platform</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      { roleFeatures.features.map((item, index) => (
                        <tr key={index}>
                          <td>{index + 1}</td>
                          <td style={{textAlign:'left', paddingLeft:30}}><p>{item.name}</p></td>
                          <td style={{textAlign:'left', paddingLeft:30}}><p>{item.name[0] === '/' ? "dashboard" : "mobile"}</p></td>
                          <td>
                            <Checkbox
                              checkboxClass="icheckbox_square-green"
                              increaseArea="-100%"
                              checked={access.find(d => d === item.id) ? true : false} />
                          </td>
                        </tr>
                      ))
                      }
                      </tbody>
                    </table>
                  </div>
                )
              }} />
          </div>
        </div>
      }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    roleReport: state.roleReport,
    roleFeatures: state.roleFeatures
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchGetFeatures: () => dispatch(dispatchGetFeatures()),
    dispatchResetFeatures: () => dispatch(dispatchResetFeatures())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Peranan)
