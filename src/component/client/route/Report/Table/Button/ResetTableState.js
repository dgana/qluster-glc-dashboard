// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'

// Plugin Dependencies
import ReactTooltip from 'react-tooltip'

const ResetTableState = props => {
  return (
    <div>
      <ReactTooltip id={props.report} place={"bottom"} />
      <button
        data-for={props.report}
        data-tip="Bersihkan Tabel"
        data-iscapture="true"
        onClick={() => props._resetState()}
        type="submit"
        className="btn btn-default btn-sm report-reset-button">
        <span className="fa fa-th-list fa-1-8x" style={{marginRight: 8}}></span>
      </button>
    </div>
  )
}

export default ResetTableState
