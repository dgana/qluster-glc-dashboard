// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'

// Plugin Dependencies
import ReactTooltip from 'react-tooltip'

// Utility
import { jsUcfirst } from '../../../../../../util'

const ReloadButton = props => {

  const dataTip = props.report === "sos" || props.report === 'cctv' ?
  "Reload Laporan " + props.report.toUpperCase() : "Reload Laporan " + jsUcfirst(props.report)
  
  return (
    <div>
      <ReactTooltip id={"reload-laporan-" + props.report} place={"bottom"} />
      <button
        data-for={"reload-laporan-" + props.report}
        data-tip={dataTip}
        data-iscapture="true"
        onClick={() => props._fetchReport(props.compId, props.report)}
        type="submit"
        className="btn btn-qluster-primary pull-right report-reload-button">
        <span className="pe-7s-refresh" style={{marginRight: 8}}></span>
        Reload
      </button>
    </div>
  )
}

export default ReloadButton
