// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'

// Plugin Dependencies
import jsPDF from 'jspdf'
import ReactTooltip from 'react-tooltip'

const ExportPdfButton = props => {
  const docType = props.orientation === 'portrait' ? new jsPDF('p', 'pt') : new jsPDF('l', 'pt')
  return (
    <div>
      <ReactTooltip id={props.report} place={"bottom"} />
      <button
        data-for={props.report}
        data-tip="Eksport ke PDF (.pdf)"
        data-iscapture="true"
        onClick={() => props._onExportPdf(props.adminReport, props.report, docType)}
        type="submit"
        className="btn btn-default btn-sm report-pdf-button">
        <span className="fa fa-file-pdf-o fa-1-8x" style={{marginRight: 8}}></span>
      </button>
    </div>
  )
}

export default ExportPdfButton
