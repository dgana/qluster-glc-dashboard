// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import ReactTable from "react-table"
import "react-table/react-table.css"

// Rumah Component
import ReportEmpty from './ReportEmpty'
import ReloadButton from './Button/ReloadButton'
import ExportPdfButton from './Button/ExportPdfButton'
import ResetTableState from './Button/ResetTableState'

// Utility
import { completeDate, limitString } from '../../../../../util'
import columns from '../../../../../util/Report/rumah'

const initialState = {
  sorted: [],
  page: 0,
  pageSize: 10,
  resized: [],
  filtered: []
}

let currentState = () => ({
  sorted: [],
  page: 0,
  pageSize: 10,
  resized: [],
  filtered: []
})

class Rumah extends Component {
  constructor(props) {
    super(props)
    this.state = currentState()
  }

  componentWillUnmount() {
    currentState = () => this.state
  }

  _resetState = () => {
    this.setState({ ...initialState })
  }

  render() {

    const {
      houseReport,
      loading,
      _fetchReport,
      _onExportExcel,
      _onExportPdf
     } = this.props

    const newHouseReportPdf = houseReport.result.map(item => {
      return {
        "ID": item.id,
        "No, Rumah": item.number,
        "Cluster": item.cluster,
        "Alamat": limitString(item.address, 25),
        "Tanggal Penyerahan": item.handOverDate,
        "Garansi": item.isGuarantee,
        "Aktif": item.isActive,
        "Jumlah Penghuni": item.numberOfMembers,
        "Tanggal Perubahan Terakhir": completeDate(item.updatedAt)
      }
    })

   const newHouseReportExcel = houseReport.result.map(item => {
     return {
       "ID": item.id,
       "No, Rumah": item.number,
       "Cluster": item.cluster,
       "Alamat": limitString(item.address, 25),
       "Tanggal Penyerahan": item.handOverDate,
       "Garansi": item.isGuarantee,
       "Aktif": item.isActive,
       "Jumlah Penghuni": item.numberOfMembers,
       "Tanggal Perubahan Terakhir": completeDate(item.updatedAt)
     }
   })

   const data = houseReport.result.map((item, index) => {
     return {
       no: index + 1,
       ...item,
       handOverDate: completeDate(item.handOverDate)
     }
   })

    return (
      <div>
      { loading ? null :
        houseReport.result.length === 0 ?
        <ReportEmpty
          _fetchReport={() => _fetchReport('rumah')}
          report="rumah" /> :
        <div>
          <div className="row">
            { houseReport.fetched ? _onExportExcel(newHouseReportExcel, 'rumah') : null }
            <ReloadButton
              _fetchReport={() => _fetchReport('rumah')}
              report="rumah" />
            <ExportPdfButton
              _onExportPdf={_onExportPdf}
              adminReport={newHouseReportPdf}
              report="rumah"
              orientation={'landscape'} />
            <ResetTableState
              report="rumah"
              _resetState={this._resetState} />
            <div className="col-xs-12">
              <ReactTable
                data={data}
                columns={columns}
                sortable={true}
                resizable={true}
                filterable={true}

                // Controlled Props
                sorted={this.state.sorted}
                page={this.state.page}
                pageSize={this.state.pageSize}
                resized={this.state.resized}
                filtered={this.state.filtered}
                onSortedChange={sorted => this.setState({ sorted })}
                onPageChange={page => this.setState({ page })}
                onPageSizeChange={(pageSize, page) => this.setState({ page, pageSize })}
                onResizedChange={resized => this.setState({ resized })}
                onFilteredChange={filtered => this.setState({ filtered })}

                // Change Text Props
                previousText={"Sebelum"}
                nextText={"Berikutnya"}
                noDataText={"Data Kosong"}
                pageText={"Halaman"}
                ofText={'dari'}
                rowsText={"baris"}
                defaultPageSize={10}
                defaultFilterMethod={(filter, row, column) => {
                  const id = filter.pivotId || filter.id
                  return row[id] !== undefined ?
                  String(row[id]).toLowerCase().startsWith(filter.value.toLowerCase()) : true
                }} />
            </div>
          </div>
          <div className="row">
            <p style={{display: 'flex', justifyContent: 'center', marginTop: 16}}>Tip: Tahan <code> Shift </code> untuk multi sortir</p>
          </div>
        </div>
      }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    houseReport: state.houseReport
  }
}

export default connect(mapStateToProps)(Rumah)
