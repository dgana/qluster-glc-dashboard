// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import Gallery from 'react-grid-gallery'
import ReactTable from "react-table"
import "react-table/react-table.css";

// Keluhan Component
import ReportEmpty from './ReportEmpty'
import ReloadButton from './Button/ReloadButton'
import ExportPdfButton from './Button/ExportPdfButton'
import ResetTableState from './Button/ResetTableState'

// Images
import noImage from '../../../../../../public/imgs/icon/no_image.jpg'
import star from '../../../../../../public/imgs/icon/star.png'

// Utility
import { limitString, completeDate } from '../../../../../util'
import columns from '../../../../../util/Report/keluhan'

const initialState = {
  sorted: [],
  page: 0,
  pageSize: 10,
  expanded: {},
  resized: [],
  filtered: []
}

let currentState = () => ({
  sorted: [],
  page: 0,
  pageSize: 10,
  expanded: {},
  resized: [],
  filtered: []
})

class Keluhan extends Component {
  constructor(props) {
    super(props)
    this.state = currentState()
  }

  componentWillUnmount() {
    currentState = () => this.state
  }

  _resetState = () => {
    this.setState({ ...initialState })
  }

  _rating = starCount => {
    let rows = []
    if(starCount === 0) {
      rows.push('-')
    } else {
      for (let i=0; i < starCount; i++) {
        rows.push(<img key={i} src={star} alt="rating" style={{width: 20, marginRight: 4}} />)
      }
    }
    return rows
  }

  _pictures = pictures => {
    let rows = []
    if (typeof pictures === 'object') {
      if (pictures.length === 0) {
        return (<span>-</span>)
      } else {
        const newPictures = pictures.map(pic =>({
          src: pic,
          thumbnail: pic,
          thumbnailWidth: 0,
          thumbnailHeight: 0,
          alt: pic,
          rowHeight: 100
        }))
        rows.push(<Gallery images={newPictures} />)
      }
    }
    return rows
  }

  render() {
    const { reportDetail, width } = this.state

    const {
      complaintReport,
      loading,
      _fetchReport,
      _onExportExcel,
      _onExportPdf
    } = this.props

    const newComplaintReportPdf = complaintReport.result.map(item => {
      return {
        id: item.id,
        judul: item.title,
        isi: limitString(item.content, 30),
        alamat: limitString(item.address, 30),
        pelapor: item.residentName,
        label: limitString(item.labelName, 15),
        divisi: item.handlerDivision,
        pelaksana: item.handlerName
      }
    })

    const newComplaintReportExcel = complaintReport.result.map(item => {
      return {
        ...item,
        pictures: '',
      }
    })

    const data = complaintReport.result.map((item, index) => {
      return {
        number: index + 1,
        ...item,
        createdAt: completeDate(item.createdAt)
      }
    })

    return (
      <div>
      { loading ? null :
        complaintReport.result.length === 0 ?
        <ReportEmpty
          _fetchReport={() => _fetchReport('keluhan')}
          report="keluhan" /> :
        <div>
          <div className="row">
            { complaintReport.fetched ? _onExportExcel(newComplaintReportExcel, 'keluhan') : null }
            <ReloadButton
              _fetchReport={() => _fetchReport('keluhan')}
              report="keluhan" />
            <ExportPdfButton
              _onExportPdf={_onExportPdf}
              adminReport={newComplaintReportPdf}
              report="keluhan"
              orientation={'landscape'} />
            <ResetTableState
              report="keluhan"
              _resetState={this._resetState} />
            <div className="col-xs-12">
              <ReactTable
                data={data}
                columns={columns}
                sortable={true}
                resizable={true}
                filterable={true}

                // Controlled Props
                sorted={this.state.sorted}
                page={this.state.page}
                pageSize={this.state.pageSize}
                expanded={this.state.expanded}
                resized={this.state.resized}
                filtered={this.state.filtered}
                onSortedChange={sorted => this.setState({ sorted })}
                onPageChange={page => this.setState({ page })}
                onPageSizeChange={(pageSize, page) => this.setState({ page, pageSize })}
                onExpandedChange={expanded => this.setState({ expanded })}
                onResizedChange={resized => this.setState({ resized })}
                onFilteredChange={filtered => this.setState({ filtered })}

                // Change Text Props
                previousText={"Sebelum"}
                nextText={"Berikutnya"}
                noDataText={"Data Kosong"}
                pageText={"Halaman"}
                ofText={'dari'}
                rowsText={"baris"}
                defaultPageSize={10}
                defaultFilterMethod= {(filter, row, column) => {
                  const id = filter.pivotId || filter.id
                  return row[id] !== undefined ?
                  String(row[id]).toLowerCase().startsWith(filter.value.toLowerCase()) : true
                }}
                SubComponent={data => {
                  const row = data.row._original
                  return (
                    <div className='col-xs-12' style={{margin: '12px 24px 30px', width: '95%'}}>
                      <h3>Detail Keluhan</h3>
                      <hr />
                      <div>
                        <div style={{padding: 0}} className="col-xs-2">
                          <p>ID</p>
                        </div>
                        <div style={{padding: 0}} className="col-xs-10">
                          <p>{row.id}</p>
                        </div>
                      </div>
                      <div>
                        <div style={{padding: 0}} className="col-xs-2">
                          <p>Isi Keluhan</p>
                        </div>
                        <div style={{padding: 0}} className="col-xs-10">
                          <p>{row.content}</p>
                        </div>
                      </div>
                      <div>
                        <div style={{padding: 0}} className="col-xs-2">
                          <p>Lokasi</p>
                        </div>
                        <div style={{padding: 0}} className="col-xs-10">
                          <p>{row.address + ' (' + row.addressDetail + ')'}</p>
                        </div>
                      </div>
                      <div>
                        <div style={{padding: 0}} className="col-xs-2">
                          <p>Garansi</p>
                        </div>
                        <div style={{padding: 0}} className="col-xs-10">
                          <p>{row.isGuarantee ? "Ya" : "Tidak"}</p>
                        </div>
                      </div>
                      <div>
                        <div style={{padding: 0}} className="col-xs-2">
                          <p>Foto</p>
                        </div>
                        <div style={{padding: 0, marginBottom: 10}} className="col-xs-10">
                          <p style={{maxHeight: 50}}>{this._pictures(row.pictures)}</p>
                        </div>
                      </div>
                      <div>
                        <div style={{padding: 0}} className="col-xs-2">
                          <p>Rating</p>
                        </div>
                        <div style={{padding: 0}} className="col-xs-10">
                          <p>{this._rating(row.rating)}</p>
                        </div>
                      </div>

                      <div style={{paddingRight:16, paddingLeft: 0}} className="col-xs-6">
                        <hr />
                        <h4 style={{marginBottom: 24}}>Pelapor</h4>
                        <div style={{padding:0}} className="col-xs-8">
                          <div style={{padding:0}} className="col-xs-3">
                            <p>Nama</p>
                            <p>Telpon</p>
                            <p>Email</p>
                            <p>Rumah</p>
                            <p>Alamat</p>
                          </div>
                          <div style={{padding:0}} className="col-xs-9">
                            <p>{row.residentName}</p>
                            <p>{row.residentPhone}</p>
                            <p>{row.residentMail}</p>
                            <p>{row.houseName}</p>
                            <p>{row.houseAddress}</p>
                          </div>
                        </div>
                        <div className="col-xs-4">
                          <img
                            style={{width: 150, borderRadius: 4}}
                            src={row.residetPic}
                            alt="resident" />
                        </div>
                      </div>
                      <div style={{paddingLeft:16, paddingRight:0}} className="col-xs-6">
                        <hr />
                        <h4 style={{marginBottom: 24}}>Pelaksana</h4>
                        <div style={{padding:0}} className="col-xs-8">
                          <div style={{padding:0}} className="col-xs-3">
                            <p>Nama</p>
                            <p>Telpon</p>
                            <p>Divisi</p>
                          </div>
                          <div style={{padding:0}} className="col-xs-9">
                            <p>{row.handlerName}</p>
                            <p>{row.handlerPhone}</p>
                            <p>{row.handlerDivision}</p>
                          </div>
                        </div>
                        <div className="col-xs-4">
                          <img
                            style={{width: 150, borderRadius: 4}}
                            src={row.handlerPic === '-' ? noImage : row.handlerPic}
                            alt="manager" />
                        </div>
                      </div>
                    </div>
                  )
                }} />
            </div>
          </div>
          <div className="row">
            <p style={{display: 'flex', justifyContent: 'center', marginTop: 16}}>Tip: Tahan <code> Shift </code> untuk multi sortir</p>
          </div>
        </div>
      }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    complaintReport: state.complaintReport,
  }
}

export default connect(mapStateToProps)(Keluhan)
