// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import ReactTable from "react-table"
import "react-table/react-table.css"

// Tagihan Component
import ReportEmpty from './ReportEmpty'
import ReloadButton from './Button/ReloadButton'
import ExportPdfButton from './Button/ExportPdfButton'
import ResetTableState from './Button/ResetTableState'

// Images
import noImage from '../../../../../../public/imgs/icon/no_image.jpg'
import star from '../../../../../../public/imgs/icon/star.png'

// Utility
import columns from '../../../../../util/Report/tagihan'

const initialState = {
  sorted: [],
  page: 0,
  pageSize: 10,
  resized: [],
  filtered: []
}

let currentState = () => ({
  sorted: [],
  page: 0,
  pageSize: 10,
  resized: [],
  filtered: []
})

class Tagihan extends Component {
  constructor(props) {
    super(props)
    this.state = currentState()
  }

  componentWillUnmount() {
    currentState = () => this.state
  }

  _resetState = () => {
    this.setState({ ...initialState })
  }

  render() {

    const {
      billingReport,
      loading,
      _fetchReport,
      _onExportExcel,
      _onExportPdf
    } = this.props

    const data = billingReport.result.map((item, index) => {
      return {
        number: index + 1,
        ...item,
        nominal: "Rp. " + item.nominal.toLocaleString() + ",-"
      }
    })

    return (
      <div>
      { loading ? null :
        billingReport.result.length === 0 ?
        <ReportEmpty
          _fetchReport={() => _fetchReport('tagihan')}
          report="tagihan" /> :
        <div>
          <div className="row">
            { billingReport.fetched ? _onExportExcel(billingReport.result, 'tagihan') : null }
            <ReloadButton
              _fetchReport={() => _fetchReport('tagihan')}
              report="tagihan" />
            <ExportPdfButton
              _onExportPdf={_onExportPdf}
              adminReport={billingReport.result}
              report="tagihan"
              orientation={'landscape'} />
            <ResetTableState
              report="tagihan"
              _resetState={this._resetState} />
            <div className="col-xs-12">
              <ReactTable
                data={data}
                columns={columns}
                sortable={true}
                resizable={true}
                filterable={true}

                // Controlled Props
                sorted={this.state.sorted}
                page={this.state.page}
                pageSize={this.state.pageSize}
                resized={this.state.resized}
                filtered={this.state.filtered}
                onSortedChange={sorted => this.setState({ sorted })}
                onPageChange={page => this.setState({ page })}
                onPageSizeChange={(pageSize, page) => this.setState({ page, pageSize })}
                onResizedChange={resized => this.setState({ resized })}
                onFilteredChange={filtered => this.setState({ filtered })}

                // Change Text Props
                previousText={"Sebelum"}
                nextText={"Berikutnya"}
                noDataText={"Data Kosong"}
                pageText={"Halaman"}
                ofText={'dari'}
                rowsText={"baris"}
                defaultPageSize={10}
                defaultFilterMethod= {(filter, row, column) => {
                  const id = filter.pivotId || filter.id
                  return row[id] !== undefined ?
                  String(row[id]).toLowerCase().startsWith(filter.value.toLowerCase()) : true
                }} />
            </div>
          </div>
          <div className="row">
            <p style={{display: 'flex', justifyContent: 'center', marginTop: 16}}>Tip: Tahan <code> Shift </code> untuk multi sortir</p>
          </div>
        </div>
      }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    billingReport: state.billingReport,
  }
}

export default connect(mapStateToProps)(Tagihan)
