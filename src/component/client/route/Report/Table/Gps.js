// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import ReactTable from "react-table"
import "react-table/react-table.css"

// GPS Component
import ReportEmpty from './ReportEmpty'
import ReloadButton from './Button/ReloadButton'
import ExportPdfButton from './Button/ExportPdfButton'
import ResetTableState from './Button/ResetTableState'

// Utility
import { completeDate } from '../../../../../util'
import columns from '../../../../../util/Report/gps'

const initialState = {
  sorted: [],
  page: 0,
  pageSize: 10,
  resized: [],
  filtered: []
}

let currentState = () => ({
  sorted: [],
  page: 0,
  pageSize: 10,
  resized: [],
  filtered: []
})

class Gps extends Component {
  constructor(props) {
    super(props)
    this.state = currentState()
  }

  componentWillUnmount() {
    currentState = () => this.state
  }

  _resetState = () => {
    this.setState({ ...initialState })
  }

  render() {

    const {
      gpsReport,
      loading,
      _fetchReport,
      _onExportExcel,
      _onExportPdf
    } = this.props

    const data = gpsReport.result.map((item, index) => {
      return {
        number: index + 1,
        ...item,
        createdAt: completeDate(item.createdAt)
      }
    })

    return (
      <div>
        <p>TES</p>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    gpsReport: state.gpsReport
  }
}

export default connect(mapStateToProps)(Gps)
