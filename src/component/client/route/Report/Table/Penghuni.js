// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import ReactTable from "react-table"
import "react-table/react-table.css"

// Penghuni Component
import ReportEmpty from './ReportEmpty'
import ReloadButton from './Button/ReloadButton'
import ExportPdfButton from './Button/ExportPdfButton'
import ResetTableState from './Button/ResetTableState'

// Utility
import { completeDate } from '../../../../../util'
import columns from '../../../../../util/Report/penghuni'

const initialState = {
  sorted: [],
  page: 0,
  pageSize: 10,
  resized: [],
  filtered: []
}

let currentState = () => ({
  sorted: [],
  page: 0,
  pageSize: 10,
  resized: [],
  filtered: []
})

class Penghuni extends Component {
  constructor(props) {
    super(props)
    this.state = currentState()
  }

  componentWillUnmount() {
    currentState = () => this.state
  }

  _resetState = () => {
    this.setState({ ...initialState })
  }

  render() {

    const {
      occupantReport,
      loading,
      compId,
      _fetchReport,
      _onExportExcel,
      _onExportPdf
    } = this.props

    const data = occupantReport.result.map((item, index) => {
      return {
        number: index + 1,
        ...item,
        createdAt: completeDate(item.createdAt),
        houseName: item.houseName.split('-')[1] + "-" + item.houseName.split('-')[2]
      }
    })

    return (
      <div>
      { loading ? null :
        occupantReport.result.length === 0 ?
        <ReportEmpty
          _fetchReport={() => _fetchReport('penghuni')}
          report="penghuni"
          compId={compId} /> :
        <div>
          <div className="row">
            { occupantReport.fetched ? _onExportExcel(occupantReport.result, 'penghuni') : null }
            <ReloadButton
              _fetchReport={() => _fetchReport('penghuni')}
              report="penghuni"
              compId={compId} />
            <ExportPdfButton
              _onExportPdf={_onExportPdf}
              adminReport={occupantReport.result}
              report="penghuni"
              orientation={'landscape'} />
            <ResetTableState
              report="penghuni"
              _resetState={this._resetState} />
            <div className="col-xs-12">
              <ReactTable
                data={data}
                columns={columns}
                sortable={true}
                resizable={true}
                filterable={true}

                // Controlled Props
                sorted={this.state.sorted}
                page={this.state.page}
                pageSize={this.state.pageSize}
                resized={this.state.resized}
                filtered={this.state.filtered}
                onSortedChange={sorted => this.setState({ sorted })}
                onPageChange={page => this.setState({ page })}
                onPageSizeChange={(pageSize, page) => this.setState({ page, pageSize })}
                onResizedChange={resized => this.setState({ resized })}
                onFilteredChange={filtered => this.setState({ filtered })}

                // Change Text Props
                previousText={"Sebelum"}
                nextText={"Berikutnya"}
                noDataText={"Data Kosong"}
                pageText={"Halaman"}
                ofText={'dari'}
                rowsText={"baris"}
                defaultPageSize={10}
                defaultFilterMethod= {(filter, row, column) => {
                  const id = filter.pivotId || filter.id
                  return row[id] !== undefined ?
                  String(row[id]).toLowerCase().startsWith(filter.value.toLowerCase()) : true
                }} />
            </div>
          </div>
          <div className="row">
            <p style={{display: 'flex', justifyContent: 'center', marginTop: 16}}>Tip: Tahan <code> Shift </code> untuk multi sortir</p>
          </div>
        </div>
      }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    occupantReport: state.occupantReport
  }
}

export default connect(mapStateToProps)(Penghuni)
