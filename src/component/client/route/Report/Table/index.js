// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import PropTypes from 'prop-types'

// common component
import Box from '../../../common/Box'
import BoxHeader from '../../../common/Box/Header'

const TableBox = (props) => (
  <Box size="col-md-12">
    <BoxHeader title={"Laporan " + props.title}>
    </BoxHeader>
    <div className="box-body" style={{padding: "12px 20px 5px" }}>
      {props.table}
    </div>
  </Box>
)

TableBox.propTypes = {
  title: PropTypes.string.isRequired,
  table: PropTypes.node.isRequired
}

export default TableBox;
