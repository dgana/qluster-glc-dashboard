// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import { connect } from 'react-redux'

// Plugin Dependencies
import $ from 'jquery'
import classNames from 'classnames'

// Jasa Component
import ReportEmpty from './ReportEmpty'
import ReloadButton from './Button/ReloadButton'
import ExportPdfButton from './Button/ExportPdfButton'

class Jasa extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tableIsEmpty : false
    }
  }

  componentDidMount() {
    $(document).ready(function() {
      $('#table-laporan-jasa').DataTable({
        iDisplayLength: 25,
        retrieve: true
      })
    })
  }

  render() {
    const { tableIsEmpty } = this.state

    const {
      serviceReport,
      loading,
      compId,
      _fetchReport,
      _onExportExcel,
      _onExportPdf
    } = this.props

    return (
      <div>
      { loading ? null :
        serviceReport.result.length === 0 ?
        <ReportEmpty
          _fetchReport={() => _fetchReport('jasa')}
          report="jasa"
          compId={compId} /> :
        <div className="row">
          { serviceReport.fetched ? _onExportExcel(serviceReport.result, 'jasa') : null }
          <ReloadButton
            _fetchReport={() => _fetchReport('jasa')}
            report="jasa"
            compId={compId} />
          <ExportPdfButton
            _onExportPdf={_onExportPdf}
            adminReport={serviceReport.result}
            report="jasa"
            orientation={'landscape'} />
          <div className="col-xs-12">
            <table id="table-laporan-jasa" className="table text-center jasaTable">
              <thead>
                <tr>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>ID</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Penghuni</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Township</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Cluster</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Rumah</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Label</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Tanggal Kirim</th>
                  <th className={classNames({ emptyTable: tableIsEmpty })}>Status</th>
                </tr>
              </thead>
              <tbody>
              { serviceReport.result.length > 0 ? serviceReport.result.map((item, index) => (
                <tr key={index}>
                  <td>{item.id}</td>
                  <td>{item.penghuni}</td>
                  <td>{item.township}</td>
                  <td>{item.cluster}</td>
                  <td>{item.house}</td>
                  <td>{item.label}</td>
                  <td>{item.createdAt}</td>
                  <td>{item.status}</td>
                </tr>
                )) : null
              }
              </tbody>
            </table>
          </div>
        </div>
      }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    serviceReport: state.serviceReport,
    compId: state.showClient.companyId
  }
}

export default connect(mapStateToProps)(Jasa)
