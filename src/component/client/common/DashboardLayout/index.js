// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

// DashboardLayout Component
import Topbar from '../Topbar'
import Leftbar from '../Leftbar'
import Footer from '../Footer'
// import Rightbar from '../Rightbar'

class DashboardLayout extends Component {
  static propTypes = {
    client: PropTypes.shape({
      user: PropTypes.shape({
        id: PropTypes.string
      })
    }).isRequired,
  }

  componentDidMount() {

    // *************************************************************************//
    /* TODO: Insert public/js/app.js module here for the Rightbar functionality after login
    // *************************************************************************/

  }

  render() {
    const { client, children } = this.props
    return (
      <div className="wrapper hold-transition sidebar-mini">
        <Topbar client={client}/>
        <Leftbar />
          {children}
        <Footer />
        { /* <Rightbar /> */ }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    clientId: state.client,
    client: state.showClient
  }
}

export default connect(mapStateToProps, null)(DashboardLayout);
