// React, React-Router, PropTypes & Redux Dependencies
import React from 'react';
import PropTypes from 'prop-types';

const Body = props => (
  <div className="box-body" style={{padding: props.padding, display: props.display}}>
    {props.children}
  </div>
)

Body.propTypes = {
  children: PropTypes.node,
  padding: PropTypes.string,
  display: PropTypes.string
}

export default Body;
