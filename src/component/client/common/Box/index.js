// React, React-Router, PropTypes & Redux Dependencies
import React from 'react';
import PropTypes from 'prop-types'

const Box = props => (
  <div className="row" style={{display: props.display}}>
    <div className={props.size}>
      <div className="box" style={{boxShadow: '0 2px 6px 0 rgba(0,0,0,0.1)', borderRadius:2, padding: props.padding}}>
        {props.children}
      </div>
    </div>
  </div>
)

Box.propTypes = {
  children: PropTypes.node,
  // padding: PropTypes.number,
  size: PropTypes.string,
  display: PropTypes.string
}

export default Box
