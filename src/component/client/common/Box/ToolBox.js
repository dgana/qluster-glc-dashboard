// React, React-Router, PropTypes & Redux Dependencies
import React from 'react';

const ToolBox = props => (
  <div className="box-tools pull-right">
    <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus"></i>
    </button>
    <div className="btn-group">
      <button type="button" className="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
        <i className="fa fa-wrench"></i></button>
      <ul className="dropdown-menu" role="menu">
        <li><a href="#">Action</a></li>
        <li><a href="#">Another action</a></li>
        <li><a href="#">Something else here</a></li>
        <li className="divider"></li>
        <li><a href="#">Separated link</a></li>
      </ul>
    </div>
    <button type="button" className="btn btn-box-tool" data-widget="remove"><i className="fa fa-times"></i></button>
  </div>
)

export default ToolBox
