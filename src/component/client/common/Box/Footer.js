// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import PropTypes from 'prop-types'

const Footer = props => (
  <div className="box-footer" style={{padding: props.padding, borderTop: props.borderTop}}>
    {props.children}
  </div>
)

Footer.propTypes = {
  children: PropTypes.node,
  padding: PropTypes.string,
  borderTop: PropTypes.string
}

export default Footer
