// React, React-Router, PropTypes & Redux Dependencies
import React from 'react';
import PropTypes from 'prop-types'

const Header = props => (
  <div className="box-header with-border" style={{padding: props.padding}}>
    <p className="box-title">{props.title}</p>
    {props.children}
  </div>
)

Header.propTypes = {
  children: PropTypes.node,
  padding: PropTypes.string,
  title: PropTypes.string
}

export default Header
