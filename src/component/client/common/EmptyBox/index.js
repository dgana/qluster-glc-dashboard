// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'

// EmptyBox Component
import Box from '../Box'

const EmptyBox = props => (
  <Box display="none">
  </Box>
)

export default EmptyBox;
