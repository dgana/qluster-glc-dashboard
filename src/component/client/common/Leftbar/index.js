// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

// Plugin Dependencies
import classNames from 'classnames'

// Redux Dispatcher
import { dispatchSidebar } from '../../../../dispatcher'

// Utility
import util from '../../../../util/sidebar'

class Leftbar extends Component {
  static propTypes = {
    url: PropTypes.objectOf(PropTypes.string),
    nav: PropTypes.object
  }

  componentWillMount() {
    const { pathname } = this.props.url
    let payload = pathname === '/dashboard' ? util.dashboard :
    pathname === '/profile' ? util.profile :
    pathname === '/edit-profile' ? util.profile:
    pathname === '/change-password' ? util.profile:
    pathname === '/map' ? util.maps :
    pathname === '/news' ? util.news :
    pathname === '/laporan-tagihan' ? util.reportBilling :
    pathname === '/laporan-keluhan' ? util.reportComplaint :
    // pathname === '/laporan-jasa' ? util.reportService :
    // pathname === '/laporan-sos' ? util.reportSos :
    pathname === '/laporan-penghuni' ? util.reportOccupant :
    pathname === '/laporan-pengelola' ? util.reportManager :
    // pathname === '/laporan-admin' ? util.reportAdmin :
    pathname === '/laporan-peranan' ? util.reportRole :
    pathname === '/laporan-cctv' ? util.reportCctv :
    pathname === '/laporan-gps' ? util.reportGps :
    pathname === '/laporan-rumah' ? util.reportHouse :
    pathname === '/laporan-cluster' ? util.reportCluster :
    pathname === '/kirim-tagihan' ?  util.invoice :
    pathname === '/daftar-aktivasi' ?  util.registerActivation :
    pathname === '/daftar-penghuni' ? util.registerOccupant :
    pathname === '/daftar-pengelola' ?  util.registerManager :
    pathname === '/daftar-admin' ?  util.registerAdmin :
    pathname === '/daftar-cctv' ?  util.registerCctv :
    pathname === '/daftar-lokasi' ?  util.registerLocation :
    pathname === '/daftar-peranan' ?  util.registerRole :
    pathname === '/manajemen-admin' ?  util.managementAdmin :
    pathname === '/manajemen-cctv' ?  util.managementCctv :
    pathname === '/manajemen-lokasi' ?  util.managementLocation :
    pathname === '/manajemen-peranan' ?  util.managementRole : util.notifications
    this.props.dispatchSidebar('path', payload)
  }

  render() {
    const { nav, dispatchSidebar, access } = this.props
    return (
      <aside className="main-sidebar">
        <section className="sidebar">
          <ul className="sidebar-menu">
            <li className="treeview">
              { /* Sidebar Dashboard */
                access.find(item => item['feature'] === "/dashboard") ? (
                  <Link
                    to="/dashboard"
                    onClick={() => dispatchSidebar('path', util.dashboard)}
                    className={classNames('sidebar-item', { sidebarFocus: nav.active[0]} )}>
                    <i className="fa fa-home sidebar-icon"></i><span className="sidebar-text">Dashboard</span>
                  </Link>
                ) : null
              }
              { /* Sidebar Peta */
                access.find(item => item['feature'] ==='/map') ? (
                  <Link
                    to="/map"
                    onClick={() =>  dispatchSidebar('path', util.maps)}
                    className={classNames('sidebar-item', { sidebarFocus: nav.active[1]} )}>
                    <i className="fa fa-globe sidebar-icon"></i><span className="sidebar-text">Peta</span>
                  </Link>
                ) : null
              }
            </li>
            { /* Sidebar Laporan */
              access.filter(item => item.feature.includes('/laporan')).length > 0 ? (
                <li className={classNames('treeview', { active: nav.active[2] })}>
                  <a
                    href="#"
                    onClick={() => !nav.active[2] ? dispatchSidebar('active', util.report) : dispatchSidebar('active', util.defaultState)}
                    className={classNames('sidebar-item', { sidebarFocus: nav.active[2]} )}>
                    <i className="fa fa-folder-open sidebar-icon"></i><span className="sidebar-text">Laporan</span>
                    <span className="pull-right-container"><i className="fa fa-angle-down pull-right"></i></span>
                  </a>
                  <ul className="treeview-menu">
                    { /* Sidebar Laporan Mobile */
                      access.filter(item =>
                      // item.feature.includes('/laporan-jasa') || item.feature.includes('/laporan-sos')
                      item.feature.includes('/laporan-keluhan') || item.feature.includes('/laporan-tagihan') || item.feature.includes('/laporan-penghuni') || item.feature.includes('/laporan-pengelola')).length > 0 ? (
                        <li className={classNames({active: nav.active[3]})}>
                          <a
                            href="#"
                            style={{marginLeft:30}}
                            onClick={() => !nav.active[3] ? dispatchSidebar('active', util.reportQlusterApp) : dispatchSidebar('active', util.report) }
                            className={classNames('sidebar-item', { sidebarFocus: nav.active[3]} )}>
                            <span className="sidebar-text">Mobile</span>
                            <span className="pull-right-container"><i className="fa fa-angle-down pull-right"></i></span>
                          </a>
                          <ul className="treeview-menu">
                          { /* Sidebar Laporan Jasa */
                            /*
                            access.find(item => item['feature'] ==='/laporan-jasa') ? (
                              <li>
                                <Link
                                  to="/laporan-jasa"
                                  onClick={() => dispatchSidebar('path', util.reportService)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[4]})}> Jasa
                                </Link>
                              </li>
                            ) : null
                            */
                          }
                          { /* Sidebar Laporan Keluhan */
                            access.find(item => item['feature'] ==='/laporan-keluhan') ? (
                              <li>
                                <Link
                                  to="/laporan-keluhan"
                                  onClick={() => dispatchSidebar('path', util.reportComplaint)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[4]})}> Keluhan
                                </Link>
                              </li>
                            ) : null
                          }
                          { /* Sidebar Laporan SOS */
                            /*
                            access.find(item => item['feature'] ==='/laporan-sos') ? (
                              <li>
                                <Link
                                  to="/laporan-sos"
                                  onClick={() => dispatchSidebar('path', util.reportSos)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[6]})}> SOS
                                </Link>
                              </li>
                            ) : null
                            */
                          }
                          { /* Sidebar Laporan Tagihan */
                            access.find(item => item['feature'] ==='/laporan-tagihan') ? (
                              <li>
                                <Link
                                  to="/laporan-tagihan"
                                  onClick={() => dispatchSidebar('path', util.reportBilling)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[5]} )}> Tagihan
                                </Link>
                              </li>
                            ) : null
                          }
                          { /* Sidebar Laporan Penghuni */
                            access.find(item => item['feature'] ==='/laporan-penghuni') ? (
                              <li>
                                <Link
                                  to="/laporan-penghuni"
                                  onClick={() => dispatchSidebar('path', util.reportOccupant)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[6]})}> Penghuni
                                </Link>
                              </li>
                            ) : null
                          }
                          { /* Sidebar Laporan Pengelola */
                            access.find(item => item['feature'] ==='/laporan-pengelola') ? (
                              <li>
                                <Link
                                  to="/laporan-pengelola"
                                  onClick={() => dispatchSidebar('path', util.reportManager)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[7]})}> Pengelola
                                </Link>
                              </li>
                            ) : null
                          }
                          </ul>
                        </li>
                      ) : null
                    }
                    { /* Sidebar Laporan Dashboard */
                      access.filter(item =>
                        // item.feature.includes('/laporan-admin') ||
                        item.feature.includes('/laporan-peranan') || item.feature.includes('/laporan-cctv') || item.feature.includes('/laporan-rumah') || item.feature.includes('/laporan-cluster')).length > 0 ? (
                        <li className={classNames({active: nav.active[8]})}>
                          <a
                            href="#"
                            style={{marginLeft:30}}
                            onClick={() => !nav.active[8] ? dispatchSidebar('active', util.reportDashboard) : dispatchSidebar('active', util.report) }
                            className={classNames('sidebar-item', { sidebarFocus: nav.active[8]} )}>
                            <span className="sidebar-text">Dashboard</span>
                            <span className="pull-right-container"><i className="fa fa-angle-down pull-right"></i></span>
                          </a>
                          <ul className="treeview-menu">
                          { /* Sidebar Laporan Admin */
                            /*
                            access.find(item => item['feature'] ==='/laporan-admin') ? (
                              <li>
                                <Link
                                  to="/laporan-admin"
                                  onClick={() => dispatchSidebar('path', util.reportAdmin)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[11]})}> Admin
                                </Link>
                              </li>
                            ) : null
                            */
                          }
                          { /* Sidebar Laporan Peranan */
                          access.find(item => item['feature'] ==='/laporan-peranan') ? (
                              <li>
                                <Link
                                  to="/laporan-peranan"
                                  onClick={() =>  dispatchSidebar('path', util.reportRole)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[9]})}> Peranan
                                </Link>
                              </li>
                            ) : null
                          }
                          { /* Sidebar Laporan CCTV */
                            access.find(item => item['feature'] ==='/laporan-cctv') ? (
                              <li>
                                <Link
                                  to="/laporan-cctv"
                                  onClick={() => dispatchSidebar('path', util.reportCctv)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[10]} )}> CCTV
                                </Link>
                              </li>
                            ) : null
                          }
                          { /* Sidebar Laporan GPS
                            access.find(item => item['feature'] ==='/laporan-gps') ? (
                              <li>
                                <Link
                                  to="/laporan-gps"
                                  onClick={() => dispatchSidebar('path', util.reportGps)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[11]} )}> GPS
                                </Link>
                              </li>
                            ) : null
                          */}
                          { /* Sidebar Laporan Rumah */
                            access.find(item => item['feature'] ==='/laporan-rumah') ? (
                              <li>
                                <Link
                                  to="/laporan-rumah"
                                  onClick={() => dispatchSidebar('path', util.reportHouse)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[12]})}> Rumah
                                </Link>
                              </li>
                            ) : null
                          }
                          { /* Sidebar Laporan Peranan */
                          access.find(item => item['feature'] ==='/laporan-cluster') ? (
                              <li>
                                <Link
                                  to="/laporan-cluster"
                                  onClick={() =>  dispatchSidebar('path', util.reportCluster)}
                                  className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[13]})}> Cluster
                                </Link>
                              </li>
                            ) : null
                          }
                        </ul>
                      </li>
                    ) : null
                  }
                  </ul>
                </li>
              ) : null
            }
            <li className="treeview">
              { /* Sidebar Kirim Tagihan */
                access.find(item => item['feature'] ==='/kirim-tagihan') ? (
                  <Link
                    to="/kirim-tagihan"
                    onClick={() => dispatchSidebar('path', util.invoice)}
                    className={classNames('sidebar-item', { sidebarFocus: nav.active[14]} )}>
                    <i className="fa fa-print sidebar-icon"></i><span className="sidebar-text">Kirim Tagihan</span>
                  </Link>
                ) : null
              }
              { /* Sidebar Berita */
                access.find(item => item['feature'] === "/news") ? (
                  <Link
                    to="/news"
                    onClick={() => dispatchSidebar('path', util.news)}
                    className={classNames('sidebar-item', { sidebarFocus: nav.active[15]} )}>
                    <i className="fa fa-newspaper-o sidebar-icon"></i><span className="sidebar-text">Berita</span>
                  </Link>
                ) : null
              }
            </li>
            { /* Sidebar Pendaftaran */
              access.filter(item => item.feature.includes('/daftar')).length > 0 ? (
                <li className={classNames('treeview', { active: nav.active[16] })}>
                  <a
                    href="#"
                    onClick={() => !nav.active[16] ? dispatchSidebar('active', util.register) : dispatchSidebar('active', util.defaultState)}
                    className={classNames('sidebar-item', { sidebarFocus: nav.active[16]} )}>
                    {/* fa fa-check-square-o */}
                    <i className="fa fa-book sidebar-icon"></i><span className="sidebar-text">Pendaftaran</span>
                    <span className="pull-right-container"><i className="fa fa-angle-down pull-right"></i></span>
                  </a>
                  <ul className="treeview-menu">
                    { /* Sidebar Mobile */
                      access.find(item => item['feature'] ==='/daftar-pengelola') || access.find(item => item['feature'] ==='/daftar-penghuni') ? (
                        <li className={classNames({active: nav.active[17]})}>
                          <a
                            href="#"
                            style={{marginLeft:30}}
                            onClick={() => !nav.active[17] ? dispatchSidebar('active', util.registerQlusterApp) : dispatchSidebar('active', util.register) }
                            className={classNames('sidebar-item', { sidebarFocus: nav.active[17]} )}>
                            <span className="sidebar-text">Mobile</span>
                            <span className="pull-right-container"><i className="fa fa-angle-down pull-right"></i></span>
                          </a>
                          <ul className="treeview-menu">
                            { /* Sidebar Daftar Pengelola */
                              access.find(item => item['feature'] ==='/daftar-pengelola') ? (
                                <li>
                                  <Link
                                    to="/daftar-pengelola"
                                    onClick={() => dispatchSidebar('path', util.registerManager)}
                                    className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[18]} )}><span>Pengelola</span>
                                  </Link>
                                </li>
                              ) : null
                            }
                            { /* Sidebar Daftar Penghuni */
                              access.find(item => item['feature'] ==='/daftar-penghuni') ? (
                                <li>
                                  <Link
                                    to="/daftar-penghuni"
                                    onClick={() => dispatchSidebar('path', util.registerOccupant)}
                                    className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[19]})}> Penghuni
                                  </Link>
                                </li>
                              ) : null
                            }
                          </ul>
                        </li>
                      ) : null
                    }
                    { /* Sidebar Daftar Dashboard */
                      access.find(item => item['feature'] ==='/daftar-admin') || access.find(item => item['feature'] ==='/daftar-cctv') || access.find(item => item['feature'] ==='/daftar-lokasi') || access.find(item => item['feature'] ==='/daftar-peranan') ? (
                        <li className={classNames({active: nav.active[20]})}>
                          <a
                            href="#"
                            style={{marginLeft:30}}
                            onClick={() => !nav.active[20] ? dispatchSidebar('active', util.registerDashboard) : dispatchSidebar('active', util.register) }
                            className={classNames('sidebar-item', { sidebarFocus: nav.active[20]} )}>
                            <span className="sidebar-text">Dashboard</span>
                            <span className="pull-right-container"><i className="fa fa-angle-down pull-right"></i></span>
                          </a>
                          <ul className="treeview-menu">
                            { /* Sidebar Daftar CCTV */
                              access.find(item => item['feature'] ==='/daftar-cctv') ? (
                                <li>
                                  <Link
                                    to="/daftar-cctv"
                                    onClick={() => dispatchSidebar('path', util.registerCctv)}
                                    className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[21]})}> CCTV
                                  </Link>
                                </li>
                              ) : null
                            }
                            { /* Sidebar Daftar Lokasi */
                              access.find(item => item['feature'] ==='/daftar-lokasi') ? (
                                <li>
                                  <Link
                                    to="/daftar-lokasi"
                                    onClick={() => dispatchSidebar('path', util.registerLocation)}
                                    className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[22]})}> Lokasi
                                  </Link>
                                </li>
                              ) : null
                            }
                            { /* Sidebar Daftar Lokasi */
                              access.find(item => item['feature'] ==='/daftar-peranan') ? (
                                <li>
                                  <Link
                                    to="/daftar-peranan"
                                    onClick={() => dispatchSidebar('path', util.registerRole)}
                                    className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[23]})}> Peranan
                                  </Link>
                                </li>
                              ) : null
                            }
                          </ul>
                        </li>
                      ) : null
                    }
                  </ul>
                </li>
              ) : null
            }
            { /* Sidebar Manajemen */
              access.filter(item => item.feature.includes('/manajemen')).length > 0 ? (
                <li className={classNames('treeview', { active: nav.active[24] })}>
                  <a
                    href="#"
                    onClick={() => !nav.active[24] ? dispatchSidebar('active', util.management) : dispatchSidebar('active', util.defaultState)}
                    className={classNames('sidebar-item', { sidebarFocus: nav.active[24]} )}>
                    <i className="fa fa-pencil sidebar-icon"></i><span className="sidebar-text">Manajemen</span>
                    <span className="pull-right-container"><i className="fa fa-angle-down pull-right"></i></span>
                  </a>
                  <ul className="treeview-menu">
                    { /* Sidebar Manajemen Admin */
                      access.find(item => item['feature'] ==='/manajemen-admin') ? (
                        <li>
                          <Link
                            to="/manajemen-admin"
                            onClick={() => dispatchSidebar('path', util.managementAdmin)}
                            className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[25]})}> Admin
                          </Link>
                        </li>
                      ) : null
                    }
                    { /* Sidebar Manajemen CCTV */
                      access.find(item => item['feature'] ==='/manajemen-cctv') ? (
                        <li>
                          <Link
                            to="/manajemen-cctv"
                            onClick={() => dispatchSidebar('path', util.managementCctv)}
                            className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[26]})}> CCTV
                          </Link>
                        </li>
                      ) : null
                    }
                    { /* Sidebar Manajemen Lokasi */
                      access.find(item => item['feature'] ==='/manajemen-lokasi') ? (
                        <li>
                          <Link
                            to="/manajemen-lokasi"
                            onClick={() => dispatchSidebar('path', util.managementLocation)}
                            className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[27]})}> Lokasi
                          </Link>
                        </li>
                      ) : null
                    }
                    { /* Sidebar Manajemen Peranan */
                      access.find(item => item['feature'] ==='/manajemen-peranan') ? (
                        <li>
                          <Link
                            to="/manajemen-peranan"
                            onClick={() => dispatchSidebar('path', util.managementRole)}
                            className={classNames('sidebar-subitem', { sidebarSubItemFocus: nav.active[28]})}> Peranan
                          </Link>
                        </li>
                      ) : null
                    }
                  </ul>
                </li>
              ) : null
            }
          </ul>
        </section>
      </aside>
    )
  }
}

const mapStateToProps = state => {
  return {
    url: state.router.location,
    nav: state.sidebar,
    client: state.client.user,
    access: state.client.user.features
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchSidebar: (path, payload) => dispatch(dispatchSidebar(path, payload))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Leftbar);
