// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import PropTypes from 'prop-types'

const Wrapper = props => (
  <div className="content-wrapper" style={{background:"#FAFAFA"}}>
    {props.children}
  </div>
)

Wrapper.propTypes = {
  children: PropTypes.node
}

export default Wrapper
