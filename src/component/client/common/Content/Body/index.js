// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

// Plugin Dependencies
import classNames from 'classnames'

class ContentBody extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			animate: true
		}
	}

	componentWillReceiveProps = (nextProps) => {
		if (nextProps.route !== null) {
			if (this.props.route.pathname !== nextProps.route.pathname){
				this.setState({animate:false})
				setTimeout(() => this.setState({ animate: true }), 1)
			}
		}
	}

	render() {
		const { animate } = this.state
		return (
		  <section
				className={classNames('content', { slidingAnimation: animate })}
				style={{padding: this.props.padding, minHeight: window.innerHeight - 100}}
				ref={ el => this.el = el}>
		    {this.props.children}
		  </section>
		)
	}
}

ContentBody.propTypes = {
  children: PropTypes.node
}

const mapStateToProps = (state) => {
	return{
		route: state.router.location
	}
}
export default connect(mapStateToProps)(ContentBody);
