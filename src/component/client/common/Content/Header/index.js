// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import PropTypes from 'prop-types'

// ContentHeader Component
import Breadcrumb from './Breadcrumb'

const ContentHeader = (props) => (
  <section className="content-header">
    <h1>{props.title}<small>{props.subTitle}</small></h1>
    <Breadcrumb title="Dashboard" />
  </section>
)

ContentHeader.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string
}

export default ContentHeader;
