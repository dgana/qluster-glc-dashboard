// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

const Breadcrumb = props => (
  <ol className="breadcrumb">
    <li><Link to="dashboard"><i className="fa fa-home"></i> Home</Link></li>
    <li className="active"><Link to="dashboard"><i className="fa fa-dashboard"></i>{props.title}</Link></li>
  </ol>
)

Breadcrumb.propTypes = {
  title: PropTypes.title
}

export default Breadcrumb
