// React, React-Router, PropTypes & Redux Dependencies
import React from 'react'
import { connect } from 'react-redux'

// Loading Image
import Qluster from '../../../../../public/imgs/loading/loading_qluster.gif'

class Loading extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nav: 'loadingContainer'
    }
  }

  componentDidMount() {
    const { toggle } = this.props
    if(toggle) {
      this.setState({nav: "loadingContainerToggled"})
    } else {
      this.setState({nav: "loadingContainer"})
    }
  }

  render () {
    return (
      <div style={{minHeight: window.innerHeight - 100}}>
        <div className="loadingStyle">
          <div className={this.state.nav} style={{paddingLeft: this.props.paddingLeft}}>
            <img src={Qluster} alt="loading" className="imgLoadingStyle" style={{marginBottom: this.props.marginBottom}} />
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    toggle: state.sidebarToggle
  }
}

export default connect(mapStateToProps)(Loading);
