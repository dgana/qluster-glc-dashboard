// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Footer extends Component {
  render() {
    return (
      <div>
        { /* <aside className="control-sidebar control-sidebar-dark">
          <ul className="nav nav-tabs nav-justified control-sidebar-tabs">
            <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i className="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i className="fa fa-gears"></i></a></li>
          </ul>
          <div className="tab-content">
            <div className="tab-pane active" id="control-sidebar-home-tab">
              <h3 className="control-sidebar-heading">Recent Activity</h3>
              <ul className="control-sidebar-menu">
                <li>
                  <Link to="dashboard">
                    <i className="menu-icon fa fa-birthday-cake bg-red"></i>
                    <div className="menu-info">
                      <h4 className="control-sidebar-subheading">Langdons Birthday</h4>
                      <p>Will be 23 on April 24th</p>
                    </div>
                  </Link>
                </li>
              </ul>
              <h3 className="control-sidebar-heading">Tasks Progress</h3>
              <ul className="control-sidebar-menu">
                <li>
                  <Link to="dashboard">
                    <h4 className="control-sidebar-subheading">Custom Template Design
                      <span className="label label-danger pull-right">70%</span>
                    </h4>

                    <div className="progress progress-xxs">
                      <div className="progress-bar progress-bar-danger" ></div>
                    </div>
                  </Link>
                </li>
              </ul>
            </div>
            <div className="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <div className="tab-pane" id="control-sidebar-settings-tab">
              <h3 className="control-sidebar-heading">General Settings</h3>
              <div className="form-group">
                <label className="control-sidebar-subheading">Report panel usage
                  <input type="checkbox" className="pull-right"  />
                </label>
                <p>Some information about this general settings option</p>
              </div>
              <h3 className="control-sidebar-heading">Chat Settings</h3>
              <div className="form-group">
                <label className="control-sidebar-subheading">Turn off notifications
                  <input type="checkbox" className="pull-right" />
                </label>
              </div>
              <div className="form-group">
                <label className="control-sidebar-subheading">Delete chat history
                  <Link to="dashboard" className="text-red pull-right"><i className="fa fa-trash-o"></i></Link>
                </label>
              </div>
            </div>
          </div>
        </aside> */ }
        <div className="control-sidebar-bg"></div>
      </div>
    );
  }
}

export default Footer;
