// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

// Dispatcher
import {
  dispatchClientLogout,
  dispatchSidebarToggle,
  dispatchHideMapMenu,
  dispatchShowMapMenu
} from '../../../../dispatcher'

// Images
import Logo from '../../../../../public/imgs/logo/logo_sedayu_negative.png'

// Plugin Dependencies
import _ from 'lodash'

// const boxCommentsStyle = {
//   padding: "16px 24px 2px",
//   borderTop: "1px solid rgb(240,240,240)",
//   borderBottom: '1px solid rgb(232, 232, 232)',
//   background: '#FFF'
// }

class Topbar extends Component {
  static propTypes = {
    client: PropTypes.object.isRequired
  }

  logout = () => {
    this.props.dispatchClientLogout(this._callbackLogout)
  }

  _callbackLogout = () => {
    window.localStorage.clear()
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.route === null) {
      setTimeout(() => window.location.reload(), 1000)
    }
  }

  setSidebarToggle = () => {
    this.props.dispatchShowMapMenu()
    this.props.dispatchSidebarToggle()
  }

  render() {
    const { client } = this.props

    return (
      <header className="main-header">
        <Link to="/dashboard" className="logo" style={{backgroundColor: '#1D9F80'}}>
          { /* <span className="logo-mini"><b>A</b>LT</span> */ }
          <span className="logo-lg"><img style={{width:45}} src={Logo} alt='Qluster Logo'></img></span>
        </Link>
        <nav className="navbar navbar-static-top">
          <Link to="/dashboard" onClick={this.setSidebarToggle} className="sidebar-toggle" data-toggle="offcanvas" role="button">
              <span className="sr-only">Toggle navigation</span>
          </Link>
          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
              {/* <li className="dropdown notifications-menu">
                <Link to="/dashboard" className="dropdown-toggle" data-toggle="dropdown" style={{color:"rgb(230,230,230)", paddingRight:0}}>
                  <i className="fa fa-bell" style={{fontSize:18}}></i>
                  <span className="label label-warning">10</span>
                </Link>
                  <ul className="dropdown-menu" style={{top: '96%'}}>
                    <li className="header" style={{borderBottom: '1px solid rgba(255,255,255,0.2)'}}>You have 4 notifications</li>
                    <div className="box-comments" style={boxCommentsStyle}>
                      <div className="box-comment">
                        <img className="img-circle img-sm" src={img1} alt="User" />
                        <div className="comment-text">
                          <span className="username">
                            Maria Gonzales
                          </span>
                          It is a long established fact that a reader.
                          <p className="text-muted" style={{marginTop: 4}}>8:03 PM Today</p>
                        </div>

                      </div>
                      <div className="box-comment">
                        <img className="img-circle img-sm" src={img2} alt="User" />
                        <div className="comment-text">
                          <span className="username">
                            Luna Stark
                          </span>
                          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                          <p className="text-muted" style={{marginTop: 4}}>8:03 PM Today</p>
                        </div>
                      </div>
                    </div>
                    <li className="footer">
                      <Link to="/notifications">
                        View All
                      </Link>
                    </li>
                  </ul>
                </li> */}
                <li onClick={this.props.dispatchHideMapMenu} className="dropdown user user-menu">
                  <Link to="/dashboard" className="dropdown-toggle" data-toggle="dropdown">
                    <img src={client.picture} className="user-image" alt="User Profile" />
                    <span className="hidden-xs text-white" style={{fontSize:13, fontFamily:'MontserratRegular', fontWeight: 300}}>{ client.name }</span>
                  </Link>
                  <ul className="dropdown-menu" style={{top:'96%'}}>
                    <Link to="/profile">
                      <li className="user-footer" style={{padding: 12, color:"#494949"}}>
                        <span className="fa fa-user" style={{marginRight:10}}></span>Profil
                      </li>
                    </Link>
                    <Link to="/change-password">
                      <li className="user-footer" style={{padding: 12, color:"#494949"}}>
                        <span className="fa fa-key" style={{marginRight:10}}></span>Ubah Sandi
                      </li>
                    </Link>
                    <li className="user-footer" onClick={this.logout} style={{cursor:'pointer', paddingLeft: 7, color: "rgb(73,73,73)", margin: 6, marginBottom: 12 }}>
                      <span className="fa fa-sign-out" style={{marginRight:10}}></span>Keluar
                    </li>
                  </ul>
                </li>
                {/*
                <li>
                  <Link to="/dashboard" data-toggle="control-sidebar">
                    <i className="fa fa-gears" style={{color:"rgb(230,230,230)"}}></i>
                  </Link>
                </li>
                */}
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}

const mapStateToProps = state => {
  return {
    route: state.router.location,
    client: state.client.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchClientLogout: (cb) => dispatch(dispatchClientLogout(cb)),
    dispatchSidebarToggle: () => dispatch(dispatchSidebarToggle()),
    dispatchHideMapMenu: () => dispatch(dispatchHideMapMenu()),
    dispatchShowMapMenu: () => dispatch(dispatchShowMapMenu()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Topbar);
