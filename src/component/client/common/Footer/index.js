// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="main-footer">
        <div className="pull-right hidden-xs">
          <strong>Version</strong> 0.1.0
        </div>
        <strong>Copyright &copy; 2017 <a href="http://qlue.co.id/site/qluster/" target="_blank">Qluster</a>.</strong> All rights reserved.
      </footer>
    );
  }
}

export default Footer
