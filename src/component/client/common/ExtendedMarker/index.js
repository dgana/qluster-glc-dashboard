import { Marker } from 'react-leaflet'

// Create your own class, extending from the Marker class.
class ExtendedMarker extends Marker {
	// "Hijack" the component lifecycle.
  componentDidMount() {
  	// Call the Marker class componentDidMount (to make sure everything behaves as normal)
  	super.componentDidMount();

    // Access the marker element and open the popup.
    setTimeout(() => this.leafletElement.openPopup(), 750)
  }
}

export default ExtendedMarker
