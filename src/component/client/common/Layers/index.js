// React, React-Router, PropTypes & Redux Dependencies
import React, { Component } from 'react'

// Leaflet.GridLayer.GoogleMutant
import { GoogleLayer } from 'react-leaflet-google'
const key = 'AIzaSyCWmYWdVY4SNR4Ni6a43g1PaX3DucLJtME';
const road = 'ROADMAP';
const satellite = "SATELLITE"
// const terrain = 'TERRAIN';

// Plugin Dependencies
import { TileLayer, LayersControl, FeatureGroup, GeoJSON } from 'react-leaflet';
const { BaseLayer, Overlay } = LayersControl

// import geoJson from '../../../../../public/GeoJSON/SITEPLAN_SH.js'
import geoJsonAmerikaLatin from '../../../../../public/GeoJSON/CLUSTER_AMERIKA_LATIN.js'
import geoJsonAmerika from '../../../../../public/GeoJSON/CLUSTER_AMERIKA.js'
import geoJsonAsia from '../../../../../public/GeoJSON/CLUSTER_ASIA.js'
import geoJsonAustralia from '../../../../../public/GeoJSON/CLUSTER_AUSTRALIA.js'
import geoJsonEastAsia from '../../../../../public/GeoJSON/CLUSTER_EAST_ASIA.js'
import geoJsonEastEurope from '../../../../../public/GeoJSON/CLUSTER_EAST_EUROPE.js'
import geoJsonWestEurope from '../../../../../public/GeoJSON/CLUSTER_WEST_EUROPE.js'
import geoJsonOthers from '../../../../../public/GeoJSON/OTHERS.js'

class Layers extends Component {

  _renderClusters = () => {
    return [
      <Overlay name="Cluster Amerika Latin" checked>
        <FeatureGroup>
          <GeoJSON data={geoJsonAmerikaLatin} style={() => {
            return {
              color: "white",
              fillColor: '#ebbe8a',
              fillOpacity: 1,
              weight: 1
            }
          }} />
        </FeatureGroup>
      </Overlay>,
      <Overlay name="Cluster Amerika" checked>
        <FeatureGroup>
          <GeoJSON data={geoJsonAmerika} style={() => {
            return {
              color: "white",
              fillColor: '#b04c41',
              fillOpacity: 1,
              weight: 1
            }
          }} />
        </FeatureGroup>
      </Overlay>,
      <Overlay name="Cluster Asia" checked>
        <FeatureGroup>
          <GeoJSON data={geoJsonAsia} style={() => {
            return {
              color: "white",
              fillColor: '#697f77',
              fillOpacity: 1,
              weight: 1
            }
          }} />
        </FeatureGroup>
      </Overlay>,
      <Overlay name="Cluster Australia" checked>
        <FeatureGroup>
          <GeoJSON data={geoJsonAustralia} style={() => {
            return {
              color: "white",
              fillColor: '#aab89c',
              fillOpacity: 1,
              weight: 1
            }
          }} />
        </FeatureGroup>
      </Overlay>,
      <Overlay name="Cluster East Asia" checked>
        <FeatureGroup>
          <GeoJSON data={geoJsonEastAsia} style={() => {
            return {
              color: "white",
              fillColor: '#cdc8bc',
              fillOpacity: 1,
              weight: 1
            }
          }} />
        </FeatureGroup>
      </Overlay>,
      <Overlay name="Cluster East Europe" checked>
        <FeatureGroup>
          <GeoJSON data={geoJsonEastEurope} style={() => {
            return {
              color: "white",
              fillColor: '#678075',
              fillOpacity: 1,
              weight: 1
            }
          }} />
        </FeatureGroup>
      </Overlay>,
      <Overlay name="Cluster West Europe" checked>
        <FeatureGroup>
          <GeoJSON data={geoJsonWestEurope} style={() => {
            return {
              color: "white",
              fillColor: 'rgb(50,50,50)',
              fillOpacity: 1,
              weight: 1
            }
          }} />
        </FeatureGroup>
      </Overlay>,
      <Overlay name="Others" checked>
        <FeatureGroup>
          <GeoJSON data={geoJsonOthers} style={() => {
            return {
              color: "white",
              fillColor: 'rgb(0,0,0)',
              fillOpacity: 1,
              weight: 1
            }
          }} />
        </FeatureGroup>
      </Overlay>,
    ]
  }

  render() {
    return (
      <LayersControl position="topleft">
        <BaseLayer name='Google Map Road'>
           <GoogleLayer googlekey={key} maptype={road}/>
        </BaseLayer>
        <BaseLayer name='Google Map Satellite'>
           <GoogleLayer googlekey={key} maptype={satellite}/>
        </BaseLayer>
        <BaseLayer name="OSM Mapnik">
          <TileLayer
            attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot; target=&quot;_blank;>OpenStreetMap</a> contributors"
            url="http://{s}.tile.osm.org/{z}/{x}/{y}.png" />
        </BaseLayer>
        <BaseLayer name="OSM Black & White">
          <TileLayer
            attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot; target=&quot;_blank;>OpenStreetMap</a> contributors"
            url="http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png" />
        </BaseLayer>
        <BaseLayer checked name="ESRI Satellite">
          <TileLayer
            url="http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}" />
        </BaseLayer>
        <BaseLayer name="MapBox Satellite">
          <TileLayer
            url="https://api.mapbox.com/styles/v1/ivanbudiman/cj6naayvg0jqg2rszgvreaqyc/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiaXZhbmJ1ZGltYW4iLCJhIjoiY2o2bmFnNTMxMDVvYTMzbWU5ZTVvZjNzZyJ9.VK1EvNRMzFrqKdcWPuCRHg" />
        </BaseLayer>
        { this._renderClusters() }
      </LayersControl>
    )
  }
}

export default Layers;
