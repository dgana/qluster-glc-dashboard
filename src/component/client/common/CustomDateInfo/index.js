// React, React-Router, PropTypes & Redux Dependencies
import React from 'react';

const CustomDateInfo = () => (
  <div
    style={{
      padding: '10px 21px',
      borderTop: '1px solid #dce0e0',
      color: '#484848',
    }}
  >
    &#x2755; Periode tanggal awal dan akhir untuk kalkulasi grafik
  </div>
)

export default CustomDateInfo;
