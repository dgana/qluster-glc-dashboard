import { MapControl } from 'react-leaflet';
import L from 'leaflet'
import 'leaflet-geocoder-mapzen';

class SearchBox extends MapControl {
  componentWillMount() {
    const geocoder = L.control.geocoder('mapzen-SoqC5MZ', {
      focus: true, // search nearby
      placeholder: 'Cari Lokasi',
      markers: false,
      position: this.props.position ? this.props.position : 'topleft',
      expanded: false
    })

    let marker, layer

    geocoder.on('select', function (e) {
      if (marker) { // check
        geocoder.removeLayer(marker); // remove
      }

      const _lat = e.latlng.lat;
      const _lng = e.latlng.lng;
      const _selectedAddress = e.feature.properties.label;

      // console.log('You have selected', _selectedAddress, _lat, _lng); // :)

      // create our own markers from mapzen information i cant get the marker icon from mapzen to display
      const pointMarker = L.icon({
        iconUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Red-circle.svg/1024px-Red-circle.svg.png',
        iconSize: [15, 15]
      });

      layer = L.marker([_lat, _lng], {
        icon: pointMarker,
        title: _lat + ' ' + _lng })
        // .bindTooltip(_selectedAddress , { permanent: true, direction: 'top', offset: [0, -5], })
        .bindPopup(_selectedAddress ).on('popupclose', function() {setTimeout(() => layer.remove(), 250) })
        .addTo(this._map)
        .openPopup()
      })

      // layer.on('popupclose', function (e) {
      //   setTimeout(() => layer.remove(), 250)
      // })

      geocoder.on('reset', function (e) {
        setTimeout(() => layer.remove(), 250)
      })

      this.leafletElement = geocoder
    }
}

export default SearchBox
