import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Route, Redirect, Switch } from 'react-router-dom'

import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter } from 'react-router-redux'
const history = createHistory()

import Recovery from './component/client/route/Recovery'

// Client Component
import CompanyLogin from './component/client/route/Login'
import Dashboard from './component/client/route/Dashboard'
import Profile from './component/client/route/Profile'
// import Notifications from './component/client/route/Notifications'
import QlusterMap from './component/client/route/Map'
import News from './component/client/route/News'
import Report from './component/client/route/Report'
import Billing from './component/client/route/Billing'
import DaftarQluster from './component/client/route/Register/QlusterApp'
import DaftarDashboard from './component/client/route/Register/Dashboard'
import ManajemenDashboard from './component/client/route/Management'

// Not Found Page Component
import NotFound from './component/NotFound'

// Import all CSS files
import 'bootstrap/dist/css/bootstrap.min.css'
import 'datatables-bootstrap3-plugin/media/css/datatables-bootstrap3.css'
import 'font-awesome/css/font-awesome.min.css'
import 'leaflet/dist/leaflet.css'
import '../public/css/animate.min.css'
import '../public/css/pe-icon-7-stroke.css'
import '../public/css/companyLogin.css'
import '../public/css/admin-dashboard.css'
import '../public/css/react-d3-components.css'
import '../public/css/important.css'
import '../public/css/datatables.css'

// Import Icheck from node modules
import 'icheck/skins/square/blue.css'
import 'icheck/skins/square/green.css'
import 'icheck/skins/square/red.css'
import 'react-dates/lib/css/_datepicker.css'

// Import React Select from node modules
import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css'
import 'react-virtualized-select/styles.css'

// Import all JS files
import 'bootstrap/dist/js/bootstrap.min.js'
import '../public/js/bootstrap-notify/index.js'
import '../public/js/jquery.dataTables.min.js'
import '../public/js/dataTables.bootstrap.min.js'
import '../public/js/admin-dashboard.js'

class App extends Component {
  static propTypes = {
    client: PropTypes.shape({ isAuthenticated: PropTypes.bool })
  }

  render() {
    const { client } = this.props

    const UserProtectedRoute = ({ component, ...rest }) => (
      <Route {...rest} render={props => (
        client.isAuthenticated ? (
          React.createElement(component, props)
        ) : (
          <Redirect to={{
            pathname: '/',
          }}/>
        )
      )}/>
    )

    const UserAuthRoute = ({ component, ...rest }) => (
      <Route {...rest} render={props => (
        !client.isAuthenticated ? (
          React.createElement(component, props)
        ) : (
          <Redirect to={{
            pathname: '/dashboard',
          }}/>
        )
      )}/>
    )

    const RecoveryPasswordRoute = ({ component, ...rest }) => (
      <Route {...rest} render={props => (
        (!client.isAuthenticated) ? (
          React.createElement(component, props)
        ) :
        (client.isAuthenticated) ? (
          <Redirect to={{
            pathname: '/',
          }}/>
        ):(
          <Redirect to={{
            pathname: '/lupa-password',
          }}/>
        )
      )}/>
    )

    return (
      <ConnectedRouter history={history}>
        <Switch>
          { /* Client Routes */ }
          <UserAuthRoute exact path='/' component={CompanyLogin}  />
          { /* Client Protected Routes */ }
          <UserProtectedRoute exact path='/dashboard' component={Dashboard} />
          { client.isAuthenticated ?
            client.user.features.map((route, index) => {
              const component = route.feature === '/map' ? QlusterMap :
              route.feature === '/kirim-tagihan' ? Billing :
              route.feature === '/news' ? News :
              route.feature === '/laporan-admin' || route.feature === '/laporan-cctv' || route.feature === '/laporan-jasa' || route.feature === '/laporan-keluhan' || route.feature === '/laporan-penghuni' || route.feature === '/laporan-pengelola' || route.feature === '/laporan-peranan' || route.feature === '/laporan-rumah' || route.feature === '/laporan-cluster' || route.feature === '/laporan-sos' || route.feature === '/laporan-tagihan' ? Report :
              route.feature === '/daftar-aktivasi' || route.feature === '/daftar-penghuni' || route.feature === '/daftar-pengelola' ? DaftarQluster :
              route.feature === '/daftar-admin' || route.feature === '/daftar-cctv' || route.feature === '/daftar-lokasi' || route.feature === '/daftar-peranan' ? DaftarDashboard :
              route.feature === '/manajemen-admin' || route.feature === '/manajemen-cctv' || route.feature === '/manajemen-lokasi' || '/manajemen-peranan' ? ManajemenDashboard : null
              return (
                <UserProtectedRoute key={index} exact path={route.feature} component={component} />
              )
            }) : null
          }
          <UserProtectedRoute exact path='/profile' component={Profile} />
          <UserProtectedRoute exact path='/edit-profile' component={Profile} />
          <UserProtectedRoute exact path='/change-password' component={Profile} />
          <RecoveryPasswordRoute exact path='/lupa-password' component={Recovery} />
          {/*
            <UserProtectedRoute exact path='/notifications' component={Notifications} />
          */}

          { /* Location Not Found */ }
          <Route component={NotFound} />
        </Switch>
      </ConnectedRouter>
    )
  }
}

const mapStateToProps = state => {
  return {
    client: state.client
  }
}

export default connect(mapStateToProps)(App);
