import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import client from './client/client'
import clientList from './client/clientList'
import showClient from './client/showClient'
import sidebarToggle from './client/sidebarToggle'
import sidebar from './client/sidebar'
import path from './client/path'
import houseCode from './client/houseCode'
import managerCode from './client/managerCode'
import roleOption from './client/roleOption'
import roleFeatures from './client/roleFeatures'
import adminReport from './client/adminReport'
import serviceReport from './client/serviceReport'
import complaintReport from './client/complaintReport'
import occupantReport from './client/occupantReport'
import managerReport from './client/managerReport'
import roleReport from './client/roleReport'
import sosReport from './client/sosReport'
import billingReport from './client/billingReport'
import cctvReport from './client/cctvReport'
import houseReport from './client/houseReport'
import clusterReport from './client/clusterReport'
import gpsReport from './client/gpsReport'
import tabPanel from './client/tabPanel'
import newsList from './client/newsList'
import houseList from './client/houseList'
import clusterList from './client/clusterList'
import cctvList from './client/cctvList'
import roleList from './client/roleList'
import adminManagementList from './client/adminManagementList'
import houseManagementList from './client/houseManagementList'
import houseSearch from './client/houseSearch'
import summary from './client/summary'
import filteredHouse from './client/filteredHouse'
import mapMenuDisplay from './client/mapMenuDisplay'

const appReducers = combineReducers({
  client,
  clientList,
  showClient,
  sidebarToggle,
  sidebar,
  path,
  houseCode,
  managerCode,
  roleOption,
  roleFeatures,
  adminReport,
  serviceReport,
  complaintReport,
  occupantReport,
  managerReport,
  roleReport,
  sosReport,
  billingReport,
  cctvReport,
  houseReport,
  clusterReport,
  gpsReport,
  tabPanel,
  newsList,
  houseList,
  filteredHouse,
  clusterList,
  cctvList,
  roleList,
  adminManagementList,
  houseManagementList,
  houseSearch,
  summary,
  mapMenuDisplay,
  router: routerReducer
})

const rootReducers = (state, action) => {
  if (action.type === 'CLIENT_LOGOUT') {
    action.payload()
    state = undefined
  }

  return appReducers(state, action)
}

export default rootReducers
