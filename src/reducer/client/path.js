const initialState = []

export default (state = initialState, action) => {
  switch(action.type) {
    case 'CLIENT_PATH':
      return action.payload
    case 'ADMIN_PATH':
      return action.payload
    default:
      return state
  }
}
