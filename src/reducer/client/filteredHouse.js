const initialState = {
  fetched: true,
  result: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'FILTER_HOUSE_LIST':
      return {
        fetched: true,
        result: state.result.concat(action.payload)
      }
    case 'SIMPLE_FILTER_HOUSE_LIST':
      return {
        fetched: true,
        result: [
          ...state.result,
          ...action.payload
        ]
      }
    case 'REMOVE_FILTER_HOUSE_LIST':
      return {
        fetched: true,
        result: state.result.filter(item => item.ClusterId !== action.payload)
      }
    case 'LOAD_FILTER_HOUSE_LIST':
      return {
        fetched: false,
        result: state.result
      }
    case 'RESET_FILTER_HOUSE_LIST':
     return initialState
    default:
      return state
  }
}
