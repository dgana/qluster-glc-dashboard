const getOccupantReport = localStorage.getItem('occupantReport')
const decode = getOccupantReport ? atob(getOccupantReport) : null
const localStorageOccupantReport = decode ? JSON.parse(decode) : null

const initialState = {
  fetched: localStorageOccupantReport ? true : false,
  result: localStorageOccupantReport ? localStorageOccupantReport : []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'OCCUPANT_REPORT':
      const stringify = JSON.stringify(action.payload)
      const encodeData = btoa(stringify)
      localStorage.setItem('occupantReport', encodeData)
      return {
        fetched: true,
        result: action.payload
      }
    default:
      return state
  }
}
