const initialState = {
  fetched: false,
  features: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case "FEATURES":
      return {
        fetched: true,
        features:action.payload
      }
    case "RESET_FEATURES":
      return initialState
    default: 
      return state
  }
}
