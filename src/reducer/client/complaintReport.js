const getComplaintReport = localStorage.getItem('complaintReport')
const decode = getComplaintReport ? atob(getComplaintReport) : null
const localStorageComplaintReport = decode ? JSON.parse(decode) : null

const initialState = {
  fetched: localStorageComplaintReport ? true : false,
  result: localStorageComplaintReport ? localStorageComplaintReport : []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'COMPLAINT_REPORT':
      const stringify = JSON.stringify(action.payload)
      const encodeData = btoa(stringify)
      localStorage.setItem('complaintReport', encodeData)
      return {
        fetched: true,
        result: action.payload
      }
    default:
      return state
  }
}
