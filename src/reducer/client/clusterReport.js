const getClusterReport = localStorage.getItem('clusterReport')
const decode = getClusterReport ? atob(getClusterReport) : null
const localStorageClusterReport = decode ? JSON.parse(decode) : null

const initialState = {
  fetched: localStorageClusterReport ? true : false,
  result: localStorageClusterReport ? localStorageClusterReport : []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'CLUSTER_REPORT':
      const stringify = JSON.stringify(action.payload)
      const encodeData = btoa(stringify)
      localStorage.setItem('clusterReport', encodeData)
      return {
        fetched: true,
        result: action.payload
      }
    default:
      return state
  }
}
