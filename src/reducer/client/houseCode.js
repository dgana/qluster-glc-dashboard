const initialState = {
  fetched: false,
  result: [],
  resultStatus:[]
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'OCCUPANT_CODE':
      return {
        fetched: true,
        result: action.payload.houseCode.map(item => ({value: item.code,label: item.address})),
        resultStatus: action.payload.status.map(item => ({value:item.id,label:item.name}))
        }
    case 'RESET_OCCUPANT_CODE':
      return initialState
    default:
      return state
  }
}
