const initialState = {
  fetched: false,
  result: {}
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'GET_SUMMARY':
      return {
        fetched: true,
        result: action.payload
      }
    default:
      return state
  }
}
