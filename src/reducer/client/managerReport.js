const getManagerReport = localStorage.getItem('managerReport')
const decode = getManagerReport ? atob(getManagerReport) : null
const localStorageManagerReport = decode ? JSON.parse(decode) : null

const initialState = {
  fetched: localStorageManagerReport ? true : false,
  result: localStorageManagerReport ? localStorageManagerReport : []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'MANAGER_REPORT':
      const stringify = JSON.stringify(action.payload)
      const encodeData = btoa(stringify)
      localStorage.setItem('managerReport', encodeData)
      return {
        fetched: true,
        result: action.payload
      }
    default:
      return state
  }
}
