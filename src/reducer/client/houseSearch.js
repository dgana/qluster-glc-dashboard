const initialState = {
  fetched: false,
  result: ''
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'CURRENT_HOUSE_SEARCH':
    console.log(action.payload);
      return {
        fetched: true,
        result: action.payload
      }
    default:
      return state
  }
}
