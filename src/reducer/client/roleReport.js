import { fullDate } from '../../util'

const initialState = {
  fetched: false,
  result: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'ROLE_REPORT':
    const newData = action.payload.map(item =>({ id: item.id, role: item.role,updatedAt:fullDate(item.updatedAt),features: item.features }))
    return {
      fetched: true,
      result: newData
    }
    
    default:
      return state
  }
}
