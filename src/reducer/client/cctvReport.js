const getCctvReport = localStorage.getItem('cctvReport')
const decode = getCctvReport ? atob(getCctvReport) : null
const localStorageCctvReport = decode ? JSON.parse(decode) : null

const initialState = {
  fetched: localStorageCctvReport ? true : false,
  result: localStorageCctvReport ? localStorageCctvReport : []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'CCTV_REPORT':
      const stringify = JSON.stringify(action.payload)
      const encodeData = btoa(stringify)
      localStorage.setItem('cctvReport', encodeData)
      return {
        fetched: true,
        result: action.payload
      }
    default:
      return state
  }
}
