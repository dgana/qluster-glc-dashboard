const initialState = {
  fetched: false,
  result: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'CLUSTER_LIST':
      return {
        fetched: true,
        result: action.payload
      }
    case 'ADD_CLUSTER_LIST':
      return {
        fetched: true,
        result: [
          ...state.result,
          ...action.payload
        ]
      }

    case 'DELETE_CLUSTER_LIST':
      let deletedResult = state.result.filter(item => item.id!==action.payload.id)
      return {
        fetched:true,
        result:deletedResult
      }

    case 'EDIT_CLUSTER_LIST':
      let editedResult = state.result.map(item => {
        if(item.id === action.payload.id)
          return action.payload
        else return item
      })
      return {
        fetched: true,
        result: editedResult
      }
    case 'RESET_CLUSTER_LIST':
      return initialState
    default:
      return state
  }
}
