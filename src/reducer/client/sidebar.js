const initialState = {
  active: [],
  title: ""
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'SIDEBAR_NAVIGATION':
      return action.payload
    case 'SIDEBAR_ACTIVE':
      return Object.assign({}, state, action.payload)
    default:
      return state
  }
}
