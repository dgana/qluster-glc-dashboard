const initialState = {
  fetched: false,
  result: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'HOUSE_LIST':
      return {
        fetched: true,
        result: action.payload
      }
    case 'ADD_HOUSE_LIST':
      return {
        fetched: true,
        result: [
          ...state.result,
          ...action.payload
        ]
      }
    case 'DELETE_HOUSE_LIST':
      return state.result.filter(item => item.id !== action.payload.id)
    case 'EDIT_HOUSE_LIST':
      return state.result.filter(item => item.id !== action.payload.id)
    case 'RESET_HOUSE_LIST':
      return initialState
    default:
      return state
  }
}
