const getHouseReport = localStorage.getItem('houseReport')
const decode = getHouseReport ? atob(getHouseReport) : null
const localStorageHouseReport = decode ? JSON.parse(decode) : null

const initialState = {
  fetched: localStorageHouseReport ? true : false,
  result: localStorageHouseReport ? localStorageHouseReport : []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'HOUSE_REPORT':
      const stringify = JSON.stringify(action.payload)
      const encodeData = btoa(stringify)
      localStorage.setItem('houseReport', encodeData)
      return {
        fetched: true,
        result: action.payload
      }
    case 'RESET_HOUSE_REPORT':
      return {
        fetched: false,
        result: []
      }
    default:
      return state
  }
}
