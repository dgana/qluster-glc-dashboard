const initialState = {
  fetched: false,
  result: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'MANAGER_CODE':
      return {
        fetched: true,
        result: action.payload.divisionCode.map(item => {
          return {
            value: item.code,
            label:item.name
          }
        }),
        resultStatus: action.payload.status.map(item => {
          return {
            value: item.id,
            label:item.name
          }
        })

      }
    case 'RESET_MANAGER_CODE':
      return initialState
    default:
      return state
  }
}
