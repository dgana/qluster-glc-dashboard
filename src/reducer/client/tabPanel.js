const initialState = "rumah"

export default (state = initialState, action) => {
  switch(action.type) {
    case 'TAB_PANEL_RUMAH':
      return action.payload
    case 'TAB_PANEL_CLUSTER':
      return action.payload
	  case 'TAB_PANEL_TOWNSHIP':
	    return action.payload
    default:
      return state
  }
}
