// Utility
import { adminDate } from '../../util'

const initialState = {
  fetched: false,
  result: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'ADMIN_MANAGEMENT_LIST':
      const { allManagerRoles,managers } = action.payload
      const roleOptions = allManagerRoles.map( item =>({value:item.name,label:item.name,id:item.id}))
      const newResult = {managers,roleOptions}
        return {
          fetched: true,
          result: newResult
        }
    case 'EDIT_ADMIN_MANAGEMENT':

      const { id,name,role,email,status } = action.payload.data
      const newAdmin = state.result.managers.map((item,i) => {
        return item.id === id ? {id,name,email,role,status:status ? 'aktif' : 'tidak aktif',updatedAt:adminDate(new Date())} : item
      })
      // console.log('new admin',newAdmin);
      return {
        fetched: true,
        result: {...state.result,managers:newAdmin}
      }
    default:
      return state
  }
}
