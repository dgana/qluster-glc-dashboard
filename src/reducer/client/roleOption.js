// Utility
import util from '../../util'

const initialState = {
  fetched: false,
  option: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'USER_ROLE':
      let newObj = {
        fetched: true,
        option: []
      }
      action.payload.map(item => {
        return newObj.option.push({ value: item.name, label: util.capitalizeFirstLetter(item.name), id:item.id })
      })
      return newObj
    case 'ADD_USER_ROLE':
      return {
        fetched: false,
        option: [...state.option, { value: action.payload, label: util.capitalizeFirstLetter(action.payload) }]
      }
    case 'RESET_ROLE_OPTION':
     return initialState
    default:
      return state
  }
}
