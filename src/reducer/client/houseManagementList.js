const initialState = {
  fetched: false,
  result: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'HOUSE_LIST_MANAGEMENT':
      return {
        fetched: true,
        result: action.payload
      }
    case 'RESET_HOUSE_LIST_MANAGEMENT':
      return initialState
    case 'EDIT_HOUSE_LIST_MANAGEMENT':
      const editedHouse = state.result.map(item => {
        if (item.id === action.payload.id)
          return action.payload
        else return item
      })
      return {
        fetched: true,
        result: editedHouse
      }
    case 'DELETE_HOUSE_LIST_MANAGEMENT':
      const deletedHouse = state.result.filter(item => item.id !== action.payload)
      return {
        fetched: true,
        result: deletedHouse
      }
    default:
      return state
  }
}
