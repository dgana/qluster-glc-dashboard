const initialState = {
  fetched: false,
  result: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'ADMIN_REPORT':
    const { allManagerRoles,managers } = action.payload
    const roleOptions = allManagerRoles.map( item =>({value:item.name,label:item.name,id:item.id}))
    const newResult = {managers,roleOptions}
      return {
        fetched: true,
        result: newResult
      }
    default:
      return state
  }
}
