// Utility
import { fullDate } from '../../util'

const getSosReport = localStorage.getItem('sosReport')
const decode = getSosReport ? atob(getSosReport) : null
const localStorageSosReport = decode ? JSON.parse(decode) : null

const initialState = {
  fetched: localStorageSosReport ? true : false,
  result: localStorageSosReport ? localStorageSosReport : []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'SOS_REPORT':
      const newData = action.payload.map(item => {
        return {
          id: item.sosId,
          penghuni: item.namaPenghuni,
          township: item.townName,
          cluster: item.clusterName,
          house: item.houseAddress,
          createdAt: fullDate(item.createdAt),
          updatedAt: item.updatedAt === "STILL IN PROGRESS" ? item.updatedAt : fullDate(item.updatedAt),
          status: item.status
        }
      })
      const stringify = JSON.stringify(newData)
      const encodeData = btoa(stringify)
      localStorage.setItem('sosReport', encodeData)
      return {
        fetched: true,
        result: newData
      }
    default:
      return state
  }
}
