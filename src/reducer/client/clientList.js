const initialState = []

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case 'COMPANY_LIST':
      return action.payload
    case 'ADD_COMPANY':
      return [ ...state, action.payload ]
    case 'UPDATE_COMPANY':
      return state.map(company => {
        if (company.id === action.payload.id) return {...company, ...action.payload}
        return company
      })
    case 'ACTIVATE_COMPANY':
      return state.map(company => {
        if (company.id === action.payload.id) return {...company, ...action.payload}
        return company
      })
    default:
      return state
  }
}
