const initialState = {
  fetched: false,
  list: []
}

import _ from 'lodash'

export default (state = initialState, action) => {
  switch(action.type) {
    case 'FETCH_NEWS':
      const newPayload = _.uniqBy(action.payload, 'createdAt')
      return {
        fetched: true,
        list: newPayload
      }

    case 'ADD_NEWS':
      //if pictures' value is 'no pictures'
      const newPictures = (typeof action.payload.pictures ==='string') ? [] : action.payload.pictures

      const news = {
        ...action.payload,
        content: action.payload.fullContent,
        pictures: newPictures
      }

      delete news.fullContent
      delete news.isLiked
      delete news.isRead
      delete news.picture

      const data = {
        fetched: true,
        list: [
          ...state.list,
          news
        ]
      }
      return data

    case 'DELETE_NEWS':
      const newsIndex = state.list.findIndex(item => item.id === action.payload)
      const newNewsList = [
        ...state.list.slice(0,newsIndex),
        ...state.list.slice(newsIndex+1)
      ]
      return {
        fetched: true,
        list: newNewsList
      }

    case 'ADD_NEWS_COMMENT':
    	const getIndex = state.list.findIndex(item => item.id === action.payload.NewsId)

    	const newComment = [
    		...state.list[getIndex].NewsComments,
    		action.payload
    	]

      const newObj = {
        ...state.list[getIndex],
        NewsComments: newComment
      }

    	const newList = [
    		...state.list.slice(0,getIndex),
        newObj,
        ...state.list.slice(getIndex + 1)
    	]

    	return {
        fetched: true,
        list: newList
      }

    case 'RESET_NEWS':
     return initialState
    default:
      return state
  }
}
