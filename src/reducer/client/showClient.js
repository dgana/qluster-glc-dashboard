import {AES,enc}  from 'crypto-js'

const token = localStorage.getItem('token')
let user
if (token) {
  const userToken = JSON.parse(localStorage.getItem('token')).usertoken.split(' ')[1]
  user = JSON.parse(AES.decrypt(userToken.toString(),'qluster123').toString(enc.Utf8))
}

const initialState = user ? user : {}

export default (state = initialState, action = {}) => {
  switch(action.type) {
    case 'SHOW_CLIENT':
      return action.payload
    case 'EDIT_PROFILE':
      return { ...state, name: action.payload.name, picture: action.payload.picture }
    default:
      return state
  }
}
