const initialState = {
  fetched: false,
  result: []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'CCTV_LIST':
      return {
        fetched: true,
        result: action.payload
      }
    case 'ADD_CCTV_LIST':
      return {
        fetched: true,
        result: [
          ...state.result,
          ...action.payload.succeed
        ]
      }
    case 'EDIT_CCTV_LIST':

      const newEditCctv = state.result.map(item =>{
        return item.id === action.payload.id ? action.payload : item
      })

      return {
        fetched: true,
        result: [
          ...newEditCctv
        ]
      }
    case 'DELETE_CCTV_LIST':
      const newDeleteCctv = state.result.filter(item => item.id !== action.payload)
      return {
        fetched: true,
        result: [
          ...newDeleteCctv,
        ]
      }
    case 'RESET_CCTV_LIST':
      return initialState
    default:
      return state
  }
}
