const initialState = false

export default (state = initialState, action) => {
  switch(action.type) {
    case 'SIDEBAR_TOGGLE':
      return (state) ? false : true
    default:
      return state
  }
}
