const initialState = {
  fetched: false,
  result: []
}

// Utility
import { fullDate } from '../../util'

export default (state = initialState, action) => {
  switch(action.type) {
    case 'ROLE_LIST':
      const newData = action.payload.map(item =>({ id: item.id, role: item.role,updatedAt:fullDate(item.updatedAt),features: item.features }))
      return {
        fetched: true,
        result: newData
      }
    case 'EDIT_ROLE':
      const { roleId,feature } = action.payload.roleFeature
      const { newName } = action.payload
      const newRole = state.result.map( (item,i) => {
        const newFeature = item.features.map( (data,i) =>({...data,picked:feature[i].status}))
        return item.id === roleId ? {id:item.id, role:newName, updatedAt:fullDate(new Date()),features:newFeature} : item
      })
      return {
        fetched: true,
        result: newRole
      }
    default:
      return state
  }
}
