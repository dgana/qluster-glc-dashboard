const getBillingReport = localStorage.getItem('billingReport')
const decode = getBillingReport ? atob(getBillingReport) : null
const localStorageBillingReport = decode ? JSON.parse(decode) : null

const initialState = {
  fetched: localStorageBillingReport ? true : false,
  result: localStorageBillingReport ? localStorageBillingReport : []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'BILLING_REPORT':
      const stringify = JSON.stringify(action.payload)
      const encodeData = btoa(stringify)
      localStorage.setItem('billingReport', encodeData)
      return {
        fetched: true,
        result: action.payload
      }
    default:
      return state
  }
}
