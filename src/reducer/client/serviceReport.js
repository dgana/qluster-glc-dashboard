// Utility
import { fullDate } from '../../util'

const getServiceReport = localStorage.getItem('serviceReport')
const decode = getServiceReport ? atob(getServiceReport) : null
const localStorageServiceReport = decode ? JSON.parse(decode) : null

const initialState = {
  fetched: localStorageServiceReport ? true : false,
  result: localStorageServiceReport ? localStorageServiceReport : []
}

export default (state = initialState, action) => {
  switch(action.type) {
    case 'SERVICE_REPORT':
      const newData = action.payload.map(item => {
        return {
          id: item.id,
          penghuni: item.User.name,
          township: item.User.House.Cluster.Town.name,
          cluster: item.User.House.Cluster.name,
          house: item.User.House.address,
          label: item.ServiceCategory.name,
          createdAt: fullDate(item.createdAt),
          status: item.status
        }
      })
      const stringify = JSON.stringify(newData)
      const encodeData = btoa(stringify)
      localStorage.setItem('serviceReport', encodeData)
      return {
        fetched: true,
        result: newData
      }
    default:
      return state
  }
}
