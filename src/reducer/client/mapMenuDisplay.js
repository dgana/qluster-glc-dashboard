const initialState = false

export default (state = initialState, action) => {
  switch(action.type) {
    case 'MAP_MENU_DISPLAY':
      return state ? false : true
    case 'MAP_MENU_SHOW':
      return false
    default:
      return state
  }
}
